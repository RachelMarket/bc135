<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * AdminStaffReservacion behavior
 */
class AdminStaffReservacionBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
    	
    ];

    public function canCancel($entity){

        
        return true ;
    }

    public function initialize(array $config){

    	
	}
     public function canCreate($entity, $options){

        $config = TableRegistry::exists('Recursos') ? [] : ['className' => 'App\Model\Table\RecursosTable'];
        $this->Recursos = TableRegistry::get('Recursos', $config);


        // busco mi recurso.


        $recurso = $this->Recursos->find('all' , [ 'conditions' => [ 'Recursos.id' => $entity->recurso_id ] ] )->first();

       


        if( $entity->cantidad > $recurso->capacidad ){

            $entity->errors('cantidad', __('Número de asistentes superior al permitido  (Max: {0} )', [$recurso->capacidad] ) );

            return false;
        }


     	return true;
     }
}

<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * ClientesReservacion behavior
 */
class ClientesReservacionBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];


    public function canCancel($entity){


        $fecha_Hoy = new Time(   'now' ,   $entity->fecha_inicio->tzName  );

        $fecha_Hoy->addDay();

        if( $entity->fecha_inicio->getTimestamp() <  $fecha_Hoy->getTimestamp() ){

            throw new \Exception( __('No se puede cancelar una reservación'), 1);
            
        }

        return true;
    }

    public function canCreate($entity, $options){


        // es un dia de asueto ??

        $fecha = $entity->fecha_inicio->format('Y-m-d');

        // Lo busco en asuetos.

        $config = TableRegistry::exists('Asuetos') ? [] : ['className' => 'App\Model\Table\AsuetosTable'];
        $this->Asuetos = TableRegistry::get('Asuetos', $config);


        $rows = $this->Asuetos->find('all' , [ 'conditions' =>  [ 'Asuetos.dia' => $fecha ]  ] );

        if( $rows->count() > 0 ){


        	$entity->errors('fecha_inicio', __('Día no disponible, marcado como inhábil') );

        	return false;
        }


        // horas habiles del recurso.

        $config = TableRegistry::exists('Recursos') ? [] : ['className' => 'App\Model\Table\RecursosTable'];
        $this->Recursos = TableRegistry::get('Recursos', $config);


        // busco mi recurso.


        $recurso = $this->Recursos->find('all' , [ 'conditions' => [ 'Recursos.id' => $entity->recurso_id ] ] )->first();

        // valido el tiempo minimo 
        

        $diff = $entity->fecha_inicio->diffInHours($entity->fecha_fin);

        if( $diff < $recurso->tiempo_minimo ){

                $entity->errors('fecha_inicio', __('El tiempo mínimo para este recurso es de {0} horas',[$recurso->tiempo_minimo ]) );

                $entity->errors('fecha_fin', __('El tiempo mínimo para este recurso es de {0} horas',[$recurso->tiempo_minimo ]) );
        }

        $fecha_inicio_recurso = new Time(  $entity->fecha_inicio->format('Y-m-d'). " " . $recurso->hora_inicio->format('H:i:s'), $entity->fecha_inicio->tzName   );



        if( $fecha_inicio_recurso->getTimestamp() >  $entity->fecha_inicio->getTimestamp()  ){

            $entity->errors('fecha_inicio', __('El recurso esta disponible apartir de las {0}' , [ $recurso->hora_inicio->format('H:i')  ]) );
            return false;
        }

        
        $fecha_fin_recurso = new Time(  $entity->fecha_inicio->format('Y-m-d'). " " . $recurso->hora_fin->format('H:i:s'), $entity->fecha_inicio->tzName   );



        if( $fecha_fin_recurso->getTimestamp() <  $entity->fecha_fin->getTimestamp()  ){

            $entity->errors('fecha_fin', __('El recurso esta disponible antes de las {0}' , [ $recurso->hora_fin->format('H:i')  ]) );

            return false;
        }




        if( $entity->cantidad > $recurso->capacidad ){

            $entity->errors('cantidad', __('Número de asistentes superior al permitido  (Max: {0} )', [$recurso->capacidad] ) );

            return false;
        }


        
        


        return true;
    }


}

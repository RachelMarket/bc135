<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * AdminStaffReservacion behavior
 */
class RangosBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        
        'key_inicial' => 'inicial',
        'key_final' => 'final',
        'key_flag' => 'rango',
        'field_2_change' =>  'nombre',
    ];

    

    public function initialize(array $config){

    	
	}

    public function beforeMarshal($event,  &$entity,  $options){

        $key_inicial = $this->_config['key_inicial'];
        $key_final = $this->_config['key_final'];
        $key_flag = $this->_config['key_flag'];
        $field_2_change = $this->_config['field_2_change'];


        $table = $event->subject();

        


        

        

        if( isset($event->data['data'][$key_flag]) && $event->data['data'][$key_flag] == 1){

            if(  isset($event->data['data'][$key_inicial]) &&
                  isset($event->data['data'][$key_final]) &&
                  intval($event->data['data'][$key_inicial]) > 0 &&
                  intval($event->data['data'][$key_final]) > 0

                 ){

                 if(intval($event->data['data'][$key_inicial]) >= intval($event->data['data'][$key_final]) ){
                     throw new \Exception("El inicio del rango no puede ser mayor o igual al final." , 1);
                 }
            
                    $entity[$field_2_change] = $event->data['data'][$key_inicial];

                    $iniciamos = $event->data['data'][$key_inicial] + 1;
                    $finalizamos = $event->data['data'][$key_final]  + 1;

                    for ($i= $iniciamos  ; $i < $finalizamos ; $i++) { 
                        
                        $dataEntidad = $entity->getArrayCopy();


                        unset( $dataEntidad[$key_inicial] );
                        unset( $dataEntidad[$key_final] );
                        unset( $dataEntidad[$key_flag] );

                        $dataEntidad[ $field_2_change] = $i;
                        
                        $en = $table->newEntity();
                            

                        
                        $en = $table->patchEntity($en, $dataEntidad);

                        

                        if( !$table->save( $en ) ){

                            throw new \Exception("Ocurrio un error en la generación del rango. El valor ".$i . " ya existe" , 1);
                            
                        }



                    }

            }else{
                throw new \Exception("Verifique el rango y vuelva a intentar." , 1);
            }

        

        }

        return true;

       
    }

    
    

}

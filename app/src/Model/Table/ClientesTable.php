<?php
namespace App\Model\Table;

use App\Model\Entity\Cliente;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

/**
 * Clientes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Paises
 * @property \Cake\ORM\Association\BelongsTo $Estados
 * @property \Cake\ORM\Association\BelongsTo $Municipios
 * @property \Cake\ORM\Association\BelongsTo $Monedas
 * @property \Cake\ORM\Association\BelongsTo $TipoClientes
 * @property \Cake\ORM\Association\BelongsTo $Centros
 * @property \Cake\ORM\Association\BelongsTo $Formasdepagos
 * @property \Cake\ORM\Association\HasMany $Facturas
 * @property \Cake\ORM\Association\HasMany $FacturasProd20160102
 * @property \Cake\ORM\Association\BelongsToMany $Servicios
 */
class ClientesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('clientes');
        $this->displayField('nombre');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Paises', [
            'foreignKey' => 'pais_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Estados', [
            'foreignKey' => 'estado_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Municipios', [
            'foreignKey' => 'municipio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Monedas', [
            'foreignKey' => 'moneda_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoClientes', [
            'foreignKey' => 'tipo_cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Centros', [
            'foreignKey' => 'centro_id'
        ]);
        $this->belongsTo('Formasdepagos', [
            'foreignKey' => 'formasdepago_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Facturas', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->belongsToMany('Servicios', [
            'foreignKey' => 'cliente_id',
            'targetForeignKey' => 'servicio_id',
            'joinTable' => 'clientes_servicios'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');
            
        $validator
            ->requirePresence('primario_persona_de_contacto', 'create')
            ->notEmpty('primario_persona_de_contacto');
            
        $validator
            ->requirePresence('primario_correo_electronico', 'create')
            ->notEmpty('primario_correo_electronico');
            
        $validator
            ->requirePresence('primario_telefono_oficina', 'create')
            ->notEmpty('primario_telefono_oficina');
            
            
        $validator
            ->allowEmpty('secundario_persona_de_contacto');
            
        $validator
            ->allowEmpty('secundario_correo_electronico');
            
        $validator
            ->allowEmpty('secundario_telefono_oficina');
            
        $validator
            ->allowEmpty('secundario_telefono_movil');
            
        $validator
            ->requirePresence('razon_social', 'create')
            ->notEmpty('razon_social');
            
            
        $validator
            ->requirePresence('calle', 'create')
            ->notEmpty('calle');
            
        $validator
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');
            
            
        $validator
            ->requirePresence('codigo_postal', 'create')
            ->notEmpty('codigo_postal');

        $validator
            ->requirePresence('estado_id', 'create')
            ->notEmpty('estado_id');

        $validator
            ->requirePresence('municipio_id', 'create')
            ->notEmpty('municipio_id');
            
        $validator
            ->add('activo', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('activo');
            
        $validator
            ->add('dia_pago', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('dia_pago');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['pais_id'], 'Paises'));
        $rules->add($rules->existsIn(['estado_id'], 'Estados'));
        $rules->add($rules->existsIn(['municipio_id'], 'Municipios'));
        $rules->add($rules->existsIn(['moneda_id'], 'Monedas'));
        $rules->add($rules->existsIn(['tipo_cliente_id'], 'TipoClientes'));
        $rules->add($rules->existsIn(['formasdepago_id'], 'Formasdepagos'));
        return $rules;
    }

    public function getClientes(){

       $clientes=$this->find('list', ['order' => ['nombre' => 'asc']])->toArray();
       $clientes=[''=>' -- Seleccione -- ']+$clientes;
       return $clientes;

    }


    public function saldodias( $cliente_id , $fecha = null ){

        $config = TableRegistry::exists('Reservaciones') ? [] : ['className' => 'App\Model\Table\ReservacionesTable'];
        $this->Reservaciones = TableRegistry::get('Reservaciones', $config);


        $options['conditions'][] = array('Reservaciones.deleted' => false);
        $options['conditions'][] = array('Reservaciones.cancelado' => false);
        $options['conditions'][] = array('Reservaciones.cliente_id' => $cliente_id);

         $options['conditions'][] = array('Reservaciones.allday' => '1');

        if(is_null( $fecha )){

            $mes = new Time( 'now' );
        }else{
            $mes = new Time( $fecha );
        }

        $options['conditions'][] = array('Reservaciones.fecha_inicio LIKE' =>  '%'.$mes->format('Y-m').'%' );


        $rows = $this->Reservaciones->find('all',$options);


        $cantidad = $rows->count( );

        $cliente = $this->get($cliente_id);

        $saldo = $cliente->saldodias - $cantidad;

        return $saldo;

    }

    public function saldo($cliente_id ,  $fecha = null){


        Log::write('debug'," Inicio Proceso de calculo de Saldo "     );

        Log::write('debug'," Cliente_id   " .$cliente_id     );  


        $config = TableRegistry::exists('Reservaciones') ? [] : ['className' => 'App\Model\Table\ReservacionesTable'];
        $this->Reservaciones = TableRegistry::get('Reservaciones', $config);


        $options['conditions'][] = array('Reservaciones.deleted' => false);
        $options['conditions'][] = array('Reservaciones.allday' => false);
        $options['conditions'][] = array('Reservaciones.cancelado' => false);
        $options['conditions'][] = array('Reservaciones.cliente_id' => $cliente_id);

        if(is_null( $fecha )){

            Log::write('debug'," No tengo fecha tomo la de hoy   "      );
            $mes = new Time( 'now' );
        }else{

          Log::write('debug'," Tengo una fecha y es:  " . $fecha );

            $mes = new Time( $fecha );
        }


        Log::write('debug'," buscando Reservaciones con fecha:  " . $mes->format('Y-m')  ); 
        $options['conditions'][] = array('Reservaciones.fecha_inicio LIKE' =>  '%'.$mes->format('Y-m').'%' );


        $rows = $this->Reservaciones->find('all',$options);

        
        Log::write('debug'," Encontre un total de :  " . count($rows)  );

 
        $horas = 0;
        foreach ($rows as $key => $row) {
            
            $diff = $row->fecha_inicio->diffInHours( $row->fecha_fin );

            $horas = $horas + $diff;


        }


        $cliente = $this->get($cliente_id);


        Log::write('debug',"El saldo de cliente es  :  " . $cliente->saldo  );

        Log::write('debug',"Horas Acumuladas del mes :  " . $horas   );


        $saldo = $cliente->saldo - $horas;

        Log::write('debug',"Saldo  :  " . $saldo   ); 
        return $saldo;

    }

    public function beforeDelete($event, $entity, $options){
       
        //Dependencias Facturtas
        $config_0 = TableRegistry::exists('Facturas') ? [] : ['className' => 'App\Model\Table\FacturasTable'];
        $this->Facturas = TableRegistry::get('Facturas', $config_0);

        //Dependencias Reservaciones
        $config_1 = TableRegistry::exists('Reservaciones') ? [] : ['className' => 'App\Model\Table\ReservacionesTable'];
        $this->Reservaciones = TableRegistry::get('Reservaciones', $config_1);

        //Dependencias Servicios Asociados
        $config_2 = TableRegistry::exists('ClientesServicios') ? [] : ['className' => 'App\Model\Table\ClientesServiciosTable'];
        $this->ClientesServicios = TableRegistry::get('ClientesServicios', $config_2);
        
        //Querys
        $facturas = $this->Facturas->find('all', ['conditions' => ['cliente_id' =>  $entity['id'] ]])->count();
        $reservaciones = $this->Reservaciones->find('all', ['conditions' => ['cliente_id' =>  $entity['id'] ]])->count();
        $servicios = $this->ClientesServicios->find('all', ['conditions' => ['cliente_id' =>  $entity['id'] ]])->count();

        $bandera = true;

        if($facturas > 0){
            $this->error = __('El cliente no se puede eliminar porque tiene facturas relacionadas a él.');
            $bandera = false;

        }elseif ($reservaciones > 0) {
            $this->error = __('El cliente no se puede eliminar porque tiene reservaciones relacionadas a él.');
            $bandera = false;

        }elseif ($servicios > 0) {
            $this->error = __('El cliente no se puede eliminar porque tiene servicios relacionadas a él.');
            $bandera = false;

        }
        return $bandera;
    }
}

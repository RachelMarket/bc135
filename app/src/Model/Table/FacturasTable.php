<?php
namespace App\Model\Table;

use App\Model\Entity\Factura;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Facturas Model
 */
class FacturasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('facturas');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Festados', [
            'foreignKey' => 'festado_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Monedas', [
            'foreignKey' => 'moneda_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Formasdepagos', [
            'foreignKey' => 'formasdepago_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Centros', [
            'foreignKey' => 'centro_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ClientesServicios', [
            'foreignKey' => 'factura_id'
        ]);
        $this->hasMany('Partidas', [
            'foreignKey' => 'factura_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('folio', 'valid', ['rule' => 'numeric'])
            ->requirePresence('folio', 'create')
            ->notEmpty('folio');
            
        $validator
            ->requirePresence('serie', 'create')
            ->notEmpty('serie');
            
        $validator
            ->add('fecha', 'valid', ['rule' => 'date'])
            ->requirePresence('fecha', 'create')
            ->notEmpty('fecha');
            
        $validator
            ->add('subtotal', 'valid', ['rule' => 'decimal'])
            ->requirePresence('subtotal', 'create')
            ->notEmpty('subtotal');
            
        $validator
            ->add('iva', 'valid', ['rule' => 'decimal'])
            ->requirePresence('iva', 'create')
            ->notEmpty('iva');
            
        $validator
            ->add('total', 'valid', ['rule' => 'decimal'])
            ->requirePresence('total', 'create')
            ->notEmpty('total');
            
        $validator
            ->requirePresence('cantidad_letra', 'create')
            ->notEmpty('cantidad_letra');
            
        $validator
            ->requirePresence('observaciones', 'create')
            ->notEmpty('observaciones');
            
        $validator
            ->requirePresence('cfdi', 'create')
            ->notEmpty('cfdi');
            
        $validator
            ->requirePresence('response', 'create')
            ->notEmpty('response');
            
        $validator
            ->requirePresence('xml', 'create')
            ->notEmpty('xml');
            
        $validator
            ->add('addRetencion', 'valid', ['rule' => 'boolean'])
            ->requirePresence('addRetencion', 'create')
            ->notEmpty('addRetencion');
            
        $validator
            ->add('retencion', 'valid', ['rule' => 'decimal'])
            ->requirePresence('retencion', 'create')
            ->notEmpty('retencion');
            
        $validator
            ->add('cfdicancelado', 'valid', ['rule' => 'numeric'])
            ->requirePresence('cfdicancelado', 'create')
            ->notEmpty('cfdicancelado');
            
        $validator
            ->add('tipo_de_cambio', 'valid', ['rule' => 'decimal'])
            ->requirePresence('tipo_de_cambio', 'create')
            ->notEmpty('tipo_de_cambio');
            
        $validator
            ->add('fecha_pago', 'valid', ['rule' => 'date'])
            ->requirePresence('fecha_pago', 'create')
            ->notEmpty('fecha_pago');
            
        $validator
            ->requirePresence('referencia_de_pago', 'create')
            ->notEmpty('referencia_de_pago');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['festado_id'], 'Festados'));
        $rules->add($rules->existsIn(['moneda_id'], 'Monedas'));
        $rules->add($rules->existsIn(['formasdepago_id'], 'Formasdepagos'));
        return $rules;
    }

    public function beforeSave( $event,  &$entity,  $options){


        if($entity->isNew()){

            $cliente = $this->Clientes->get($entity->cliente_id);
            $entity->formasdepago_id = $cliente->formasdepago_id;
            $entity->cuentabanco = $cliente->cuentabanco;

        }


    }
}

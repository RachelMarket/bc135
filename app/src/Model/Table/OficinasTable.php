<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Oficinas Model
 *
 * @property \App\Model\Table\CentrosTable|\Cake\ORM\Association\BelongsTo $Centros
 * @property |\Cake\ORM\Association\BelongsToMany $Cotizaciones
 *
 * @method \App\Model\Entity\Oficina get($primaryKey, $options = [])
 * @method \App\Model\Entity\Oficina newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Oficina[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Oficina|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Oficina patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Oficina[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Oficina findOrCreate($search, callable $callback = null, $options = [])
 */
class OficinasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('oficinas');
        $this->setDisplayField('numero');
        $this->setPrimaryKey('id');
        $this->belongsTo('Centros', [
            'foreignKey' => 'centro_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Cotizaciones', [
            'foreignKey' => 'oficina_id',
            'targetForeignKey' => 'cotizacion_id',
            'joinTable' => 'cotizaciones_oficinas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('numero')
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        $validator
            ->scalar('capacidad')
            ->allowEmpty('capacidad');

        $validator
            ->numeric('precio_minimo')
            ->allowEmpty('precio_minimo');

        $validator
            ->numeric('precio_medio')
            ->allowEmpty('precio_medio');

        $validator
            ->numeric('precio_maximo')
            ->allowEmpty('precio_maximo');

        $validator
            ->boolean('eliminado')
            ->allowEmpty('eliminado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['centro_id'], 'Centros'));

        return $rules;
    }
}

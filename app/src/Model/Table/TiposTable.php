<?php
namespace App\Model\Table;

use App\Model\Entity\Tipo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tipos Model
 */
class TiposTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('tipos');
        $this->displayField('nombre');
        $this->primaryKey('id');
        $this->hasMany('ClientesServicios', [
            'foreignKey' => 'tipo_id'
        ]);
        $this->hasMany('Servicios', [
            'foreignKey' => 'tipo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        return $validator;
    }

    public function getTipos(){

       $tipos=$this->find('list', ['order' => ['nombre' => 'asc']])->toArray();
       $tipos=[''=>' -- Seleccione -- ']+$tipos;
       return $tipos;

    }
}

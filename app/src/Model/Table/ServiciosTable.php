<?php
namespace App\Model\Table;

use App\Model\Entity\Servicio;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Servicios Model
 */
class ServiciosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('servicios');
        $this->displayField('nombre');
        $this->primaryKey('id');
        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Unidades', [
            'foreignKey' => 'unidad_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Clientes', [
            'foreignKey' => 'servicio_id',
            'targetForeignKey' => 'cliente_id',
            'joinTable' => 'clientes_servicios'
        ]);
        $this->belongsTo('Centros', [
            'foreignKey' => 'centro_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');
            
        $validator
            ->add('precio_mxn', 'valid', ['rule' => 'decimal'])
            ->requirePresence('precio_mxn', 'create')
            ->notEmpty('precio_mxn');
            
        $validator
            ->add('precio_usd', 'valid', ['rule' => 'decimal'])
            ->requirePresence('precio_usd', 'create')
            ->notEmpty('precio_usd');
            
        $validator
            ->requirePresence('descripcion', 'create')
            ->notEmpty('descripcion');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['unidad_id'], 'Unidades'));
        return $rules;
    }
}

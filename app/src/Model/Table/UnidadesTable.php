<?php
namespace App\Model\Table;

use App\Model\Entity\Unidad;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Unidades Model
 */
class UnidadesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('unidades');
        $this->displayField('nombre');
        $this->primaryKey('id');
        $this->hasMany('ClientesServicios', [
            'foreignKey' => 'unidad_id'
        ]);
        $this->hasMany('Partidas', [
            'foreignKey' => 'unidad_id'
        ]);
        $this->hasMany('Servicios', [
            'foreignKey' => 'unidad_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        return $validator;
    }
    public function getUnidades(){

       $unidades=$this->find('list', ['order' => ['nombre' => 'asc']])->toArray();
       $unidades=[''=>' -- Seleccione -- ']+$unidades;
       return $unidades;

    }
}

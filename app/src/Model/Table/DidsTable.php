<?php
namespace App\Model\Table;

use App\Model\Entity\Did;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;
use App\Model\Rule\IsUniqueD;

/**
 * Dids Model
 *
 */
class DidsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('dids');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Rangos', array('field_2_change' => 'did' ) );

        $this->belongsTo('Ladas', [
            'foreignKey' => 'lada_id'
        ]);

        $this->hasMany('ContactosDids', [
            'foreignKey' => 'did_id'
        ]);

        $this->hasOne('ContactosDidsJoin', [
            'foreignKey' => 'did_id',
            'joinType'   => 'LEFT',
            'className'  => 'ContactosDids'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
            
        $validator
            ->requirePresence('did')
            ->notEmpty('did', 'Please fill this field');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        
        $rules->add($rules->isUnique(['did','lada_id'], 'El DID ya existe.'));

        //$rules->add( new IsUniqueD(['did','lada_id']) , '_isUnique', [ 'errorField' => ['did','lada_id'],  'message' => 'El DID ya existe.' ] );

        return $rules;
    }
}

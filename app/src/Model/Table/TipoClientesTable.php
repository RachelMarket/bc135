<?php
namespace App\Model\Table;

use App\Model\Entity\TipoCliente;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TipoClientes Model
 *
 * @property \Cake\ORM\Association\HasMany $Clientes
 */
class TipoClientesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('tipo_clientes');
        $this->displayField('nombre');
        $this->primaryKey('id');
        $this->hasMany('Clientes', [
            'foreignKey' => 'tipo_cliente_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        return $validator;
    }

    public function getTipoClientes(){

       $tclientes=$this->find('list', ['order' => ['nombre' => 'asc']])->toArray();
       $tclientes=[''=>' -- Seleccione -- ']+$tclientes;
       return $tclientes;

    }
}

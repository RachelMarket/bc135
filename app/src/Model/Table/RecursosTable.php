<?php
namespace App\Model\Table;

use App\Model\Entity\Recurso;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Recursos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Centros
 * @property \Cake\ORM\Association\BelongsTo $Tipos
 */
class RecursosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('recursos');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Centros', [
            'foreignKey' => 'centro_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RecursosTipos', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('RecursosFotos', [
            'foreignKey' => 'recurso_id',
            'conditions' => ''
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');
            
        $validator
            ->allowEmpty('descripcion');
            
        $validator
            ->add('capacidad', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('capacidad');
            
        $validator
            ->add('tiempo_minimo', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('tiempo_minimo');
            
        $validator
            ->add('hora_inicio', 'valid', ['rule' => 'date'])
            ->requirePresence('hora_inicio', 'create')
            ->notEmpty('hora_inicio');
            
        $validator
            ->add('hora_fin', 'valid', ['rule' => 'date'])
            ->requirePresence('hora_fin', 'create')
            ->notEmpty('hora_fin');
            
        $validator
            ->add('minutos_espera', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('minutos_espera');
            

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['centro_id'], 'Centros'));
        $rules->add($rules->existsIn(['tipo_id'], 'RecursosTipos'));
        return $rules;
    }



   
}

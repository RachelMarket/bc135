<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cotizaciones Model
 *
 * @property \App\Model\Table\CentrosTable|\Cake\ORM\Association\BelongsTo $Centros
 * @property \App\Model\Table\UsuariosTable|\Cake\ORM\Association\BelongsTo $Usuarios
 * @property \App\Model\Table\MonedasTable|\Cake\ORM\Association\BelongsTo $Monedas
 * @property \App\Model\Table\OficinasTable|\Cake\ORM\Association\BelongsToMany $Oficinas
 *
 * @method \App\Model\Entity\Cotizacion get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cotizacion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cotizacion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cotizacion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cotizacion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cotizacion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cotizacion findOrCreate($search, callable $callback = null, $options = [])
 */
class CotizacionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cotizaciones');
        $this->setDisplayField('cliente');
        $this->setPrimaryKey('id');
        $this->belongsTo('Centros', [
            'foreignKey' => 'centro_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Monedas', [
            'foreignKey' => 'moneda_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Usuarios', [
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Oficinas', [
            'foreignKey' => 'cotizacion_id',
            'targetForeignKey' => 'oficina_id',
            'joinTable' => 'cotizaciones_oficinas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('cliente')
            ->requirePresence('cliente', 'create')
            ->notEmpty('cliente');

        $validator
            ->scalar('contacto')
            ->requirePresence('contacto', 'create')
            ->notEmpty('contacto');

        $validator
            ->scalar('correo')
            ->allowEmpty('correo');

        $validator
            ->scalar('telefono')
            ->allowEmpty('telefono');

        $validator
            ->scalar('telefono_movil')
            ->allowEmpty('telefono_movil');

        $validator
            ->integer('vigencia')
            ->requirePresence('vigencia', 'create')
            ->notEmpty('vigencia');

        $validator
            ->dateTime('vence')
            ->requirePresence('vence', 'create')
            ->notEmpty('vence');

        $validator
            ->integer('moneda_id')
            ->requirePresence('moneda_id', 'create')
            ->notEmpty('moneda_id');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('comentario')
            ->allowEmpty('comentario');

        $validator
            ->decimal('tipo_cambio')
            ->requirePresence('tipo_cambio', 'create')
            ->notEmpty('tipo_cambio');

        $validator
            ->boolean('aplica_promocion')
            ->requirePresence('aplica_promocion', 'create')
            ->notEmpty('aplica_promocion');

        $validator
            ->scalar('texto_inicial')
            ->allowEmpty('texto_inicial');

        $validator
            ->scalar('texto_final')
            ->allowEmpty('texto_final');

        $validator
            ->boolean('layout_centro')
            ->requirePresence('layout_centro', 'create')
            ->notEmpty('layout_centro');

        $validator
            ->dateTime('creada')
            ->requirePresence('creada', 'create')
            ->notEmpty('creada');

        $validator
            ->boolean('eliminado')
            ->requirePresence('eliminado', 'create')
            ->notEmpty('eliminado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['centro_id'], 'Centros'));
        $rules->add($rules->existsIn(['usuario_id'], 'Usuarios'));
        $rules->add($rules->existsIn(['moneda_id'], 'Monedas'));

        return $rules;
    }
}

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CotizacionesOficinas Model
 *
 * @property \App\Model\Table\CotizacionTable|\Cake\ORM\Association\BelongsTo $Cotizacion
 * @property \App\Model\Table\OficinaTable|\Cake\ORM\Association\BelongsTo $Oficina
 *
 * @method \App\Model\Entity\CotizacionesOficina get($primaryKey, $options = [])
 * @method \App\Model\Entity\CotizacionesOficina newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CotizacionesOficina[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CotizacionesOficina|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CotizacionesOficina patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CotizacionesOficina[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CotizacionesOficina findOrCreate($search, callable $callback = null, $options = [])
 */
class CotizacionesOficinasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cotizaciones_oficinas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cotizaciones', [
            'foreignKey' => 'cotizacion_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Oficinas', [
            'foreignKey' => 'oficina_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cotizacion_id'], 'Cotizaciones'));
        $rules->add($rules->existsIn(['oficina_id'], 'Oficinas'));

        return $rules;
    }
}

<?php
namespace App\Model\Table;

use App\Model\Entity\Lada;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Rule\IsUniqueD;

/**
 * Ladas Model
 *
 * @property \Cake\ORM\Association\HasMany $Dids
 */
class LadasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('ladas');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        /*$this->hasMany('Dids', [
            'foreignKey' => 'lada_id'
        ]);*/
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('lada', 'create')
            ->notEmpty('lada', __('Please fill this field'));

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        
        $rules->add( new IsUniqueD(['lada']) , '_isUnique', [ 'errorField' => 'lada',  'message' => 'La lada ya existe.' ] );

        return $rules;
    }
}

<?php
namespace App\Model\Table;

use App\Model\Entity\ClientesServicio;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClientesServicios Model
 */
class ClientesServiciosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('clientes_servicios');
        $this->displayField('nombre');
        $this->primaryKey('id');
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Servicios', [
            'foreignKey' => 'servicio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Facturas', [
            'foreignKey' => 'factura_id'
        ]);
        $this->belongsTo('Usuarios', [
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tipos', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Unidades', [
            'foreignKey' => 'unidad_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('fecha', 'valid', ['rule' => 'date'])
            ->requirePresence('fecha', 'create')
            ->notEmpty('fecha');
            
        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');
            
        $validator
            ->add('precio_mxn', 'valid', ['rule' => 'decimal'])
            ->requirePresence('precio_mxn', 'create')
            ->notEmpty('precio_mxn');
            
        $validator
            ->add('precio_usd', 'valid', ['rule' => 'decimal'])
            ->requirePresence('precio_usd', 'create')
            ->notEmpty('precio_usd');
            
        $validator
            ->requirePresence('descripcion', 'create')
            ->notEmpty('descripcion');
            
        $validator
            ->add('cantidad', 'valid', ['rule' => 'numeric'])
            ->requirePresence('cantidad', 'create')
            ->notEmpty('cantidad');
            
        $validator
            ->add('notificacion', 'valid', ['rule' => 'numeric'])
            ->requirePresence('notificacion', 'create')
            ->notEmpty('notificacion');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['servicio_id'], 'Servicios'));
        $rules->add($rules->existsIn(['factura_id'], 'Facturas'));
        $rules->add($rules->existsIn(['usuario_id'], 'Usuarios'));
        $rules->add($rules->existsIn(['tipo_id'], 'Tipos'));
        $rules->add($rules->existsIn(['unidad_id'], 'Unidades'));
        return $rules;
    }
}

<?php
namespace App\Model\Table;

use App\Model\Entity\Extension;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;
use App\Model\Rule\IsUniqueD;

/**
 * Extensiones Model
 *
 */
class ExtensionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('extensiones');
        $this->displayField('extension');
        $this->primaryKey('id');

        $this->hasMany('ContactosExtensiones', [
            'foreignKey' => 'extension_id'
        ]);

        $this->hasOne('ContactosExtensionesJoin', [
            'foreignKey' => 'extension_id',
            'joinType' => 'LEFT',
            'className' => 'ContactosExtensiones'
        ]);
        

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('extension', 'create')
            ->notEmpty('extension', __('Please fill this field'));

        $validator
            ->requirePresence('ip', 'create')
            ->notEmpty('ip', __('Please fill this field'));

        $validator
            ->requirePresence('mac', 'create')
            ->notEmpty('mac', __('Please fill this field'));

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['extension'], 'La extensión ya existe.'));
        $rules->add($rules->isUnique(['mac'], 'La MAC Address ya existe.'));
        $rules->add($rules->isUnique(['ip'], 'La IP ya existe.'));
        
        return $rules;
    }
}

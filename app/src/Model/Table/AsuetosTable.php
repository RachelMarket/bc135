<?php
namespace App\Model\Table;

use App\Model\Entity\Asueto;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Asuetos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Useres
 */
class AsuetosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('asuetos');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('dia', 'valid', ['rule' => 'date'])
            ->requirePresence('dia', 'create')
            ->notEmpty('dia');

        $validator
            ->add('user_id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('user_id', 'create')
            ->notEmpty('user_id');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        $rules->add($rules->isUnique(['dia']));

        
        return $rules;
    }


     public function beforeMarshal( $event,  &$data,  &$options){


        if( isset($data['dia'] ) && 
            !empty($data['dia'] )  && 
            !is_array($data['dia']) ){

            list($a,$m,$d) = explode( "-" , $data['dia']) ;
            unset($data['dia']);
            $data['dia'] = array();

            $data['dia']['year'] = $a ;
            $data['dia']['month'] = $m;
            $data['dia']['day'] = $d;


        }

     }

     public function findEvent($query, $options){


        $query->select( [ 'Asuetos.dia' ,'Asuetos.id'   ] );
        $array = $query->toArray();

        $result = [];
        foreach ($array as $key => $value) {
        
            $result[] = array( 'asueto_id' => $value['id'] , 'title' => ' Desmarcar ', 'start'  => $value['dia'] ,'allDay' => true );
 
        }


        return $result; 
     }
}

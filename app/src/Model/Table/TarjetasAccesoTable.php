<?php
namespace App\Model\Table;

use App\Model\Entity\TarjetasAcceso;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;
use App\Model\Rule\IsUniqueD;

/**
 * TarjetasAcceso Model
 *
 */
class TarjetasAccesoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('tarjetas_acceso');
        $this->displayField('num_tarjeta');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Rangos', array('field_2_change' => 'num_tarjeta' ) );

        $this->hasMany('ContactosTarjetasAcceso', [
            'foreignKey' => 'tarjetas_acceso_id'
        ]);

        $this->hasOne('TarjetasAccesoJoin', [
            'foreignKey' => 'tarjetas_acceso_id',
            'joinType' => 'LEFT',
            'className' => 'ContactosTarjetasAcceso'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('num_tarjeta', 'create')
            ->notEmpty('num_tarjeta', __('Please fill this field'));


        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        $rules->add( new IsUniqueD(['num_tarjeta']) , '_isUnique', [ 'errorField' => 'num_tarjeta',  'message' => 'El número de la tarjeta ya existe.' ] );

        return $rules;
    }
}

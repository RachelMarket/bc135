<?php
namespace App\Model\Table;

use App\Model\Entity\Centro;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Rule\IsUniqueD;
use Cake\ORM\TableRegistry;

/**
 * Centros Model
 *
 * @property \Cake\ORM\Association\HasMany $Clientes
 */
class CentrosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('centros');
        $this->displayField('nombre');
        $this->primaryKey('id');
        $this->hasMany('Clientes', [
            'foreignKey' => 'centro_id'
        ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre', 'Este campo es requerido.');

        $validator
            ->requirePresence('nombre_fiscal', 'create')
            ->notEmpty('nombre_fiscal', 'Este campo es requerido.');

        $validator
            ->requirePresence('rfc', 'create')
            ->notEmpty('rfc', 'Este campo es requerido.');

        $validator
            ->requirePresence('direccion_fiscal', 'create')
            ->notEmpty('direccion_fiscal', 'Este campo es requerido.');

        $validator
            ->requirePresence('colonia_fiscal', 'create')
            ->notEmpty('colonia_fiscal', 'Este campo es requerido.');

        $validator
            ->requirePresence('ciudad_fiscal', 'create')
            ->notEmpty('ciudad_fiscal', 'Este campo es requerido.');

        $validator
            ->requirePresence('cp_fiscal', 'create')
            ->notEmpty('cp_fiscal', 'Este campo es requerido.');

        $validator
            ->requirePresence('estado_fiscal', 'create')
            ->notEmpty('estado_fiscal', 'Este campo es requerido.');

        $validator
            ->requirePresence('pais_fiscal', 'create')
            ->notEmpty('pais_fiscal', 'Este campo es requerido.');

        $validator
            ->requirePresence('url_timbrado', 'create')
            ->notEmpty('url_timbrado', 'Este campo es requerido.');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username', 'Este campo es requerido.');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password', 'Este campo es requerido.');

        return $validator;
    }

    public function buildRules(RulesChecker $rules){

        $rules->add( new IsUniqueD(['nombre']) , '_isUnique', [ 'errorField' => 'nombre',  'message' => 'Ya existe un centro con este nombre.' ] );

        return $rules;
    }

    public function getCentros(){

       $centros=$this->find('list', ['order' => ['nombre' => 'asc']])->where(['deleted' => FALSE])->toArray();
       $centros=[''=>'Seleccione']+$centros;
       
       return $centros;

    }

    public function getCentrosSelect(){

       $centros=$this->find('list', ['order' => ['nombre' => 'asc']])->where(['deleted' => FALSE])->toArray();
       $result = [];
       foreach ($centros as $centro) {
           $result[$centro] = $centro;
       }
       $result=[''=>'Seleccione']+$result;
       return $result;

    }

    public function beforeSave($event, $entity, $options){
        
        $config_0 = TableRegistry::exists('Recursos') ? [] : ['className' => 'App\Model\Table\RecursosTable'];
        $this->Recursos = TableRegistry::get('Recursos', $config_0);

        $config_1 = TableRegistry::exists('Clientes') ? [] : ['className' => 'App\Model\Table\ClientesTable'];
        $this->Clientes = TableRegistry::get('Clientes', $config_1);

        //Querys
        $recursos = $this->Recursos->find('all', ['conditions' => ['centro_id' =>  !empty($entity['id'])? $entity['id'] : '' , 'deleted' => FALSE ]])->count();

        $clientesRelacionados = $this->Clientes->find('all', ['conditions' => ['centro_id' =>  !empty($entity['id'])? $entity['id'] : '' ]])->count();

        $bandera = true;
        
        if( isset( $entity['flag'] ) && $entity['flag'] == true ){
        
            if(!empty($recursos) && $recursos > 0){
                $this->error = __('El centro no se puede eliminar porque tiene salas asociados a él.');
                $bandera = false;
            }elseif (!empty($clientesRelacionados) && $clientesRelacionados > 0) {
                $this->error = __('El centro no se puede eliminar porque tiene clientes asociados a él.');
                $bandera = false;
            }

        }

        return $bandera;
    }
}

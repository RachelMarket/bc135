<?php
namespace App\Model\Table;

use App\Model\Entity\Reservacion;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Network\Email\Email;

use Cake\Log\Log;
use Cake\Network\Session;
/**
 * Reservaciones Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Recursos
 */
class ReservacionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('reservaciones');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');


       


        $this->belongsTo('Recursos', [
            'foreignKey' => 'recurso_id'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);

        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id'
        ]);

        $this->belongsTo('Reservaciones', [
            'foreignKey' => 'reservacion_id',
            'className' => 'Reservaciones',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('fecha_inicio', 'valid', ['rule' => 'datetime'])
            ->requirePresence('fecha_inicio');
            
        $validator
            ->add('fecha_fin', 'valid', ['rule' => 'datetime'])
            ->requirePresence('fecha_fin');

        $validator
            ->add('user_id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('user_id');
        
        $validator
            ->add('cantidad', 'valid', ['rule' => 'numeric'])
            ->notEmpty('cantidad');   

        $validator
            ->add('user_id', 'valid', ['rule' => 'numeric'])
            ->notEmpty('user_id');

        $validator
            ->add('cliente_id', 'valid', ['rule' => 'numeric'])
            ->notEmpty('cliente_id');      
       
            
       

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recurso_id'], 'Recursos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));


        $rules->add( [ $this,'canCreate' ],'canCreate' );

        $rules->add( [ $this,'validD' ],'validD' );

        return $rules;
    }


    public function validD($entity, $options){
        // validamos que no se empalme con algun otro evento.


        $config = TableRegistry::exists('Recursos') ? [] : ['className' => 'App\Model\Table\RecursosTable'];
        $this->Recursos = TableRegistry::get('Recursos', $config);


        // valido que mi fecha de inicio y fin tenga el mismo dia 

        if( $entity->fecha_inicio->format('Y-m-d') !=  $entity->fecha_fin->format('Y-m-d')  ){

            $entity->errors('fecha_inicio', __('Debe ser un rango del mismo día') );
            $entity->errors('fecha_fin', __('Debe ser un rango del mismo día') );
            return false;
        }


        // valido mi fecha de inicio debe ser menor a la fecha fin

        if( $entity->fecha_inicio->getTimestamp() >= $entity->fecha_fin->getTimestamp() ){

            $entity->errors('fecha_inicio', __('Seleccione un rango valido') );
            $entity->errors('fecha_fin', __('Seleccione un rango valido') );

            return false;
        } 

        // busco mi recurso.

        // validamos que la fecha de inicio y fin sean mayores al dia de hoy.


        $fecha_Hoy = new Time(   date('Y-m-d')." 00:00:00" ,   $entity->fecha_inicio->tzName  );

        if( $entity->fecha_inicio->getTimestamp() <  $fecha_Hoy->getTimestamp() ){

            $entity->errors('fecha_inicio', __('Seleccione un rango de fecha de hoy en adelante') );
            
            return false;
        }



        $recurso = $this->Recursos->find('all' , [ 'conditions' => [ 'Recursos.id' => $entity->recurso_id ] ] )->first();



        //busco todos los eventos del día;

        $options['conditions'][ ] =  ['Reservaciones.recurso_id' => $entity->recurso_id];
        $options['conditions'][ ] =  ['Reservaciones.fecha_inicio >=' => $entity->fecha_inicio->format('Y-m-d'). " 00:00:00"];
        $options['conditions'][ ] =  ['Reservaciones.fecha_fin <=' => $entity->fecha_fin->format('Y-m-d'). " 23:59:59" ];

        $options['conditions'][ ] =  ['Reservaciones.cancelado' => false ];
        $options['conditions'][ ] =  ['Reservaciones.deleted' => false ];

        $options['order'] =  ['Reservaciones.fecha_inicio' => 'ASC' ];

        if( !$entity->isNew() ){
            $options['conditions'][ ] =  ['Reservaciones.id !=' => $entity->id ];
        }


        $rows = $this->find( 'all', $options );

        $horario_disponible_flag = true;

        $session = new Session();
        $user_group_id = $session->read('Auth.User.user_group_id');
        $is_cliente = (in_array( $user_group_id, [5,6]))? 1 : 0;

        foreach ($rows as $key => $row) {
            
            // valido la fecha de inicio.  primero aumento el tiempo de espera del recurso.
            
            if($is_cliente){
                $row->fecha_fin = $row->fecha_fin->addMinutes( $recurso->minutos_espera ) ;
            }
            

            $row->fecha_inicio = new Time(   $row->fecha_inicio->format('Y-m-d H:i:s') ,   $entity->fecha_inicio->tzName  );
            
            $row->fecha_fin = new Time(   $row->fecha_fin->format('Y-m-d H:i:s') ,  $entity->fecha_inicio->tzName );

            
            if($row->fecha_inicio->getTimestamp() == $entity->fecha_inicio->getTimestamp() && $row->fecha_fin->getTimestamp() == $entity->fecha_fin->getTimestamp()){
                $horario_disponible_flag = false;
            }

            // interior

            if( $entity->fecha_inicio->getTimestamp()  <  $row->fecha_inicio->getTimestamp()  &&
                $entity->fecha_fin->getTimestamp() > $row->fecha_fin->getTimestamp()
                 ){

                $horario_disponible_flag = false;

            }


            if( $entity->fecha_inicio->getTimestamp() > $row->fecha_inicio->getTimestamp() && 
                $entity->fecha_inicio->getTimestamp() <   $row->fecha_fin->getTimestamp() &&
                $entity->fecha_inicio->getTimestamp() > $row->fecha_fin->getTimestamp()
             ){
                
                $horario_disponible_flag = false;
            }
             

            if( $entity->fecha_fin->getTimestamp()  < $row->fecha_fin->getTimestamp() &&
                $entity->fecha_fin->getTimestamp()  > $row->fecha_inicio->getTimestamp()
             ){

               $horario_disponible_flag = false;

            }

            

            if( $entity->fecha_inicio->getTimestamp()  < $row->fecha_fin->getTimestamp() &&
                $entity->fecha_fin->getTimestamp()  > $row->fecha_fin->getTimestamp()
             ){

               $horario_disponible_flag = false;

            }

            
            // validamos el tiempo extra del recurso.

            $fecha_fin_ = new Time(   $entity->fecha_fin->format('Y-m-d H:i:s')  , $entity->fecha_inicio->tzName );

            if($is_cliente){
                $fecha_fin_ = $fecha_fin_->addMinutes($recurso->minutos_espera  );
            }




            if(   $fecha_fin_->getTimestamp() > $row->fecha_inicio->getTimestamp() &&
                   $entity->fecha_inicio->getTimestamp()  < $row->fecha_inicio->getTimestamp() 
               ){

                $horario_disponible_flag = false;
            }
            

            if(!$horario_disponible_flag){
                $options['conditions'] = [];
                $options['conditions'][ ] =  ['Reservaciones.recurso_id' => $entity->recurso_id];

                $fecha_proxima_disponible = $this->proximoHorarioDisponible($row->fecha_inicio->format('Y-m-d H:i:s'), $row->fecha_inicio->format('Y-m-d').' 23:59:59', $options, $recurso->minutos_espera);
               
                
                $entity->errors('fecha_inicio', __('El recurso más próximo esta disponible apartir de las '.$fecha_proxima_disponible['fecha_inicio_proximo']->format('d/m/Y H:i:s A').', seleccione a partir de ese horario.') );

                if(!empty($fecha_proxima_disponible['fecha_fin_proximo'])){
                    $entity->errors('fecha_fin', __('El recurso más próximo esta disponible hasta las '.$fecha_proxima_disponible['fecha_fin_proximo']->format('d/m/Y H:i:s A').', seleccione a partir de ese horario.') );
                }

                return false;
            }
        }

        return true;
    }


    public function proximoHorarioDisponible($fecha_inicio = "", $fecha_fin = "", $options = [], $minutos_espera = 0)
    {
        $session = new Session();
        $user_group_id = $session->read('Auth.User.user_group_id');
        $is_cliente = (in_array( $user_group_id, [5,6]))? 1 : 0;

        $fecha_proxima_disponible = [];

        $options['conditions'][ ] =  ['Reservaciones.fecha_inicio >=' => $fecha_inicio];
        $options['conditions'][ ] =  ['Reservaciones.fecha_fin <=' => $fecha_fin];
        $options['conditions'][ ] =  ['Reservaciones.cancelado' => false ];
        $options['conditions'][ ] =  ['Reservaciones.deleted' => false ];

        $options['order'] =  ['Reservaciones.fecha_inicio' => 'ASC' ];
    
        $rows = $this->find( 'all', $options )->toArray(); 
        foreach ($rows as $key => $row) {
           if(isset($rows[$key + 1])){
                $one_day = new \DateTime($row->fecha_fin->format('Y-m-d H:i'));

                if($is_cliente){
                    $fecha_inicio_siguiente = $rows[$key + 1]->fecha_inicio->addMinutes( $minutos_espera );
                }else{
                    $fecha_inicio_siguiente = $rows[$key + 1]->fecha_inicio;
                }

                $tow_day = new \DateTime($fecha_inicio_siguiente->format('Y-m-d H:i'));
                
                $diff = $one_day->diff($tow_day); 
                
                if($diff->h > 0){
                    $fecha_proxima_disponible['fecha_inicio_proximo'] = $tow_day->modify('-'.$diff->h.' hours');
                    $fecha_proxima_disponible['fecha_fin_proximo'] = clone $tow_day;
                    $fecha_proxima_disponible['fecha_fin_proximo'] = $fecha_proxima_disponible['fecha_fin_proximo']->modify('+'.$diff->h.' hours');

                    if($is_cliente){
                        $fecha_proxima_disponible['fecha_fin_proximo'] = $fecha_proxima_disponible['fecha_fin_proximo']->modify('-'.$minutos_espera.' minutes');
                    }


                    return $fecha_proxima_disponible;
                }
           }
        }

        if($is_cliente){
            $fecha_proxima_disponible['fecha_inicio_proximo'] = $row->fecha_fin->addMinutes( $minutos_espera );
        }else{
            $fecha_proxima_disponible['fecha_inicio_proximo'] = $row->fecha_fin;
        }

        $fecha_proxima_disponible['fecha_fin_proximo'] = '';

        return $fecha_proxima_disponible;
    }

    public function beforeMarshal( $event,  &$data,  &$options){


        



        if( isset($data['fecha_inicio'] ) && 
            !empty($data['fecha_inicio'] )  && 
            !is_array($data['fecha_inicio']) ){


            list($fecha,$horas) = explode( " " , $data['fecha_inicio']) ;

            list($a,$m,$d) = explode( "-" , $fecha ) ;
            unset($data['fecha_inicio']);

            list($h,$min) = explode( ":" , $horas ) ;

            $data['fecha_inicio'] = array();

            $data['fecha_inicio']['year'] = $a ;
            $data['fecha_inicio']['month'] = $m;
            $data['fecha_inicio']['day'] = $d;
            $data['fecha_inicio']['hour'] = $h;
            $data['fecha_inicio']['minute'] = $min;


        }


        if( isset($data['fecha_fin'] ) && 
            !empty($data['fecha_fin'] )  && 
            !is_array($data['fecha_fin']) ){


            list($fecha,$horas) = explode( " " , $data['fecha_fin']) ;

            list($a,$m,$d) = explode( "-" , $fecha ) ;
            unset($data['fecha_fin']);

            list($h,$min) = explode( ":" , $horas ) ;

            $data['fecha_fin'] = array();

            $data['fecha_fin']['year'] = $a ;
            $data['fecha_fin']['month'] = $m;
            $data['fecha_fin']['day'] = $d;
            $data['fecha_fin']['hour'] = $h;
            $data['fecha_fin']['minute'] = $min;


        }

        $modOficinaVirtual = $this->isModOficinaVirtual($data['cliente_id'] , $data['recurso_id']  );

        
        
        if( $modOficinaVirtual ){
            $recurso = $this->Recursos->get( $data['recurso_id']   );

           

            $data['fecha_inicio']['hour'] = $recurso->hora_inicio->format('H') ;
            $data['fecha_inicio']['minute'] =$recurso->hora_inicio->format('i') ;
            $data['fecha_fin']['hour'] = $recurso->hora_fin->format('H');
            $data['fecha_fin']['minute'] = $recurso->hora_fin->format('i');

            // no se permite recurencias.
            $data['recurrente']=false;
            $data['allday']=1; // marcamos esta reservacion como

        }


        
       

     }


     protected function crearservicioadicional($entity , $horas){


        $clientesServicioTable = TableRegistry::get('ClientesServicios');
        $servicio = $clientesServicioTable->newEntity();

        $servicioObject = $clientesServicioTable->Servicios->find('all', ['conditions'=> [ 'Servicios.id' => 26 ] ] ) ;

        $servicioObject = $servicioObject->toArray()[0];
        $servicio->nombre = $servicioObject['nombre'];
        $servicio->unidad_id = $servicioObject['unidad_id'];
        $servicio->precio_mxn = $servicioObject['precio_mxn'];
        $servicio->precio_usd = $servicioObject['precio_usd'];
        $servicio->descripcion = $servicioObject['descripcion'];
        $servicio->tipo_id = 3;
        $servicio->cantidad = $horas;
        $servicio->notificacion = 0;
        $servicio->servicio_id = 26;
        $servicio->cliente_id = $entity->cliente_id ;
        $servicio->usuario_id = $entity->user_id;
        
        $servicio->fecha = $entity->fecha_fin->format('Y-m-d H:i:s');
        $clientesServicioTable->save($servicio);

        return $servicio;


     }


     public function isModOficinaVirtual( $cliente_id , $recurso_id  ){

        $flag = false;

        $cliente = $this->Clientes->get( $cliente_id );
        $recurso = $this->Recursos->get( $recurso_id  );


        if( $cliente->tipo_cliente_id == 6 && $recurso->tipo_id == 2  ){
            $flag = true;
        }

        return $flag;

     }
     public function beforeSave($event,  &$entity,  $options){

        if( $entity->isNew() ){

            // estoy a punto de crear una nueva reservación
            // debo ver como va el saldo del cliente. para el mes en que separa 

            // $this->Clientes->get()


            // Soy Un cliente virtual ???

            Log::write('debug'," Nueva Reservacion.  "     ); 


            if( $this->isModOficinaVirtual($entity->cliente_id , $entity->recurso_id  ) ){
                // Saldo por Días.

                //@todo  restar el saldo en dias.
                Log::write('debug'," Modo Oficina Virtual "     ); 
                $saldo = $this->Clientes->saldodias($entity->cliente_id, $entity->fecha_inicio);

                if( $saldo < 1){

                throw new \Exception("No cuenta con saldo para realizar la reservación", 1);
                    
                }

            }else{


                // Saldo por horas
                Log::write('debug'," Modo Saldo En Horas  "     ); 


                Log::write('debug'," Checando el saldo del   " .$entity->cliente_id     ); 

                Log::write('debug'," Checando el saldo del   " .$entity->fecha_inicio     ); 
                $saldo = $this->Clientes->saldo($entity->cliente_id, $entity->fecha_inicio);

                Log::write('debug'," Saldo del Cliente    " .$saldo    );

                $horas = $entity->fecha_fin->diffInHours( $entity->fecha_inicio );
                // Si mi saldo es ya negativo debo cobrar solamente el tiempo extra de esta reservación

                if( $saldo < 0){

                    Log::write('debug'," Mi saldo ya es negativo cobro el tiempo en hora     " .$horas     );
                    // obtengo la duración de la reservacion
                     $servicio = $this->crearservicioadicional($entity,$horas);
                     $entity->servicio_id = $servicio->id;
                }else{

                    Log::write('debug'," Aun tengo saldo    "    );
                    // aun tengo saldo o es cero.
                    $resto = $saldo - $horas;
                    
                    Log::write('debug'," Mi saldo futuro:    ". $resto     );
                    if( $resto  < 0 ){
                        
                        Log::write('debug'," Creo el Servicio:    ". $resto     );
                        $servicio = $this->crearservicioadicional($entity, abs($resto ) );
                        $entity->servicio_id = $servicio->id;
                    }
                }

                

            }
        }

     }


    public function findEvent($query, $options){

        $config = TableRegistry::exists('Recursos') ? [] : ['className' => 'App\Model\Table\RecursosTable'];
        $this->Recursos = TableRegistry::get('Recursos', $config);


        $conditionsRecurso[] = [ 'Recursos.deleted' => false];


        if( isset( $options['centro_id'] ) && !empty( $options['centro_id'] ) ){

            if( intval( $options['centro_id'] ) > 0 )
                $conditionsRecurso[ ] = [ 'Recursos.centro_id' => $options['centro_id']  ];


        }

        $recursos = $this->Recursos->find('all', [ 
            'order' => ['Recursos.id' => 'ASC'],
            'conditions' => $conditionsRecurso ,  
            'contain' => ['RecursosFotos'] ] ) ;


        $listaRecursos = [];
        $recursosids  = [];

        foreach ($recursos as $key => $value) {
            if( count($value->recursos_fotos ) < 1 )
                continue;

            if( !file_exists(  WWW_ROOT.'files/fotos/'.$value->recursos_fotos[0]->name  ) )
                continue;
            $listaRecursos[] = $value->name;
            $recursosids[] = $value->id;
        } 

        $modo_protegido = false;
        $userId = false;
        $mostrar_tiempo_espera = false;
        $modo_color  = false;

        if( isset($options['modo_protegido']) && $options['modo_protegido'] ){
            $modo_protegido = true;
        }

        if( isset($options['modo_color']) && $options['modo_color'] ){
            $modo_color = true;
        }

        if( isset($options['userId']) && isset( $options['user'] ) ){

            $userId = $options['user']['User']['cliente_id'] ;
        }


        if( isset( $options['mostrar_tiempo_espera'] ) ){

            if($options['mostrar_tiempo_espera']){
                $mostrar_tiempo_espera = true;
            }
        }

        $query->select( [ 'Reservaciones.tipo', 'Reservaciones.recurso_id', 'Reservaciones.cantidad', 'Reservaciones.asunto'  ,'Reservaciones.cliente_id','Reservaciones.reservacion_id', 'Reservaciones.fecha_inicio' ,'Reservaciones.id' , 'Reservaciones.fecha_fin' , 'hijos' => ' (select count( Reservaciones2.id ) from reservaciones as Reservaciones2 where Reservaciones2.reservacion_id = Reservaciones.id   )  '   ] )->contain([ 'Clientes' => function ($value) {
                return $value->autoFields(false)
                         ->select(['id','nombre']);
            }
        ]);


        $query->where( [ 'Reservaciones.cancelado' => false ] );

        $query->where( [ 'Reservaciones.deleted' => false ] );
        $query->where( [ 'Reservaciones.recurso_id IN' => $recursosids ] );
        $query->order( ['Reservaciones.recurso_id' => 'ASC'] );
        
        $array = $query->toArray();

        //debug($array);

        $result = [];
        

        foreach ($array as $key => $value) {
            
            $class = 'reservacion';
            $title = $value['asunto'];

            $cliente = ( !empty( $value['cliente']['nombre'] ) )? $value['cliente']['nombre'] : "";

            if($modo_protegido && $userId != $value['cliente_id'] ){
                $title = 'Reservado';
                $value['id'] = -1; // no puedo editar
                $cliente = '';
            }

            $fecha_Hoy = new Time(   date('Y-m-d')." 00:00:00" ,   $value['fecha_inicio']->tzName  );

            if( $value['fecha_inicio']->getTimestamp() <  $fecha_Hoy->getTimestamp() ){
                $value['id'] = -1; // no puedo editar
                
            }

            if( $modo_protegido ){

                // solo puedo editar lo de mañana 

                $fecha_Hoy->addDay();
                if( $value['fecha_inicio']->getTimestamp() <  $fecha_Hoy->getTimestamp() ){
                    $value['id'] = -1; // no puedo editar
                
                }

            }

            $session = new Session();
            $user_group_id = $session->read('Auth.User.user_group_id');
            
            //Socios ejecutivos y Virtuales
            $is_cliente = (in_array( $user_group_id, [5,6]))? 1 : 0;
            
            //Admin y Staff
            $is_admin = (in_array( $user_group_id, [1,4]))? 1 : 0;

            if($is_cliente > 0){
                if($userId && $userId == $value['cliente_id'] ){ $class = 'mireservacion';  $title = $value['asunto']; }else {
                    $title = 'Reservado';
                    $value['id'] = -1;
                    $cliente = '';
                }
            }

            if($is_admin > 0){  
                $class = 'mireservacion';  
                $title = $value['asunto'];
            }

            if( $value['hijos'] > 0 ){
                 $value['reservacion_id'] = $value['id'];

            }

            

            // busco mi recurso
            $recurso = $this->Recursos->get( $value['recurso_id'] );

            
            $col = array_search( $recurso->name,$listaRecursos );

            $event = array( 
                    'class' => $class, 
                    'recurso_id' => $value['recurso_id'],
                    'asunto' => $value['asunto'],
                    'cantidad' => $value['cantidad'],
                    'reservacion_id' => $value['id'] , 
                    'cliente_id' => $value['cliente_id'],
                    'padre' => $value['reservacion_id'],
                    'title' => $title, 
                    'start'  => $value['fecha_inicio'],
                    'end'  => $value['fecha_fin'] ,
                    'hijos' => $value['hijos'],
                    'tipo' => $value['tipo'],
                    'allDay' => false ,
                    'backgroundColor' =>  $recurso->color,
                    'borderColor' =>  $recurso->color,
                    'column' => $col,
                    'cliente' => $cliente 
                    );
            
            if(!$modo_color && $cliente == "" ){
                unset( $event['backgroundColor' ] );
                unset( $event['borderColor' ] );
            }
            $result[] = $event;
            if($mostrar_tiempo_espera){

                // crear otro tipo de evento 

                $fecha_fin = new Time(  $value['fecha_fin']  );

                $fecha_fin->addMinutes( $recurso->minutos_espera);

                $result[] = array( 
                    'class' => 'nodisponible', 
                    'reservacion_id' => -1 , 
                    'title' => __('No Disponible'), 
                    'start'  => $value['fecha_fin'] ,
                    'end'  => $fecha_fin->format('Y-m-d H:i:s' ) ,
                    'allDay' => false, 
                    'backgroundColor' => "#E74C3C", 
                    'borderColor' => '#E74C3C',
                   'column' => $col, 
                   'cliente' => ""
                    );

            }
        }

        

        return $result; 
     }


     private function cambio_horario($rows , $entity  ){
        // se va a repetir mientras sea este año
        $a = date('Y');

        $DIA = 1;
        $SEMANAL = 2;
        $MES = 3;
        $continuar= true;
        foreach ($rows as $key => $reservacion) {
            

            

            if( $entity->tipo == $DIA ){

                $entity->fecha_inicio->addDay();
                $entity->fecha_fin->addDay();


           }

           if( $entity->tipo == $SEMANAL ){

                $entity->fecha_inicio->addWeek();
                $entity->fecha_fin->addWeek();
            

           }

           if( $entity->tipo == $MES ){

                $entity->fecha_inicio->addMonth();
                $entity->fecha_fin->addMonth();

           }

           if($a  < $entity->fecha_inicio->format('Y')){

                $continuar = false;

           }

         

           if($continuar){

                $dataE['asunto'] =  $entity->asunto ;
                $dataE['cantidad'] =  $entity->cantidad ;
                $dataE['cliente_id'] = $entity->cliente_id;

                $dataE['fecha_inicio'] = $entity->fecha_inicio->format('Y-m-d H:i:s');
                $dataE['fecha_fin'] = $entity->fecha_fin->format('Y-m-d H:i:s'); ;
                $dataE['recurso_id'] = $entity->recurso_id;
                $dataE['user_id'] =  $entity->user_id ;
                $dataE['reservacion_id'] = $entity->id;
                $dataE['tipo'] =  $entity->tipo;

                $reservacion = $this->Reservaciones->patchEntity($reservacion, $dataE);

                $this->actualiza_datos_servicio( $reservacion );
               
                $success = $this->save( $reservacion  );

                if(!$success){
                    $lbl = "Ocurrio un error al intentar guardar la reservación: <br>";

                    $lbl .= 'Fecha Inicio: ' . $reservacion->fecha_inicio->format('d-m-Y H:i'). "<br>";

                    $lbl .= 'Fecha Fin: ' . $reservacion->fecha_fin->format('d-m-Y H:i')."<br>";
                    $arrayE = $reservacion->errors();
                    foreach ($arrayE  as $field => $errors) {
                            
                        $lbl .= "<br>" . $errors[0];

                    }
                throw new \Exception($lbl , 1);
                }


           }else{
                //las cancelo o borro

           }


        }
     }

     public function actualiza_datos_servicio( $entity ){


        if(!is_null( $entity->servicio_id )){
            $config = TableRegistry::exists('clientes_servicios') ? [] : ['className' => 'App\Model\Table\ClientesServiciosTable'];

            $ClientesServiciosTable = TableRegistry::get('ClientesServicios', $config);

            $servicio = $ClientesServiciosTable->get( $entity->servicio_id);

            $horas = $entity->fecha_fin->diffInHours( $entity->fecha_inicio );

            $servicio->cantidad = $horas;

            $ClientesServiciosTable->save( $servicio );

        }
     }

     public function guardar_replicar($entity,$data, $tipo){

        


        $cambioTipo = false;

        if( isset( $data['Reservaciones']['recurrente']) && 
            isset($data['Reservaciones']['tipo']) && 
            intval($data['Reservaciones']['tipo']) > 0  && 
            $data['Reservaciones']['recurrente']  && 
            $tipo != $entity->tipo
            ){


            $cambioTipo = true;

        }

        $this->actualiza_datos_servicio( $entity );
        

        if( isset( $data['Reservaciones']['guardar_replicar'])  && intval($data['Reservaciones']['guardar_replicar']) > 0   ){


            // buscar todos las reservaciones 

            $options['conditions'][] = ['Reservaciones.cancelado' => false];
            $options['conditions'][] = ['Reservaciones.deleted' => false];
            $options['conditions'][] = ['Reservaciones.id !=' => $entity->id];

            $options['conditions'][] = ['Reservaciones.fecha_fin >' => $entity->fecha_fin->format('Y-m-d H:i:s')];


            if($entity->reservacion_id == null){
                // soy el padre :D
                $options['conditions'][] = ['Reservaciones.reservacion_id' => $entity->id];
            }else{
                // busco a mis hermanos 
                $options['conditions'][] = ['Reservaciones.reservacion_id' => $entity->reservacion_id ];
            }
            $options['contain'] = [];


            $rows = $this->find( 'all', $options );

            if( !$cambioTipo ){

                $this->cambio_horario( $rows,$entity );

            }else{

                foreach ($rows as $key => $reservacion) {

                    $this->cancelar($reservacion);

                }

                $this->repetir($entity,$data);

            }




            
        }else{

            if($cambioTipo){

                    $this->repetir($entity,$data);
            }
        }
     }

     public function repetir($entity,$data){



        // se va a repetir mientras sea este año
        $a = date('Y');

        $DIA = 1;
        $SEMANAL = 2;
        $MES = 3;
        $entities = array();
        // $data['Reservaciones']['recurso_id'] 
        // $data['Reservaciones']['cliente_id'] 
        // $data['Reservaciones']['saldo']
        // $data['Reservaciones']['asunto']
        // $data['Reservaciones']['cantidad']
        // $data['Reservaciones']['fecha_inicio']
        // $data['Reservaciones']['fecha_fin']
        // $data['Reservaciones']['recurrente']
        // $data['Reservaciones']['tipo']

        if( isset( $data['Reservaciones']['recurrente']) && isset($data['Reservaciones']['tipo']) && intval($data['Reservaciones']['tipo']) > 0  && $data['Reservaciones']['recurrente'] ){
            $continuar = true;


            

            do{


                
               if( $data['Reservaciones']['tipo'] == $DIA ){

                    $entity->fecha_inicio->addDay();
                    $entity->fecha_fin->addDay();


               }

               if( $data['Reservaciones']['tipo'] == $SEMANAL ){

                    $entity->fecha_inicio->addWeek();
                    $entity->fecha_fin->addWeek();
                

               }

               if( $data['Reservaciones']['tipo'] == $MES ){

                    $entity->fecha_inicio->addMonth();
                    $entity->fecha_fin->addMonth();

               }

               if($a  < $entity->fecha_inicio->format('Y')){

                    $continuar = false;

               }


               
                if( $continuar ){
                    $nueva = $this->newEntity();
                    $dataE = array();

                    $dataE['asunto'] =  $entity->asunto ;
                    $dataE['cantidad'] =  $entity->cantidad ;
                    $dataE['cliente_id'] = $entity->cliente_id;

                    $dataE['fecha_inicio'] = $entity->fecha_inicio->format('Y-m-d H:i:s');
                    $dataE['fecha_fin'] = $entity->fecha_fin->format('Y-m-d H:i:s'); ;
                    $dataE['recurso_id'] = $entity->recurso_id;
                    $dataE['user_id'] =  $entity->user_id ;
                    $dataE['reservacion_id'] = $entity->id;
                    $dataE['tipo'] =  $data['Reservaciones']['tipo'];

                    
                    $reservacion = $this->patchEntity($nueva, $dataE);

                   
                    $this->canCreate($reservacion, null);
                    $this->validD( $reservacion,null );

                    $errors = $reservacion->errors();

                    if( empty( $errors ) ){
                        $entities[] = $reservacion;
                    }else{

                        $config = TableRegistry::exists('Recursos') ? [] : ['className' => 'App\Model\Table\RecursosTable'];
                        $this->Recursos = TableRegistry::get('Recursos', $config);

                        $recurso = $this->Recursos->get( $entity->recurso_id);

                        $arrayE = $reservacion->errors();

                        $lbl = "Ocurrio un error al intentar crear la reservación: <br>";

                        $lbl .= 'Fecha Inicio: ' . $reservacion->fecha_inicio->format('d-m-Y H:i'). "<br>";

                        $lbl .= 'Fecha Fin: ' . $reservacion->fecha_fin->format('d-m-Y H:i')."<br>";

                        $lbl .= 'Recurso: ' . $recurso->name . "<br>";

                        $lbl .=  $recurso->descripcion . "<br>";



                        foreach ($arrayE as $field => $errors) {
                                
                            $lbl .= "<br>" . $errors[0];

                        }
                        throw new \Exception($lbl , 1);
                        
                    }
                }
               

           }while($continuar );


           foreach ($entities as $key => $reservacion) {
               $success = $this->save( $reservacion  );

                if(!$success){
                    $arrayE = $reservacion->errors();

                    $lbl = "Ocurrio un error al intentar crear la reservación: <br>";

                    $lbl .= 'Fecha Inicio: ' . $reservacion->fecha_inicio->format('d-m-Y H:i'). "<br>";

                    $lbl .= 'Fecha Fin: ' . $reservacion->fecha_fin->format('d-m-Y H:i')."<br>";

                    foreach ($arrayE as $field => $errors) {
                            
                        $lbl .= "<br>" . $errors[0];

                    }
                throw new \Exception($lbl , 1);
                }
           }    
           

           return $entities ;

        }



     }

     private function eliminar_servicio_asociado($entity){

        // Compruebo que este servicio no este facturado .

        $config = TableRegistry::exists('clientes_servicios') ? [] : ['className' => 'App\Model\Table\ClientesServiciosTable'];

        $ClientesServiciosTable = TableRegistry::get('ClientesServicios', $config);

        $servicio = $ClientesServiciosTable->get( $entity->servicio_id);

        if( is_null( $servicio->factura_id ) ){

            // lo puedo borrar.

            $ClientesServiciosTable->delete( $servicio  );

        }else{

            throw new \Exception("Imposible cancelar la reservación del ".$entity->fecha_inicio->format('d-m-Y H:i ') . " - " . $entity->fecha_fin->format('d-m-Y H:i ') . " por que cuenta con una factura asociada " , 1);
            
        }


     }

     private function cancelar( $entity ){

        $settings = getAllSettings();
        $tzName = $settings['defaultTimeZone']['value'];



        $fecha_Hoy = new Time(   'now' ,   $tzName  );

        
        $fecha_inicio = new Time($entity->fecha_inicio->format('Y-m-d H:i:s') , $tzName  );




        Log::write('debug'," Fecha de Hoy .  "  . $fecha_Hoy   );

        Log::write('debug'," Fecha de Inicio .  "  . $fecha_inicio   );


        Log::write('debug'," Fecha de Hoy .  "  . $fecha_Hoy->getTimestamp()   );

        Log::write('debug'," Fecha de Inicio .  "  . $fecha_inicio->getTimestamp()   );



        if( $fecha_inicio->getTimestamp() <  $fecha_Hoy->getTimestamp() ){

            throw new \Exception( __('No se puede cancelar una reservación que ya paso'), 1);
            
        }

        // compruebo que mi entidada tenga un servicio. si tengo lo borro.

        if( !is_null( $entity->servicio_id ) ){

            $this->eliminar_servicio_asociado(  $entity );
        }

        $this->canCancel($entity);


        $entity->cancelado = 1 ;

        if( !$this->save( $entity   ) ){
            throw new \Exception("Ocurrio un error al intentar cancelar una reservación ", 1);
            
        }




     }

     public function enviar_correo($entity){


        // busco el recurso 
        $config = TableRegistry::exists('Recursos') ? [] : ['className' => 'App\Model\Table\RecursosTable'];
        $this->Recursos = TableRegistry::get('Recursos', $config);
        $config = TableRegistry::exists('Clientes') ? [] : ['className' => 'App\Model\Table\ClientesTable'];
        $this->Clientes = TableRegistry::get('Clientes', $config);
        $config = TableRegistry::exists('Centros') ? [] : ['className' => 'App\Model\Table\CentrosTable'];
        $this->Centros = TableRegistry::get('Centros', $config);

        $recurso = $this->Recursos->get($entity->recurso_id );

        $centro = $this->Centros->get( $recurso->centro_id );

        $cliente = $this->Clientes->get( $entity->cliente_id );


        $email = new Email('default');
        //debug($factura);

        $to =$cliente->primario_correo_electronico;

        //$to = "francisco.carrizales@webpoint.mx";

        

        $email->from(['noreplay@135-bc.com' => 'Notificaciones BCM'])
        ->emailFormat('html')
        ->viewVars(['recurso'=>$recurso, 'centro'=>$centro, 'reservacion' => $entity])
        ->template('reservaciones')
        ->to($to)
        ->subject('Reservación de '. $recurso->name . " 135 Business Center" )
        ->send();

     }

     public function cancelar_reservacion($id , $data = null){


        $reservacion = $this->get($id);


        $this->cancelar($reservacion);


        if( isset($data['Reservaciones']['cancelar_replicar'] ) &&  $data['Reservaciones']['cancelar_replicar'] == 1 ){
        // buscar todos las reservaciones 

            $options['conditions'][] = ['Reservaciones.cancelado' => false];
            $options['conditions'][] = ['Reservaciones.deleted' => false];
            $options['conditions'][] = ['Reservaciones.id !=' => $reservacion->id];

            $options['conditions'][] = ['Reservaciones.fecha_fin >' => $reservacion->fecha_fin->format('Y-m-d H:i:s')];


            if($reservacion->reservacion_id == null){
                // soy el padre :D
                $options['conditions'][] = ['Reservaciones.reservacion_id' => $reservacion->id];
            }else{
                // busco a mis hermanos 
                $options['conditions'][] = ['Reservaciones.reservacion_id' => $reservacion->reservacion_id ];
            }
            $options['contain'] = [];


            $rows = $this->find( 'all', $options );

            foreach ($rows as $key => $entity) {
                $this->cancelar($entity);
            }
        }



     }

}

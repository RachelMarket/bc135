<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reservacion Entity.
 */
class Reservacion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'recurso_id' => true,
        'user_id' => true,
        'fecha_inicio' => true,
        'fecha_fin' => true,
        'cancelado' => true,
        'deleted' => true,
        'recurso' => true,
        'cliente_id' => true,
        'asunto' => true,
        'cantidad' => true,
        'cantidad' => true,
        'reservacion_id' => true,
        'tipo' => true,
        'servicio_id' => true,
        'allday' => true 
    ];
}

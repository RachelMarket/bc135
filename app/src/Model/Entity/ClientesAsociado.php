<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClientesAsociado Entity.
 */
class ClientesAsociado extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'comentario' => true,
        'cliente_id' => true,
        'cliente' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TarjetasAcceso Entity.
 */
class TarjetasAcceso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'num_tarjeta' => true,
    ];
}

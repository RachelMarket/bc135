<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Moneda Entity.
 */
class Moneda extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'clientes' => true,
        'facturas' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Centro Entity.
 */
class Centro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'calle' => true,
        'numero' => true,
        'colonia' => true,
        'ciudad' => true,
        'cp' => true,
        'estado' => true,
        'pais' => true,
        'email' => true,
        'telefono' => true,
        'telefono2' => true,
        'nombre_fiscal' => true,
        'rfc' => true,
        'direccion_fiscal' => true,
        'colonia_fiscal' => true,
        'ciudad_fiscal' => true,
        'cp_fiscal' => true,
        'estado_fiscal' => true,
        'pais_fiscal' => true,
        'referencias_fiscales' => true,
        'url_timbrado' => true,
        'username' => true,
        'password' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'clientes' => true
    ];
}

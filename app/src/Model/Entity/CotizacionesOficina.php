<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CotizacionesOficina Entity
 *
 * @property int $id
 * @property int $cotizacion_id
 * @property int $oficina_id
 *
 * @property \App\Model\Entity\Cotizacion $cotizacion
 * @property \App\Model\Entity\Oficina $oficina
 */
class CotizacionesOficina extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cotizacion_id' => true,
        'oficina_id' => true,
        'cotizacion' => true,
        'oficina' => true
    ];
}

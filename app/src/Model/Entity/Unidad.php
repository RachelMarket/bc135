<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Unidad Entity.
 */
class Unidad extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'clientes_servicios' => true,
        'partidas' => true,
        'servicios' => true,
    ];
}

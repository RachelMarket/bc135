<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Oficina Entity.
 */
class Oficina extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'centro_id' => true,
        'numero' => true,
        'capacidad' => true,
        'precio_minimo' => true,
        'precio_medio' => true,
        'precio_maximo' => true,
        'eliminado' => true,
        'centro' => true,
    ];
}

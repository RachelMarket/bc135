<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
/**
 * Recurso Entity.
 */
class Recurso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'descripcion' => true,
        'centro_id' => true,
        'capacidad' => true,
        'tiempo_minimo' => true,
        'tipo_id' => true,
        'hora_inicio' => true,
        'hora_fin' => true,
        'minutos_espera' => true,
        'deleted' => true,
        'centro' => true,
        'tipo' => true,
        'color' => true
    ];



    protected function _getColor($color){

        if( empty( $color ) ){
            $color = sprintf( "#%06X\n", mt_rand( 0, 0xFFFFFF ));
            // $config = TableRegistry::exists('Recursos') ? [] : ['className' => 'App\Model\Table\RecursosTable'];
            // $table = TableRegistry::get('Recursos', $config);

            // $entity = $table->get($this->_properties['id'] );
            // $entity->color = $color;

            // $table->save( $entity );


        }

        return $color;

    }
}

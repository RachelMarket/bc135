<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Servicio Entity.
 */
class Servicio extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'tipo_id' => true,
        'nombre' => true,
        'unidad_id' => true,
        'precio_mxn' => true,
        'precio_usd' => true,
        'descripcion' => true,
        'tipo' => true,
        'unidad' => true,
        'clientes' => true,
    ];
}

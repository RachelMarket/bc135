<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Asueto Entity.
 */
class Asueto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'dia' => true,
        'user_id' => true,
        'user' => true,
    ];
}

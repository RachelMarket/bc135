<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Recordatorio Entity.
 */
class Recordatorio extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'subject' => true,
        'header' => true,
        'footer' => true,
        'diferencia' => true,
        'activo' => true,
    ];
}

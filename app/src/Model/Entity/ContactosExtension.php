<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContactosExtension Entity.
 */
class ContactosExtension extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'contacto_id' => true,
        'extension_id' => true,
        'contacto' => true,
        'extension' => true,
    ];
}

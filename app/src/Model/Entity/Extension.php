<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Extension Entity.
 */
class Extension extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'extension' => true,
        'did_recepcion' => true,
        'did_directo' => true,
        'ip' => true,
        'mac' => true,
    ];
}

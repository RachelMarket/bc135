<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Grupo Entity.
 */
class Grupo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'url' => true,
        'permisos' => true,
        'usuarios' => true,
    ];
}

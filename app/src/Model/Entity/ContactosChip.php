<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContactosChip Entity.
 */
class ContactosChip extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'contacto_id' => true,
        'chip_id' => true,
        'contacto' => true,
        'chip' => true,
    ];
}

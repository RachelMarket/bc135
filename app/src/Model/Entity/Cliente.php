<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cliente Entity.
 */
class Cliente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'primario_persona_de_contacto' => true,
        'primario_correo_electronico' => true,
        'primario_telefono_oficina' => true,
        'primario_telefono_movil' => true,
        'secundario_persona_de_contacto' => true,
        'secundario_correo_electronico' => true,
        'secundario_telefono_oficina' => true,
        'secundario_telefono_movil' => true,
        'razon_social' => true,
        'rfc' => true,
        'calle' => true,
        'numero' => true,
        'colonia' => true,
        'pais_id' => true,
        'estado_id' => true,
        'municipio_id' => true,
        'codigo_postal' => true,
        'moneda_id' => true,
        'activo' => true,
        'tipo_cliente_id' => true,
        'dia_pago' => true,
        'numero_interior' => true,
        'oficina' => true,
        'centro_id' => true,
        'formasdepago_id' => true,
        'cuentabanco' => true,
        'pais' => true,
        'estado' => true,
        'municipio' => true,
        'moneda' => true,
        'tipo_cliente' => true,
        'facturas' => true,
        'servicios' => true,
        'centro' => true,
        'saldo' => true,
        'saldodias' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContactosTarjetasAcceso Entity.
 */
class ContactosTarjetasAcceso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'contacto_id' => true,
        'tarjetas_acceso_id' => true,
        'contacto' => true,
        'tarjetas_acceso' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClientesServicio Entity.
 */
class ClientesServicio extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'cliente_id' => true,
        'servicio_id' => true,
        'factura_id' => true,
        'usuario_id' => true,
        'tipo_id' => true,
        'fecha' => true,
        'nombre' => true,
        'unidad_id' => true,
        'precio_mxn' => true,
        'precio_usd' => true,
        'descripcion' => true,
        'cantidad' => true,
        'notificacion' => true,
        'cliente' => true,
        'servicio' => true,
        'factura' => true,
        'usuario' => true,
        'tipo' => true,
        'unidad' => true,
        'ultimo' => true
    ];
}

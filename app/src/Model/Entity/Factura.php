<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Factura Entity.
 */
class Factura extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'cliente_id' => true,
        'folio' => true,
        'serie' => true,
        'fecha' => true,
        'subtotal' => true,
        'iva' => true,
        'total' => true,
        'cantidad_letra' => true,
        'festado_id' => true,
        'moneda_id' => true,
        'observaciones' => true,
        'cfdi' => true,
        'response' => true,
        'xml' => true,
        'addRetencion' => true,
        'retencion' => true,
        'cfdicancelado' => true,
        'tipo_de_cambio' => true,
        'fecha_pago' => true,
        'referencia_de_pago' => true,
        'formasdepago_id' => true,
        'cliente' => true,
        'festado' => true,
        'moneda' => true,
        'formasdepago' => true,
        'clientes_servicios' => true,
        'partidas' => true,
        'created' => true,
    ];
}

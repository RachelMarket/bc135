<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Folio Entity.
 */
class Folio extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'serie' => true,
        'numero' => true,
    ];
}

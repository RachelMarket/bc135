<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contacto Entity.
 */
class Contacto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'apellido_paterno' => true,
        'apellido_materno' => true,
        'sexo' => true,
        'correo' => true,
        'telefono_movil' => true,
        'telefono_fijo' => true,
        'fecha_ingreso' => true,
        'domicilio' => true,
        'extension_id' => true,
        'tarjeta_id' => true,
        'facturas' => true,
        'notificaciones' => true,
        'cliente_id' => true,
        'deleted' => true,
        'extension' => true,
        'tarjetas_acceso' => true,
        'cliente' => true,
        'rfc' => true
    ];
}

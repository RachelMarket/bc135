<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RecursosFoto Entity.
 */
class RecursosFoto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'recurso_id' => true,
        'name' => true,
        'deleted' => true,
        'recurso' => true,
    ];
}

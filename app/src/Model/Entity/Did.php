<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Did Entity.
 */
class Did extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'lada' => true,
        'did' => true,
        'deleted' => true,
        'lada_id' => true
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContactosDid Entity.
 */
class ContactosDid extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'contacto_id' => true,
        'did_id' => true,
        'contacto' => true,
        'did' => true,
    ];
}

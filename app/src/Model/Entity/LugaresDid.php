<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LugaresDid Entity.
 */
class LugaresDid extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'lugar' => true,
        'deleted' => true,
        'assigned' => true,
    ];
}

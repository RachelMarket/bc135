<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tipo Entity.
 */
class Tipo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'clientes_servicios' => true,
        'servicios' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cotizacion Entity
 *
 * @property int $id
 * @property string $cliente
 * @property string $contacto
 * @property int $centro_id
 * @property string $correo
 * @property string $telefono
 * @property string $telefono_movil
 * @property int $vigencia
 * @property \Cake\I18n\Time $vence
 * @property string $moneda
 * @property string $status
 * @property int $usuario_id
 * @property float $tipo_cambio
 * @property bool $aplica_promocion
 * @property string $texto_inicial
 * @property string $texto_final
 * @property bool $layout_centro
 * @property \Cake\I18n\Time $creada
 * @property bool $eliminado
 *
 * @property \App\Model\Entity\Oficina $oficina
 * @property \App\Model\Entity\Centro $centro
 * @property \App\Model\Entity\Usuario $usuario
 */
class Cotizacion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cliente' => true,
        'contacto' => true,
        'centro_id' => true,
        'correo' => true,
        'telefono' => true,
        'telefono_movil' => true,
        'vigencia' => true,
        'vence' => true,
        'moneda' => true,
        'status' => true,
        'usuario_id' => true,
        'tipo_cambio' => true,
        'aplica_promocion' => true,
        'texto_inicial' => true,
        'texto_final' => true,
        'layout_centro' => true,
        'creada' => true,
        'eliminado' => true,
        'oficina' => true,
        'centro' => true,
        'usuario' => true
    ];
}

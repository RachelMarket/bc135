<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FacturasMasiva Entity.
 */
class FacturasMasiva extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'factura_id' => true,
        'usuario_id' => true,
        'fecha_envio' => true,
        'factura' => true,
        'usuario' => true,
    ];
}

<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Network\Email\Email;
use Cake\Orm\TableRegistry;

class NotificacionesShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('ClientesServicios');
    }
    
    public function main()
    {

        $userSettingsTable = TableRegistry::get('UserSettings');

        $settings = $userSettingsTable->find('list', [
            'keyField' => 'name', 
            'valueField' => 'value'
        ])->toArray();

        $recipient = $settings['adminEmailAddress'];

        $this->out('Iniciando proceso de envío de notificaciones.');
        $this->out("------------------------------------------");

        //Inicializo los datos de los notificacios
        $notificaciones=$this->ClientesServicios->find('all',['contain' => 'Clientes'])
                                            ->where(function ($exp) {
                                                                        return $exp
                                                                            ->eq('ultimo', 1)
                                                                            ->between('fecha', date("Y-m-d 00:00:00"),date("Y-m-d 23:59:59"));
                                                                    });
        //debug($notificaciones->toArray());
        $mensaje="";
        
        $titulo="Contratos por vencer";
            
            $this->out("Enviando correo.");
            $email = new Email('default');
            
            $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
            ->emailFormat('html')
            ->viewVars(['titulo'=>$titulo, 'notificaciones'=>$notificaciones])
            ->template('fincontrato')
            ->to($recipient)
            ->subject($titulo)
            ->send();
        
        $this->out("------------------------------------------");
        $this->out("Termina proceso de notificaciones.");
        $this->out($this->nl(2));
    }
    
}
?>
<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Network\Email\Email;


class EnviaFacturasShell extends Shell
{


    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Facturas');
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');
        $this->loadModel('Usermgmt.UserSettings');

    }

    
    public function main()
    {
        $this->out('Iniciando proceso de envío de facturas.');
        $this->out("------------------------------------------");
        //debug($recordatorios);

        $options=array('contain'=>['Clientes','Monedas']);
        $facturas = $this->Facturas->find('all', $options)
                                    ->where(['Facturas.festado_id' => 1,
                                                'Facturas.enviada IS NOT NULL',
                                                'Facturas.enviada' => 0])
                                    ->select(['Facturas.id',
                                                'Facturas.cliente_id',
                                                'Facturas.folio',
                                                'Facturas.serie',
                                                'Facturas.fecha',
                                                'Facturas.total',
                                                'Facturas.festado_id',
                                                'Facturas.moneda_id',
                                                'Facturas.recordatorio',
                                                'Monedas.nombre',
                                                'Clientes.nombre',
                                                'Clientes.primario_persona_de_contacto', 
                                                'Clientes.primario_correo_electronico', 
                                                'Clientes.secundario_persona_de_contacto', 
                                                'Clientes.secundario_correo_electronico',
                                                'diferencia' => 'DATEDIFF(NOW(),Facturas.fecha)']
                                            );

        foreach($facturas as $factura){
                    $this->out("------------------------------------------");
                    $this->out("Procesando Factura: ". $factura->id. "(".$factura->diferencia.") ");                                        
                    $this->enviacorreo($factura);
                    $factura->enviada=1;
                    $this->Facturas->save($factura);
            }
        
        
        $this->out("------------------------------------------");
        $this->out("Termina proceso de envío de facturas.");
        $this->out($this->nl(2));
    }

    public function webPrefactura($id=null)
    {

        //$this->out('Iniciando proceso de envío de facturas.');
        //$this->out("------------------------------------------");

        //debug($recordatorios);

        $options=array('contain'=>['Clientes','Monedas']);
        $factura = $this->Facturas->find('all', $options)
                                    ->where(['Facturas.id' => $id])
                                    ->select(['Facturas.id',
                                                'Facturas.cliente_id',
                                                'Facturas.folio',
                                                'Facturas.serie',
                                                'Facturas.fecha',
                                                'Facturas.total',
                                                'Facturas.festado_id',
                                                'Facturas.moneda_id',
                                                'Facturas.recordatorio',
                                                'Monedas.nombre',
                                                'Clientes.nombre',
                                                'Clientes.primario_persona_de_contacto', 
                                                'Clientes.primario_correo_electronico', 
                                                'Clientes.secundario_persona_de_contacto', 
                                                'Clientes.secundario_correo_electronico',
                                                'diferencia' => 'DATEDIFF(NOW(),Facturas.fecha)']
                                            )
                                    ->first();
        

        //foreach($facturas as $factura){
                    //$this->out("------------------------------------------");
                    //$this->out("Procesando Factura: ". $factura->id. "(".$factura->diferencia.") ");                                        
                    $this->enviacorreo($factura);
                    $factura->enviada=1;
                    $this->Facturas->save($factura);
         //   }

        //$this->out("------------------------------------------");
        //$this->out("Termina proceso de envío de facturas.");
        //$this->out($this->nl(2));
        $this->out('|'.$factura->serie.'-'.$factura->folio.'|');
                                      
    }

    public function enviacorreo($factura){
        $this->out("Enviando correo.");
        $email = new Email('default');
        //debug($factura);
        $this->descargarpdf($factura->id);
        
        $mensaje_config=$this->UserSettings->find('all')->where(['name'=>'MensajeEnvioFactura'])->first();
        
        $secundario='noresponder@135-bc.com';
        if($factura->cliente->secundario_correo_electronico !=''){
            $secundario=$factura->cliente->secundario_correo_electronico;
        }       

        $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
        ->domain('135-bc.com')
        ->emailFormat('html')
        ->viewVars(['factura'=>$factura, 'mensaje' => $mensaje_config->value])
        ->template('enviafactura')
        ->to($factura->cliente->primario_correo_electronico)
        ->addTo($secundario)
        ->subject("Factura 135 Business Center")
        ->attachments(WWW_ROOT.'facturas/factura_'.$factura->id.'.pdf')
        ->send();
        
        unlink(WWW_ROOT.'facturas/factura_'.$factura->id.'.pdf');

        $this->out("Correo enviado.");
        return true;
    }

    public function descargarpdf($id=null) {
        //Tomo la factura.
        $factura = $this->Facturas->get($id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);
         
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();
       
       // die(debug($factura->unidades));
       

        //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        //App::import('Vendor', 'html2pdf', array('file' => 'html2pdf'.DS.'html2pdf.class.php'));
        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        //$body = $this->_pdf_content(compact('factura'), 'Facturas/prefactura');
        //$body = $this->cell('Pdf::prefactura', ['factura' => $factura]);
        ob_start();
        include(APP.'Template/Cell/Pdf/prefactura.ctp');
        $body= ob_get_clean();
        //$body="<page>aa</page>";
        $html2pdf->WriteHTML($body);

        $this->out("Generando archivo: ".WWW_ROOT.'facturas/factura_'.$id.'.pdf');
        $html2pdf->Output(WWW_ROOT.'facturas/factura_'.$id.'.pdf','F');
        return true;
    }
}
?>
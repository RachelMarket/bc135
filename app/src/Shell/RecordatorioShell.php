<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Network\Email\Email;

class RecordatorioShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Facturas');
        $this->loadModel('Recordatorios');
    }
    
    public function main()
    {
        $this->out('Iniciando proceso de envío de recordatorios.');
        $this->out("------------------------------------------");

        //Inicializo los datos de los recordatorios
        $recordatorios=$this->Recordatorios->find('all')
                                            ->order(['diferencia' => 'DESC'])
                                            ->where(['activo' => 1]);
        //debug($recordatorios);

        $options=array('contain'=>['Clientes','Monedas']);
        $facturas = $this->Facturas->find('all', $options)
                                    ->where(['Facturas.festado_id' => 1])
                                    ->select(['Facturas.id',
                                                'Facturas.cliente_id',
                                                'Facturas.folio',
                                                'Facturas.serie',
                                                'Facturas.fecha',
                                                'Facturas.total',
                                                'Facturas.festado_id',
                                                'Facturas.moneda_id',
                                                'Facturas.recordatorio',
                                                'Monedas.nombre',
                                                'Clientes.nombre',
                                                'Clientes.primario_persona_de_contacto', 
                                                'Clientes.primario_correo_electronico', 
                                                'Clientes.secundario_persona_de_contacto', 
                                                'Clientes.secundario_correo_electronico',
                                                'diferencia' => 'DATEDIFF(NOW(),Facturas.fecha)']
                                            );

        foreach($facturas as $factura){

            
            foreach($recordatorios as $recordatorio){
                //debug($recordatorio);
                if($factura->diferencia >= $recordatorio->diferencia && $factura->recordatorio < $recordatorio->diferencia ){
                    $this->out("------------------------------------------");
                    $this->out("Procesando Factura: ". $factura->id. "(".$factura->diferencia.") - Recordatorio de ".$recordatorio->diferencia." días. ");
                    
                    $factura->recordatorio=$recordatorio->diferencia;
                    //Guardo el valor del recordatorio para la factura.
                    $this->Facturas->save($factura);
                    $this->enviacorreo($factura,$recordatorio);
                    break;
                }
            }
        }
        
        $this->out("------------------------------------------");
        $this->out("Termina proceso de recordatorios.");
        $this->out($this->nl(2));
    }

    public function enviacorreo($factura,$recordatorio){
        $this->out("Enviando correo.");
        $email = new Email('default');
        //debug($factura);

        $email->from(['noreplay@135-bc.com' => 'Notificaciones BCM'])
        ->emailFormat('html')
        ->viewVars(['factura'=>$factura, 'recordatorio'=>$recordatorio])
        ->template('recordatorio')
        ->to($factura->cliente->primario_correo_electronico)
        ->subject($recordatorio->subject)
        ->send();
        
        return true;
    }
}
?>
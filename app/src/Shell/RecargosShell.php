<?php
namespace App\Shell;

use Cake\Console\Shell;
//use Cake\Network\Email\Email;

class RecargosShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Facturas');
        $this->loadModel('ClientesServicios');
        $this->loadModel('Servicios');
    }
    
    public function main()
    {
        
        $hoy = date('Y-m-d');

        // RECARGOS DE UN DIA
        $facturas = $this->Facturas->find('all', [
            'conditions' => ['festado_id IS NOT' => 3, 'fecha_pago IS NULL', 'fecha <' => $hoy, 'DATEDIFF(NOW(),fecha)' => 1]
            ]);

        $recargo = $this->Servicios->get(90);
        $cant_recargo = 0;
        foreach ($facturas as $factura) {

            $cliente_servicio = $this->ClientesServicios->newEntity();

            $cliente_servicio->cliente_id = $factura['cliente_id'];
            $cliente_servicio->servicio_id = 90; // Tipo Recargo
            //$cliente_servicio->factura_id = '';
            $cliente_servicio->usuario_id = 1; // Administrador PREGUNTAR
            $cliente_servicio->tipo_id = 3; // Adicional
            $cliente_servicio->fecha = date('Y-m-d H:m:s');
            $cliente_servicio->nombre = $recargo['nombre'];
            $cliente_servicio->unidad_id = $recargo['unidad_id'];
            $cliente_servicio->precio_mxn = $recargo['precio_mxn'];
            $cliente_servicio->precio_usd = $recargo['precio_usd'];
            $cliente_servicio->descripcion = $recargo['descripcion'];
            $cliente_servicio->cantidad = 1;

            $this->ClientesServicios->save($cliente_servicio);

            $cant_recargo++;

        }

        $this->out("Recargos de 1 dia: ".$cant_recargo);



        // RECARGOS DE DOS DIAS
        $facturas = $this->Facturas->find('all', [
            'conditions' => ['festado_id IS NOT' => 3, 'fecha_pago IS NULL', 'fecha <' => $hoy, 'DATEDIFF(NOW(),fecha)' => 2]
            ]);

        $recargo = $this->Servicios->get(91);
        $cant_recargo = 0;
        foreach ($facturas as $factura) {

            $cliente_servicio = $this->ClientesServicios->newEntity();

            $cliente_servicio->cliente_id = $factura['cliente_id'];
            $cliente_servicio->servicio_id = 91; // Tipo Recargo
            //$cliente_servicio->factura_id = '';
            $cliente_servicio->usuario_id = 1; // Administrador PREGUNTAR
            $cliente_servicio->tipo_id = 3; // Adicional
            $cliente_servicio->fecha = date('Y-m-d H:m:s');
            $cliente_servicio->nombre = $recargo['nombre'];
            $cliente_servicio->unidad_id = $recargo['unidad_id'];
            $cliente_servicio->precio_mxn = $recargo['precio_mxn'];
            $cliente_servicio->precio_usd = $recargo['precio_usd'];
            $cliente_servicio->descripcion = $recargo['descripcion'];
            $cliente_servicio->cantidad = 1;

            $this->ClientesServicios->save($cliente_servicio);

            $cant_recargo++;

        }

        $this->out("Recargos de 2 dias: ".$cant_recargo);



        //$this->out($facturas->toArray()); exit;
        /*
        $hoy = strtotime($hoy);
        $dia = 24 * 60 * 60;
        foreach ($facturas as $factura) {

            $fecha = strtotime(date($factura->fecha->i18nFormat('YYYY-MM-dd'))); 
            $this->out($factura->fecha->i18nFormat('YYYY-MM-dd'));
            $this->out(floor($hoy/$dia) - floor($fecha/$dia));
        }
        */
    }
    
}
?>
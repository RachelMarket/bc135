<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Network\Email\Email;
use Cake\Utility\Xml;
use SoapClient;

use Cake\Utility\Inflector;


class FacturasMasivaShell extends Shell
{

    public function initialize()
    {

        parent::initialize();
        $this->loadModel('Facturas');
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');
        $this->loadModel('Usermgmt.UserSettings');
        $this->loadModel('Clientes');
        $this->loadModel('FacturasMasivas');

    }

    
    public function main()
    {
        $mensaje_config=$this->UserSettings->find('all')->where(['name'=>'MensajeEnvioFactura'])->first();

        while ($factura_masiva = $this->FacturasMasivas->find('all', ['conditions'=>['fecha_envio is NULL']])->first()) {

//debug($factura_masiva);

                $factura_id = $factura_masiva['factura_id'];
                
                //Tomo la factura, para ver que voy a enviar:
                $factura = $this->Facturas->find('all', ['conditions'=>['Facturas.id'=>$factura_id], 'contain'=>['Clientes', 'Monedas']])->first();

                $replace=array(' ',',','.');

                $nombre=Inflector::slug($factura->cliente->nombre);
                $nombre=str_replace($replace,'',$nombre);
                $nombre=substr($nombre, 0, 10);
                $nombre="_".$factura->serie."".$factura->folio."_".$nombre;

                if($factura->festado_id == 1){ // PREFACTURAS

                    //$enlace_factura = $this->descargarprefacturapdf($factura_id,1);

                    //$nombre_factura=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura);

                    if(filter_var($factura->cliente->primario_correo_electronico, FILTER_VALIDATE_EMAIL)){
                        $email = new Email('default');

                    $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                        ->domain('135-bc.com')
                        ->emailFormat('html')
                        ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
                        ->template('enviafactura')
                        ->to($factura->cliente->primario_correo_electronico)
                        ->subject("Factura 135 Business Center")
                        //->attachments([$nombre_factura => ['file' => $enlace_factura, 'mimetype' => 'application/pdf']])
                        ->attachments(['Prefactura'.$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/prefacturas/prefactura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf']])
                        ->send();
                    $this->out("Enviando Prefactura: ".'Prefactura'.$nombre.'.pdf ('.$factura->id.')');
                }

                   if(filter_var($factura->cliente->secundario_correo_electronico, FILTER_VALIDATE_EMAIL)){
                            $email = new Email('default');
                            $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                        ->domain('135-bc.com')
                        ->emailFormat('html')
                        ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
                        ->template('enviafactura')
                        ->to($factura->cliente->secundario_correo_electronico)
                        ->subject("Factura 135 Business Center")
                        //->attachments([$nombre_factura => ['file' => $enlace_factura, 'mimetype' => 'application/pdf']])
                        ->attachments(['Prefactura'.$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/prefacturas/prefactura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf']])
                        ->send();
                        $this->out("Enviando Prefactura: ".'Prefactura'.$nombre.'.pdf ('.$factura->id.') (Secundario)');

                        }

                }elseif($factura->festado_id == 2){ // TIMBRADAS

                    //$enlace_factura = $this->descargarfacturapdf($factura_id,1);
                    //$nombre_factura=array();
                    //$nombre_factura['factura']=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura['factura']);
                    //$nombre_factura['xml']=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura['xml']);
                    
                   if(filter_var($factura->cliente->primario_correo_electronico, FILTER_VALIDATE_EMAIL)){
                    $email = new Email('default');

                    $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                        ->domain('135-bc.com')
                        ->emailFormat('html')
                        ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
                        ->template('enviafactura')
                        ->to($factura->cliente->primario_correo_electronico)
                        ->subject("Factura 135 Business Center")
                        //->attachments([$nombre_factura['factura'] => ['file' => $enlace_factura['factura'], 'mimetype' => 'application/pdf'],
                        //    $nombre_factura['xml'] => ['file' => $enlace_factura['xml'], 'mimetype' => 'application/xml']])
                        ->attachments(['Factura'.$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/timbradas/factura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf'],
                            'Factura'.$nombre.'.xml' => ['file' => WWW_ROOT.'files/facturas/timbradas/xml_'.$factura_id.'.xml', 'mimetype' => 'application/xml']])
                        ->send();
                    }

                       if(filter_var($factura->cliente->secundario_correo_electronico, FILTER_VALIDATE_EMAIL)){
                        $email = new Email('default');
                            $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                        ->domain('135-bc.com')
                        ->emailFormat('html')
                        ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
                        ->template('enviafactura')
                        ->to($factura->cliente->secundario_correo_electronico)
                        ->subject("Factura 135 Business Center")
                        //->attachments([$nombre_factura['factura'] => ['file' => $enlace_factura['factura'], 'mimetype' => 'application/pdf'],
                        //    $nombre_factura['xml'] => ['file' => $enlace_factura['xml'], 'mimetype' => 'application/xml']])
                        ->attachments(['Factura'.$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/timbradas/factura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf'],
                            'Factura'.$nombre.'.xml' => ['file' => WWW_ROOT.'files/facturas/timbradas/xml_'.$factura_id.'.xml', 'mimetype' => 'application/xml']])
                        ->send();

                        }

                        $this->out("Enviando Factura Timbrada: ".'Factura'.$nombre.'.pdf ('.$factura->id.')');

                }elseif($factura->festado_id == 3){ // CANCELADAS

                    if(filter_var($factura->cliente->primario_correo_electronico, FILTER_VALIDATE_EMAIL)){

                    $email = new Email('default');

                    $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                        ->domain('135-bc.com')
                        ->emailFormat('html')
                        ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
                        ->template('enviafactura')
                        ->to($factura->cliente->primario_correo_electronico)
                        ->subject("Factura 135 Business Center")
                        //->attachments([$nombre_factura => ['file' => $enlace_factura, 'mimetype' => 'application/pdf']])
                        ->attachments(['Factura_Cancelada'.$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/canceladas/factura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf']])
                        ->send();
                    }

                    if(filter_var($factura->cliente->secundario_correo_electronico, FILTER_VALIDATE_EMAIL)){

                    $email = new Email('default');

                    $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                        ->domain('135-bc.com')
                        ->emailFormat('html')
                        ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
                        ->template('enviafactura')
                        ->to($factura->cliente->secundario_correo_electronico)
                        ->subject("Factura 135 Business Center")
                        //->attachments([$nombre_factura => ['file' => $enlace_factura, 'mimetype' => 'application/pdf']])
                        ->attachments(['Factura_Cancelada'.$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/canceladas/factura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf']])
                        ->send();
                    }

                        $this->out("Enviando Factura Cancelada: ".'Factura'.$nombre.'.pdf ('.$factura->id.')');

                }


                $factura_masiva['fecha_envio'] = date('Y-m-d H:i:s');
                $this->FacturasMasivas->save($factura_masiva);

        }

    }

    
}
?>
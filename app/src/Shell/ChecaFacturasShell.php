<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Network\Email\Email;
use Cake\Utility\Xml;
use SoapClient;


class ChecaFacturasShell extends Shell
{

    public $usuario = 'admin@135-bc.com'; //'testing@solucionfactible.com';//
    public $password = 'solrol14'; //'a0123456789';//

    public $ws_url='https://solucionfactible.com/ws/services/CFDI?wsdl';//'https://testing.solucionfactible.com/ws/services/CFDI?wsdl';//


    public function initialize()
    {

        parent::initialize();
        $this->loadModel('Facturas');
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');
        $this->loadModel('Usermgmt.UserSettings');
        $this->loadModel('Clientes');
        $this->loadModel('FacturasMasivas');
        $this->loadModel('Centros');

    }

    
    public function main()
    {
        $mensaje_config=$this->UserSettings->find('all')->where(['name'=>'MensajeEnvioFactura'])->first();

        $facturas =$this->Facturas->find('all',['conditions'=>['serie'=>'B','Facturas.id'=>1928],'contain'=>['Clientes', 'Monedas']]);
        
        $this->out('Iniciando');

        foreach($facturas as $factura)
        {
            $this->out('Ejecutando: '.$factura->id.' ('.$factura->festado_id.')');
            
            $replace=array(' ',',','.');
            $nombre=str_replace($replace,'',$factura->cliente->nombre);
            $nombre=substr($nombre, 0, 10);
            $nombre="_".$factura->serie."".$factura->folio."_".$nombre;
            $factura_id=$factura->id;
            if($factura->festado_id == 1){ // PREFACTURAS

                if(!file_exists(WWW_ROOT.'/files/facturas/prefacturas/prefactura_'.$factura_id.'.pdf')){
                    $enlace_factura = $this->descargarprefacturapdf($factura_id,1);
                    $this->out($enlace_factura);
                    $this->out("No existe: ".$factura->festado_id." - ".$factura_id);
                }                

            }elseif($factura->festado_id == 2){ // TIMBRADAS

                if(!file_exists(WWW_ROOT.'/files/facturas/timbradas/factura_'.$factura_id.'.pdf')){
                    $enlace_factura = $this->descargarfacturapdf($factura_id,1);
                    $this->out($enlace_factura);
                    $this->out("No existe: ".$factura->festado_id." - ".$factura_id);
                }

                // $nombre_factura=array();
                // $nombre_factura['factura']=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura['factura']);
                // $nombre_factura['xml']=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura['xml']);
                

            }elseif($factura->festado_id == 3){ // CANCELADAS

                if(!file_exists(WWW_ROOT.'/files/facturas/canceladas/factura_'.$factura_id.'.pdf')){
                    $enlace_factura = $this->descargarfacturapdf($factura_id,1);
                    $this->out($enlace_factura);
                    //$this->out("No existe: ".$factura->festado_id." - ".$factura_id);
                }

                // $nombre_factura=array();
                // $nombre_factura['factura']=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura['factura']);
                // $nombre_factura['xml']=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura['xml']);

                
            }
        }
    }



    public function descargarprefacturapdf($factura_id,$masivo=0){

        set_time_limit(0);
               
        $factura = $this->Facturas->get($factura_id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);
        
        $uuid = $factura->uuid;

        $cliente = $factura->cliente;
        
        $replace=array(' ',',','.');

        $nombre=str_replace($replace,'',$cliente->nombre);
        $nombre=substr($nombre, 0, 10);

        $nombre="Prefactura_".$factura->serie."".$factura->folio."_".$nombre;
        // $completo=WWW_ROOT.'/files/descargar_facturas/'.$nombre.'.pdf';
        // if(file_exists($completo)){
        //     //$this->out('Existe:');
        //     //return $completo;
        //     return true;
        // }
        // $this->out('NO:');
        //     return $completo;
 
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();


        //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        //$body = $this->_pdf_content(compact('factura'), 'Facturas/prefactura');
        //$body = $this->cell('Pdf::prefactura', ['factura' => $factura]);
        ob_start();
        include(APP.'Template/Cell/Pdf/prefactura.ctp');
        $body= ob_get_clean();
        $html2pdf->WriteHTML($body);
        
        //$pdf=$html2pdf->Output($filename.'.pdf', true);
        //$pdf=$html2pdf->Output('', true);
        $html2pdf->Output(WWW_ROOT.'/files/facturas/prefacturas/prefactura_'.$factura_id.'.pdf', 'F');    

        if($masivo){
            $enlace_factura = WWW_ROOT.'/files/facturas/prefacturas/prefactura_'.$factura_id.'.pdf';
            return $enlace_factura;
            die;
        }
    }

    public function descargarfacturapdf($factura_id,$masivo=0){

        set_time_limit(0);
        $factura = $this->Facturas->get($factura_id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);

        $datos = $this->Centros->get($factura->centro_id);

        //debug($datos); die;
        
        $uuid = $factura->uuid;

        $cliente = $factura->cliente;
        
        $replace=array(' ',',','.');

        $nombre=str_replace($replace,'',$cliente->nombre);
        $nombre=substr($nombre, 0, 10);

        $nombre="Factura_".$factura->serie."".$factura->folio."_".$nombre;

        // $completo=WWW_ROOT.'/files/descargar_facturas/'.$nombre.'.pdf';
        

        // if(file_exists($completo)){
        //     //$this->out('Existe:');
            
        //     //return $enlace_factura;
        //     return true;
        // }
        // $this->out('NO:');
        // return  $completo;
 
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();       

        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        //$body = $this->cell('Pdf::factura', ['datos' => $datos ]);

        ob_start();
        
        //include(APP.'Template/Cell/Pdf/factura.ctp');
        $view = new \Cake\View\View($this->request, new \Cake\Network\Response);

        $body= ob_get_clean();
        $html2pdf->WriteHTML( $view->element('pdf/factura', compact('factura', 'datos')) );
        
        $path=WWW_ROOT.'/files/facturas/timbradas/';
        if($factura->festado_id == 3){
            $path=WWW_ROOT.'/files/facturas/canceladas/';

        }
        $html2pdf->Output($path.'factura_'.$factura_id.'.pdf', 'F');   

        /*$datos_xml = $this->obtenerDatos($uuid, null, null);
        $xml = Xml::build($datos_xml);
        $xml->asXML($path.'xml_'.$factura_id.'.xml'); 
*/
        if($masivo){

            
            $enlace_factura= $path.'/factura_'.$factura_id.'.pdf';

            return $enlace_factura;
            die;
        }
        
    }

    public function obtenerDatos($uuid = null, $folio = null, $serie = null){

        $client = new SoapClient($this->ws_url);

            try {
                    $params = array('usuario' => $this->usuario, 'password' => $this->password, 'uuid'=>$uuid,'folio'=>$folio, 'serie'=>$serie);
                    $response = $client->__soapCall('obtenerDatos', array('parameters' => $params));
                    
                    $responseData = $response->return;
                    return $responseData->comprobantes->xml;
            } catch (SoapFault $fault) {
                    echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
            }
    }


    
}
?>
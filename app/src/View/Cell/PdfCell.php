<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
/**
 * Pdf cell
 */
class PdfCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
    }
    public function prefactura($factura)
    {
        $centroTable = TableRegistry::get('Centros');
        $datos = $centroTable->get($factura->centro_id);
        $this->set('datos', $datos);
        $this->set('factura', $factura);
        $emisor = getAllSettings();
       
        $this->set('emisor',$emisor);
        $this->set('meses', array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio',
                      'Agosto','Septiembre','Octubre','Noviembre','Diciembre'));
    }
    public function edocuenta($movimientos, $cliente, $periodo)
    {
        $emisor = getAllSettings();
        $this->set('emisor',$emisor);

        $this->set(compact('movimientos', 'cliente', 'periodo','emisor'));

        //debug($movimientos);
        //die(debug($cliente));
    }

    public function factura($factura)
    {
        $centroTable = TableRegistry::get('Centros');
        $datos = $centroTable->get($factura->centro_id);
        $this->set('datos', $datos);
        $this->set('factura', $factura);
        
        $emisor = getAllSettings();
        $this->set('emisor',$emisor);

        $this->set('meses', array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio',
                      'Agosto','Septiembre','Octubre','Noviembre','Diciembre'));
    }

    public function reservaciones($reservations, $cliente, $periodo)
    {
        $emisor = getAllSettings();
        $this->set('emisor',$emisor);

        $this->set(compact('reservations', 'cliente', 'periodo','emisor'));

        //debug($movimientos);
        //die(debug($cliente));
    }
}


<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="panel-title">
            Contactos
        </span>
        <span class="panel-title-right">
            <?= $this->Html->link(__('Agregar Contacto'), ['action' => 'add']); ?> 

            <?= $this->Html->link('<i class="fa fa-download"></i> Exportar Excel', '/contactos.xlsx', ['class'=>'btn-success', 'escape'=>false]); ?> 

        </span>
    </div>
    <div class="panel-body">
        <?php echo $this->element('all_contactos');?>
    </div>
</div>
<style type="text/css">
    .fa-remove{
        margin-left: 50px;
        color: #F44336;
    }
</style>
<?= $this->Form->create($contacto,['id' => 'editFormContacto']); ?>
        <div class="row">
            <div class="col-md-4">
                <?= $this->Form->input('nombre', ['class'=>'form-control','div'=>false,'label'=>'Nombre']); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->input('apellido_paterno', ['class'=>'form-control','div'=>false,'label'=>'Apellido Paterno']); ?>
            </div>
            <div class="col-md-4">
                <?= $this->Form->input('apellido_materno', ['class'=>'form-control','div'=>false,'label'=>'Apellido Materno']); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
                <?= $this->Form->input('sexo',[
                    'type'   =>  'radio',
                    'class' => 'form-control radio-inline',
                    'label' => 'sexo',
                    'hidden' =>  false,
                    'options'   =>  $options
                ]);?>
            </div>
            <div class="col-md-4">
            <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Correo Electrónico'); ?></label>
                <div class="input-group relative-absolute">
                <?= $this->Form->input('correo', ['class'=>'form-control','div'=>false,'label'=>false]); ?>
                <span class="input-group-btn">
                        <button class="btn default" type="button">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        </button>
                </span>
                </div>
            </div>
            <div class="col-md-4">
            <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Teléfono Móvil'); ?></label>
                <div class="input-group relative-absolute">
                <?= $this->Form->input('telefono_movil', ['class'=>'form-control phone','div'=>false,'label'=>false]); ?>
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </button>
                </span>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
             <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Teléfono Fijo'); ?></label>
                <div class="input-group relative-absolute">
                <?= $this->Form->input('telefono_fijo', ['class'=>'form-control phone','div'=>false,'label'=>false]); ?>
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </button>
                </span>
                </div>
            </div>
            
            <div class="col-md-4">
                <label style="padding-left: 0px;" class="col-md-12 control-label"><?=  __('Fecha Ingreso'); ?></label>     
              <div class="input-group relative-absolute">
                <?= $this->Form->input('fecha_ingreso', ['type' => 'text','label'=>false,'div'=>false, 'class'=>'form-control datepicker', 'value' => $this->Time->format($contacto->fecha_ingreso, 'YYYY-MM-dd') ]); ?>
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
              </div>   
            </div>

            <div class="col-md-4">
                <?= $this->Form->input('contrasenia_alarma', ['type' => 'text','class'=>'form-control','div'=>false,'label'=>'Contraseña Alarma']); ?>
                <label id="contrasenia-alarma-error" class="error" style="display: none;"></label>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">
             <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('RFC'); ?></label>
                <div class="input-group relative-absolute">
                <?= $this->Form->input('rfc', ['class'=>'form-control','div'=>false,'label'=>false]); ?>
               
                </div>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= $this->Form->input('domicilio', ['type'=>'textarea','class' => 'form-control', 'div'=>false, 'label'=>'Domicilio']); ?>
            </div>
        </div>
        <br>
        <div class="row">

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Extensión'); ?></label>
                <div class="input-group relative-absolute">
                    <?= $this->Form->input('extension', ['type'=>'select','class' => 'form-control chosen', 'div'=>false, 'label'=>false,'empty' => '[Selecciona una opción]', 'options' => $extends]); ?>
                    <span class="input-group-btn">
                          <button style="margin-left: 10px;" id="newExtension" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></button>
                    </span>
                </div>
                <ul style="margin-left: -40px" class="listExtensiones">
                  
                </ul>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Tarjetas de Acceso'); ?></label>
                <div class="input-group relative-absolute">
                    <?= $this->Form->input('tarjeta', ['type'=>'select','class' => 'form-control chosen', 'div'=>false, 'label'=>false,'empty' => '[Selecciona una opción]', 'options' => $tajects]); ?>
                    <span class="input-group-btn">
                          <button style="margin-left: 10px;" id="newTarjet" type="button" class="btn btn-primary btn-sm btnAdd"><i class="fa fa-plus"></i></button>
                    </span>
                </div>
                <ul style="margin-left: -40px" class="listTarjetas">
                  
                </ul>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Chip'); ?></label>
                <div class="input-group relative-absolute">
                    <?= $this->Form->input('chip', ['type'=>'select','class' => 'form-control chosen', 'div'=>false, 'label'=>false,'empty' => '[Selecciona una opción]', 'options' => $chips]); ?>
                    <span class="input-group-btn">
                          <button style="margin-left: 10px;" id="newChip" type="button" class="btn btn-primary btn-sm btnAdd"><i class="fa fa-plus"></i></button>
                    </span>
                </div>
                <ul style="margin-left: -40px" class="listChips">
                  
                </ul>
            </div>
            
        </div>
        <br>
        <div class="row">

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Lada DID'); ?></label>
                <div class="input-group relative-absolute">

                    <?= $this->Form->input('lada', ['type'=>'select','class' => 'form-control chosen', 'div'=>false, 'label'=>false,'empty' => '[Selecciona una opción]', 'options' => $didsLada]); ?>
                    <span class="input-group-btn">
                          
                    </span>
                </div>
            </div>
            <div class="spiner">
                <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Número DID'); ?></label>
                <div class="input-group relative-absolute">
                    <?= $this->Form->input('did', ['type'=>'select','class' => 'form-control chosen', 'div'=>false, 'label'=>false,'empty' => '[Selecciona una opción]']); ?>

                    <span class="input-group-btn">
                          <!--<button style="margin-left: 10px;" type="button" id="newDID" class="btn btn-primary btn-sm btnAdd"><i class="fa fa-plus"></i></button>-->
                    </span>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <label style="padding-left: 0px" class="col-md-12 control-label"><?=  __('Tipo DID'); ?></label>
                <div class="input-group relative-absolute">

                    <?= $this->Form->input('lugares_did', ['type'=>'select','class' => 'form-control chosen', 'div'=>false, 'label'=>false,'options' => $lugaresDID]); ?>

                    <span class="input-group-btn">
                          <button style="margin-left: 10px;" type="button" id="newLugarDID" class="btn btn-primary btn-sm btnAdd"><i class="fa fa-plus"></i></button>
                    </span>
                </div>
            </div>

            
        </div>
        <br>
        <div class="col-md-12">
            <ul style="margin-left: -50px" class="listDID">
              
            </ul>
        </div>
        <br>
        <div class="col-md-12">
            <div style="top: 10px;" class="ibox-tools pull-right">
                <button id="btnCancel" type="button" class="btn btn-danger"><i class="fa fa-undo"></i> Cancelar</button>
            <?= $this->Form->button(__('<i class="fa fa-save"></i> Guardar'),['type' => 'button', 'id' => 'addContact', 'class'=>'btn btn-primary']) ?>
            </div> 
        </div>

<?= $this->Form->end() ?>


<script type="text/javascript">
    
    $(document).ready(function(){

        $('.chosen').chosen();
        $('.spiner').hide();

        loadExtensiones();
        loadTarjetas();
        loadChips();
        loadDID();

        $('#addContact').on('click', function(){
            $("#editFormContacto").submit();
        });

        $('.datepicker').datepicker({
            format: 'yyyy/mm/dd',
        });

        $('#btnCancel').on('click',function(){
            $("#editContacto").fadeOut(2000, function(){
                $("#addButtonContact").fadeIn(3000, function(){
                    $("#contentContactos").fadeIn(3000);
                });
            });
        });

        var lada = 0;

        $('#lada').on('change', function(){

            lada = $(this).attr('selected', 'selected').val();
            $('#did').find('option:gt(0)').remove();
            $('.spiner').show();
            $.ajax({
                    type: 'POST',
                    url: '/contactos/getLada',
                    data: {id:lada},
                    dataType: 'json',
                    success: function(data){
                        if(data.resultado == 200){ // Todo bien
                           jQuery.each( data.array, function( i, val ) {
                              $('#did').append('<option value="'+i+'">'+val+'</option>');
                            });
                           $('#did').trigger("chosen:updated");
                           $('.spiner').hide();
                        }
                    }
            });
        });

        var did = 0;

        $('#did').on('change', function(){

            did = $(this).attr('selected', 'selected').val();
            
        });

        var lugar = $("#lugares-did").attr('selected', 'selected').val();
        
        $('#lugares-did').on('change', function(){
            lugar = $(this).attr('selected', 'selected').val();
        });


        
        

        $("#newLugarDID").on('click', function(){

            var id = <?php echo $contacto->id ?>;

            if($('#did').attr('selected', 'selected').val() != '' && $('#lada').attr('selected', 'selected').val() != ''){
                $.ajax({
                    type: 'POST',
                    url: '/contactos/saveDID',
                    data: {did_id:did,id:id,lada:lada,lugar:lugar},
                    dataType: 'json',
                    success: function(data){
                        
                        if(data.resultado ==  200){
                            $('.listDID').append('<li id="'+data.contacto_id+'" style="list-style:none; font-size: 14px">'+data.numberLada+' - '+data.numberDID+' - '+data.nameLugar+'<i id="'+data.contacto_id+'" class="fa fa-remove didNumber" value="'+data.numberLada+' - '+data.numberDID+'" style="margin-left:20px; color:#f44336; cursor:pointer;"></i></li>');
                           
                        }else if(data.resultado ==  500){
                            alert('¡ERROR no se pudo guardar el registro!');
                        }
                    }
                });
            }else{
                alert('Selecciona lada seguido de número DID');
            }
        });

        $("#newChip").on('click', function(){

            var id = <?php echo $contacto->id ?>;
            var chip = $('#chip').attr('selected', 'selected').val();

            if(chip != ''){
                $.ajax({
                        type: 'POST',
                        url: '/contactos/addChips',
                        data: {chip_id:chip,contacto_id:id},
                        dataType: 'json',
                        success: function(data){
                            
                            if(data.resultado ==  200){
                                $('.listChips').append('<li id="'+data.chip_id+'" style="list-style:none; font-size: 14px">'+data.numberChip+'<i id="'+data.chip_id+'" class="fa fa-remove listNumberChip" value="'+data.numberChip+'" style="margin-left:20px; color:#f44336; cursor:pointer;"></i></li>');

                            }else if(data.resultado == 500){
                                alert('¡ERROR el chip ya esta asignado!');
                            }
                        }
                });
            }else{
                alert('Selecciona una opción');
            }
        });

        $('#newTarjet').on('click', function(){
            var id = <?php echo $contacto->id ?>;
            var tarjeta = $('#tarjeta').attr('selected', 'selected').val();

            if(tarjeta != ''){
                $.ajax({
                        type: 'POST',
                        url: '/contactos/addTarjetas',
                        data: {tarjeta_id:tarjeta,contacto_id:id},
                        dataType: 'json',
                        success: function(data){
                            
                            if(data.resultado ==  200){
                                $('.listTarjetas').append('<li id="'+data.tarjeta_id+'" style="list-style:none; font-size: 14px">'+data.numTarjeta+'<i id="'+data.tarjeta_id+'" class="fa fa-remove listNumberTarjetas" value="'+data.numTarjeta+'" style="margin-left:20px; color:#f44336; cursor:pointer;"></i></li>');

                            }else if(data.resultado == 500){
                                alert('¡ERROR el número de tarjeta ya esta asignado!');
                            }
                        }
                });
            }else{
                alert('Selecciona una opción');
            }
        });
        
        $('#newExtension').on('click', function(){
            var id = <?php echo $contacto->id ?>;
            var extension = $('#extension').attr('selected', 'selected').val();
            
            if(extension != ''){
                $.ajax({
                        type: 'POST',
                        url: '/contactos/addExtension',
                        data: {extension_id:extension,contacto_id:id},
                        dataType: 'json',
                        success: function(data){
                            
                            if(data.resultado ==  200){
                                $('.listExtensiones').append('<li id="'+data.extension_id+'" style="list-style:none; font-size: 14px">'+data.numExtension+'<i  id="'+data.extension_id+'" class="fa fa-remove listNumberExtension" value="'+data.numExtension+'" style="margin-left:20px; color:#f44336; cursor:pointer;"></i></li>');
                                
                            }else if(data.resultado ==  500){
                                alert('¡ERROR la extensión ya esta asignado!');
                            }
                        }
                });
            }else{
                alert('Selecciona una opción');
            }
        });


        $(document).on('click','.didNumber',function(){
            var id = $(this).attr('id');
            var text = $(this).attr('value');
            var x = confirm("¿Esta seguro de eliminar el DID #"+text+"?");
            
            if(x){
                $('#'+id).remove();
                $.ajax({
                        type: 'POST',
                        url: '/contactos/removeDID',
                        data: {id:id},
                        dataType: 'json',
                        success: function(data){
                           
                    }
                });
            }    
        });

        $(document).on('click','.listNumberChip',function(){
            
            var id = $(this).attr('id');
            var text = $(this).attr('value');
            var x = confirm("¿Esta seguro de eliminar el Chip #"+text+"?");
            
            if(x){
                $('#'+id).remove();
                $.ajax({
                        type: 'POST',
                        url: '/contactos/removeChips',
                        data: {id:id},
                        dataType: 'json',
                        success: function(data){
                           
                    }
                });
            }
            
        });

        $(document).on('click','.listNumberTarjetas',function(){
            var id = $(this).attr('id');
            var text = $(this).attr('value');
            var x = confirm("¿Esta seguro de eliminar la Tarjeta de Acceso #"+text+"?");
            if(x){
                $('#'+id).remove();
                $.ajax({
                        type: 'POST',
                        url: '/contactos/removeTarjetas',
                        data: {id:id},
                        dataType: 'json',
                        success: function(data){
                           
                    }
                }); 
            }
        });

        

        $(document).on('click','.listNumberExtension',function(){
            var id = $(this).attr('id');
            var text = $(this).attr('value');
            var x = confirm("¿Esta seguro de eliminar la Extensión #"+text+"?");
            
            if(x){
                $('#'+id).remove();
                $.ajax({
                        type: 'POST',
                        url: '/contactos/removeExtension',
                        data: {id:id},
                        dataType: 'json',
                        success: function(data){
                           
                    }
                }); 
            }
            
        });

        function loadExtensiones(){

            var id = <?php echo $contacto->id ?>;

            $.ajax({
                    type: 'POST',
                    url: '/contactos/loadExtensiones',
                    data: {id:id},
                    dataType: 'json',
                    success: function(data){

                    if(data.resultado == 200){ // Todo bien
                       jQuery.each( data.array, function( i, val ) {

                          $('.listExtensiones').append('<li id="'+val.id+'" style="list-style:none; font-size: 14px">'+val.extension['extension']+'<i id="'+val.id+'" class="fa fa-remove listNumberExtension" value="'+val.extension['extension']+'" style="margin-left:20px; color:#f44336; cursor:pointer;"></i></li>');
                          
                        });
                    }

                }
            });
        }

        function loadTarjetas(){

            var id = <?php echo $contacto->id ?>;

            $.ajax({
                    type: 'POST',
                    url: '/contactos/loadTarjetas',
                    data: {id:id},
                    dataType: 'json',
                    success: function(data){

                    if(data.resultado == 200){ // Todo bien
                       jQuery.each( data.array, function( i, val ) {

                          $('.listTarjetas').append('<li id="'+val.id+'" style="list-style:none; font-size: 14px">'+val.tarjetas_acceso['num_tarjeta']+'<i id="'+val.id+'" class="fa fa-remove listNumberTarjetas" value="'+val.tarjetas_acceso['num_tarjeta']+'" style="margin-left:20px; color:#f44336; cursor:pointer;"></i></li>');
                          
                        });
                    }
                }
            });
        }

        function loadChips(){

            var id = <?php echo $contacto->id ?>;

            $.ajax({
                    type: 'POST',
                    url: '/contactos/loadChips',
                    data: {id:id},
                    dataType: 'json',
                    success: function(data){

                    if(data.resultado == 200){ // Todo bien
                       jQuery.each( data.array, function( i, val ) {

                          $('.listChips').append('<li id="'+val.id+'" style="list-style:none; font-size: 14px">'+val.chip['numero']+'<i id="'+val.id+'" class="fa fa-remove listNumberChip" value="'+val.chip['numero']+'" style="margin-left:20px; color:#f44336; cursor:pointer;"></i></li>');
                          
                        });
                    }
                }
            });
        }

        function loadDID(){

            var id = <?php echo $contacto->id ?>;

            $.ajax({
                    type: 'POST',
                    url: '/contactos/loadDID',
                    data: {id:id},
                    dataType: 'json',
                    success: function(data){

                    if(data.resultado == 200){ // Todo bien
                       jQuery.each( data.array, function( i, val ) {

                          $('.listDID').append('<li id="'+val.id+'" style="list-style:none; font-size: 14px">'+val.did.lada['lada']+' - '+val.did['did']+' - '+val.lugares_did['lugar']+'<i id="'+val.id+'" class="fa fa-remove didNumber" value="'+val.did.lada['lada']+' - '+val.did['did']+' - '+val.lugares_did['lugar']+'" style="margin-left:20px; color:#f44336; cursor:pointer;"></i></li>');
                        });
                    }
                    
                }
            });
        }

    });

$('#contrasenia-alarma').change(function() {
    validatePassword( $(this) );
});

$('#contrasenia-alarma').keyup(function() {
    validatePassword( $(this) );
});


function validatePassword( input ){
    if( input.val() != '' ){
        $.get('/Contactos/verifica_alarma/'+ $('#contrasenia-alarma').val() +'/<?php echo $contacto->id; ?>',
        function( data ) {
            mensaje=JSON.parse(data);
            mensaje.error=mensaje.error*1;
            if(mensaje.error == 1){
                $('#contrasenia-alarma').addClass('error');
                $('#contrasenia-alarma-error').html('Esta contraseña ya ha sido utilizada, proporcione otra');
                $('#contrasenia-alarma-error').show();
            }else{
                $('#contrasenia-alarma-error').hide();
                $('#contrasenia-alarma-error').html('');
                $('#contrasenia-alarma').removeClass('error');
            }
        });
    }
}
</script>

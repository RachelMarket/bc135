<?php 
   
    $nombre_reporte="contactos_".date('Y-m-d_H_i_s');
    $this->Excel->set_header('contactos');

    $row=3;
    $col=0;

            
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('id'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('nombre'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('apellido_paterno'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('apellido_materno'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('sexo'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('correo'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('telefono_movil'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('telefono_fijo'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('fecha_ingreso'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('domicilio'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('extension_id'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('tarjeta_id'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('facturas'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('notificaciones'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('cliente_id'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('created'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('modified'));
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,__('deleted'));

    foreach ($contactos as $contacto){
        $row++;
        $col=0;
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row, $this->Number->format($contacto->id));
                $valor=h($contacto->nombre);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->apellido_paterno);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->apellido_materno);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->sexo);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->correo);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->telefono_movil);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->telefono_fijo);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->fecha_ingreso);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->domicilio);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row, $this->Number->format($contacto->extension_id));
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row, $this->Number->format($contacto->tarjeta_id));
                $valor=h($contacto->facturas);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->notificaciones);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                

                $valor = $contacto->has('cliente') ? $contacto->cliente->nombre : '';

                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);
                $valor=h($contacto->created);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->modified);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

                $valor=h($contacto->deleted);
                $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$valor);

        }

        $this->Excel->do_print($nombre_reporte.'.xlsx');
                
            exit;

?>

<?= $this->Form->create($contacto,['id' => 'addFormContacto']); ?>

  <div class="row ca-forms">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('nombre', ['class'=>'form-control','div'=>false,'label'=>'Nombre']); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('apellido_paterno', ['class'=>'form-control','div'=>false,'label'=>'Apellido Paterno']); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('apellido_materno', ['class'=>'form-control','div'=>false,'label'=>'Apellido Materno']); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('sexo',[
            'type'   =>  'radio',
            'class' => 'form-control radio-inline',
            'label' => 'sexo',
            'hidden' =>  false,
            'options'   =>  $options
            ]);?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="required"><?=  __('Correo Electrónico'); ?></label>
              <?= $this->Form->input('correo', ['class'=>'form-control','div'=>false,'label'=>false]); ?>
            </div>
          </div>
          <div class="col-md-6">
            <label class=""><?=  __('Teléfono Móvil'); ?></label>
            <?= $this->Form->input('telefono_movil', ['class'=>'form-control phone','div'=>false,'label'=>false]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label class=""><?=  __('Teléfono Fijo'); ?></label>
            <?= $this->Form->input('telefono_fijo', ['class'=>'form-control phone','div'=>false,'label'=>false]); ?>
          </div>
          <div class="col-md-6">
            <label class=""><?=  __('Fecha Ingreso'); ?></label>
            <?= $this->Form->input('fecha_ingreso', ['type' => 'text','label'=>false,'div'=>false, 'class'=>'form-control datepicker', 'value' => (!empty( $contacto->fecha_ingreso ))? $contacto->fecha_ingreso->format('d/m/Y'):""]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <br>
            <?= $this->Form->input('contrasenia_alarma', ['type' => 'text', 'class'=>'form-control','div'=>false,'label'=>'Contraseña Alarma']); ?>
            <label id="contrasenia-alarma-error" class="error" style="display: none;"></label>
          </div>
          <div class="col-md-6">
            <br>
            <label class=""><?=  __('RFC'); ?></label>
            <?= $this->Form->input('rfc', ['class'=>'form-control','div'=>false,'label'=>false]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <label class=""></label>
            <?= $this->Form->input('domicilio', ['type'=>'textarea','class' => 'form-control', 'div'=>false, 'label'=>'Domicilio']); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="last-btns">
              <button id="btnCancelAdd" type="button" class="btn btn-cancel">Cancelar</button>
              <?= $this->Form->button(__('Guardar'),['type' => 'submit', 'id' => 'addContacto', 'class'=>'btn btn-save']) ?>
          </div>
        </div>

<?= $this->Form->end() ?>
<script type="text/javascript">
  $(document).ready(function(){
      $('#btnCancelAdd').on('click',function(){
          $("#addContacto").fadeOut(2000, function(){
              $("#addButtonContact").fadeIn(3000, function(){
                  $("#contentContactos").fadeIn(3000);
              });
          });
      });

      $('#contrasenia-alarma').change(function() {
          validatePassword( $(this) );
      });

      $('#contrasenia-alarma').keyup(function() {
          validatePassword( $(this) );
      });
  
  });

function validatePassword( input ){
  if( input.val() != '' ){
      $.get('/Contactos/verifica_alarma/'+ $('#contrasenia-alarma').val() +'/<?php echo $contacto->id; ?>',
      function( data ) {
          mensaje=JSON.parse(data);
          mensaje.error=mensaje.error*1;
          if(mensaje.error == 1){
              $('#contrasenia-alarma').addClass('error');
              $('#contrasenia-alarma-error').html('Esta contraseña ya ha sido utilizada, proporcione otra');
              $('#contrasenia-alarma-error').show();
          }else{
              $('#contrasenia-alarma-error').hide();
              $('#contrasenia-alarma-error').html('');
              $('#contrasenia-alarma').removeClass('error');
          }
      });
  }
}
</script>
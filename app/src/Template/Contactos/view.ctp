<style type="text/css"> h5{ font-weight: bold; font-size: 12px; } </style>

<div class="row">
    <div class="col-md-4">
        <h5><?= __('Nombre') ?></h5>
        <?= h($contacto->nombre) ?>
    </div>
    <div class="col-md-4">
        <h5><?= __('Apellido Paterno') ?></h5>
        <?= h($contacto->apellido_paterno) ?>
    </div>
    <div class="col-md-4">
        <h5><?= __('Apellido Materno') ?></h5>
        <?= h($contacto->apellido_materno) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4">
        <h5><?= __('Sexo') ?></h5>
        <p><?= (!empty($contacto->sexo))? h($options[$contacto->sexo]):"" ?></p>
    </div>
    
    <div class="col-md-4">
        <h5><?= __('Correo Electrónico') ?></h5>
        <p><?= h($contacto->correo) ?></p>
    </div>
    
    <div class="col-md-4">
        <h5><?= __('Teléfono Móvil') ?></h5>
        <p><?= h($contacto->telefono_movil) ?></p>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-4">
        <h5><?= __('Teléfono Fijo') ?></h5>
        <p><?= h($contacto->telefono_fijo) ?></p>
    </div>
    
    <div class="col-md-4">
        <h5><?= __('Fecha Ingreso') ?></h5>
        <p><?= (!empty($contacto->fecha_ingreso))? $contacto->fecha_ingreso->format('d/m/Y g:i A'):"" ?></p> 

    </div>

    <div class="col-md-4">
        <h5><?= __('Contraseña Alarma') ?></h5>
        <p><?= h($contacto->contrasenia_alarma) ?></p>
    </div>

</div>
<br>
<div class="row">
    <div class="col-md-4">
        <h5><?= __('Rfc') ?></h5>
        <p><?= h($contacto->rfc) ?></p>
    </div>
    <div class="col-md-8">
        <h5><?= __('Domicilio') ?></h5>
        <p><?= h($contacto->domicilio) ?></p>
    </div>
</div>
<br>
<div class="row">

    <div class="col-md-4">
        <h5><?= __('Extensión') ?></h5>
        <?php foreach ($extensiones as $row): ?>
            <?php echo $row->extension->extension ?>
        <?php endforeach; ?>
    </div>

    <div class="col-md-4">
        <h5><?= __('Tarjetas de Acceso') ?></h5>
        <?php foreach ($tarjetas as $row): ?>
            <?php echo $row->tarjetas_acceso->num_tarjeta ?>
        <?php endforeach; ?>
    </div>

    <div class="col-md-4">
        <h5><?= __('Chip') ?></h5>
        <?php foreach ($chips as $row): ?>
            <?php echo $row->chip->numero ?>
        <?php endforeach; ?>
    </div>
    
</div>
<br>
<div class="row">

    <div class="col-md-4">
        <h5><?= __('Lada DID') ?></h5>
    </div>

    <div class="col-md-4">
        <h5><?= __('Número DID') ?></h5>
    </div>

    <div class="col-md-4">
        <h5><?= __('Tipo DID') ?></h5>
    </div>
</div>
<br>
<div class="row">
    <?php foreach ($dids as $row): ?>
        <div class="col-md-4"><?= h($row->did->lada->lada) ?></div>
        <div class="col-md-4"><?= h($row->did->did) ?></div>
        <div class="col-md-4"><?= h($row->lugares_did->lugar) ?></div>
    <?php endforeach; ?>
</div>
    


<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Agregar Mensaje'), ['action' => 'add']); ?></li>
        <li><?= $this->Html->link(__('Listar Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Usuario'), ['controller' => ' Usuarios', 'action' => 'add']); ?></li>
                </ul>
<?php $this->end(); ?>



<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-12"  style="margin-top:5px;">
                            <span style="font-size:16px; font-weight:600;">Mensajes</span>
                        </div>
                    </div>
                    



                    <div class="wrapper wrapper-content">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content mailbox-content">
                                        <div class="file-manager">
                                            <a class="btn btn-block btn-primary compose-mail" id="btn_escribir_mensaje" href="#">Escribir un Mensaje</a>
                                            <div class="space-25"></div>
                                            <h5>Carpetas</h5>
                                            <ul class="folder-list m-b-md" style="padding: 0">
                                                <li><a href="#" id="btn_recibidos"> <i class="fa fa-inbox "></i> Recibidos <span class="label label-warning pull-right"><?php echo count($mensajes_usuario->toArray()); ?></span> </a></li>
                                
                                                <li><a href="#" id="btn_enviados"> <i class="fa fa-envelope-o"></i> Enviados <span class="label label-primary pull-right"><?php echo count($mensajes_enviados->toArray()); ?></a></li>

                                                <li><a href="#" id="btn_eliminados"> <i class="fa fa-trash-o"></i> Eliminados <span class="label label-danger pull-right"><?php echo count($mensajes_eliminados->toArray()); ?></a></li>
                                
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


            <!-- MENSAJES RECIBIDOS -->

                            <div class="col-lg-9 animated fadeInRight" id="modal_recibidos">
                                <div class="mail-box-header">
                                    <h2>
                                        Recibidos (<?php echo count($mensajes_usuario->toArray()); ?>)
                                    </h2>
                                    
                                </div>


                                <div class="mail-box" style="padding:10px;">

                                    <table data-order='[[ 3, "DESC" ]]' data-page-length='10' class="table table-mail dataTables-example">
                                        <thead>
                                            <tr>
                                
                                            <th>De</th>
                                            <th>Mensaje</th>
                                            <th>Fecha</th>
                                            <th></th>
                                            <th></th>
                                           
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach ($mensajes_usuario as $mensajes): ?>

                                        <tr class="<?php if($mensajes->visto==0){ echo 'unread';}else{ echo 'read';} ?>">
                                            <td style="min-width:100px;"><?php echo $usuarios[$mensajes->envia]; ?></td>
                                            <td align="justify"><?php echo $mensajes->mensaje;?></td>
                                            <td style="min-width:150px;"><?php echo date("M j, Y, g:i a", strtotime($mensajes->fecha));?></td>
                                            <td class=""><?php

                                                if($mensajes->visto==0){   
                                                    echo $this->Form->postLink('<i class="fa fa-eye-slash"></i>', ['action' => 'vistoMensaje', $mensajes->id], ['title' => __('Marcar mensaje como visto'), "escape" => false]);
                                    
                                                }else{
                                                    echo '<i class="fa fa-eye"></i>';
                                                }
                                            ?>
                                            </td>
                                            <td class=""><?php
                                                echo $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'eliminarMensaje', $mensajes->id], ['confirm' => __('Seguro que quiere borrar el mensaje?'), 'title' => __('Eliminar mensaje'), "escape" => false]);
                                                ?>
                                            </td>
                                        </tr>

                                        <?php endforeach; ?>
                
                                        </tbody>
                                    </table>

                                </div>

                                
                            </div>


            <!-- MENSAJES ENVIADOS -->

                            <div class="col-lg-9 animated fadeInRight" id="modal_enviados" style="display:none">
                                <div class="mail-box-header">
                                    <h2>
                                        Enviados (<?php echo count($mensajes_enviados->toArray()); ?>)
                                    </h2>
                                   
                                </div>


                                <div class="mail-box" style="padding:10px;">

                                    <table data-order='[[ 3, "DESC" ]]' data-page-length='10' class="table table-mail dataTables-example">
                                        <thead>
                                            <tr>
                                
                                            <th>Para</th>
                                            <th>Mensaje</th>
                                            <th>Fecha</th>
                                            <th></th>
                                           
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach ($mensajes_enviados as $mensajes): ?>

                                        <tr class="<?php if($mensajes->visto==0){ echo 'unread';}else{ echo 'read';} ?>">
                                            <td style="min-width:100px;"><?php echo $usuarios[$mensajes->usuario_id]; ?></td>
                                            <td align="justify"><?php echo $mensajes->mensaje;?></td>
                                            <td style="min-width:150px;"><?php echo date("M j, Y, g:i a", strtotime($mensajes->fecha));?></td>
                                            <td class=""><?php

                                                if($mensajes->visto==0){   
                                                    echo '<i class="fa fa-eye-slash"></i>';
                                    
                                                }else{
                                                    echo '<i class="fa fa-eye"></i>';
                                                }
                                            ?>
                                            </td>
                                            <!--
                                            <td class=""><?php
                                                echo $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'eliminarMensaje', $mensajes->id], ['confirm' => __('Seguro que quiere borrar el mensaje?'), 'title' => __('Eliminar mensaje'), "escape" => false]);
                                                ?>
                                            </td>
                                            -->
                                        </tr>

                                        <?php endforeach; ?>
                
                                        </tbody>
                                    </table>


                                </div>
                            </div>                 

            <!-- MENSAJES ELIMINADOS -->

                            <div class="col-lg-9 animated fadeInRight" id="modal_eliminados" style="display:none">
                                <div class="mail-box-header">
                                    <h2>
                                        Eliminados (<?php echo count($mensajes_eliminados->toArray()); ?>)
                                    </h2>
                                    <!--
                                    <div class="mail-tools tooltip-demo m-t-md">
                                        <a href="/" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Actualizar"><i class="fa fa-refresh"></i> Actualizar</a>
                                    </div>
                                    -->
                                </div>


                                <div class="mail-box" style="padding:10px;">

                                    <table data-order='[[ 3, "DESC" ]]' data-page-length='10' class="table table-mail dataTables-example">
                                        <thead>
                                            <tr>
                                
                                            <th>De</th>
                                            <th>Mensaje</th>
                                            <th>Fecha</th>
                                            <th></th>
                                           
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach ($mensajes_eliminados as $mensajes): ?>

                                        <tr class="<?php if($mensajes->visto==0){ echo 'unread';}else{ echo 'read';} ?>">
                                            <td style="min-width:100px;"><?php echo $usuarios[$mensajes->envia]; ?></td>
                                            <td align="justify"><?php echo $mensajes->mensaje;?></td>
                                            <td style="min-width:150px;"><?php echo date("M j, Y, g:i a", strtotime($mensajes->fecha));?></td>
                                            <td></td>
                                        </tr>

                                        <?php endforeach; ?>
                
                                        </tbody>
                                    </table>


                                </div>
                            </div> 





                            

                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>


<div class="modal fade" id="vista_mensaje">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Escribir Mensaje</h4>
      </div>
      <div class="modal-body">

        
        <div class="row">
            <div class="col-md-12">             
                <div class="row">
                                        
                    <div class="col-md-12">


                        <?= $this->Form->create($mensaje); ?>

                        <div class="row" style="margin-top:10px;">
                           
                                <div class="col-md-2">
                                    Usuario:
                                </div>
                                <div class="col-md-10">
                                    <?php   echo $this->Form->input('usuario_id' ,array('multiple' => 'multiple','type' => 'select', 'label' => false,'options' => $usuarios)); ?>
                                </div>
                           
                        </div>
                        <div class="row" style="margin-top:10px;">
                           
                                <div class="col-md-2">
                                    Mensaje:
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('mensaje', ['rows' => '5', 'label' => false]);?>
                                </div>
                        
                        </div>

                    </div>  

                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>          
            <?= $this->Form->button(__('Enviar Mensaje'), ['class' => 'btn btn-primary pull-right']) ?>
            <?= $this->Form->end() ?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">

        $(document).ready(function() {
            $('.dataTables-example').dataTable({
                responsive: true,
                "dom": 'T<"clear">lfrtip',
                "lengthChange": false,
                 "footerCallback": function( tfoot, data, start, end, display ) {
                    $(tfoot).html( "" );
                }
            });
        });


        $('#btn_escribir_mensaje').click(function(e){
            e.preventDefault();
            $('#vista_mensaje').modal('show')
        })

        $('#btn_recibidos').click(function(e){
            e.preventDefault();

            document.getElementById("modal_recibidos").style.display = 'block';
            document.getElementById("modal_enviados").style.display = 'none';
            document.getElementById("modal_eliminados").style.display = 'none';
        })

        $('#btn_enviados').click(function(e){
            e.preventDefault();

            document.getElementById("modal_recibidos").style.display = 'none';
            document.getElementById("modal_enviados").style.display = 'block';
            document.getElementById("modal_eliminados").style.display = 'none';
        })

        $('#btn_eliminados').click(function(e){
            e.preventDefault();

            document.getElementById("modal_recibidos").style.display = 'none';
            document.getElementById("modal_enviados").style.display = 'none';
            document.getElementById("modal_eliminados").style.display = 'block';
        })
</script>
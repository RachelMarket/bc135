<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Editar Factura'), ['action' => 'edit', $factura->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Eliminar Factura'), ['action' => 'delete', $factura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $factura->id)]) ?> </li>
    <li><?= $this->Html->link(__('Listar Facturas'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('Nuevo Factura'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Festados'), ['controller' => 'Festados', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Festado'), ['controller' => 'Festados', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Monedas'), ['controller' => 'Monedas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Moneda'), ['controller' => 'Monedas', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Formasdepagos'), ['controller' => 'Formasdepagos', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Formasdepago'), ['controller' => 'Formasdepagos', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['controller' => 'ClientesServicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => 'ClientesServicios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Partidas'), ['controller' => 'Partidas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Partida'), ['controller' => 'Partidas', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>

<h2><?= h($factura->id) ?></h2>
<div class="row">
        <div class="col-lg-5">
                                    <h6><?= __('Cliente') ?></h6>
                    <p><?= $factura->has('cliente') ? $this->Html->link($factura->cliente->nombre, ['controller' => 'Clientes', 'action' => 'view', $factura->cliente->id]) : '' ?></p>
                                                    <h6><?= __('Serie') ?></h6>
                    <p><?= h($factura->serie) ?></p>
                                                    <h6><?= __('Cantidad Letra') ?></h6>
                    <p><?= h($factura->cantidad_letra) ?></p>
                                                    <h6><?= __('Festado') ?></h6>
                    <p><?= $factura->has('festado') ? $this->Html->link($factura->festado->nombre, ['controller' => 'Festados', 'action' => 'view', $factura->festado->id]) : '' ?></p>
                                                    <h6><?= __('Moneda') ?></h6>
                    <p><?= $factura->has('moneda') ? $this->Html->link($factura->moneda->nombre, ['controller' => 'Monedas', 'action' => 'view', $factura->moneda->id]) : '' ?></p>
                                                    <h6><?= __('Referencia De Pago') ?></h6>
                    <p><?= h($factura->referencia_de_pago) ?></p>
                                                    <h6><?= __('Formasdepago') ?></h6>
                    <p><?= $factura->has('formasdepago') ? $this->Html->link($factura->formasdepago->nombre, ['controller' => 'Formasdepagos', 'action' => 'view', $factura->formasdepago->id]) : '' ?></p>
                                </div>
            <div class="col-lg-2">
                    <h6><?= __('Id') ?></h6>
                <p><?= $this->Number->format($factura->id) ?></p>
                    <h6><?= __('Folio') ?></h6>
                <p><?= $this->Number->format($factura->folio) ?></p>
                    <h6><?= __('Subtotal') ?></h6>
                <p><?= $this->Number->format($factura->subtotal) ?></p>
                    <h6><?= __('Iva') ?></h6>
                <p><?= $this->Number->format($factura->iva) ?></p>
                    <h6><?= __('Total') ?></h6>
                <p><?= $this->Number->format($factura->total) ?></p>
                    <h6><?= __('Retencion') ?></h6>
                <p><?= $this->Number->format($factura->retencion) ?></p>
                    <h6><?= __('Cfdicancelado') ?></h6>
                <p><?= $this->Number->format($factura->cfdicancelado) ?></p>
                    <h6><?= __('Tipo De Cambio') ?></h6>
                <p><?= $this->Number->format($factura->tipo_de_cambio) ?></p>
                </div>
            <div class="col-lg-2">
                    <h6><?= __('Fecha') ?></h6>
                <p><?= h($factura->fecha) ?></p>
                    <h6><?= __('Fecha Pago') ?></h6>
                <p><?= h($factura->fecha_pago) ?></p>
                </div>
            <div class="col-lg-2">
                    <h6><?= __('AddRetencion') ?></h6>
                <p><?= $factura->addRetencion ? __('Yes') : __('No'); ?></p>
                </div>
    </div>
    <div class="row texts">
            <div class="col-lg-9">
                <h6><?= __('Observaciones') ?></h6>
                <?= $this->Text->autoParagraph(h($factura->observaciones)); ?>
            </div>
        </div>
    <div class="row texts">
            <div class="col-lg-9">
                <h6><?= __('Cfdi') ?></h6>
                <?= $this->Text->autoParagraph(h($factura->cfdi)); ?>
            </div>
        </div>
    <div class="row texts">
            <div class="col-lg-9">
                <h6><?= __('Response') ?></h6>
                <?= $this->Text->autoParagraph(h($factura->response)); ?>
            </div>
        </div>
    <div class="row texts">
            <div class="col-lg-9">
                <h6><?= __('Xml') ?></h6>
                <?= $this->Text->autoParagraph(h($factura->xml)); ?>
            </div>
        </div>
    <ul id="myTab" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#ClientesServicios" id="ClientesServicios-tab" role="tab" data-toggle="tab" aria-controls="ClientesServicios" aria-expanded="true">ClientesServicios</a>
      </li>
          <li role="presentation" class="">
        <a href="#Partidas" id="Partidas-tab" role="tab" data-toggle="tab" aria-controls="Partidas" aria-expanded="true">Partidas</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="ClientesServicios" aria-labelledBy="ClientesServicios-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($factura->clientes_servicios)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Cliente Id') ?></th>
                                            <th><?= __('Servicio Id') ?></th>
                                            <th><?= __('Factura Id') ?></th>
                                            <th><?= __('Usuario Id') ?></th>
                                            <th><?= __('Tipo Id') ?></th>
                                            <th><?= __('Fecha') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Unidad Id') ?></th>
                                            <th><?= __('Precio Mxn') ?></th>
                                            <th><?= __('Precio Usd') ?></th>
                                            <th><?= __('Descripcion') ?></th>
                                            <th><?= __('Cantidad') ?></th>
                                            <th><?= __('Notificacion') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($factura->clientes_servicios as $clientesServicios): ?>
                    <tr>
                                            <td><?= h($clientesServicios->id) ?></td>
                                            <td><?= h($clientesServicios->cliente_id) ?></td>
                                            <td><?= h($clientesServicios->servicio_id) ?></td>
                                            <td><?= h($clientesServicios->factura_id) ?></td>
                                            <td><?= h($clientesServicios->usuario_id) ?></td>
                                            <td><?= h($clientesServicios->tipo_id) ?></td>
                                            <td><?= h($clientesServicios->fecha) ?></td>
                                            <td><?= h($clientesServicios->nombre) ?></td>
                                            <td><?= h($clientesServicios->unidad_id) ?></td>
                                            <td><?= h($clientesServicios->precio_mxn) ?></td>
                                            <td><?= h($clientesServicios->precio_usd) ?></td>
                                            <td><?= h($clientesServicios->descripcion) ?></td>
                                            <td><?= h($clientesServicios->cantidad) ?></td>
                                            <td><?= h($clientesServicios->notificacion) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'ClientesServicios', 'action' => 'view', $clientesServicios->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'ClientesServicios', 'action' => 'edit', $clientesServicios->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'ClientesServicios', 'action' => 'delete', $clientesServicios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientesServicios->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen ClientesServicios asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in " id="Partidas" aria-labelledBy="Partidas-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($factura->partidas)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Factura Id') ?></th>
                                            <th><?= __('Cantidad') ?></th>
                                            <th><?= __('Unidad Id') ?></th>
                                            <th><?= __('Concepto') ?></th>
                                            <th><?= __('Precio') ?></th>
                                            <th><?= __('Nota') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($factura->partidas as $partidas): ?>
                    <tr>
                                            <td><?= h($partidas->id) ?></td>
                                            <td><?= h($partidas->factura_id) ?></td>
                                            <td><?= h($partidas->cantidad) ?></td>
                                            <td><?= h($partidas->unidad_id) ?></td>
                                            <td><?= h($partidas->concepto) ?></td>
                                            <td><?= h($partidas->precio) ?></td>
                                            <td><?= h($partidas->nota) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Partidas', 'action' => 'view', $partidas->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Partidas', 'action' => 'edit', $partidas->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Partidas', 'action' => 'delete', $partidas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $partidas->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Partidas asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
</div>


<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  $this->start('tb_sidebar');
  ?>

<?php $this->end(); ?>


<?php

echo $this->Html->script('jquery.blockUI.js?q='.QRDN);

?>
<div class="ibox float-e-margins">
  <div class="ibox-title">
    <h2>Creación de Estado de Cuenta (Prefactura)</h2>
  </div>
  <div class="ibox-content">
    <div class="row ca-forms">
      <div class="col-md-6">
        <label>Movimientos del mes</label>
        <?php 
            //$options = array(01 => 'Enero',02 => 'Febrero', 03 => 'Marzo', 04 => 'Abril', 05 => 'Mayo', 06 => 'Junio', 07 => 'Julio');
            
            //echo $this->Form->input('meses' ,array('type' => 'select', 'label' => false,'options' => $options));
            
            
            echo $this->Form->input('meses',array('class ' => 'form-control', 'type'=>'text', 'label' => false));
            
            ?>
      </div>
      <div class="col-md-6">
        <label>Tipo de Cambio</label>
        <?php
            echo $this->Form->input('tipo_cambio', array('label' => false));
            ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="last-btns">
          <a href="#" class="btn btn-save" id="btn-listaPrevia">Crear Lista Previa</a>
        </div>
      </div>
    </div>

    <div class="row ca-forms" style="margin-top:20px;">
      <div class="col-md-12">
        <?php 
          //debug($comentario);
              echo $this->Form->input('observaciones', ['rows' => '3', 'label' => 'Comentarios', 'value' => $comentario]);
          ?>
      </div>
    </div>
    <div class="row" style="margin-top:20px;">
      <div class="col-md-1">
        <?php
          echo $this->Form->input('mes', array('label' => false, 'type' => 'hidden'));
          ?>
        <input type="checkbox" value="0" class='checkall'>
      </div>
      <div class="col-md-4">
        <b>Cliente</b>
      </div>
      <div class="col-md-2">
        <b>RFC</b>
      </div>
      <div class="col-md-1">
        <b>Saldo</b>
      </div>
      <div class="col-md-1">
        <b>Moneda</b>
      </div>
    </div>
    <legend></legend>
    <div class="row">
      <div id="listado_partidas">
        <table style="width:100%; max-width:100%; margin-bottom:0px;" id="tabla_partidas">
          <tbody></tbody>
        </table>
      </div>
    </div>
    <div class="row" style="text-align:right;">
      <div class="col-md-12">
        <a href="" class="btn btn-primary btn-crearPrefactura">Crear Prefactura</a>
      </div>
    </div>
  </div>
</div>
<? echo $this->Html->script('underscore');?>
<script type="text/javascript">
  $('#btn-listaPrevia').click(function(){
  
      var tabla = $('#tabla_partidas');
  
      if($("#meses").val() != $("#mes").val())
      {
          mes = $("#meses").val();
          $("#mes").val(mes);
  
           $('#tabla_partidas tr').remove();
           
      }else{
          mes = '';
      }
      
      
      $.ajax({
  
              type: 'POST',
              url: '<?php echo $this->Url->build(["controller" => "facturas","action" => "listPrefacturaMensual"]); ?>',
              data: {mes:mes},
              dataType: 'json',
              success: function(data){
                  $.each(data, function(k, lista){
  
                          var id_cliente = lista['id_cliente'];
                          var nombre = lista['nombre'];
                          var rfc = lista['rfc'];
                          var saldo = lista['saldo'];
                          var moneda = lista['moneda'];
                          var saldoMXN = lista['saldoMXN'];
                          var saldoUSD = lista['saldoUSD'];
  
                          //alert(nombre);
  
                         
                          var timestamp = new Date().getTime();
                      
                          //Fila de partidas
                          if(saldo > 0)
                          {
                              var tr = '<tr  class="'+id_cliente+'_'+timestamp+' " ><td class="col-md-1" align="center" style="padding:5px;"><?php echo $this->Form->checkbox("'+id_cliente+'", array("hiddenField" => false));?>\
                              <input type="hidden" name="saldousd_'+id_cliente+'_'+timestamp+'" id="saldousd-'+id_cliente+'_'+timestamp+'" value="'+saldoUSD+'">\
                              <input type="hidden" name="saldomxn_'+id_cliente+'_'+timestamp+'" id="saldomxn-'+id_cliente+'_'+timestamp+'" value="'+saldoMXN+'">\
                              </td><td class="col-md-4">'+nombre+'</td><td class="col-md-2">'+rfc+'</td><td class="col-md-1">$<label id="saldo-'+id_cliente+'_'+timestamp+'">'+saldo+'</label></td><td class="col-md-1" id="'+id_cliente+'_'+timestamp+'">'+moneda+'</td>\
                              <td class="col-md-1">\
                              <div class="actions">\
                                      <div class="dropdown">\
                                         <button class="btn btn-default dropdown-toggle" type="button" id="menu" data-toggle="dropdown"><i class="fa fa-bars"></i></button>\
                                          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">\
                                            <li role="presentation"><a href="#" class="accionMonedaMXN" data-rel="'+id_cliente+'_'+timestamp+'">MXN</a></li>\
                                            <li role="presentation"><a href="#" class="accionMonedaUSD" data-rel="'+id_cliente+'_'+timestamp+'">USD</a></li>\
                                          </ul>\
                                      </div>\
                                  </div>\
                              </td>\
                              <td class="col-md-2"></td>\
                              <td style="display:none" data-rel="'+id_cliente+'_'+timestamp+'" class="td_hiddens">'+id_cliente+'</td></tr>';
  
                              /*
                              var tr = '<tr><td class="col-md-1" align="center" style="padding:5px;"><?php echo $this->Form->checkbox("published", array("hiddenField" => false));?></td><td class="col-md-4">'+nombre+'</td><td class="col-md-2">'+rfc+'</td><td class="col-md-2">00-00-0000</td><td class="col-md-1">$'+saldo+'</td><td class="col-md-1" id="'+timestamp+'">'+moneda+'</td><td class="col-md-1"><a href="#" class="accionMoneda" data-rel="'+timestamp+'">Acciones</a></td><td style="display:none" data-input-rel="'+timestamp+'" class="td_hiddens"></td></tr>';*/
  
  
                              tabla.find('tbody').append(tr);
                          }
                  }); 
                      
                      /*
                      */
                  
                   
              }
          });
  
  });
  
  $(document).on({click: function(e){
              e.preventDefault();
              var _this = $(this)
              var rel = _this.data('rel');
  
              //alert(rel);
  
              $('#saldo-'+rel).text($('#saldomxn-'+rel).val());
  
              $('#'+rel).text('MXN');  
  
              /*
              if ($('#'+rel).text() == 'USD' && ($('#tipo-cambio').val() != '' && $('#tipo-cambio').val() > 0))
              {
                  var saldo = $('#saldo-'+rel).text();
                  var tipo_cambio = $('#tipo-cambio').val();
  
              
                  $('#saldo-'+rel).text(saldo * tipo_cambio);
  
                  $('#'+rel).text('MXN');  
  
              }else if($('#tipo-cambio').val() == '' || $('#tipo-cambio').val() < 1){
                  alert('El tipo de cambio no es valido.');
              }  
              */   
          }
  }, '.accionMonedaMXN');
  
  $(document).on({click: function(e){
              e.preventDefault();
              var _this = $(this)
              var rel = _this.data('rel');
  
              $('#saldo-'+rel).text($('#saldousd-'+rel).val());
  
              $('#'+rel).text('USD');  
  
              /*
              if ($('#'+rel).text() == 'MXN' && ($('#tipo-cambio').val() != '' && $('#tipo-cambio').val() > 0))
              {
                  var saldo = $('#saldo-'+rel).text();
                  var tipo_cambio = $('#tipo-cambio').val();
  
              
                  $('#saldo-'+rel).text(saldo / tipo_cambio);
  
                  $('#'+rel).text('USD');  
  
              }else if($('#tipo-cambio').val() == '' || $('#tipo-cambio').val() < 1){
                  alert('El tipo de cambio no es valido.');
              }   
              */
  
          }
  }, '.accionMonedaUSD');
  
  var facturas = null;


  var procesar_factura = function(tipo_cambio,mes,comentario){


    var valor = facturas.pop()
    
    clientes_factura = [

        valor
    ];

    var classRow = valor[3];

    
      


    return $.ajax({
  
                  type: 'POST',
                  url: '<?php echo $this->Url->build(["controller" => "facturas","action" => "genera_facturaMasiva"]); ?>',
                  data: {
                      clientes_factura:clientes_factura, 
                      tipo_cambio:tipo_cambio,
                      mes:mes,
                      comentario:comentario
                  },
                  dataType: 'json',
                  success: function(data){


                      $('.'+valor[3]).remove();


                    if( facturas.length > 0 ){
                      
                      procesar_factura(  tipo_cambio,mes,comentario );
                    
                    }else{

                      $.unblockUI()
                      alert('Prefacturas creadas');

                      $('.btn-crearPrefactura').removeAttr('disabled');


                    }
                     //  if (data.confirma == 100)
                     //  {
                     //      
                     //      window.location.href = '/facturas/';
                     //  }
                     // $('.btn-crearPrefactura').removeAttr('disabled');
                     // $('#btn-listaPrevia').trigger('click');
                  },
                  error: function(data){

                      $.unblockUI()
                      alert('Ocurrió un error vuelva a intentarlo ');
                      // $('.btn-crearPrefactura').removeAttr('disabled');
          
                      // $('#btn-listaPrevia').trigger('click');
                  }
                  
                 
  
               });



  }


  var procesar_facturas  = function(tipo_cambio,mes,comentario ){

      
      $.blockUI({ message: '<h1>Procesando...</h1>' }); 
      for (var i = 0; i < facturas.length; i++) {
        var factura = facturas[i];

        $('.'+factura[3]).find('td:eq(6)').html("<span class='submit-indicator'></span>");

      }
      

      procesar_factura( tipo_cambio,mes,comentario  )



  };

  $('.btn-crearPrefactura').click(function(e){
          e.preventDefault();
          
          $('.btn-crearPrefactura').attr('disabled','disabled');
  
          var cant_partidas = $('#tabla_partidas tbody tr').length;
              
          var clientes_factura = [];
          var i=0;
  
          if (cant_partidas >= 1) {
              
              $('#tabla_partidas tbody tr').each(function(k,v){
  
                  var tr = $(this);
                  //var tr_insert;
  
                  var id_cliente = tr.find('td:eq(7)').html(); // td oculto
  
                  var rel = tr.find('td:eq(7)').data('rel');
                  
                  var check = document.getElementsByName(id_cliente)[0].checked;
                  
                  if(check == true)
                  {   
                      clientes_factura[i] = [];
                      clientes_factura[i][0] = id_cliente; // cliente
                      clientes_factura[i][1] = $('#saldo-'+rel).text(); // saldo
                      clientes_factura[i][2] = tr.find('td:eq(4)').html(); // moneda
                      clientes_factura[i][3] =  rel;
  
                      i=i+1;
                  }
  
              });
              
          } 
  
          
          if(i > 0)
          {
              var tipo_cambio = $('#tipo-cambio').val();
              var mes = $('#mes').val();
              var comentario = $('#observaciones').val();
  
              if(tipo_cambio > 0 && tipo_cambio != '')
              {


                facturas = clientes_factura;
                procesar_facturas(  tipo_cambio,mes ,comentario )


               // $.ajax({
  
               //    type: 'POST',
               //    url: '<?php echo $this->Url->build(["controller" => "facturas","action" => "genera_facturaMasiva"]); ?>',
               //    data: {
               //        clientes_factura:clientes_factura, 
               //        tipo_cambio:tipo_cambio,
               //        mes:mes,
               //        comentario:comentario
               //    },
               //    dataType: 'json',
               //    success: function(data){
  
               //        if (data.confirma == 100)
               //        {
               //            alert('La seleccion se Prefacturo con Exito');
               //            window.location.href = '/facturas/';
               //        }
               //       $('.btn-crearPrefactura').removeAttr('disabled');
               //       $('#btn-listaPrevia').trigger('click');
               //    },
               //    error: function(data){
               //        alert('Ocurrió un error vuelva a intentarlo ');
               //        $('.btn-crearPrefactura').removeAttr('disabled');
  
               //        $('#btn-listaPrevia').trigger('click');
               //    }
                  
                 
  
               // });


              }else{
                  alert('El tipo de cambio no es valido.');
                  $('.btn-crearPrefactura').removeAttr('disabled');
              }
          }else{
              alert('Selecciona uno o mas clientes.');
  
              $('.btn-crearPrefactura').removeAttr('disabled');
          }
  
  
      })
  
  $('#meses').datepicker({ 
      autoclose: true,
      format: "mm-yyyy", 
      viewMode: "months", 
      minViewMode: "months"
  
      });
  
  $('.checkall').click(function(e) {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
  
</script>
<!--
  <div class="page-padding">
      
  </div>
  -->
<?php 
   
   $nombre_reporte="Reporte_Facturas_".date("Ymd_His").'.xlsx';    
   $this->Excel->set_header('Facturas');
              
            //Aqui hay que llenar los encabezados:
            $row=3;
            $col=0;
            
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'Factura'); 
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'Creación'); 
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'Cliente'); 
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'RFC'); 
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'Fecha de vencimiento'); 
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'Días vencidos'); 
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'Monto'); 
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'Fecha de pago'); 
            $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,'Timbre'); 
    

$hoy = new DateTime();

foreach ($facturas as $factura){
    $row++;
    $col=0;

    $folio='';
    $created='';
    $dias='';
    $fecha_factura=date_format($factura->fecha,"Y-m-d");
    $subtotal="$".number_format($factura->subtotal, 2) . " ".$factura->moneda->nombre;

    if($factura->folio != ''){ $folio=$factura->serie."-".$factura->folio; }
    if($factura->created != ''){ $created= date_format($factura->created,"Y-m-d");}

    if ($factura->festado_id != 3){
        if($factura->fecha_pago == ''){
            $fecha = new DateTime($factura->fecha->i18nFormat('YYYY-MM-dd HH:mm:ss'));

             if($fecha <= $hoy){
                $interval = $fecha->diff($hoy);
                $dias= $interval->format('%a días');
            }

        }else{
             $fecha = new DateTime($factura->fecha->i18nFormat('YYYY-MM-dd HH:mm:ss'));
             $pago = new DateTime($factura->fecha_pago->i18nFormat('YYYY-MM-dd HH:mm:ss'));

            
            if($fecha <= $pago){
                $interval = $fecha->diff($pago);
                $dias= $interval->format('%a días');
            }
            
        }

    }
    $fecha_pago='';
    if($factura->fecha_pago != ''){ $fecha_pago= date_format($factura->fecha_pago,"Y-m-d"); }

    $status='';
    if($factura->festado_id == 2){ 
        $status='Timbrada';
    }elseif($factura->festado_id == 3){ 
        $status='Cancelada';
    }

    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$folio);
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$created);
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$factura->cliente->nombre);
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$factura->cliente->rfc);
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$fecha_factura);
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$dias); 
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$subtotal); 
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$fecha_pago); 
    $this->Excel->objWorksheet->setCellValueByColumnAndRow($col++, $row,$status); 

}



$this->Excel->do_print($nombre_reporte);
                
            exit;

                                
                                
                                 
                                    

                                
<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
        <li><?=
            $this->Form->postLink(
            __('Eliminar'),
            ['action' => 'delete', $factura->id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $factura->id)]
            )
            ?></li>
        <li><?= $this->Html->link(__('Listar Facturas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Festados'), ['controller' => 'Festados', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Festado'), ['controller' => 'Festados', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Monedas'), ['controller' => 'Monedas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Moneda'), ['controller' => 'Monedas', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Formasdepagos'), ['controller' => 'Formasdepagos', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Formasdepago'), ['controller' => 'Formasdepagos', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['controller' => 'ClientesServicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => 'ClientesServicios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Partidas'), ['controller' => 'Partidas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Partida'), ['controller' => 'Partidas', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>
<div class="page-padding">
    <?= $this->Form->create($factura); ?>
    <fieldset>
        <legend><?= __('Edit {0}', ['Factura']) ?></legend>
        
        <?php
                echo $this->Form->input('cliente_id', ['options' => $clientes]);
                        echo $this->Form->input('folio');
                        echo $this->Form->input('serie');
                        echo $this->Form->input('fecha');
                        echo $this->Form->input('subtotal');
                        echo $this->Form->input('iva');
                        echo $this->Form->input('total');
                        echo $this->Form->input('cantidad_letra');
                        echo $this->Form->input('festado_id', ['options' => $festados]);
                        echo $this->Form->input('moneda_id', ['options' => $monedas]);
                        echo $this->Form->input('observaciones');
                        echo $this->Form->input('cfdi');
                        echo $this->Form->input('response');
                        echo $this->Form->input('xml');
                        echo $this->Form->input('addRetencion');
                        echo $this->Form->input('retencion');
                        echo $this->Form->input('cfdicancelado');
                        echo $this->Form->input('tipo_de_cambio');
                        echo $this->Form->input('fecha_pago');
                        echo $this->Form->input('referencia_de_pago');
                        echo $this->Form->input('formasdepago_id', ['options' => $formasdepagos]);
                        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
 <?= $this->Form->create(null ,[ 'id' => 'frm_guardar_categoria_gasto', 'class'=>'form-horizontal', 'url' => [  'action' =>'cambiarformapagocuentabanco' ,  $factura->id]    ] ); ?>

<div class="um-form-row row  form-group">


		<div class="col-sm-6  ">
			<?= $this->Form->input('formasdepago_id',[ 'class'=>'form-control', 'label'=> 'Forma de pago' , 'options' => $formasdepagos ,'default' => $factura->formasdepago_id ]); ?>
		</div>

		<div class="col-sm-6  ">
			<?= $this->Form->input('cuentabanco',[ 'class'=>'form-control', 'label'=> 'Cuenta Banco','value'=> $factura->cuentabanco   ]); ?>
		</div>

</div>

<div class="um-form-row row  form-group">
	<div class="col-sm-10 ">
<input type="submit" class="btn btn-primary" value="<?php echo __('Cambiar') ?>" />
	</div>

</div>
<?= $this->Form->end(); ?>
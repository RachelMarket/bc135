<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  $this->start('tb_sidebar');
  ?>
<ul class="nav nav-sidebar">
  <li><?= $this->Html->link(__('Listar Facturas'), ['action' => 'index']) ?></li>
  <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Festados'), ['controller' => 'Festados', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Festado'), ['controller' => 'Festados', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Monedas'), ['controller' => 'Monedas', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Moneda'), ['controller' => 'Monedas', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Formasdepagos'), ['controller' => 'Formasdepagos', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Formasdepago'), ['controller' => 'Formasdepagos', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['controller' => 'ClientesServicios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => 'ClientesServicios', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Partidas'), ['controller' => 'Partidas', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Partida'), ['controller' => 'Partidas', 'action' => 'add']) ?> </li>
</ul>
<?php $this->end(); ?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h2>Registrar Factura Manual</h2>
          <a href="/clientes/" class="btn btn-back btn-cancel">Regresar</a> <!-- FALTA FLECHA DE EN EL BOTON Y OPCIONES -->
        </div>
        <div class="ibox-content">
          <div class="row">
            <div class="col-md-12">
              <h6>Información de Contacto</h6>
            </div>
            <div class="col-md-3">  
              Contacto: <br> <b><?php echo $clientes->nombre; ?></b>
            </div>
            <div class="col-md-offset-1 col-md-3">
              Correo Electrónico: <br> <b><?php echo $clientes->primario_correo_electronico; ?></b>
            </div>
            <div class="col-md-offset-1 col-md-3">
              Teléfono: <br> <b><?php echo $clientes->primario_telefono_oficina; ?></b>
            </div>
          </div>
          <legend><br></legend>
          <div class="row">
            <div class="col-md-12">
              <h6>Datos Fiscales</h6>
            </div>
            <div class="col-md-3">  
              Razon Social: <br> <b><?php echo $clientes->razon_social; ?></b>
            </div>
            <div class="col-md-offset-1 col-md-3">
              RFC: <br> <b> <?php echo $clientes->rfc; ?> </b>
            </div>
            <div class="col-md-offset-1 col-md-3">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-3">  
              Calle y Numero: <br> <b><?php echo $clientes->calle.' '.$clientes->numero; ?></b>
            </div>
            <div class="col-md-offset-1 col-md-3">
              Colonia: <br> <b><?php echo $clientes->colonia; ?></b>
            </div>
            <div class="col-md-offset-1 col-md-3">
              Municipio: <br> <b><?php echo $clientes->municipio->municipio; ?></b>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-3">  
              Estado: <br> <b><?php echo $clientes->estado->estado; ?></b>
            </div>
            <div class="col-md-offset-1 col-md-3">
              Código Postal: <br> <b> <?php echo $clientes->codigo_postal; ?> </b>
            </div>
            <div class="col-md-offset-1 col-md-3">
            </div>
          </div>
          <br><br>
          <div class="row">
            <div class="col-md-12">
              <legend>Detalles</legend>
            </div>
          </div>
          <div class="row">
            <div class="col-md-1" style="padding: 6px 0px 0px 20px;">
              Moneda
            </div>
            <div class="col-md-2">
              <?php echo $this->Form->input('moneda_id', ['options' => $monedas, 'label' => false, 'default'=>$clientes->moneda_id]); 
                ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12" style="background-color:#f2f4f8; border: 1px solid #e4e9f0; padding:10px;">
                <div class="row">
                  <div class="col-md-1">
                    <?php echo $this->Form->input('cantidad', ['type' => 'number', 'style' => 'min-width:60px;']);?>
                  </div>
                  <div class="col-md-1">
                    <?php echo $this->Form->input('unidad_id', ['options' => $unidades, 'label' => 'Unidad', 'style' => 'min-width:60px;']); ?>
                  </div>
                  <div class="col-md-5">
                    <?php echo $this->Form->input('concepto', ['label' => 'Descripción', 'style' => 'min-width:305px;']);?>
                  </div>
                  <div class="col-md-2">
                    <?php echo $this->Form->input('precio', ['label' => 'Precio']);?>
                  </div>
                  <div class="col-md-2">
                    <h3 style="padding: 25px 0px 0px 0px;" id="total_partida">$0.00</h3>
                  </div>
                  <div class="col-md-1">
                    <a href="#" class="add-fct btn_partidas"><span>+</span></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br><br><br>
          <div class="row">
            <div class="col-md-12">
              <div id="listado_partidas">
                <table style="width:100%; max-width:100%; margin-bottom:0px" id="tabla_partidas">
                  <thead>
                    <tr>
                      <td class="col-md-1">
                        <b>Código</b>
                      </td>
                      <td class="col-md-1">
                        <b>Cantidad</b>
                      </td>
                      <td class="col-md-1">
                        <b>Unidad</b>
                      </td>
                      <td class="col-md-5">
                        <b>Descripción</b>
                      </td>
                      <td class="col-md-2">
                        <b>Precio Unitario</b>
                      </td>
                      <td class="col-md-1">
                        <b>Subtotal</b>
                      </td>
                      <td class="col-md-1">
                      </td>
                    </tr>
                    <tr>
                      <td colspan=7>
                        <legend style="border: 1px solid #ccc;"></legend>
                      </td>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
          </div>
          <br>
          <legend style="border: 1px solid #ccc;"></legend>
          <div class="row">
            <b>
              <div class="col-md-7">
                Importes
              </div>
              <div class="col-md-2" style="text-align:right;">
                Subtotal:
              </div>
              <div class="col-md-2" style="text-align:right;" id="subtotal-general">$0.00</div>
            </b>
          </div>
          <br>
          <div class="row">
            <div class="col-md-7" id="total-general-letra">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
            <b>
              <div class="col-md-2" style="text-align:right;">
                IVA (16%):
              </div>
              <div class="col-md-2" style="text-align:right;" id="iva-general">
                $0.00
              </div>
            </b>
          </div>
          <legend><br></legend>
          <div class="row">
            <div class="col-md-7" style="min-width:450px;">
              <div class="col-md-2" style="min-width:110px; padding: 6px 0px 0px 0px;">
                Tipo de Cambio
              </div>
              <div class="col-md-2" style="padding: 0px 20px 0px 0px;">
                <?php echo $this->Form->input('tipo_de_cambio', ['label' => false, 'style' => 'min-width:0px;']);?>
              </div>
              <div class="col-md-3" style="min-width:130px;  padding: 6px 0px 0px 0px;">
                pesos por dolar
              </div>
            </div>
            <b>
              <div class="col-md-5" style="text-align:right; font-size:18px;">
                <div class="col-md-5" style="text-align:right;">
                  Total:
                </div>
                <div class="col-md-7" style="text-align:right;" id="total-general">
                  $0.00
                </div>
              </div>
            </b>
          </div>
          <legend> </legend>
          <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12" style="min-width:450px;">
              <div class="col-md-3 col-lg-3 col-sm-3" style="min-width:110px; padding: 6px 0px 0px 0px;">
                Fecha de vencimiento
              </div>
              <div class="col-md-2 col-lg-2 col-sm-2" style="padding: 0px 20px 0px 0px;">
                <?php echo $this->Form->input('fecha_vencimiento', array('class ' => 'form-control', 'type'=>'text', 'label' => false, 'value' => date('d-m-Y')));?>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <?php 
                $comentario = 'Por favor revisa tu factura, cuentas con 5 días para cualquier cambio o aclaración.  Te pedimos por favor realizar tu pago en la fecha que te corresponda para evitar gestiones de cobranza.';
                echo $this->Form->input('observaciones', ['rows' => '5', 'label' => 'Comentarios', 'value' => $comentario]);
                ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="last-btns">
                <a href="#" class="btn btn-save btn-cancel" id="btn-guarda">Guardar Prefactura</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<? echo $this->Html->script('underscore');?>
<script type="text/javascript">
  $('#fecha-vencimiento').datepicker({ 
      autoclose: true,
      format: "dd-mm-yyyy", 
      //viewMode: "months", 
      //minViewMode: "months"
      });
  
  $('#cantidad').keyup(function(e){
          calculadora();
  });
  
  $('#precio').keyup(function(e){
          calculadora();
  });
  
  function calculadora () {
  
      var cantidad = 0;
      var precio = 0;
  
      if($('#cantidad').val() > 0)
      {
          cantidad = $('#cantidad').val();
      }
  
      if($('#precio').val() > 0)
      {
          precio = $('#precio').val();
      }
  
      $('#total_partida').text('$'+number_format(cantidad * precio, 2, '.', ','));
  
      //alert();
  }
  
  $('.btn_partidas').click(function(){
  
      //alert("entra");
     
      //alert($("#unidad-id option:selected").html());
      if($("#cantidad").val() > 0 && $("#concepto").val() != '' && $("#precio").val() > 0)
      {
          var timestamp = new Date().getTime();
          var tabla = $('#tabla_partidas');
  
              //Fila de partidas
              var tr = _.template('<tr>\
                  <td class="col-md-1"></td>\
                  <td class="col-md-1"><%= cantidad %></td>\
                  <td class="col-md-1"><%= unidad %></td>\
                  <td class="col-md-5"><%= concepto %></td>\
                  <td class="col-md-2">$<%= precio %></td>\
                  <td class="col-md-1">$<%= subtotal %></td>\
                  <td class="col-md-1" style="align:center;"><a href="#" class="delete_element" data-rel="<%= timestamp %>">Eliminar</a></td>\
                  <td style="display:none" data-input-rel="<%= timestamp %>" class="td_hiddens"><%= unidad_id %></td>\
                  </tr>');
  
              //Se asigna valores
              cantidad = $("#cantidad").val();
              unidad = $("#unidad-id option:selected").html();
              unidad_id = $("#unidad-id").val();
              concepto = $("#concepto").val();
              precio = number_format($("#precio").val(), 2, '.', ',');
              subtotal = number_format(parseFloat(cantidad) * parseFloat(replaceAll(precio,',','')), 2, '.', ','); 
            
  
              tabla.find('tbody').append(tr({cantidad:cantidad, unidad:unidad, unidad_id:unidad_id, concepto:concepto, precio:precio, subtotal:subtotal, timestamp:timestamp}));
  
  
              //Actualizo totales
              var subtotal_general = $('#subtotal-general').text();
              
              subtotal_general = parseFloat(replaceAll(subtotal_general.substring(1),',','')) + parseFloat(cantidad * replaceAll(precio,',',''));
              $('#subtotal-general').text('$'+number_format(subtotal_general, 2, '.', ','));
              $('#iva-general').text('$'+number_format(subtotal_general * .16, 2, '.', ','));
              $('#total-general').text('$'+number_format(subtotal_general + (subtotal_general * .16), 2, '.', ','));
  
              //Limpio input
              $("#cantidad").val('');
              $("#concepto").val('');
              $("#precio").val('');
              $('#total_partida').text('$0.00');
      }else{
          alert('Los datos no son validos');
      }
  
  });
  
  // Borra tr de la tabla y sus inputs hidden que corresponden a esa partida
  $(document).on({click: function(e){
              e.preventDefault();
              var _this = $(this)
              var rel = _this.data('rel');
  
              _this.closest('tr').fadeOut(function(){
  
                  var subtotal = $(this).find('td:eq(5)').html(); //subtotal de partida para recalcular totales
                  var subtotal_general = $('#subtotal-general').text();
                  subtotal_general = parseFloat(replaceAll(subtotal_general.substring(1),',','')) - parseFloat(replaceAll(subtotal.substring(1),',',''));
                  $('#subtotal-general').text('$'+number_format(subtotal_general, 2, '.', ','));
                  $('#iva-general').text('$'+number_format(subtotal_general * .16, 2, '.', ','));
                  $('#total-general').text('$'+number_format(subtotal_general + (subtotal_general * .16), 2, '.', ','));
  
  
                  $(this).remove();
                  $('#listado_productos').find('input[data-rel = '+ rel +']').remove();
  
              });             
          }
      }, '.delete_element');
  
  
  // Guarda Prefactura
  $('#btn-guarda').click(function(e){
          e.preventDefault();
          var cant_partidas = $('#tabla_partidas tbody tr').length;
          
  
          $('#btn-guarda').attr('disabled','disabled');
  
          var factura = [];
          var servicios = [];
          var i=0;
  
  
          if (cant_partidas >= 1 && $('#tipo-de-cambio').val() > 0) {
  
              var moneda_id = $('#moneda-id').val(); // 1-USD 2-MXN
              var tipo_cambio = $('#tipo-de-cambio').val(); // 1-USD 2-MXN
              var fecha_vencimiento = $('#fecha-vencimiento').val(); 
  
  
              var subtotal_general = $('#subtotal-general').text();
              subtotal_general = parseFloat(replaceAll(subtotal_general.substring(1),',',''));
  
              factura[0] = '<?php echo $clientes->id; ?>'; //cliente_id
              factura[1] = subtotal_general; //subtotal
              factura[2] = subtotal_general * .16; //iva
              factura[3] = factura[1] + factura[2]; //total
              factura[4] = moneda_id; //moneda_id
              factura[5] = tipo_cambio; //tipo_de_cambio
              factura[6] = $('#observaciones').val(); //observaciones
              factura[7] = fecha_vencimiento; //fecha_vencimiento
  
              //alert(factura);
              
              $('#tabla_partidas tbody tr').each(function(k,v){
  
                  var tr = $(this);
                  //var tr_insert;
                  
                  var precio_unitario = tr.find('td:eq(4)').html();
                  precio_unitario = replaceAll(precio_unitario.substring(1),',','');
                  
                  servicios[i] = [];
                  servicios[i][0] = tr.find('td:eq(7)').html(); // unidad_id
  
  
                  if(moneda_id == 1) // Checo el tipo de moneda para calculo MXN o USD
                  {
                      servicios[i][1] = precio_unitario; // USD
                      servicios[i][2] = parseFloat(precio_unitario) * parseFloat(tipo_cambio); // MXN 
                  }else{
                      servicios[i][1] = parseFloat(precio_unitario) / parseFloat(tipo_cambio); // USD
                      servicios[i][2] = precio_unitario; // MXN
                  }
                  
  
                  servicios[i][3] = tr.find('td:eq(3)').html(); // descripcion
                  servicios[i][4] = tr.find('td:eq(1)').html(); // cantidad
  
                  //alert(servicios[i]);  
  
                  i=i+1;
                  
                  
              });
  
         
              $.ajax({
  
                  type: 'POST',
                  url: '<?php echo $this->Url->build(["controller" => "facturas","action" => "genera_facturaManual"]); ?>',
                  data: {
                      servicios:servicios, 
                      factura:factura
                  },
                  dataType: 'json',
                  success: function(data){
  
                      if (data.confirma == 100)
                      {
                          alert('La Prefactura se creo con Exito');
                          window.location.href = '/facturas/';
                      }
                      $('#btn-guarda').removeAttr('disabled');
                      
                  },
                  error:function(data){
  
                      $('#btn-guarda').removeAttr('disabled');
                       alert('Ocurrió un error vuelva a intentarlo ');
                  }
                  
                 
  
               });
              
          }else{
              $('#btn-guarda').removeAttr('disabled');
              alert('Servicios o tipo de cambio sin especificar correctamente');
          }
  
      
  
      });
  
</script>
<!--
  <div class="page-padding">
      
  </div>
  -->
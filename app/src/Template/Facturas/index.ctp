<?php
  $hoy = new DateTime();
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  $this->start('tb_sidebar');
  ?>
<?php $this->end(); ?>
<?php echo $this->element('all_facturas'); ?>
<!-- Modal de pagos -->
<div class="modal fade" id="modalPago">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="pago_factura_form">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Pagar Factura</h4>
        </div>
        <div class="modal-body">
          <table width="100%">
            <tbody>
              <tr height="40px">
                <td width="60%">
                  <h4>Factura No.</h4>
                </td>
                <td width="40%">
                  <div id="numero_modal"></div>
                </td>
              </tr>
              <tr height="40px">
                <td width="60%">
                  <h4>Método de pago</h4>
                </td>
                <td width="40%">
                  <label id="metodop"></label>
                </td>
              </tr>
              <tr  height="40px">
                <td width="60%">
                  <h4>Fecha de pago:</h4>
                </td>
                <td  width="40%">
                  <input type="text" name="fecha" class="input-sm form-control" id="datepicker_pago">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="submit" id="pagar_factura_boton" class="btn btn-primary">Pagar</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
  var payment_selected_id = 0;
  var selected_menu = null;
  var dTable = null;
  $(document).ready(function(){
     // $('.dataTables-example').dataTable({
     //     responsive: true,
  
     //     "dom": 'T<"clear">lfrtip',
     //     "lengthChange": false,
     //      "footerCallback": function( tfoot, data, start, end, display ) {
     //         $(tfoot).html( "" );
     //     }
     // });
  
       dTable = $('.dataTables-example').dataTable({
         responsive: true,
         "paging":   false,
         "info":     false,
         "dom": 'T<"clear">lfrtip',
         "lengthChange": false,
          "footerCallback": function( tfoot, data, start, end, display ) {
             $(tfoot).html( "" );
         }
     });
      
  
     $('.pagar_factura').on('click', function(ev){
         ev.preventDefault();
         selected_menu = ev.target;
         payment_selected_id = ev.target.getAttribute('data-id');
         var metodop = ev.target.getAttribute('data-metodop');
  
         $( '#metodop' ).text(metodop);
  
         $( "#pago_factura_form" ).attr('action','/facturas/pagar/'+payment_selected_id);  
  
  
         $('#modalPago').modal('show');
        
     })
     $('.filtrar_fechas').on('click', function(ev){
         ev.preventDefault();
         var date1 = $("#datepicker1").val();
         var date2 = $("#datepicker2").val();
  
         var param1 = insertParam("fecha_inicio", date1);
         var param2 = insertParam("fecha_fin", date2);
  
         //console.log(param2);
         param1.push(param2);
         document.location.search = param1.join("&");
     });
  
     $('#pagar_factura_boton').on('click', function(ev){
         ev.preventDefault();
         var action_url = $("#pago_factura_form" ).attr('action');
         $.post(action_url , $("#pago_factura_form").serialize(), function(data) {
             $('#modalPago').modal('hide');
             $(selected_menu).css("display", "none");
             var f = $('#datepicker_pago').val();
             var d = f.split('/');
             $("#pago_"+payment_selected_id).html(d[1]+'-'+d[0]+''+d[2]);
         });
     });
  
  });
  
  $(document).on({click: function(ev){
     ev.preventDefault();
     var factura_id = $(this).data("id");
     var action_url = "/facturas/crearTimbrado/"+factura_id;
     $("#timbre_"+factura_id).html( '<i class="fa fa-spinner fa-spin"></i>');
     var element = $(this);
     $.post(action_url , function(data) {
             var response = JSON.parse(data);
             //console.log(response);
             if(response.return.estatus == 200){
                 $("#timbre_"+factura_id).html( '<i class="fa fa-check"></i>');
                 var children = $("#dropdown-menu-"+factura_id).children();
                 var start = children.slice(0, -2);
                 var end_chunk = '<li role="presentation"><a href="/facturas/download/'+factura_id+'/0" data-id="'+factura_id+'" title="Descargar PDF" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;Descargar PDF</a></li>\
                 <li role="presentation"><a href="/facturas/download/'+factura_id+'/1" data-id="'+factura_id+'" title="Descargar XML" target="_blank"><i class="fa fa-file-code-o"></i>&nbsp;Descargar XML</a></li>\
                 <li role="presentation"><a href="javascript:void(0);" onclick="cancelar(this);" class="cancelar_factura" data-id="'+factura_id+'"><i class="fa fa-money"></i>&nbsp;Cancelar Factura</a></li>';
                 $("#dropdown-menu-"+factura_id).html($(start).prop('outerHTML')+end_chunk);
             }
             else{
                 alert(response.return.mensaje);
             }
         });
  
  }}, '.timbrar_factura');
  
  
  $('#datepicker1').datepicker({
  "setDate": new Date(),
  "format": 'dd-mm-yyyy',
  "autoclose": true
  });
  
  $('#datepicker2').datepicker({
  "setDate": new Date(),
  "format": 'dd-mm-yyyy',
  "autoclose": true
  }); 
  $('#datepicker_pago').datepicker({
     "setDate": new Date(),
     "autoclose": true
  });
  $('#datepicker_pago').datepicker('update');
  
  function pago(ev){
     console.log(ev);
     payment_selected_id = ev.getAttribute('data-id');
     folio = ev.getAttribute('data-folio');
     $( "#pago_factura_form" ).attr('action','/facturas/pagar/'+payment_selected_id);
     $('#numero_modal').html(folio);
     $('#modalPago').modal('show');
  }
  
  function cancelar(ev){
     console.log(ev);
     payment_selected_id = ev.getAttribute('data-id');
     var action_url = "/facturas/cancelar/"+payment_selected_id;
  
  
      $.post(action_url , function(data) {
             var response = JSON.parse(data);
             //console.log(response);
             if(response.return.estatus == 200){
                 $("#timbre_"+payment_selected_id).html( 'Cancelada');
                 $("#dropdown-menu-"+payment_selected_id).html('<li role="presentation"><a href="/facturas/download/'+payment_selected_id+'/0" data-id="'+payment_selected_id+'" title="Descargar PDF" target="_blank"><i class="fa fa-file-pdf-o"></i> Descargar PDF</a></li>');
             }
             else{
                 alert(response.return.mensaje);
             }
         });
     
  }
  
  function insertParam(key, value)
  {
     key = encodeURI(key); value = encodeURI(value);
  
     var kvp = [];//document.location.search.substr(1).split('&');
  
     var i=kvp.length; var x; while(i--) 
     {
         x = kvp[i].split('=');
  
         if (x[0]==key)
         {
             x[1] = value;
             kvp[i] = x.join('=');
             break;
         }
     }
  
     if(i<0) {kvp[kvp.length] = [key,value].join('=');}
  
     //this will reload the page, it's likely better to store this until finished
     // document.location.search = kvp.join('&'); 
     return kvp;
  }
  
  function enviar_seleccion(){
  
     $('#formFacturas').attr('action', '/facturas/enviar_seleccion');
     $('#formFacturas').submit();
  
  }
  
  function descargar_seleccion(){
  
     $('#formFacturas').attr('action', '/facturas/descargar_seleccion');
     $('#formFacturas').submit();
  
  }
  
  function delete_prefactura(id){
  
     $('#formFacturas').attr('action', '/facturas/delete/'+id);
     $('#formFacturas').attr('method', 'post');
     // alert($('#formFacturas').attr('action'));
     // alert($('#formFacturas').attr('method'));
     // return false;
     $('#formFacturas').submit();
  
  }
  
  function timbrar_seleccion(){
  
      //console.log(dTable.fnGetNodes());
      //$('input:checked', dTable.fnGetNodes() );             
      //$('input:checked', dTable.fnGetNodes() ).each(function(index){
         $(':checkbox').each(function(){
             if(this.checked){
  
                 var factura_id = $(this).data("id");
                 var action_url = "/facturas/crearTimbrado/"+factura_id;
                 var element = $(this);
                 $.post(action_url , function(data) {
                     var response = JSON.parse(data);
                     //console.log(response);
                     if(response.return.estatus == 200){
                         $("#timbre_"+factura_id).html( '<i class="fa fa-check"></i>');
                         var children = $("#dropdown-menu-"+factura_id).children();
                         var start = children.slice(0, -2);
                         var end_chunk = '<li role="separator" class="divider"></li>\
                         <li role="presentation"><a href="/facturas/download/'+factura_id+'/0" data-id="'+factura_id+'" title="Descargar PDF" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;Descargar PDF</a></li>\
                         <li role="presentation"><a href="/facturas/download/'+factura_id+'/1" title="Descargar XML" target="_blank"><i class="fa fa-file-code-o"></i>&nbsp;Descargar XML</a></li>\
                         <li role="presentation"><a href="javascript:void(0);" onclick="cancelar(this);" class="cancelar_factura" data-id="'+factura_id+'"><i class="fa fa-money"></i>&nbsp;Cancelar Factura</a></li>\
                         <li role="separator" class="divider"></li>\
                         <li role="presentation"><a href="/facturas/enviarfacturapdf/'+factura_id+'" data-id="'+factura_id+'" title="Enviar Factura PDF"><i class="fa fa-envelope-o"></i>&nbsp;Enviar Factura PDF</a></li>\
                         ';
                         $("#dropdown-menu-"+factura_id).html($(start).prop('outerHTML')+end_chunk);
                     }
                     else{
                         //alert(response.return.mensaje);
                     }
                 });
             }
  
         }).prop('checked',false);
  
  
  }
  
  
</script>
<?php echo $this->element('modal_cambiar_formapago') ?>
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center"><center>
                                            <img src="http://135-bc.webpoint.mx/img/logo_email_bcm.png"  align="center">
                                        </center><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <h3><?php echo $titulo; ?></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <?php echo $mensaje; ?>
                                    </td>
                                </tr>                                
                              </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tr>
                            <td class="aligncenter content-block"> 135 Business Center </td>
                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
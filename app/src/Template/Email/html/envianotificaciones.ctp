<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center"><center>
                                            <img src="http://135-bc.webpoint.mx/img/logo_email_bcm.png"  align="center">
                                        </center><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <h3><?php echo $titulo; ?></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        
                                        <div style="text-align: left; font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;box-sizing:border-box;font-size:14px">

                                            <p>Hola, </p>

                                            <p>Te informamos que para mejorar tu experiencia en nuestros centros de negocios, estaremos usando un nuevo sistema para reservación de salas de juntas a partir del 1 de junio de 2017. Este nuevo sistema es muy similar al anterior, por lo que la transición será muy simple. Si tienes alguna reservación en a partir del 1 de junio de 2017, nosotros la volveremos a reservar por ti, pero por favor te pedimos que lo revises.</p>

                                            <p>Para accesar al nuevo sistema de reservaciones deberás ingresar a la siguiente página de Internet: <a href="www.tubcm.com" style="box-sizing:border-box;color:rgb(26,179,148)" target="_blank">www.tubcm.com</a> en donde encontrarás el nuevo sistema y, con tu correo electrónico (<a href="mailto:<?php echo $email ?>" style="box-sizing:border-box;color:rgb(26,179,148)" target="_blank"><?php echo $email ?></a>) deberás solicitar recuperar contraseña en la siguiente liga <a href="http://www.tubcm.com/forgotPassword" style="box-sizing:border-box;color:rgb(26,179,148)" target="_blank">http://www.tubcm.com/forgotPassword</a> (puede ser igual a la que usabas antes).</p>

                                            <p>Esperemos te guste nuestro nuevo sistema.</p>

                                            <p>Saludos,</p>
                                        </div>

                                    </td>
                                </tr>                                
                              </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tr>
                            <td class="aligncenter content-block"> 135 Business Center </td>
                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
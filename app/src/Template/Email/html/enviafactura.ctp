<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center">
                                    	<center>
                                        	<img src="http://135-bc.webpoint.mx/img/logo_email_bcm.png"  align="center">
                                        </center>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <h3>Factura 135 Business Center</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                    <?php echo $mensaje; ?>
                                        
                                    </td>
                                </tr>                                
                                <tr>
                                	<td>
                                		<br>
                                		<table width='100%' class="table">
					                      <tbody>
					                        <tr>
					                          <td>Cliente</td>
					                          <td>Factura</td>
					                          <td>Monto</td>
					                          <td>Fecha</td>
					                        </tr>
					                        <tr>
					                          <td>&nbsp;<?= $factura->cliente->nombre; ?></td>
					                          <td>&nbsp;<?= $factura->serie."-".$factura->folio; ?></td>
					                          <td>&nbsp;$<?= number_format($factura->total,2); ?>
                                              &nbsp;<?= $factura->moneda->nombre; ?></td>
					                          <td>&nbsp;<?= $factura->fecha; ?></td>
					                        </tr>
					                      </tbody>
					                    </table>
					                    <br>
                                	</td>
                                </tr>
                                <tr>
                                    <td class="content-block aligncenter">
                                        &nbsp;
                                    </td>
                                </tr>
                              </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tr>
                            <td class="aligncenter content-block"> 135 Business Center </td>
                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
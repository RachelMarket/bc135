<style type="text/css">
</style>
<page pageset="old">		
<!-- empieza header-->
<div style="display:inline">
<div id="page_1">

<div style="float:left; width:750px; margin-left:10px; font-size: 10px; font-face:'Arial';">
  <div style="float:left; width:100%;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
      <tbody>
          <tr>
          <td rowspan="7" style="width:auto;"><img height="35" src="<?php echo WWW_ROOT ?>/img/logo_black.png"></td>
        
          <td  rowspan="7" align="center" style="width:40%;">
            <h4>| Reservaciones |</h4>
          </td>

          </tr>
      </tbody>
    </table>
  </div>

  <div style="float:left; width:100%;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
      <tbody>
        <tr>
        <td rowspan="9" style="width:70%;"> 
          <strong><?php echo $cliente->razon_social; ?></strong><br>
          <br>
        </td>
        </tr>
        <tr>
          <td  align="center" style="border: #000000 1px solid; width:30%;"><strong>Periodo</strong></td>
        </tr>
        <tr>
          <td  align="center" style="border: #000000 1px solid; width:30%; border-top:0px;"><?php echo $periodo; ?></td>
        </tr>
      </tbody>
    </table>
  </div>

  <div style="float:left; width:100%; margin-top:20px;">

    <table  width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
                       <thead>
                        <tr>
                            <!--<th style="width:5%; border: #000000 1px solid;" align="center">#</th>-->
                            <th style="width:20%; border: #000000 1px solid; border-left:#000000 1px solid;" align="center">Centro</th>
                            <th style="width:20%; border: #000000 1px solid; border-left:0px;" align="center">Sala</th>
                            <th style="width:10%; border: #000000 1px solid; border-left:0px;" align="center">Fecha</th>
                            <th style="width:15%; border: #000000 1px solid; border-left:0px;" align="center">Horario</th>
                            <th style="width:30%; border: #000000 1px solid; border-left:0px;" align="center">Persona que realiz&oacute;</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($reservations as $row): ?>
                            <tr>
                                <!--<td style="border: #000000 1px solid;  border-top:0px;" align="center"><?= h($row->id) ?></td>-->
                                <td style="border: #000000 1px solid; border-left:#000000 1px solid; border-top:0px;" align="center"><?= h($row->recurso->centro->nombre) ?></td>
                                <td style="border: #000000 1px solid; border-left:0px; border-top:0px;" align="center"><?= h($row->recurso->name) ?></td>
                                <td style="border: #000000 1px solid; border-left:0px; border-top:0px;" align="center"><?= $this->Time->format($row['fecha_inicio'], 'YYYY-MM-dd') ?></td>
                                <td style="border: #000000 1px solid; border-left:0px; border-top:0px;" align="center"><?= $this->Time->format($row['fecha_inicio'], 'HH:mm a').' '.$this->Time->format($row['fecha_fin'], 'HH:mm a')?></td>
                                <td style="border: #000000 1px solid; border-left:0px; border-top:0px;" align="center"><?= h($row->user->first_name.' '.$row->user->last_name) ?></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
  </div>

</div>
</div>
</div>
</page>

<style type="text/css">
</style>
<page pageset="old">		
<!-- empieza header-->
<div style="display:inline">
<div id="page_1">

<div style="float:left; width:750px; margin-left:10px; font-size: 8px; font-face:'Arial';">
  <div style="float:left; width:100%;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 8px; font-face:'Arial';" >
    <tbody>
        <tr>
        <td rowspan="7" style="width:auto;"><img height="35" src="<?php echo WWW_ROOT ?>/img/logo_black.png"></td>
      
        <td  rowspan="7" align="center" style="width:40%;">
          <h4>| ESTADO DE CUENTA |</h4>
        </td>

        </tr>
    </tbody>
  </table>
  </div>


  <div style="float:left; width:100%;">
  <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 8px; font-face:'Arial';" >
    <tbody>
      <tr>
      <td rowspan="9" style="width:70%;"> 
        <p><strong><?php echo $emisor['RazonSocialEmisor']['value']; ?></strong><br>
              <?php echo $emisor['CalleEmisor']['value']; ?><br>
              <?php echo $emisor['ColoniaEmisor']['value']; ?>, 
              <?php echo $emisor['CiudadEmisor']['value'];?><br>
              <?php echo $emisor['EstadoEmisor']['value']; ?>, 
              <?php echo $emisor['PaisEmisor']['value']; ?>, 
              C.P. <?php echo $emisor['CPEmisor']['value']; ?><br>
              RFC <?php echo $emisor['RfcEmisor']['value']; ?><br>
          </p>
        <br>
        <strong><?php echo $cliente->razon_social; ?></strong><br>
        <?php echo $cliente->calle.' '.$cliente->numero.' '.$cliente->numero_interior; ?><br>
        <?php echo $cliente->colonia; ?><br>
        <?php echo $cliente['estado']['estado']; ?>, 
        <?php echo $cliente['municipio']['municipio']; ?>, 
        C.P. <?php echo $cliente->codigo_postal; ?><br>
        País <?php echo $cliente['pais']['nombre']; ?><br>
        RFC <?php echo $cliente->rfc; ?>
        <br>
      </td>
      </tr>
      <tr>
        <td  align="center" style="border: #000000 1px solid; width:30%;"><strong>Periodo</strong></td>
      </tr>
      <tr>
        <td  align="center" style="border: #000000 1px solid; width:30%; border-top:0px;"><?php echo $periodo; ?></td>
      </tr>
    </tbody>
  </table>
  </div>

  <div style="float:left; width:100%; margin-top:20px;">

    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 8px; font-face:'Arial';" >
    <thead>
      
      <tr>
        <td style="width:15%; border: #000000 1px solid;" align="center"> 
          Fecha
        </td>
        <td style="width:40%; border: #000000 1px solid; border-left:0px;" align="center"> 
          Concepto de pago
        </td>
        <td style="width:15%; border: #000000 1px solid; border-left:0px;" align="center"> 
          Precio <?php echo $cliente['moneda']['nombre']; ?></td>
        <td style="width:15%; border: #000000 1px solid; border-left:0px;" align="center"> 
          Cantidad
        </td>
        <td style="width:15%; border: #000000 1px solid;  border-left:0px;" align="center"> 
          Subtotal
        </td>
      </tr>
    </thead>
    <tbody>

      <?php 
      $suma_subtotal = 0;
      foreach ($movimientos as $movimiento):
      $subtotal = 0;
    ?>
    <?php if(is_null($movimiento->factura) || $movimiento->factura->festado_id != 3): ?>
      <tr>
        <td style="width:10%; border: #000000 1px solid;" align="center"> 
        <?php echo date_format($movimiento->fecha, "d/m/y"); ?>
        </td>
        <td style="width:40%; border: #000000 1px solid; border-left:0px;" align="center"> 
        <?php echo $movimiento->nombre; ?>
        </td>
        <td style="width:10%; border: #000000 1px solid; border-left:0px;" align="center"> 
        <?php if($cliente['moneda']['id'] == 1): ?>
        <?php
         $subtotal = $movimiento->cantidad*$movimiento->precio_usd;
         $suma_subtotal += $subtotal;
         echo "$".number_format($movimiento->precio_usd, 2);?>
        <?php else: ?>
        <?php
         $subtotal = $movimiento->cantidad*$movimiento->precio_mxn;
         $suma_subtotal += $subtotal;
         echo "$".number_format($movimiento->precio_mxn, 2); ?> 
        <?php endif; ?>
        </td>
        <td style="width:10%; border: #000000 1px solid; border-left:0px;" align="center"> 
         <?= $movimiento->cantidad; ?>
        </td>
        <td style="width:15%; border: #000000 1px solid;  border-left:0px;" align="center"> 
          <?= "$".number_format($subtotal, 2) ?>
        </td>
      </tr>
      <?php endif; ?>
      <?php endforeach; ?>


    </tbody>
    </table>
  </div>

  <div style="float:left; width:100%; margin-top:5px;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 8px; font-face:'Arial';" >
    <tbody>
      <tr>
        <td style="width:80%;" align="right"> <strong>Subtotal: </strong>&nbsp;&nbsp; </td>
        <td style="width:20%; border: #000000 1px solid;" align="right"><?= "$".number_format($suma_subtotal, 2); ?>&nbsp;&nbsp;</td>
      </tr>
      <tr>
        <td style="width:80%;" align="right"> <strong>IVA (16%): </strong>&nbsp;&nbsp; </td>
        <td style="width:20%; border: #000000 1px solid; border-top: 0px;" align="right"><?= "$".number_format($suma_subtotal*0.16, 2); ?>&nbsp;&nbsp;</td>
      </tr>
      <tr>
        <td style="width:80%;" align="right"> <strong>Total: </strong>&nbsp;&nbsp; </td>
        <td style="width:20%; border: #000000 1px solid; border-top: 0px;" align="right"><?= "$".number_format($suma_subtotal*1.16, 2); ?>&nbsp;&nbsp;</td>
      </tr>
    </tbody>
    </table>
  </div>



</div>


</div>


</div>
</page>

<div class="ibox">
        <div class="ibox-title">

            <h5> <?php echo __('Did Details'); ?> </h5>

            <span class="ibox-tools">

                <?= $this->Html->link(__('Edit Did'), ['action' => 'edit', $did->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

            </span>

        </div>

        <div class="ibox-content">
                
            <dl class="dl-horizontal">
                
                <dt><?= __('Clave') ?>:</dt> 
                <dd><?= $this->Number->format($did->id) ?></dd>

                <dt><?= __('Lada') ?>:</dt> 
                <dd><?= h($did->lada) ?></dd>
                            
                <dt><?= __('Did') ?>:</dt> 
                <dd><?= h($did->did) ?></dd>
                                 
                <dt><?= __('Created') ?>:</dt> 
                <dd><?= date('d/m/Y h:i A',strtotime($did->created)) ?></dd>
                                   
                <dt><?= __('Modified') ?>:</dt> 
                <dd><?= date('d/m/Y h:i A',strtotime($did->modified)) ?></dd>

            </dl>

        <ul id="myTab" class="nav nav-tabs" role="tablist">
           
        </ul>

        <div id="myTabContent" class="tab-content">
        </div>
    </div>
</div>


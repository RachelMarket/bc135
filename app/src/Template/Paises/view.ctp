<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Editar Pais'), ['action' => 'edit', $pais->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Eliminar Pais'), ['action' => 'delete', $pais->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pais->id)]) ?> </li>
    <li><?= $this->Html->link(__('Listar Paises'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('Nuevo Pais'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>

<h2><?= h($pais->nombre) ?></h2>
<div class="row">
        <div class="col-lg-5">
                                    <h6><?= __('Nombre') ?></h6>
                    <p><?= h($pais->nombre) ?></p>
                                </div>
            <div class="col-lg-2">
                    <h6><?= __('Id') ?></h6>
                <p><?= $this->Number->format($pais->id) ?></p>
                </div>
            </div>
<ul id="myTab" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#Clientes" id="Clientes-tab" role="tab" data-toggle="tab" aria-controls="Clientes" aria-expanded="true">Clientes</a>
      </li>
          <li role="presentation" class="">
        <a href="#Estados" id="Estados-tab" role="tab" data-toggle="tab" aria-controls="Estados" aria-expanded="true">Estados</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="Clientes" aria-labelledBy="Clientes-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($pais->clientes)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Primario Persona De Contacto') ?></th>
                                            <th><?= __('Primario Correo Electronico') ?></th>
                                            <th><?= __('Primario Telefono Oficina') ?></th>
                                            <th><?= __('Primario Telefono Movil') ?></th>
                                            <th><?= __('Secundario Persona De Contacto') ?></th>
                                            <th><?= __('Secundario Correo Electronico') ?></th>
                                            <th><?= __('Secundario Telefono Oficina') ?></th>
                                            <th><?= __('Secundario Telefono Movil') ?></th>
                                            <th><?= __('Razon Social') ?></th>
                                            <th><?= __('Rfc') ?></th>
                                            <th><?= __('Calle') ?></th>
                                            <th><?= __('Numero') ?></th>
                                            <th><?= __('Colonia') ?></th>
                                            <th><?= __('Pais Id') ?></th>
                                            <th><?= __('Estado Id') ?></th>
                                            <th><?= __('Municipio Id') ?></th>
                                            <th><?= __('Codigo Postal') ?></th>
                                            <th><?= __('Moneda Id') ?></th>
                                            <th><?= __('Activo') ?></th>
                                            <th><?= __('Created') ?></th>
                                            <th><?= __('Tipo Cliente Id') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($pais->clientes as $clientes): ?>
                    <tr>
                                            <td><?= h($clientes->id) ?></td>
                                            <td><?= h($clientes->nombre) ?></td>
                                            <td><?= h($clientes->primario_persona_de_contacto) ?></td>
                                            <td><?= h($clientes->primario_correo_electronico) ?></td>
                                            <td><?= h($clientes->primario_telefono_oficina) ?></td>
                                            <td><?= h($clientes->primario_telefono_movil) ?></td>
                                            <td><?= h($clientes->secundario_persona_de_contacto) ?></td>
                                            <td><?= h($clientes->secundario_correo_electronico) ?></td>
                                            <td><?= h($clientes->secundario_telefono_oficina) ?></td>
                                            <td><?= h($clientes->secundario_telefono_movil) ?></td>
                                            <td><?= h($clientes->razon_social) ?></td>
                                            <td><?= h($clientes->rfc) ?></td>
                                            <td><?= h($clientes->calle) ?></td>
                                            <td><?= h($clientes->numero) ?></td>
                                            <td><?= h($clientes->colonia) ?></td>
                                            <td><?= h($clientes->pais_id) ?></td>
                                            <td><?= h($clientes->estado_id) ?></td>
                                            <td><?= h($clientes->municipio_id) ?></td>
                                            <td><?= h($clientes->codigo_postal) ?></td>
                                            <td><?= h($clientes->moneda_id) ?></td>
                                            <td><?= h($clientes->activo) ?></td>
                                            <td><?= h($clientes->created) ?></td>
                                            <td><?= h($clientes->tipo_cliente_id) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Clientes', 'action' => 'view', $clientes->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Clientes', 'action' => 'edit', $clientes->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Clientes', 'action' => 'delete', $clientes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientes->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Clientes asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in " id="Estados" aria-labelledBy="Estados-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($pais->estados)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Pais Id') ?></th>
                                            <th><?= __('Estado') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($pais->estados as $estados): ?>
                    <tr>
                                            <td><?= h($estados->id) ?></td>
                                            <td><?= h($estados->pais_id) ?></td>
                                            <td><?= h($estados->estado) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Estados', 'action' => 'view', $estados->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Estados', 'action' => 'edit', $estados->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Estados', 'action' => 'delete', $estados->id], ['confirm' => __('Are you sure you want to delete # {0}?', $estados->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Estados asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
</div>


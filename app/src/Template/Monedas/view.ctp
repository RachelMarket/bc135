<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Editar Moneda'), ['action' => 'edit', $moneda->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Eliminar Moneda'), ['action' => 'delete', $moneda->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moneda->id)]) ?> </li>
    <li><?= $this->Html->link(__('Listar Monedas'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('Nuevo Moneda'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Facturas'), ['controller' => 'Facturas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Factura'), ['controller' => 'Facturas', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>

<h2><?= h($moneda->nombre) ?></h2>
<div class="row">
        <div class="col-lg-5">
                                    <h6><?= __('Nombre') ?></h6>
                    <p><?= h($moneda->nombre) ?></p>
                                </div>
            <div class="col-lg-2">
                    <h6><?= __('Id') ?></h6>
                <p><?= $this->Number->format($moneda->id) ?></p>
                </div>
            </div>
<ul id="myTab" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#Clientes" id="Clientes-tab" role="tab" data-toggle="tab" aria-controls="Clientes" aria-expanded="true">Clientes</a>
      </li>
          <li role="presentation" class="">
        <a href="#Facturas" id="Facturas-tab" role="tab" data-toggle="tab" aria-controls="Facturas" aria-expanded="true">Facturas</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="Clientes" aria-labelledBy="Clientes-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($moneda->clientes)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Primario Persona De Contacto') ?></th>
                                            <th><?= __('Primario Correo Electronico') ?></th>
                                            <th><?= __('Primario Telefono Oficina') ?></th>
                                            <th><?= __('Primario Telefono Movil') ?></th>
                                            <th><?= __('Secundario Persona De Contacto') ?></th>
                                            <th><?= __('Secundario Correo Electronico') ?></th>
                                            <th><?= __('Secundario Telefono Oficina') ?></th>
                                            <th><?= __('Secundario Telefono Movil') ?></th>
                                            <th><?= __('Razon Social') ?></th>
                                            <th><?= __('Rfc') ?></th>
                                            <th><?= __('Calle') ?></th>
                                            <th><?= __('Numero') ?></th>
                                            <th><?= __('Colonia') ?></th>
                                            <th><?= __('Pais Id') ?></th>
                                            <th><?= __('Estado Id') ?></th>
                                            <th><?= __('Municipio Id') ?></th>
                                            <th><?= __('Codigo Postal') ?></th>
                                            <th><?= __('Moneda Id') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($moneda->clientes as $clientes): ?>
                    <tr>
                                            <td><?= h($clientes->id) ?></td>
                                            <td><?= h($clientes->nombre) ?></td>
                                            <td><?= h($clientes->primario_persona_de_contacto) ?></td>
                                            <td><?= h($clientes->primario_correo_electronico) ?></td>
                                            <td><?= h($clientes->primario_telefono_oficina) ?></td>
                                            <td><?= h($clientes->primario_telefono_movil) ?></td>
                                            <td><?= h($clientes->secundario_persona_de_contacto) ?></td>
                                            <td><?= h($clientes->secundario_correo_electronico) ?></td>
                                            <td><?= h($clientes->secundario_telefono_oficina) ?></td>
                                            <td><?= h($clientes->secundario_telefono_movil) ?></td>
                                            <td><?= h($clientes->razon_social) ?></td>
                                            <td><?= h($clientes->rfc) ?></td>
                                            <td><?= h($clientes->calle) ?></td>
                                            <td><?= h($clientes->numero) ?></td>
                                            <td><?= h($clientes->colonia) ?></td>
                                            <td><?= h($clientes->pais_id) ?></td>
                                            <td><?= h($clientes->estado_id) ?></td>
                                            <td><?= h($clientes->municipio_id) ?></td>
                                            <td><?= h($clientes->codigo_postal) ?></td>
                                            <td><?= h($clientes->moneda_id) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Clientes', 'action' => 'view', $clientes->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Clientes', 'action' => 'edit', $clientes->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Clientes', 'action' => 'delete', $clientes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientes->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Clientes asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in " id="Facturas" aria-labelledBy="Facturas-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($moneda->facturas)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Cliente Id') ?></th>
                                            <th><?= __('Folio') ?></th>
                                            <th><?= __('Serie') ?></th>
                                            <th><?= __('Fecha') ?></th>
                                            <th><?= __('Subtotal') ?></th>
                                            <th><?= __('Iva') ?></th>
                                            <th><?= __('Total') ?></th>
                                            <th><?= __('Cantidad Letra') ?></th>
                                            <th><?= __('Festado Id') ?></th>
                                            <th><?= __('Moneda Id') ?></th>
                                            <th><?= __('Observaciones') ?></th>
                                            <th><?= __('Cfdi') ?></th>
                                            <th><?= __('Response') ?></th>
                                            <th><?= __('Xml') ?></th>
                                            <th><?= __('AddRetencion') ?></th>
                                            <th><?= __('Retencion') ?></th>
                                            <th><?= __('Cfdicancelado') ?></th>
                                            <th><?= __('Tipo De Cambio') ?></th>
                                            <th><?= __('Fecha Pago') ?></th>
                                            <th><?= __('Referencia De Pago') ?></th>
                                            <th><?= __('Formasdepago Id') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($moneda->facturas as $facturas): ?>
                    <tr>
                                            <td><?= h($facturas->id) ?></td>
                                            <td><?= h($facturas->cliente_id) ?></td>
                                            <td><?= h($facturas->folio) ?></td>
                                            <td><?= h($facturas->serie) ?></td>
                                            <td><?= h($facturas->fecha) ?></td>
                                            <td><?= h($facturas->subtotal) ?></td>
                                            <td><?= h($facturas->iva) ?></td>
                                            <td><?= h($facturas->total) ?></td>
                                            <td><?= h($facturas->cantidad_letra) ?></td>
                                            <td><?= h($facturas->festado_id) ?></td>
                                            <td><?= h($facturas->moneda_id) ?></td>
                                            <td><?= h($facturas->observaciones) ?></td>
                                            <td><?= h($facturas->cfdi) ?></td>
                                            <td><?= h($facturas->response) ?></td>
                                            <td><?= h($facturas->xml) ?></td>
                                            <td><?= h($facturas->addRetencion) ?></td>
                                            <td><?= h($facturas->retencion) ?></td>
                                            <td><?= h($facturas->cfdicancelado) ?></td>
                                            <td><?= h($facturas->tipo_de_cambio) ?></td>
                                            <td><?= h($facturas->fecha_pago) ?></td>
                                            <td><?= h($facturas->referencia_de_pago) ?></td>
                                            <td><?= h($facturas->formasdepago_id) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Facturas', 'action' => 'view', $facturas->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Facturas', 'action' => 'edit', $facturas->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Facturas', 'action' => 'delete', $facturas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $facturas->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Facturas asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
</div>


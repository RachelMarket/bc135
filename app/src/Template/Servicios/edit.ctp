<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  $this->start('tb_sidebar');
  ?>
<ul class="nav nav-sidebar">
  <li><?=
    $this->Form->postLink(
    __('Eliminar'),
    ['action' => 'delete', $servicio->id],
    ['confirm' => __('Are you sure you want to delete # {0}?', $servicio->id)]
    )
    ?></li>
  <li><?= $this->Html->link(__('Listar Servicios'), ['action' => 'index']) ?></li>
  <li><?= $this->Html->link(__('Listar Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Unidades'), ['controller' => 'Unidades', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Unidad'), ['controller' => 'Unidades', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
</ul>
<?php $this->end(); ?>
<?= $this->Form->create($servicio); ?>
<div class="ibox float-e-margins">
  <div class="ibox-title">
    <h2><?= __('Editar {0}', ['Servicio']) ?></h2>
    <a class="btn btn-back btn-cancel" href="<?= $this->request->referer(); ?>">Cancelar</a>
  </div>
  <div class="ibox-content">
    <div class="row ca-forms mtp-40">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <div>
                <?= $this->Form->input('tipo_id', ['options' => $tipos]); ?> 
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <div>
                <?= $this->Form->input('precio_mxn'); ?>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div>
                <?= $this->Form->input('precio_usd'); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <div>
                <?= $this->Form->input('centro_id', ['options' => $centros]); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <div>
                <?= $this->Form->input('descripcion'); ?>      
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="last-btns">
              <a class="btn btn-cancel" href="<?= $this->request->referer(); ?>">Cancelar</a>
              <?= $this->Form->button(__('Registrar'), ['class' => 'btn btn-save']) ?>
              <?= $this->Form->end() ?>       
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

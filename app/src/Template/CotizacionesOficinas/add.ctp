<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CotizacionesOficina $cotizacionesOficina
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cotizaciones Oficinas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="cotizacionesOficinas form large-9 medium-8 columns content">
    <?= $this->Form->create($cotizacionesOficina) ?>
    <fieldset>
        <legend><?= __('Add Cotizaciones Oficina') ?></legend>
        <?php
            echo $this->Form->control('cotizacion_id');
            echo $this->Form->control('oficina_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

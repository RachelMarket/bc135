<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CotizacionesOficina[]|\Cake\Collection\CollectionInterface $cotizacionesOficinas
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Cotizaciones Oficina'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cotizacionesOficinas index large-9 medium-8 columns content">
    <h3><?= __('Cotizaciones Oficinas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cotizacion_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('oficina_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cotizacionesOficinas as $cotizacionesOficina): ?>
            <tr>
                <td><?= $this->Number->format($cotizacionesOficina->id) ?></td>
                <td><?= $this->Number->format($cotizacionesOficina->cotizacion_id) ?></td>
                <td><?= $this->Number->format($cotizacionesOficina->oficina_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $cotizacionesOficina->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cotizacionesOficina->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cotizacionesOficina->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cotizacionesOficina->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CotizacionesOficina $cotizacionesOficina
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cotizacionesOficina->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cotizacionesOficina->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Cotizaciones Oficinas'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="cotizacionesOficinas form large-9 medium-8 columns content">
    <?= $this->Form->create($cotizacionesOficina) ?>
    <fieldset>
        <legend><?= __('Edit Cotizaciones Oficina') ?></legend>
        <?php
            echo $this->Form->control('cotizacion_id');
            echo $this->Form->control('oficina_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CotizacionesOficina $cotizacionesOficina
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cotizaciones Oficina'), ['action' => 'edit', $cotizacionesOficina->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cotizaciones Oficina'), ['action' => 'delete', $cotizacionesOficina->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cotizacionesOficina->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cotizaciones Oficinas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cotizaciones Oficina'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cotizacionesOficinas view large-9 medium-8 columns content">
    <h3><?= h($cotizacionesOficina->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cotizacionesOficina->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cotizacion Id') ?></th>
            <td><?= $this->Number->format($cotizacionesOficina->cotizacion_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Oficina Id') ?></th>
            <td><?= $this->Number->format($cotizacionesOficina->oficina_id) ?></td>
        </tr>
    </table>
</div>

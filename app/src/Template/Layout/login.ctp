<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->fetch('title') ?></title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
  </head>
  <body  class="ca-black-bg">
    <div class="login-enter animated fadeInDown">
      <div>
        <div class="logs-sect">
          <img src="/img/bc-white.png" class="logo-original" >
          <img src="/img/bc-white-text.png" class="logo-text" >
        </div>
        <!-- Contenido -->
        <?php echo $this->element('Usermgmt.message'); ?>
        <?php echo $this->fetch('content'); ?>
        <!-- Contenido -->
        <br>
      </div>
    </div>
    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(".alert-error").addClass("alert-danger").removeClass("alert-error");
      
    </script>
  </body>
</html>
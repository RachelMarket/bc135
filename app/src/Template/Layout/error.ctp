<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?=  $this->Html->css('bootstrap.min.css?q='.QRDN) ?>
     <?=  $this->Html->css('style.css?q='.QRDN) ?>
    <?=  $this->Html->css('font-awesome/css/font-awesome.css?q='.QRDN) ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <style type="text/css">
        


/* Clouds */
#clouds {
  top: 100px;
  position: relative;
  -webkit-animation: move 50s infinite linear;  
  -moz-animation: move 50s infinite linear;  
  -ms-animation: move 50s infinite linear;
  z-index: 1;
}

#clouds2 {
  position: relative;
  -webkit-animation: backup 30s infinite linear;
  -moz-animation: backup 30s infinite linear;
  -ms-animation: backup 30s infinite linear;
  z-index: 2;
}

.cloud1, .cloud2, .cloud3 { 
  opacity: 1;  
}
.cloud1, .cloud2{ 
  opacity: .6;  
}

.cloud1 {
  width: 200px;
  height: 60px;
  background: #fff;
  position: absolute;
  top: 80px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;
  
}

.cloud1:after {
  content: '';
  position: absolute;
  background: #fff;
  width: 80px;
  height: 80px;
  top: -40px;
  left: 20px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;

}

.cloud1:before {
  content: '';
  position: absolute;
  background: #fff;
  width: 100px;
  height: 100px;
  top: -60px;
  right: 30px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;
}

.cloud2 {
  width: 100px;
  height: 30px;
  background: #fff;
  position: absolute;
  top: 300px;
  left: 400px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;
}

.cloud2:after {
  content: '';
  position: absolute;
  background: #fff;
  width: 40px;
  height: 40px;
  top: -20px;
  left: 10px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;
}

.cloud2:before {
  content: '';
  position: absolute;
  background: #fff;
  width: 50px;
  height: 50px;
  top: -30px;
  right: 15px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;

}

.cloud3 {
  width: 200px;
  height: 60px;
  background: #fff;
  position: absolute;
  top: 100px;
  left: 740px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;
}

.cloud3:after {
  content: '';
  position: absolute;
  background: #fff;
  width: 80px;
  height: 80px;
  top: -40px;
  left: 20px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;
}

.cloud3:before {
  content: '';
  position: absolute;
  background: #fff;
  width: 100px;
  height: 100px;
  top: -60px;
  right: 30px;
  
  -webkit-border-radius: 200px;
  -moz-border-radius: 200px;
  border-radius: 200px;
}



@-webkit-keyframes move {
  0% {left: 0px;}
  49% {left: 940px; opacity: 1;}
  50% {left: 940px; opacity: 0;}
  51% {left: -940px; opacity: 0;} 
  52% {left: -940px; opacity: 1;} 
  100% {left: 0px;} 
}

@-webkit-keyframes backup {
  0% {left: -940px;}
  100% {left: 940px;}  

}  

@-moz-keyframes move {
  0% {left: 0px;}
  49% {left: 940px; opacity: 1;}
  50% {left: 940px; opacity: 0;}
  51% {left: -940px; opacity: 0;} 
  52% {left: -940px; opacity: 1;} 
  100% {left: 0px;} 
}

@-moz-keyframes backup {
  0% {left: -940px;}
  100% {left: 940px;}  

}  

@-ms-keyframes move {
  0% {left: 0px;}
  49% {left: 940px; opacity: 1;}
  50% {left: 940px; opacity: 0;}
  51% {left: -940px; opacity: 0;} 
  52% {left: -940px; opacity: 1;} 
  100% {left: 0px;} 
}

@-ms-keyframes backup {
  0% {left: -940px;}
  100% {left: 940px;}  

}  
 

#clouds .cloud1, #clouds .cloud2, #clouds .cloud2:after, #clouds .cloud2:before, #clouds .cloud3, #clouds .cloud1:after, #clouds .cloud1:before, #clouds .cloud3:before, #clouds .cloud3:after {
    background: #fff;
}

.cloudC{
background-color: #D3E1F7;

}
    </style>
</head>


<body class="cloudC" >

    <div class="container">
    <div class="oops">
        
    </div>
  <div id="clouds">
    <div class="cloud1"></div>
    <div class="cloud2"></div>
    <div class="cloud3"></div>
  </div>
  <div id="clouds2">
    <div class="cloud1"></div>
    <div class="cloud2"></div>
    <div class="cloud3"></div>
  </div>
</div>
    <div class="middle-box text-center animated fadeInDown">
       
        <h1><?= $code ?></h1>
        <h3 class="font-bold"> </h3>

        <div class="error-desc">
            

        </div>

        <div class="error-desc">
            <?= $this->Flash->render() ?>

            <?= $this->fetch('content') ?>
            
            <a href="/">  Regresar   </a>
            
        </div>
    </div>

    <?=  $this->Html->script('jquery-2.1.1.js?q='.QRDN) ?>
    <?=  $this->Html->script('bootstrap.min.js?q='.QRDN) ?>



</body>
</html>

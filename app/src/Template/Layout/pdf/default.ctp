<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->css('style');
	?>
</head>
<body>
	<div id="container" style="background-color: #D6D6D3;">
		<header>
			<p>Cd.Juarez Workplaces</p>
			<p>Cotización de servicios de oficina</p>
		</header>
		<main>
			<?php echo $this->fetch('content'); ?>
		</main>
	</div>
</body>
</html>
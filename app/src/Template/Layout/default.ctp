<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->fetch('title') ?></title>
    <script language="javascript">
      var urlForJs="<?php echo SITE_URL ?>";
    </script>
    <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="/js/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <?php
      echo $this->Html->meta('icon');
      /* Bootstrap CSS */
      echo $this->Html->css('/plugins/bootstrap/css/bootstrap.min.css?q='.QRDN);
      
      /* Usermgmt Plugin CSS */
      echo $this->Html->css('/usermgmt/css/umstyle.css?q='.QRDN);
      
      /* Bootstrap Datepicker is taken from https://github.com/eternicode/bootstrap-datepicker */
      echo $this->Html->css('/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css?q='.QRDN);
      
      /* Bootstrap Datepicker is taken from https://github.com/smalot/bootstrap-datetimepicker */
      echo $this->Html->css('/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css?q='.QRDN);
      
      /* Chosen is taken from https://github.com/harvesthq/chosen/releases/ */
      echo $this->Html->css('/plugins/chosen/chosen.min.css?q='.QRDN);
      
      /* Jquery latest version taken from http://jquery.com */
      //echo $this->Html->script('/plugins/jquery-1.11.2.min.js');
      
      /* Bootstrap JS */
      //echo $this->Html->script('/plugins/bootstrap/js/bootstrap.min.js?q='.QRDN);
      
      /* Bootstrap Datepicker is taken from https://github.com/eternicode/bootstrap-datepicker */
      echo $this->Html->script('/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js?q='.QRDN);
      
      /* Bootstrap Datepicker is taken from https://github.com/smalot/bootstrap-datetimepicker */
      echo $this->Html->script('/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js?q='.QRDN);
      
      /* Bootstrap Typeahead is taken from https://github.com/biggora/bootstrap-ajax-typeahead */
      echo $this->Html->script('/plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js?q='.QRDN);
      
      /* Chosen is taken from https://github.com/harvesthq/chosen/releases/ */
      echo $this->Html->script('/plugins/chosen/chosen.jquery.min.js?q='.QRDN);
      
      /* Usermgmt Plugin JS */
      echo $this->Html->script('/usermgmt/js/umscript.js?q='.QRDN);
      echo $this->Html->script('/usermgmt/js/ajaxValidation.js?q='.QRDN);
      
      echo $this->Html->script('/usermgmt/js/chosen/chosen.ajaxaddition.jquery.js?q='.QRDN);
      
      echo $this->fetch('meta');
      echo $this->fetch('css');
      echo $this->fetch('script');
      
      ?>
    <!--Dropzone-->
    <?=  $this->Html->css('plugins/dropzone/style.css') ?>
    <?=  $this->Html->css('plugins/dropzone/dropzone.css') ?>
    <?= $this->Html->script('plugins/dropzone/dropzone.js') ?>
    <!-- <link href="/font-awesome/css/font-awesome.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"> -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/1.10.7/integration/font-awesome/dataTables.fontAwesome.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/1.0.6/css/dataTables.responsive.css">
    <!-- Related to dataTables -->
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/chosen.min.css" rel="stylesheet">
    <!-- Local style only for demo purpose -->
    <style>
      .directive-list {
      list-style: none;
      }
      .directive-list li {
      background: #f3f3f4;
      padding: 10px 20px;
      margin: 4px;
      }
      .search{
      width: 300px;
      background-color: transparent;
      background-repeat: no-repeat;
      border: 2px solid #ccc;
      border-radius: 8px;
      border-color: currentColor;
      }
      .search:focus{
      background-color: #fff;
      border-color: currentColor;
      }
      .icon{
      position: absolute;
      left: 275px;
      margin-top: -37px;
      width: 50px;
      }
    </style>
    <!-- Custom and plugin javascript -->
    <script src="/js/inspinia.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>
    <script src="/js/plugins/pace/pace.min.js"></script>
    <script src="/js/helper.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/responsive/1.0.6/js/dataTables.responsive.js
      "></script>
    <script type="text/javascript" src="/js/typeahead.bundle.js"></script>
  </head>
  <body>
    <div id="wrapper">
      <div class="row">
        <nav class="navbar navbar-static-top purple-bg" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
            <img class="bc-logo" src="/img/bc-white.png">
            <a class="navbar-minimalize" href="#"><i class="fa fa-bars"></i> </a>
          </div>
          <ul class="nav navbar-top-links navbar-right">
            <?php if ($this->UserAuth->getGroupId() != 5 && $this->UserAuth->getGroupId() != 6): ?>
            <li class="dropdown">
              <!-- Inicio correo-->
              <a class="dropdown-toggle count-info" data-toggle="dropdown">
                <div class="env">
                  <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 400 400" enable-background="new 0 0 400 400" xml:space="preserve">
                    <g>
                      <path fill="#FFFFFF" d="M19.002,80.688v239.957H381V80.688H19.002z M200.003,228.871L51.526,98.487h296.95L200.003,228.871z
                         M140.856,200.624L36.801,292.002V109.247L140.856,200.624z M154.343,212.47l45.655,40.094l45.655-40.094l102.916,90.376H51.433
                        L154.343,212.47z M259.142,200.629L363.2,109.247v182.755L259.142,200.629z"/>
                    </g>
                  </svg>
                </div>  
                <span class="label" id="cant_mensajes">0</span>
              </a>
              <ul class="dropdown-menu dropdown-messages" id='datos_mensajes'>
                <li>
                  <div class="text-center link-block">
                    <a href="mailbox.html">
                    <i class="fa fa-envelope"></i> 
                    <strong>Read All Messages</strong>
                    </a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Fin de correo -->
            <?php endif; ?>
            <li>
              <?php
                use Cake\Utility\Inflector;
                $actionUrl = Inflector::camelize($this->request['controller']).'/'.$this->request['action'];
                $activeClass = 'active';
                $inactiveClass = '';
                
                $usuario=$this->UserAuth->getUser(); 
                ?>
              <div class="dropdown profile-element">
                <span class="ca-bubble">
                <img class="img-circle" alt="<?php echo h($usuario['User']['first_name'].' '.$usuario['User']['last_name']); ?>" src="<?php echo $this->Image->resize('library/'.IMG_DIR, $usuario['User']['photo'], 80, null, true);?>">
                </span>
                <a data-toggle="dropdown" class="dropdown-toggle ca-drop" href="#">
                  <span class="clear"> 
                    <span class="block m-t-xs"> 
                      <strong class="font-bold"><?php echo $usuario['User']['first_name']." ".$usuario['User']['last_name'];?></strong>
                    </span> 
                    <!--<span class="text-muted text-xs block">
                      <?php echo $usuario['User']['email'];?><b class="caret"></b>
                    </span> -->
                  </span> 
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                  <?php 
                    if($this->UserAuth->HP('Users', 'myprofile', 'Usermgmt')) {
                        echo "<li class='".(($actionUrl=='Users/myprofile') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('My Profile'), ['controller'=>'Users', 'action'=>'myprofile', 'plugin'=>'Usermgmt'])."</li>";
                    }
                    if($this->UserAuth->HP('Users', 'editProfile', 'Usermgmt')) {
                        echo "<li class='".(($actionUrl=='Users/editProfile') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Edit Profile'), ['controller'=>'Users', 'action'=>'editProfile', 'plugin'=>'Usermgmt'])."</li>";
                    }
                    if($this->UserAuth->HP('Users', 'changePassword', 'Usermgmt')) {
                        echo "<li class='".(($actionUrl=='Users/changePassword') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Change Password'), ['controller'=>'Users', 'action'=>'changePassword', 'plugin'=>'Usermgmt'])."</li>";
                    }
                    if($this->UserAuth->HP('Users', 'deleteAccount', 'Usermgmt') && ALLOW_DELETE_ACCOUNT && !$this->UserAuth->isAdmin()) {
                        echo "<li>".$this->Form->postlink(__('Delete Account'), ['controller'=>'Users', 'action'=>'deleteAccount', 'plugin'=>'Usermgmt'], ['escape'=>false, 'confirm'=>__('Are you sure you want to delete your account?')])."</li>";
                    }
                    ?>
                  <li class="divider"></li>
                  <li><a href="/logout">Logout</a></li>
                </ul>
              </div>
            </li>
            <li>
              <a href="/logout" class="off">
               <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve">
<rect x="93" y="1" fill="#EBE042" width="14" height="77"/>
<path fill="#EBE042" d="M127,25.413v14.39c22.982,10.318,39,33.373,39,60.197c0,36.45-29.55,66-66,66c-36.451,0-66-29.55-66-66
  c0-26.827,16.015-49.897,39-60.216v-14.35C42.486,36.481,20.667,65.673,20.667,100c0,43.814,35.519,79.333,79.333,79.333
  s79.333-35.519,79.333-79.333C179.333,65.67,157.518,36.458,127,25.413z"/>
</svg>
              </a>
            </li>
          </ul>
        </nav>
      </div>
      <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
          <?php if( $this->UserAuth->getGroupId() != 5 && $this->UserAuth->getGroupId() != 6 ): ?>
            <div class="srch">
              <?php echo $this->Form->create('Clientes', ['id' => 'contact-search-form', 'role' => 'role', 'name' => 'searchForm', 'url' => ['controller' => 'Clientes','action' => 'search', 'plugin' => false]]); ?>
              <?php echo $this->Form->input('name-contact', ['label' => false, 'class' => 'form-control search', 'placeholder' => 'Buscar', 'type' => 'text', 'name' => 'search']); ?></button>
              <i style="cursor: pointer;" onclick="document.getElementById('contact-search-form').submit()" class="fa fa-search fa-5 icon" aria-hidden="true"></i>
              <?php echo $this->Form->end(); ?>
            </div>
          <?php endif ?>

          <h4 class="ca-menu-title">Menu principal</h4>
          <ul class="nav" id="side-menu">
            
            <?php if($this->UserAuth->isLogged()) { 
              echo $this->element('Usermgmt.dashboard');
              echo $this->element('menu');
              } ?>
          </ul>
        </div>
      </nav>
      <div id="page-wrapper" class="gray-bg">
        
        <div class="wrapper wrapper-content animated fadeInRight">
          <!-- Contenido -->
          <?php echo $this->element('Usermgmt.message'); ?>
          <?php //echo $this->fetch('tb_flash') ?>
          <?php echo $this->fetch('content'); ?>
          <!-- Contenido -->
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(".um-panel-header").addClass("ibox-title").removeClass("um-panel-header");
      $(".um-panel-title-right").addClass("ibox-tools col-md-2 pull-right").removeClass("um-panel-title-right");
      $(".um-panel-title").addClass("h3").removeClass("um-panel-title");
      // $(".alert-error").addClass("alert-danger").removeClass("alert-error");
      
      
      function number_format(number, decimals, decPoint, thousandsSep){
      decimals = decimals || 0;
      number = parseFloat(number);
      
      if(!decPoint || !thousandsSep){
         decPoint = '.';
         thousandsSep = ',';
      }
      
      var roundedNumber = Math.round( Math.abs( number ) * ('1e' + decimals) ) + '';
      var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
      var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
      var formattedNumber = "";
      
      while(numbersString.length > 3){
         formattedNumber += thousandsSep + numbersString.slice(-3)
         numbersString = numbersString.slice(0,-3);
      }
      
      return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
      }
      
      function replaceAll(text, busca, reemplaza){
      while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
      return text;
      }
      
      function getMessages(){
      $.ajax({url:'/mensajes/listMensajes', dataType: 'json', success: function(result){
         
         $("#cant_mensajes").text(result.length);
         
         var datos_mensajes = $('#datos_mensajes');
      
         $('#datos_mensajes li').remove();
      
         
         $.each(result, function(k, lista){
             //alert(lista['mensaje']);
      
           
             var dateString = lista['fecha'];
             //dateString = dateString.toLocaleDateString();
      
             var li = '<li>\
                             <div class="dropdown-messages-box">\
                                 <div class="media-body">\
                                     '+lista['mensaje'].substr(0,80)+'... <br>\
                                     <strong>'+lista['usuario']['username']+'</strong>. <br>\
                                     <!--<small class="text-muted">'+dateString+'</small>-->\
                                 </div>\
                             </div>\
                         </li>\
                         <li class="divider"></li>';
      
             datos_mensajes.append(li);
      
         });
      
         datos_mensajes.append('<li>\
                             <div class="text-center link-block">\
                                 <a href="/">\
                                     <i class="fa fa-envelope"></i> <strong>Leer todos los Mensajes</strong>\
                                 </a>\
                             </div>\
                         </li>');
      }});
      }
      
      getMessages();
      setInterval("getMessages()",60000);
      
      var FormInputMask = function () {
      
      var handleInputMasks = function () {
         
         $(".phone").inputmask("99-9999-9999", {
             placeholder: " ",
             clearMaskOnLostFocus: true
         }); 
      }
      
      
      
      return {
         //main function to initiate the module
         init: function () {
             handleInputMasks();
         }
      };
      
      }();
      
      jQuery(document).ready(function() {
      FormInputMask.init(); // init metronic core componets
      });
      
      <?php if($this->UserAuth->getGroupId() == 5 || $this->UserAuth->getGroupId() == 6):?>
      jQuery(document).ready(function() {
        $("body").addClass("mini-navbar");
        $(".ca-menu-title").css('display', 'none');
      });
      <?php endif;?>
      
      
    </script>
  </body>
</html>

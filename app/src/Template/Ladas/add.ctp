<div class="panel-heading">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title"> <?= __("Add Lada") ?> </h4>
</div>
<?= $this->Form->create($lada , [ 'class'=>''] ); ?>
<div class="modal-body">
  <div class="row ca-forms">
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label class="required"><?=  __('Lada'); ?></label>
            <div>
              <?= $this->Form->input('lada', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="last-btns">
            <button type="button" class="btn btn-cancel" data-dismiss="modal"><?php echo __('Cancel') ?></button>
            <?= $this->Form->button( __('Save' ) , ['class'=>'btn btn-save'  ]) ?>
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




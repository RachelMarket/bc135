<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Agregar Unidad'), ['action' => 'add']); ?></li>
        <li><?= $this->Html->link(__('Listar ClientesServicios'), ['controller' => 'ClientesServicios', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => ' ClientesServicios', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Partidas'), ['controller' => 'Partidas', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Partida'), ['controller' => ' Partidas', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => ' Servicios', 'action' => 'add']); ?></li>
                </ul>
<?php $this->end(); ?>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="col-md-4">
                        <h2> Unidades</h2>
                    </div>
                    <div class="ibox-tools col-md-4 pull-right">
                        <a href="/unidades/add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Unidad</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table ata-order='[[ 1, "asc" ]]' data-page-length='50' class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id'); ?></th>
                                                                <th><?= $this->Paginator->sort('nombre'); ?></th>
                                                                <th class="actions"><?= __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($unidades as $unidad): ?>
                            <tr>
                                                                <td><?= $this->Number->format($unidad->id) ?></td>
                                                                            <td><?= h($unidad->nombre) ?></td>
                                                        <!-- <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $unidad->id], ['title' => __('Ver'), 'class' => 'btn btn-default fa fa-eye']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $unidad->id], ['title' => __('Editar'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $unidad->id], ['confirm' => __('Seguro que quiere borrar # {0}?', $unidad->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>
            </td> -->
            <td class="actions">
                <div class="dropdown">
                   <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown"><i class="fa fa-bars"></i>
                    <!-- <span class="caret"></span> --></button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                      <li role="presentation"><?= $this->Html->link('<i class="fa fa-eye"></i>&nbsp;Agregar', ['action' => 'view', $unidad->id], ['title' => __('Ver'), "escape" => false]) ?></li>
                      <li role="presentation"><?= $this->Html->link('<i class="fa fa-pencil"></i>&nbsp;Editar', ['action' => 'edit', $unidad->id], ['title' => __('Editar'), "escape" => false]) ?></li>
                      <li role="presentation"><?= $this->Form->postLink('<i class="fa fa-trash"></i>&nbsp;Borrar', ['action' => 'delete', $unidad->id], ['confirm' => __('Seguro que quiere borrar # {0}?', $unidad->id), 'title' => __('Delete'), "escape" => false]) ?></li>
                    </ul>
                </div>
                </td>
        </tr>

        <?php endforeach; ?>
    </tbody>
</table>
<!-- <div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div> -->


<script type="text/javascript">
        $(document).ready(function() {
            $('.dataTables-example').dataTable({
                responsive: true,
                "dom": 'T<"clear">lfrtip',
                "lengthChange": false,
                 "footerCallback": function( tfoot, data, start, end, display ) {
                    $(tfoot).html( "" );
                }
            });
        });

</script>
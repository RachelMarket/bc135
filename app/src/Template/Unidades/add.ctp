<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
        <li><?= $this->Html->link(__('Listar Unidades'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['controller' => 'ClientesServicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => 'ClientesServicios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Partidas'), ['controller' => 'Partidas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Partida'), ['controller' => 'Partidas', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => 'Servicios', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>
<?= $this->Form->create($unidad); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Unidad']) ?></legend>
    <?php
        echo $this->Form->input('nombre');
                ?>
</fieldset>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>
<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Editar Unidad'), ['action' => 'edit', $unidad->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Eliminar Unidad'), ['action' => 'delete', $unidad->id], ['confirm' => __('Are you sure you want to delete # {0}?', $unidad->id)]) ?> </li>
    <li><?= $this->Html->link(__('Listar Unidades'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('Nuevo Unidad'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['controller' => 'ClientesServicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => 'ClientesServicios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Partidas'), ['controller' => 'Partidas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Partida'), ['controller' => 'Partidas', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => 'Servicios', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>

<h2><?= h($unidad->nombre) ?></h2>
<div class="row">
        <div class="col-lg-5">
                                    <h6><?= __('Nombre') ?></h6>
                    <p><?= h($unidad->nombre) ?></p>
                                </div>
            <div class="col-lg-2">
                    <h6><?= __('Id') ?></h6>
                <p><?= $this->Number->format($unidad->id) ?></p>
                </div>
            </div>
<ul id="myTab" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#ClientesServicios" id="ClientesServicios-tab" role="tab" data-toggle="tab" aria-controls="ClientesServicios" aria-expanded="true">ClientesServicios</a>
      </li>
          <li role="presentation" class="">
        <a href="#Partidas" id="Partidas-tab" role="tab" data-toggle="tab" aria-controls="Partidas" aria-expanded="true">Partidas</a>
      </li>
          <li role="presentation" class="">
        <a href="#Servicios" id="Servicios-tab" role="tab" data-toggle="tab" aria-controls="Servicios" aria-expanded="true">Servicios</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="ClientesServicios" aria-labelledBy="ClientesServicios-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($unidad->clientes_servicios)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Cliente Id') ?></th>
                                            <th><?= __('Servicio Id') ?></th>
                                            <th><?= __('Factura Id') ?></th>
                                            <th><?= __('Usuario Id') ?></th>
                                            <th><?= __('Tipo Id') ?></th>
                                            <th><?= __('Fecha') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Unidad Id') ?></th>
                                            <th><?= __('Precio Mxn') ?></th>
                                            <th><?= __('Precio Usd') ?></th>
                                            <th><?= __('Descripcion') ?></th>
                                            <th><?= __('Cantidad') ?></th>
                                            <th><?= __('Notificacion') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($unidad->clientes_servicios as $clientesServicios): ?>
                    <tr>
                                            <td><?= h($clientesServicios->id) ?></td>
                                            <td><?= h($clientesServicios->cliente_id) ?></td>
                                            <td><?= h($clientesServicios->servicio_id) ?></td>
                                            <td><?= h($clientesServicios->factura_id) ?></td>
                                            <td><?= h($clientesServicios->usuario_id) ?></td>
                                            <td><?= h($clientesServicios->tipo_id) ?></td>
                                            <td><?= h($clientesServicios->fecha) ?></td>
                                            <td><?= h($clientesServicios->nombre) ?></td>
                                            <td><?= h($clientesServicios->unidad_id) ?></td>
                                            <td><?= h($clientesServicios->precio_mxn) ?></td>
                                            <td><?= h($clientesServicios->precio_usd) ?></td>
                                            <td><?= h($clientesServicios->descripcion) ?></td>
                                            <td><?= h($clientesServicios->cantidad) ?></td>
                                            <td><?= h($clientesServicios->notificacion) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'ClientesServicios', 'action' => 'view', $clientesServicios->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'ClientesServicios', 'action' => 'edit', $clientesServicios->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'ClientesServicios', 'action' => 'delete', $clientesServicios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientesServicios->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen ClientesServicios asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in " id="Partidas" aria-labelledBy="Partidas-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($unidad->partidas)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Factura Id') ?></th>
                                            <th><?= __('Cantidad') ?></th>
                                            <th><?= __('Unidad Id') ?></th>
                                            <th><?= __('Concepto') ?></th>
                                            <th><?= __('Precio') ?></th>
                                            <th><?= __('Nota') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($unidad->partidas as $partidas): ?>
                    <tr>
                                            <td><?= h($partidas->id) ?></td>
                                            <td><?= h($partidas->factura_id) ?></td>
                                            <td><?= h($partidas->cantidad) ?></td>
                                            <td><?= h($partidas->unidad_id) ?></td>
                                            <td><?= h($partidas->concepto) ?></td>
                                            <td><?= h($partidas->precio) ?></td>
                                            <td><?= h($partidas->nota) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Partidas', 'action' => 'view', $partidas->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Partidas', 'action' => 'edit', $partidas->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Partidas', 'action' => 'delete', $partidas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $partidas->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Partidas asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in " id="Servicios" aria-labelledBy="Servicios-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($unidad->servicios)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Tipo Id') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Unidad Id') ?></th>
                                            <th><?= __('Precio Mxn') ?></th>
                                            <th><?= __('Precio Usd') ?></th>
                                            <th><?= __('Descripcion') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($unidad->servicios as $servicios): ?>
                    <tr>
                                            <td><?= h($servicios->id) ?></td>
                                            <td><?= h($servicios->tipo_id) ?></td>
                                            <td><?= h($servicios->nombre) ?></td>
                                            <td><?= h($servicios->unidad_id) ?></td>
                                            <td><?= h($servicios->precio_mxn) ?></td>
                                            <td><?= h($servicios->precio_usd) ?></td>
                                            <td><?= h($servicios->descripcion) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Servicios', 'action' => 'view', $servicios->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Servicios', 'action' => 'edit', $servicios->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Servicios', 'action' => 'delete', $servicios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $servicios->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Servicios asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
</div>


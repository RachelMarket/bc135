<?php
  //debug($cliente);
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  $this->start('tb_sidebar');
  $activo=array("NO","SI");
  ?>
<ul class="nav nav-sidebar">
  <li><?= $this->Html->link(__('Editar Cliente'), ['action' => 'edit', $cliente->id]) ?> </li>
  <li><?= $this->Form->postLink(__('Eliminar Cliente'), ['action' => 'delete', $cliente->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cliente->id)]) ?> </li>
  <li><?= $this->Html->link(__('Listar Clientes'), ['action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Nuevo Cliente'), ['action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Paises'), ['controller' => 'Paises', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Pais'), ['controller' => 'Paises', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Monedas'), ['controller' => 'Monedas', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Moneda'), ['controller' => 'Monedas', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Facturas'), ['controller' => 'Facturas', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Factura'), ['controller' => 'Facturas', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => 'Servicios', 'action' => 'add']) ?> </li>
</ul>
<?php $this->end(); ?>
<!-- <h2><?= h($cliente->id) ?></h2>-->


<div class="ibox float-e-margins">
  <div class="ibox-title">
    <!--<?php 
      $columsize = [];
      if ($cliente->activo) { $columsize['col-1'] = 6; $columsize['col-2'] = 6; } else {
          $columsize['col-1'] = 10;
          $columsize['col-2'] = 2;
      } ?>
      <div class="col-md-<?= $columsize['col-1'] ?> "></div>-->
    <h2><?= h($cliente->nombre) ?></h2>
    <!--<div class="col-md-<?= $columsize['col-2'] ?>"></div>-->
            
  </div>
  <div class="ibox-content">
    <div class="col-md-12">
      <div class="last-btns ttf">
        <? if($cliente->activo):?>
        <a href="<?= "/clientes_servicios/add/3/$cliente->id" ?>">
        <button type="button" onclick="registrar_servicio();" class="btn btn-cancel btn-back">Registrar Servicio</button>
        </a>
        <a href="<?= "/clientes_servicios/add_free/$cliente->id" ?>"><button type="button" onclick="registrar_servicio();" class="btn btn-cancel btn-back">Registrar Servicio Libre</button></a>
        <?php endif;?>
        <?php if($this->UserAuth->isAdmin() || strpos($this->UserAuth->getUser()['User']['user_group_id'], '9') !== FALSE ):?>
        <div class="mx-100" style="float:right;margin-left:4px;">
          <div class="dropdown">
            <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <? if($cliente->activo):?>
              <li role="presentation"><a href="/facturas/add/<?php echo $cliente->id; ?>" title="Agregar Factura Manual">Agregar Factura Manual</a></li>
              <li><a href="<?= "/clientes_servicios/add/2/".$cliente->id ?>">Agregar Servicio Renta</a></li>
              <?php endif;?>
              <li><?= $this->Html->link(__('Editar Cliente'), ['controller' => 'Clientes', 'action' => 'edit',$cliente->id]) ?></li>
              <li>
                <?php if($cliente->activo):?>
                <form name="post_58c1ee532166d688913880" style="display:none;" method="post" action="/clientes/suspender/<?=$cliente->id?>/0"><input type="hidden" name="_method" value="POST">
                </form>
                <a href="#" class="suspender" data-id = "<?= $cliente->id ?>"> Suspender</a>
                <?php else:?>
                <?= $this->Form->postLink('Activar', ['action' => 'suspender', $cliente->id,1], ['confirm' => '¿Deseas activar este cliente?', 'title' => __('Activar Cliente'), "escape" => false]) ?>
                <?php endif;?>
              </li>
              <li role="separator" class="divider"></li>
              <li><?= $this->Form->postLink('Eliminar', ['action' => 'delete', $cliente->id], ['confirm' => '¿Deseas eliminar este cliente?', 'title' => __('Eliminar Cliente'), "escape" => false]) ?></li>
              <li>
                
                <?= $this->Html->link( $cliente->sendEmail == TRUE ? __('Reenviar invitación') : __('Enviar invitación')  , ['controller' => 'Clientes', 'action' => 'sendNotificationsMail',$cliente->id]) ?>
                
              </li>
            </ul>
          </div>
        </div>
        <?php endif; ?>
      </div>
    </div>
    <!--botones-->
    <div class="">
      <?php if(in_array($this->UserAuth->getGroupId(),[1,4,8,9])):?>
      <ul id="myTabGeneral" class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
          <a href="#DatosGenerales" id="DatosGenerales-tab" role="tab" data-toggle="tab" aria-controls="DatosGenerales" aria-expanded="true">Datos Generales</a>
        </li>
        <li role="presentation" class="">
          <a href="#Contactos" id="Contactos-tab" role="tab" data-toggle="tab" aria-controls="Contactos" aria-expanded="true">Contactos</a>
        </li>
        <li role="presentation" class="">
          <a href="#Asociados" id="Asociados-tab" role="tab" data-toggle="tab" aria-controls="Asociados" aria-expanded="true">Asociados</a>
        </li>
      </ul>
      <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="DatosGenerales" aria-labelledBy="DatosGenerales-tab">
          <div class="related row">
            <div class="col-md-12">
              <?php echo $this->element('all_datos_clientes'); ?>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade in" id="Contactos" aria-labelledBy="Contactos-tab">
          <div class="related row">
            <div class="col-md-12">
              <br>
              <div class="ibox-tools col-md-12 pull-right">
                <button id="listButtonContact" class="btn btn-cancel btn-back">Listado</button>
              </div>
              <div class="ibox-tools col-md-12 pull-right">
                <button id="addButtonContact" class="btn btn-cancel btn-back">Agregar Contacto</button>
              </div>
              <br>
              <br>
              <div id="contentContactos" class="ibox-content ca-xvg">
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox float-e-margins no-mar">
                    <div id="viewContact">
                    </div>
                    <div id="addContacto" class="ibox-content">
                    </div>
                    <div id="editContacto" class="ibox-content">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade in " id="Asociados" aria-labelledBy="Asociados-tab">
          <div class="related row">
            <?php echo $this->element('all_asociados'); ?>
            <div id="addClienteAsociado">
              <?= $this->Form->create(null,array('id' => 'formAsociados', 'action'=>'addAsociados', 'method'=>'post')); ?>
              <fieldset>
                <div class="row">
                  <div class="col-md-12">
                    <div class="ibox float-e-margins no-mar">
                      <div class="row ca-forms">
                        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
                          <div class="row">
                            <div class="col-md-12 mtp-40">
                              <?= $this->Form->input('cliente_id', ['type'=>'hidden','class' => 'form-control','div'=>false,'value'=>$id]); ?>
                              <?= $this->Form->input('nombre', ['class'=>'form-control','div'=>false,'label'=>'Nombre']); ?>
                              <label id="_span" style="font-style:italic; font-size:8px; color:red">Nombre requerido</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <?= $this->Form->input('comentario', ['type'=>'textarea','class' => 'form-control', 'div'=>false, 'label'=>'Comentario']); ?>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="last-btns">
                                <button id="cancelButton" type="button" class="btn btn-cancel">Cancelar</button>
                                <?= $this->Form->button(__('Guardar'),['type'=>'button','id'=>'saveAsociado', 'class'=>'btn btn-save']) ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <?= $this->Form->end() ?>
            </div>
          </div>
            <div id="editClienteAsociado">
              <?= $this->Form->create(null, ['id' => 'formEdit']); ?>
              <fieldset>
                <div class="row">
                  <div class="col-md-4">
                    <?= $this->Form->input('cliente_id', ['type'=>'hidden','class' => 'form-control','div'=>false,'value'=>$id]); ?>
                    <?= $this->Form->input('nombre-asociado', ['class'=>'form-control','div'=>false,'label'=>'Nombre']); ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <?= $this->Form->input('comentario-asociado', ['type'=>'textarea','class' => 'form-control', 'div'=>false, 'label'=>'Comentario']); ?>
                  </div>
                </div>
                <div style="top: 10px;" class="ibox-tools pull-right">
                  <button id="btnCancel" type="button" class="btn btn-cancel">Cancelar</button>
                  <?= $this->Form->button(__('Guardar'),['class'=>'btn btn-save']) ?>
                </div>
              </fieldset>
              <?= $this->Form->end() ?>
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>





 
<!-- Modal servicios -->
<div class="modal fade" id="servicios">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Movimientos:</h4>
      </div>
      <div class="modal-body">
        <form>
          Servicios:
          <select class="form-control" onchange="anadir_contenido_modal(this)" id="sevicios-select">
            <option>Seleccione un servicio a registrar</option>
            ;
            <optgroup label="-Renta"></optgroup>
            <?php foreach ($servicios_renta as $renta): ?>
            <option data-type="<?= $renta['tipo_id'] ?>" value="<?= $renta['id'] ?>"><?= $renta['nombre'] ?></option>
            ";
            <? endforeach; ?>    
            <optgroup label="-Adicional"></optgroup>
            <?php foreach ($servicios_adicional as $adicional): ?>
            <option data-type="<?= $adicional['tipo_id'] ?>" value="<?= $adicional['id'] ?>"><?= $adicional['nombre'] ?></option>
            ";
            <?php endforeach; ?>   
          </select>
          <div class="form-content">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Registrar movimiento</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modalSuspender" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
          <h2 class="modal-title lmss"><?= __('Mensaje de Advertencia') ?></h2>
      </div>
        <div class="modal-body">
          <span> "Si desactiva este cliente, la información de telefonía y accesos se desligará de sus contactos"</span>
        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col-md-12">
              <div class="last-btns">
                <button type="button" id="activoCancel" class="btn btn-cancel" data-dismiss="modal">Cancelar</button>
                <button type="button" id="activoSubmit" class="btn btn-save" data-dismiss="modal">OK</button>
              </div>
            </div>
          </div>
        </div>
      <!-- /.modal-header -->
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
  $( document ).ready(function() {

      var url = window.location.href.split('/');

      if ( url[6] != undefined ) {
        
          activaTab(url[6]);
      }

         $('#edo-cuenta').on('click', function(ev){
              ev.preventDefault();
              var date = $('#meses').val().split("-");
              var month = date[0];
              var year = date[1];
              window.open('/clientes/viewedodecuenta/<?php echo $cliente->id; ?>/'+month+'/'+year,'_blank');
          });
  
         $('#pdf-reservaciones').on('click', function(ev){
              ev.preventDefault();
              var date = $('#meses').val().split("-");
              var month = date[0];
              var year = date[1];
              window.open('/reservaciones/reservacionespdf/<?php echo $cliente->id; ?>/'+month+'/'+year,'_blank');
          });
  
         dateChanged();
  
         $("#contentContactos").load("/Contactos/index/<?php echo $cliente->id; ?>");
  
         $('.suspender').on('click', function(){  $('#modalSuspender').modal('show'); });
  
         $('#activoSubmit').on('click', function(){
              document.post_58c1ee532166d688913880.submit(); 
              //event.returnValue = false; 
              return false;
         });
  
  });
  
  $('#meses').datepicker({ 
      autoclose: true,
      format: "mm-yyyy", 
      viewMode: "months", 
      minViewMode: "months"
      });
  
   $('#meses').on('changeDate', dateChanged);
   function dateChanged(ev){
      //event.preventDefault();
      var mes = $('#meses').val();
      var cliente_id = "<?php echo $cliente->id ?>";
      var moneda_id = "<?php echo $cliente->moneda_id ?>";
      var existen=0;
       $.ajax({
              type: 'POST',
              url: '<?php echo $this->Url->build(["controller" => "Clientes","action" => "filtroClientesServiciosMensual"]); ?>',
              data: {cliente_id:cliente_id, mes:mes},
              dataType: 'json',
              success: function(data){
  
                  var saldo = 0;
                  var tablaServicios = $("#Servicios .related table tbody").empty();
                  var subtotal = 0;
                  var moneda = "MXN";
                  if(moneda_id == 1){ moneda = "USD";}
  
                   $.each(data, function(i, respuesta){
  
                      if(moneda_id == 1){
                          if(!respuesta.factura || respuesta.factura.festado_id != 3){
                              saldo += respuesta.cantidad*respuesta.precio_usd;
                          }
                          moneda = "USD";
                          subtotal = (respuesta.cantidad*respuesta.precio_usd).toFixed(2)+" "+moneda;
                          precio=respuesta.precio_usd;
                      }
                      if(moneda_id == 2){
                          if(!respuesta.factura || respuesta.factura.festado_id != 3){
                              saldo += respuesta.cantidad*respuesta.precio_mxn;
                          }
                          moneda = "MXN";
                          subtotal = (respuesta.cantidad*respuesta.precio_mxn).toFixed(2)+" "+moneda;
                          precio=respuesta.precio_mxn;
                      }
                      utcArray = respuesta.fecha.split("+");
                      fecha = utcArray[0].replace("T"," ");
                      
                      console.log('FACT:'+respuesta.factura);
                      console.log(respuesta.factura);
  
                      numero_factura="";
                      acciones='<form name="post_'+respuesta.id+'" style="display:none;" method="post" action="/clientes/delete_servicio/'+respuesta.id+'/'+cliente_id+'"><input type="hidden" name="_method" value="POST"></form><a href="#" title="Eliminar Servicio" onclick="if (confirm(&quot;\u00bfDeseas eliminar el servicio?&quot;)) { document.post_'+respuesta.id+'.submit(); } event.returnValue = false; return false;"><i class="fa fa-trash-o"></i>&nbsp;Eliminar</a>';
                      if(respuesta.factura_id != null){
                          numero_factura='<a href="/facturas/download/'+respuesta.factura.id+'" target="_blank">'+respuesta.factura.serie+'-'+respuesta.factura.folio+'</a>';
  
                          //Si la factura está cancelada no muestro el folio
                          if(respuesta.factura.festado_id == 3){
                              numero_factura='<a href="/facturas/download/'+respuesta.factura.id+'" target="_blank">Cancelada</a>';
                          }
  
                          acciones="";
                      }
  
                      $(tablaServicios).append("<tr>"
                                                  +"<td>"+fecha+"</td>"
                                                  +"<td>"+respuesta.nombre+"</td>"
                                                  +"<td>$"+precio.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
                                                  +"<td>"+respuesta.cantidad+"</td>"
                                                  +"<td>$"+(subtotal).replace(/\B(?=(\d{3})+(?!\d))/g, ",")+"</td>"
                                                  +"<td>"+numero_factura+"</td>"
                                                  +"<td>"+acciones+"</td>"
                      );
                      existen=1;
                   });
                   $("#saldo_mes").html("$"+saldo.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+moneda); 
  
                   if(!existen){
                      $(tablaServicios).append('<tr><td colspan="8"><h4><?= __('No existen Servicios asociados') ?></h4></td></tr>');
                  }
      }
       });
       
   }
  

function activaTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};
  
</script>

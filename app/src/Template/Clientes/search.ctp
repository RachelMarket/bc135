<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="col-md-4">
                        <h2> Resultados</h2>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th><?= __('Nombre') ?></th>
                                <th><?= __('Cliente') ?></th>
                                <th><?= __('Centro de Negocios') ?></th>
                                <th><?= __('Comentario') ?></th>
                                <th><?= __('Tipo') ?></th>
                                <th><?= __('Contacto Primario') ?></th>
                                <th><?= __('Contacto Secundario') ?></th>
                                <th><?= __('Estatus') ?></th>
                            </tr>
                        </thead>
                        <?php if(!empty($datos)): ?>
                            <?php foreach ($datos as $row): ?>

                                <tr class="<?php $style=''; if($row['status'] == 'Suspendido'){ $style='background-color: #F08080;'; 
                                        echo 'danger" style="background-color: #F08080;';
                                    }?>">

                                    <td><?= $row['nombre'] ?></td>
                                    <td><?= $this->Html->link($row['cliente'],['controller' => 'clientes', 'action' => 'view', $row['id']]);?></td>
                                    <td><?= $row['centro'] ?></td>
                                    <td><?= $row['comentario'] ?></td>
                                    <td><?= $row['tipo'] ?></td>
                                    <td><?= $row['primer_contacto'] ?></td>
                                    <td><?= $row['segundo_contacto'] ?></td> 
                                    <td><?= $row['status'] ?></td>                                   
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td align="center" colspan="8"><span style="font-size: 14px; font-family: 'open sans', 'Helvetica Neue', Helvetica, Arial, sans-serif">No se encontró ningún registro de <?php echo $filtro; ?> </span></td>
                            </tr>
                        <?php endif; ?>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
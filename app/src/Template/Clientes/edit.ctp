<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  $this->start('tb_sidebar');
  ?>
<ul class="nav nav-sidebar">
  <li><?=
    $this->Form->postLink(
    __('Eliminar'),
    ['action' => 'delete', $cliente->id],
    ['confirm' => __('Are you sure you want to delete # {0}?', $cliente->id)]
    )
    ?></li>
  <li><?= $this->Html->link(__('Listar Clientes'), ['action' => 'index']) ?></li>
  <li><?= $this->Html->link(__('Listar Paises'), ['controller' => 'Paises', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Pais'), ['controller' => 'Paises', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Monedas'), ['controller' => 'Monedas', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Moneda'), ['controller' => 'Monedas', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Facturas'), ['controller' => 'Facturas', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Factura'), ['controller' => 'Facturas', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => 'Servicios', 'action' => 'add']) ?> </li>
</ul>
<?php $this->end(); ?>
<div class="ibox float-e-margins">
  <div class="ibox-title">
    <?= $this->Form->create($cliente, ['id' => 'FormAddClient']); ?>
    <h2><?= __('Editar {0}', ['Cliente']) ?></h2>
    <a class="btn btn-cancel btn-back" href="<?= $this->request->referer(); ?>">Cancelar</a>
  </div>
  <div class="ibox-content">
    <div class="row ca-forms mtp-40">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('nombre', array('class' => 'form-control','div'=>false,'label'=>'Nombre Comercial')); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('tipo_cliente_id', ['options' => $tipoclientes]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?php $dias=range(0,28);
              unset($dias[0]);?>
            <?= $this->Form->input('dia_pago', ['class' => 'form-control','div'=>false,'label'=>'Dia de pago','options'=>$dias,'value'=>5]); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('formasdepago_id',['label'=> 'Forma de pago' , 'options' => $formasdepagos ]); ?>
            
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('moneda_id', ['options' => $monedas]); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('cuentabanco',['label'=> 'Cuenta Banco'] ); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('centro_id', ['options' => $centros]); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('oficina', [  'class' => 'form-control','div'=>false,'label'=>'Oficina Asignada']); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <?= $this->Form->input('activo',['checked'=>'checked']); ?>
            </div>
          </div>
        </div>
        <h5 class="div-line">Contacto primario</h5>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('primario_persona_de_contacto',array('class' => 'form-control','div'=>false,'label'=>'Persona de contacto')); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('primario_correo_electronico',array('class' => 'form-control','div'=>false,'label'=>'Correo electrónico')); ?>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('primario_telefono_oficina',array('class' => 'form-control','div'=>false,'label'=>'Teléfono Oficina')); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('primario_telefono_movil',array('class' => 'form-control','div'=>false,'label'=>'Teléfono Móvil')); ?>
          </div>
        </div>

        <h5 class="div-line">Contacto secundario</h5>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('secundario_persona_de_contacto',array('class' => 'form-control','div'=>false,'label'=>'Persona de contacto')); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('secundario_correo_electronico',array('class' => 'form-control','div'=>false,'label'=>'Correo electrónico')); ?>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('secundario_telefono_oficina',array('class' => 'form-control','div'=>false,'label'=>'Teléfono Oficina')); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('secundario_telefono_movil',array('class' => 'form-control','div'=>false,'label'=>'Teléfono Movil')); ?>
          </div>
        </div>

        <h5 class="div-line">Datos Fiscales</h5>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('razon_social',array('class' => 'form-control','div'=>false,'label'=>'Razón social')); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('rfc',array('class' => 'form-control','div'=>false,'label'=>'RFC')); ?>
          </div>
        </div>



        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('calle',array('class' => 'form-control','div'=>false,'label'=>'Calle')); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('colonia',array('class' => 'form-control','div'=>false,'label'=>'Colonia')); ?>
            
          </div>
        </div>


        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('numero',array('class' => 'form-control','div'=>false,'label'=>'No. Exterior')); ?>
              
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('numero_interior',array('class' => 'form-control','div'=>false,'label'=>'Interior')); ?>
          </div>
        </div>



        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('pais_id', ['options' => $paises]); ?>
            </div>
          </div>
          <div class="col-md-6">
            <? //$this->Form->input('estado_id', ['options' => $estados]); ?>
            <?= $this->Form->input('estado_id', array('class' => 'form-control input-select','div'=>false,'label'=>'Estado','options'=>array('' => 'Primero seleccione un país'))); ?>
          </div>
        </div>


        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('municipio_id',array('class' => 'form-control input-select','div'=>false,'label'=>'Municipio','options'=>array('' => 'Primero seleccione un estado'))); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('codigo_postal'); ?>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <?= $this->Form->input('saldo', array('type' => 'number','class' => 'form-control', 'div' => false, 'label' => 'Saldo')); ?>
            </div>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('saldodias', array('type' => 'number','class' => 'form-control', 'div' => false, 'label' => 'Saldo Días')); ?>
          </div>
        </div>
        <div class="col-md-12">
          <div class="last-btns">   
            <a class="btn btn-cancel" href="<?= $this->request->referer(); ?>">Cancelar</a>         
              <?= $this->Form->button(__('Editar'),['type' => 'button', 'id' => 'add-client', 'class'=>'btn btn-save']) ?>          
          </div>
        </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>





<div class="modal fade" id="modalActivo" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
          <h2 class="modal-title lmss"><?= __('Mensaje de Advertencia') ?></h2>
      </div>
        <div class="modal-body">
          <span> "Si desactiva este cliente, la información de telefonía y accesos se desligará de sus contactos"</span>
        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col-md-12">
              <div class="last-btns">
                <button type="button" id="activoCancel" class="btn btn-cancel" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-save" data-dismiss="modal">OK</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="/js/clientes.js"></script>

<script type="text/javascript">
  $('#estado-id').change(function(){
     var selected = $(this).val();
     
     $.ajax({
  
         type: 'POST',
         url: '<?php echo $this->Url->build(["controller" => "municipios","action" => "listMunicipios"]); ?>',
         data: {selected:selected},
         dataType: 'json',
         success: function(data){
             $('#municipio-id').html('');
             $('#municipio-id').append('<option value="">Selecciona un municipio</option>');
             $.each(data, function(k,v){
                     $('#municipio-id').append('<option value="'+k+'">'+v+'</option>');
             });  
         
             
         }
     });
  });    
  
  $('#pais-id').change(function(){
     var selected = $(this).val();
     
     $.ajax({
  
         type: 'POST',
         url: '<?php echo $this->Url->build(["controller" => "estados","action" => "listestados"]); ?>',
         data: {selected:selected},
         dataType: 'json',
         success: function(data){
             $('#estado-id').html('');
             $('#estado-id').append('<option value="">Selecciona un estado</option>');
             $.each(data, function(k,v){
                     $('#estado-id').append('<option value="'+k+'">'+v+'</option>');
             });  
         
             
         }
     });
  });    
  
  $('#tipo-cliente-id').on('change', function(){

    var input1 = $('#saldo');
    var input2 = $('#saldodias');

    setValueSaldos($(this),input1,input2);

  });


  function setValueSaldos(selected, input1, input2 ){

    if( selected.val() == 5 ){
          
          input1.val(<?= SALDO_HORAS_CLIENTE_EJECUTIVO ?>);
          input2.val(<?= SALDO_DIAS_CLIENTE_EJECUTIVO ?>);

        }else if( selected.val() == 6 ) {
          
          input1.val(<?= SALDO_HORAS_CLIENTE_VIRTUAL ?>);
          input2.val(<?= SALDO_DIAS_CLIENTE_VIRTUAL ?>);
        
        }else {
          input1.val(0);
          input2.val(0);
        }

  }
  
  $(document).ready(function(){
    
    var saldo = $('#saldo');
    var saldodias = $('#saldodias');

    var tipoCliente = $('#tipo-cliente-id option:selected');

    //setValueSaldos(tipoCliente,saldo,saldodias);

     //Tomo los estados del pais seleccionado
     var selected = <?php echo $cliente->pais_id;?>;
     
     $.ajax({
  
         type: 'POST',
         url: '<?php echo $this->Url->build(["controller" => "estados","action" => "listestados"]); ?>',
         data: {selected:selected},
         dataType: 'json',
         success: function(data){
             $('#estado-id').html('');
             $('#estado-id').append('<option value="">Selecciona un estado</option>');
             $.each(data, function(k,v){
                     sel='';
                     if(k == <?php echo $cliente->estado_id;?>){
                         sel='selected';
                     }
                     $('#estado-id').append('<option value="'+k+'" '+sel+'>'+v+'</option>');
             });  
         
             
         }
     });
  
     //Ahora tomo ciudades
     selected = <?php echo $cliente->estado_id;?>;
     $.ajax({
  
         type: 'POST',
         url: '<?php echo $this->Url->build(["controller" => "municipios","action" => "listMunicipios"]); ?>',
         data: {selected:selected},
         dataType: 'json',
         success: function(data){
             $('#municipio-id').html('');
             $('#municipio-id').append('<option value="">Selecciona un municipio</option>');
             $.each(data, function(k,v){
                 sel='';
                     if(k == <?php echo $cliente->municipio_id;?>){
                         sel='selected';
                     }
                     $('#municipio-id').append('<option value="'+k+'" '+sel+'>'+v+'</option>');
             });  
         
             
         }
     });
  
     $('#activo').change(function(){ if($(this).is(":checked") == false) {
             $('#modalActivo').modal('show');
         }
     });
  
     $('#activoCancel').click(function(){ $('#activo').prop('checked', true); });
  
  });    
  
  
</script>

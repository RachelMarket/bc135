
<div class="ibox">
    <div class="ibox-title">
        <h2><?= __('Editar {0}', ['Oficina']) ?></h2>
        <a class="btn btn-cancel btn-back" href="<?= $this->request->referer(); ?>">Cancelar</a>
    </div>
    <div class="ibox-content">
    <?= $this->Form->create($oficina , [ 'class'=>'form-horizontal'] ); ?>  
        <div class="um-form-row form-group">
            <div class="um-form-row form-group">
                <label class="col-sm-2 control-label" >
                <?=  __('Centro'); ?>
                </label>
                <div class ="col-sm-3">
                    <?= $this->Form->input('centro_id', [ 'div'=>false, 'label' => false, 'options' => $centros]); ?>
                </div>
            </div>
            <div class="um-form-row form-group">
                <label class="col-sm-2 control-label" >
                <?=  __('Numero'); ?>
                </label>
                <div class="col-sm-3">
                    <?= $this->Form->input('numero', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="um-form-row form-group">
                <label class="col-sm-2 control-label" >
                <?=  __('Capacidad'); ?>
                </label>
                <div class="col-sm-3">                    
                    <?= $this->Form->input('capacidad', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="um-form-row form-group">
                <label class="col-sm-2 control-label" >
                    <?=  __('Precio_minimo'); ?>
                </label>
                <div class="col-sm-3">
                    <?= $this->Form->input('precio_minimo', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>  
            </div>
            <div class="um-form-row form-group">
                <label class="col-sm-2 control-label" >
                    <?=  __('Precio_medio'); ?>
                </label>
                <div class="col-sm-3">
                    <?= $this->Form->input('precio_medio', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="um-form-row form-group">
                <label class="col-sm-2 control-label" >
                    <?=  __('Precio_maximo'); ?>
                </label>
                <div class="col-sm-3">
                    <?= $this->Form->input('precio_maximo', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="um-form-row form-group">
                <label class="col-sm-2 control-label" >
                    <?=  __('Eliminado'); ?>
                </label>
                <div class="col-sm-3">
                    <?= $this->Form->input('eliminado', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>

    <div class="um-button-row">
    <?= $this->Form->button( __('Edit' ) , ['class'=>'btn btn-primary'  ]) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
</div>
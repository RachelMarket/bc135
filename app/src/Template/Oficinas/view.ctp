
<div class="ibox">
    <div class="ibox-title">

        <h5> <?php echo __('oficina Details'); ?> </h5>

        <span class="ibox-tools">
            

            <?= $this->Html->link(__('PDF'), ['action' => 'view', '_ext'=>'pdf' , $oficina->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

            <?= $this->Html->link(__('Edit Oficina'), ['action' => 'edit', $oficina->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

        </span>

    </div>

    <div class="ibox-content">
        
  

<dl class="dl-horizontal">

        
                       
                        <dt><?= __('Centro') ?>:</dt>
                    <dd><?= $oficina->has('centro') ? $this->Html->link($oficina->centro->nombre, ['controller' => 'Centros', 'action' => 'view', $oficina->centro->id]) : '' ?></dd>
                
          
                       
                        <dt><?= __('Numero') ?>:</dt> 
                    <dd><?= h($oficina->numero) ?></dd>
                
          
                       
                        <dt><?= __('Capacidad') ?>:</dt> 
                    <dd><?= h($oficina->capacidad) ?></dd>
                
          
                
    


        
                       
        <dt><?= __('Id') ?>:</dt> 
            <dd><?= $this->Number->format($oficina->id) ?></dd>
         
                       
        <dt><?= __('Precio Minimo') ?>:</dt> 
            <dd><?= $this->Number->format($oficina->precio_minimo) ?></dd>
         
                       
        <dt><?= __('Precio Medio') ?>:</dt> 
            <dd><?= $this->Number->format($oficina->precio_medio) ?></dd>
         
                       
        <dt><?= __('Precio Maximo') ?>:</dt> 
            <dd><?= $this->Number->format($oficina->precio_maximo) ?></dd>
         
               
                
                  
        <dt><?= __('Eliminado') ?>:</dt>
                <dd><?= $oficina->eliminado ? __('Yes') : __('No'); ?></dd>
             
               
    


</dl>

<ul id="myTab" class="nav nav-tabs" role="tablist">
   
</ul>

<div id="myTabContent" class="tab-content">
</div>
</div>
</div>


<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
        <li><?= $this->Html->link(__('Listar Usuarios'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['controller' => 'ClientesServicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => 'ClientesServicios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Mensajes'), ['controller' => 'Mensajes', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Mensaje'), ['controller' => 'Mensajes', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Recuperaciones'), ['controller' => 'Recuperaciones', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Recuperacion'), ['controller' => 'Recuperaciones', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>
<?= $this->Form->create($usuario); ?>
<fieldset>
    <legend><?= __('Add {0}', ['Usuario']) ?></legend>
    <?php
        echo $this->Form->input('uid');
                echo $this->Form->input('nombre');
                echo $this->Form->input('telefono');
                echo $this->Form->input('correo');
                echo $this->Form->input('password');
                echo $this->Form->input('grupo_id', ['options' => $grupos]);
                echo $this->Form->input('activo');
                echo $this->Form->input('foto');
                echo $this->Form->input('randfoto');
                echo $this->Form->input('filepath');
                ?>
</fieldset>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>
<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Agregar Usuario'), ['action' => 'add']); ?></li>
        <li><?= $this->Html->link(__('Listar Grupos'), ['controller' => 'Grupos', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Grupo'), ['controller' => ' Grupos', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar ClientesServicios'), ['controller' => 'ClientesServicios', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => ' ClientesServicios', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Mensajes'), ['controller' => 'Mensajes', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Mensaje'), ['controller' => ' Mensajes', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Recuperaciones'), ['controller' => 'Recuperaciones', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Recuperacion'), ['controller' => ' Recuperaciones', 'action' => 'add']); ?></li>
                </ul>
<?php $this->end(); ?>
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
                        <th><?= $this->Paginator->sort('id'); ?></th>
                        <th><?= $this->Paginator->sort('uid'); ?></th>
                        <th><?= $this->Paginator->sort('nombre'); ?></th>
                        <th><?= $this->Paginator->sort('telefono'); ?></th>
                        <th><?= $this->Paginator->sort('correo'); ?></th>
                        <th><?= $this->Paginator->sort('password'); ?></th>
                        <th><?= $this->Paginator->sort('grupo_id'); ?></th>
                        <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($usuarios as $usuario): ?>
        <tr>
                        <td><?= $this->Number->format($usuario->id) ?></td>
                                    <td><?= h($usuario->uid) ?></td>
                                    <td><?= h($usuario->nombre) ?></td>
                                    <td><?= h($usuario->telefono) ?></td>
                                    <td><?= h($usuario->correo) ?></td>
                                    <td><?= h($usuario->password) ?></td>
                                                    <td>
                                <?= $usuario->has('grupo') ? $this->Html->link($usuario->grupo->nombre, ['controller' => 'Grupos', 'action' => 'view', $usuario->grupo->id]) : '' ?>
                            </td>
                                        <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $usuario->id], ['title' => __('Ver'), 'class' => 'btn btn-default fa fa-eye']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $usuario->id], ['title' => __('Editar'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $usuario->id], ['confirm' => __('Seguro que quiere borrar # {0}?', $usuario->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>
            </td>
        </tr>

        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
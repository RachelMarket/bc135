<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Editar Usuario'), ['action' => 'edit', $usuario->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Eliminar Usuario'), ['action' => 'delete', $usuario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->id)]) ?> </li>
    <li><?= $this->Html->link(__('Listar Usuarios'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('Nuevo Usuario'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Grupos'), ['controller' => 'Grupos', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Grupo'), ['controller' => 'Grupos', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['controller' => 'ClientesServicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => 'ClientesServicios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Mensajes'), ['controller' => 'Mensajes', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Mensaje'), ['controller' => 'Mensajes', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Recuperaciones'), ['controller' => 'Recuperaciones', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Recuperacion'), ['controller' => 'Recuperaciones', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>

<h2><?= h($usuario->nombre) ?></h2>
<div class="row">
        <div class="col-lg-5">
                                    <h6><?= __('Uid') ?></h6>
                    <p><?= h($usuario->uid) ?></p>
                                                    <h6><?= __('Nombre') ?></h6>
                    <p><?= h($usuario->nombre) ?></p>
                                                    <h6><?= __('Telefono') ?></h6>
                    <p><?= h($usuario->telefono) ?></p>
                                                    <h6><?= __('Correo') ?></h6>
                    <p><?= h($usuario->correo) ?></p>
                                                    <h6><?= __('Password') ?></h6>
                    <p><?= h($usuario->password) ?></p>
                                                    <h6><?= __('Grupo') ?></h6>
                    <p><?= $usuario->has('grupo') ? $this->Html->link($usuario->grupo->nombre, ['controller' => 'Grupos', 'action' => 'view', $usuario->grupo->id]) : '' ?></p>
                                                    <h6><?= __('Foto') ?></h6>
                    <p><?= h($usuario->foto) ?></p>
                                                    <h6><?= __('Randfoto') ?></h6>
                    <p><?= h($usuario->randfoto) ?></p>
                                                    <h6><?= __('Filepath') ?></h6>
                    <p><?= h($usuario->filepath) ?></p>
                                </div>
            <div class="col-lg-2">
                    <h6><?= __('Id') ?></h6>
                <p><?= $this->Number->format($usuario->id) ?></p>
                </div>
            <div class="col-lg-2">
                    <h6><?= __('Created') ?></h6>
                <p><?= h($usuario->created) ?></p>
                </div>
            <div class="col-lg-2">
                    <h6><?= __('Activo') ?></h6>
                <p><?= $usuario->activo ? __('Yes') : __('No'); ?></p>
                </div>
    </div>
<ul id="myTab" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#ClientesServicios" id="ClientesServicios-tab" role="tab" data-toggle="tab" aria-controls="ClientesServicios" aria-expanded="true">ClientesServicios</a>
      </li>
          <li role="presentation" class="">
        <a href="#Mensajes" id="Mensajes-tab" role="tab" data-toggle="tab" aria-controls="Mensajes" aria-expanded="true">Mensajes</a>
      </li>
          <li role="presentation" class="">
        <a href="#Recuperaciones" id="Recuperaciones-tab" role="tab" data-toggle="tab" aria-controls="Recuperaciones" aria-expanded="true">Recuperaciones</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="ClientesServicios" aria-labelledBy="ClientesServicios-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($usuario->clientes_servicios)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Cliente Id') ?></th>
                                            <th><?= __('Servicio Id') ?></th>
                                            <th><?= __('Factura Id') ?></th>
                                            <th><?= __('Usuario Id') ?></th>
                                            <th><?= __('Tipo Id') ?></th>
                                            <th><?= __('Fecha') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Unidad Id') ?></th>
                                            <th><?= __('Precio Mxn') ?></th>
                                            <th><?= __('Precio Usd') ?></th>
                                            <th><?= __('Descripcion') ?></th>
                                            <th><?= __('Cantidad') ?></th>
                                            <th><?= __('Notificacion') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($usuario->clientes_servicios as $clientesServicios): ?>
                    <tr>
                                            <td><?= h($clientesServicios->id) ?></td>
                                            <td><?= h($clientesServicios->cliente_id) ?></td>
                                            <td><?= h($clientesServicios->servicio_id) ?></td>
                                            <td><?= h($clientesServicios->factura_id) ?></td>
                                            <td><?= h($clientesServicios->usuario_id) ?></td>
                                            <td><?= h($clientesServicios->tipo_id) ?></td>
                                            <td><?= h($clientesServicios->fecha) ?></td>
                                            <td><?= h($clientesServicios->nombre) ?></td>
                                            <td><?= h($clientesServicios->unidad_id) ?></td>
                                            <td><?= h($clientesServicios->precio_mxn) ?></td>
                                            <td><?= h($clientesServicios->precio_usd) ?></td>
                                            <td><?= h($clientesServicios->descripcion) ?></td>
                                            <td><?= h($clientesServicios->cantidad) ?></td>
                                            <td><?= h($clientesServicios->notificacion) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'ClientesServicios', 'action' => 'view', $clientesServicios->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'ClientesServicios', 'action' => 'edit', $clientesServicios->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'ClientesServicios', 'action' => 'delete', $clientesServicios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientesServicios->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen ClientesServicios asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in " id="Mensajes" aria-labelledBy="Mensajes-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($usuario->mensajes)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Usuario Id') ?></th>
                                            <th><?= __('Mensaje') ?></th>
                                            <th><?= __('Envia') ?></th>
                                            <th><?= __('Fecha') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($usuario->mensajes as $mensajes): ?>
                    <tr>
                                            <td><?= h($mensajes->id) ?></td>
                                            <td><?= h($mensajes->usuario_id) ?></td>
                                            <td><?= h($mensajes->mensaje) ?></td>
                                            <td><?= h($mensajes->envia) ?></td>
                                            <td><?= h($mensajes->fecha) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Mensajes', 'action' => 'view', $mensajes->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Mensajes', 'action' => 'edit', $mensajes->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Mensajes', 'action' => 'delete', $mensajes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mensajes->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Mensajes asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in " id="Recuperaciones" aria-labelledBy="Recuperaciones-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($usuario->recuperaciones)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Usuario Id') ?></th>
                                            <th><?= __('Code') ?></th>
                                            <th><?= __('Created') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($usuario->recuperaciones as $recuperaciones): ?>
                    <tr>
                                            <td><?= h($recuperaciones->id) ?></td>
                                            <td><?= h($recuperaciones->usuario_id) ?></td>
                                            <td><?= h($recuperaciones->code) ?></td>
                                            <td><?= h($recuperaciones->created) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Recuperaciones', 'action' => 'view', $recuperaciones->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Recuperaciones', 'action' => 'edit', $recuperaciones->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Recuperaciones', 'action' => 'delete', $recuperaciones->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recuperaciones->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Recuperaciones asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
</div>


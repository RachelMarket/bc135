<?php echo $this->Html->css('../css/modal_styles.css'); ?>
<div class="ibox">
<div class="ibox-title">
    <?= __('Agregar {0}', ['Cotizacion']) ?>
    <a class="btn btn-cancel btn-back" href="<?= $this->request->referer(); ?>">Cancelar</a>
</div>
<div class="ibox-content">
    <?= $this->Form->create($cotizacion , [ 'class'=>'horizontal'] ); ?>                       
        <div class="row">
            <div class="col-sm-4 form-group">
                <label class="control-label" >
                    <?=  __('Cliente'); ?>
                </label>
                <div>
                    <?= $this->Form->input('cliente', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="col-sm-4 form-group">
                <label class="control-label" >
                    <?=  __('Contacto'); ?>
                </label>
                <div>
                    <?= $this->Form->input('contacto', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="col-sm-4 form-group">
                <label class="control-label" >
                    <?=  __('Centro'); ?>
                </label>
                <div>
                    <?= $this->Form->input('centro_id', [ 'id'=> 'select_centros', 'div'=>false, 'label' => false, 'options' => $centros]); ?>
                </div>
            </div>
        </div>
        <div class="row">
             <div class="col-sm-4 form-group" > 
                <label class="control-label">
                    <?=  __('Correo'); ?>
                </label>
                <div>
                    <?= $this->Form->input('correo', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="col-sm-4 form-group" > 
                <label class="control-label">
                    <?=  __('telefono'); ?>
                </label>
                <div>
                    <?= $this->Form->input('telefono', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
             <div class="col-sm-4 form-group" > 
                <label class="control-label">
                    <?=  __('Movil'); ?>
                </label>
                <div>
                    <?= $this->Form->input('telefono_movil', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 form-group"> 
                <label class="control-label">
                    <?=  __('Vigencia'); ?>
                </label>
                <div>
                    <?= $this->Form->input('vigencia', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="col-sm-3 form-group"> 
                <label class="control-label">
                    <?=  __('Moneda'); ?>
                </label>
                <div>
                    <?= $this->Form->input('moneda_id', [ 'div'=>false, 'label' => false, 'options' => $monedas]); ?>
                </div>
            </div>
           <div class="col-sm-3 form-group"> 
                <label class="control-label">
                    <?=  __('Tipo de Cambio'); ?>
                </label>
                <div>
                    <?= $this->Form->input('tipo_cambio', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
            <div class="col-sm-3 form-group"> 
                <label class="control-label">
                    <?=  __('Aplica promoción'); ?>
                </label>
                <div>
                    <?= $this->Form->input('aplica_promocion', ['id'=> 'aplica_promocion', 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 form-group"> 
                <label class="control-label" >
                    <?=  __('Texto inicial'); ?>
                </label>
                <div>
                    <?= $this->Form->input('texto_inicial', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <label class="control-label">Oficinas</label>
                <div class="pull-right">
                    <button type="button" class="btn btn-cancel btn-back" data-toggle="modal" id="add_oficina" data-target="#modalURL">Agregar oficina</button>
                </div>
                <table class="table ca-table" id="tabla_oficinas">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Número</th>
                            <th>Capacidad</th>
                            <th>Precio mínimo</th>
                            <th>Precio medio</th>
                            <th>Precio máximo</th>
                            <th>Precio regular</th>
                            <th>Precio promocional</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 form-group"> 
                <label class="control-label" >
                    <?=  __('Texto final'); ?>
                </label>
                <div>
                    <?= $this->Form->input('texto_final', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 form-group"> 
                <label class="control-label" >
                    <?=  __('Layout Centro'); ?>
                </label>
                <div>
                    <?= $this->Form->input('layout_centro', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
                </div>
            </div>
        </div>

   

    <!--MODAL ADD OFICINAS A UNA NUEVA COTIZACIÓN-->
    <div id ="modalURL" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title">Agregar Oficina</h3>
                </div>
                <div class="modal-body">
                    <div class="ca-forms">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="required"><?php echo __('Oficina'); ?></label>
                              <div>
                                <select id="select_oficina" class="form-control">
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label><?php echo __('Capacidad'); ?></label>
                              <div>
                                <input type="text" class="form-control" id="capacidad" readonly>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="input_precio_minimo"><?php echo __('Precio mínimo'); ?></label>
                              <div>
                                    <input id="input_precio_minimo" type="radio" name="precio">
                                    <label for="input_precio_minimo" ><span id="precio_minimo"></span></label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="input_precio_medio"><?php echo __('Precio medio'); ?></label>
                              <div>
                                    <input id="input_precio_medio" type="radio" name="precio">
                                    <label for="input_precio_medio"><span id="precio_medio"></span></label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="input_precio_maximo"><?php echo __('Precio máximo'); ?></label>
                              <div>
                                    <input id="input_precio_maximo" type="radio" name="precio">
                                    <label for="input_precio_maximo"><span id="precio_maximo"></span></label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label><?php echo __('Precio Regular'); ?></label>
                                <div>
                                      <input type="text" class="form-control" id="precio_regular">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6 hidden precio-promo">
                              <div class="form-group">
                                <label><?php echo __('Precio Promocional'); ?></label>
                                <div>
                                      <input type="text" class="form-control" id="precio_promocional">
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="last-btns">
                                <button type="button" class="btn btn-cancel" data-dismiss="modal"><?php echo __('Cancel') ?></button>
                              <?= $this->Form->button( __('Save' ) , ['id'=>'save_modal', 'class'=>'btn btn-save'  ]) ?>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="um-button-row">
        <input type="hidden" id="oficinas_array" name="oficinas_array">
        <input type="hidden" id="precio_" name="">
        <?= $this->Form->button( __('Agregar' ) , ['class'=>'btn btn-primary'  ]) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    var flag = false;
    var allOficinas = <?php echo json_encode($allOficinas); ?>;
    
    $('#select_centros').change(function(){
        changeSelectCentros($(this).val());
    });
       


    function changeSelectCentros(id){
        $.ajax({
            type: 'POST',
            url: '<?php echo $this->Url->build(["controller" => "Cotizaciones","action" => "getOficinas"]); ?>',
            data: {centro_id:id},
            dataType: 'json',
            async: false,
            success: function(data){
                allOficinas = data;
                var temp = '';
                $.each(allOficinas, function(key, oficina) {
                    temp += '<option value="'+oficina.id+'">'+oficina.numero+'</option>';
                });
                $('#select_oficina').html(temp);
                changeSelect($('#select_oficina').val());
                $('#tabla_oficinas tbody').html('');
                array = [];
                oficinas = [];
                $('#add_oficina').attr('disabled',false);
                $("input[name='precio']").prop('checked', false); 

            }
        });
    }


    var oficinas = [];
    var array =[];

    function removeOficina(id){
        var index = oficinas.findIndex(i => i.id ==id);
        console.log(oficinas);
        console.log(index);
        if (index > -1) {
            $('#select_oficina').append('<option value="'+id+'">'+array[index].numero+'</option>');
            changeSelect($('#select_oficina').val());
            oficinas.splice(index, 1)
            array.splice(index, 1);
            printOficinas();
            $('#add_oficina').attr('disabled',false);
            $("input[name='precio']").prop('checked', false); 
        }        
        return false;
    }

    function printOficinas(){
        var tbody = '';
        for (var i = 0; i < array.length; i++) {
            tbody += '<tr>'+
                        '<td>'+array[i].id+'</td>'+
                        '<td>'+array[i].numero+'</td>'+
                        '<td>'+array[i].capacidad+'</td>'+
                        '<td>'+array[i].precio_minimo+'</td>'+
                        '<td>'+array[i].precio_medio+'</td>'+
                        '<td>'+array[i].precio_maximo+'</td>'+
                        '<td>'+array[i].precio_regular+'</td>'+
                        '<td>'+array[i].precio_promocional+'</td>'+
                        '<td><a onclick="removeOficina('+array[i].id+')"><i class="fa fa-times-circle"></i></a></td>'+
                    '</tr>';
        }
        $('#tabla_oficinas tbody').html(tbody);
        $('#oficinas_array').val(JSON.stringify(oficinas));
    }

    function changeSelect(id){
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->Url->build(["controller" => "Cotizaciones","action" => "getInfoOficinas"]); ?>',
                data: {oficina_id:id},
                dataType: 'json',
                success: function(data){
                    var datas = data[0];
                    $('input#capacidad').val(datas.capacidad);
                    $('#precio_minimo').html('$ '+datas.precio_minimo);
                    $('#precio_medio').html('$ '+datas.precio_medio);
                    $('#precio_maximo').html('$ '+datas.precio_maximo);
                    $('input#precio_regular').val(''); 
                    $('input#precio_promocional').val('');
                }
            });
        }


    
    $(document).ready(function(){
        
        $('#aplica_promocion').change(function(){
            if($(this).prop('checked')){
                flag = true;
                $('.precio-promo').removeClass('hidden'); 
            }else{
                flag = false;
                $('.precio-promo').addClass('hidden'); 
            }
        });

        changeSelectCentros($('#select_centros').val());

        $('input[type=radio][name=precio]').change(function() {
            $('input#precio_regular').val(parseFloat($(this).siblings('label').text().replace('$', '')));
        });
        
        $('#select_oficina').change(function(){
            changeSelect($(this).val());
        });
        
        $('#save_modal').click(function(){
            if(oficinas.findIndex(i => i.id == $('#select_oficina').val()) < 0){
                var valor = $('#select_oficina').val();
                var temp = {'id': valor, 'precio_regular': $('#precio_regular').val(), 'precio_promocional': $('#precio_promocional').val()};
                oficinas.push(temp);

                var dump = [];
                dump['id'] = $('#select_oficina').val(); 
                dump['numero'] = $('#select_oficina option:checked').text(); 
                dump['capacidad'] = $('#capacidad').val(); 
                dump['precio_minimo'] = parseFloat($('#precio_minimo').text().replace('$','')); 
                dump['precio_medio'] = parseFloat($('#precio_medio').text().replace('$','')); 
                dump['precio_maximo'] = parseFloat($('#precio_maximo').text().replace('$','')); 
                dump['precio_regular'] = parseFloat($("input[name='precio']:checked").siblings('label').text().replace('$','')); 
                if(flag == true){
                    dump['precio_promocional'] = $('#precio_promocional').val(); 
                }else{
                    dump['precio_promocional'] = ''; 
                }
                array.push(dump)
                printOficinas();
                $("select#select_oficina option[value='"+valor+"']").remove();
            }

            if($('#select_oficina option').length == 0){
                $('#add_oficina').attr('disabled',true);
            }else{
                changeSelect($('#select_oficina').val());
            }
            $(this).siblings('.btn-cancel').click();

        });
    });
</script>
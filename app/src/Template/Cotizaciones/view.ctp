<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cotizacion $cotizacion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cotizacion'), ['action' => 'edit', $cotizacion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cotizacion'), ['action' => 'delete', $cotizacion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cotizacion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cotizaciones'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cotizacion'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Oficinas'), ['controller' => 'Oficinas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Oficina'), ['controller' => 'Oficinas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Centros'), ['controller' => 'Centros', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Centro'), ['controller' => 'Centros', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cotizaciones view large-9 medium-8 columns content">
    <h3><?= h($cotizacion->cliente) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Cliente') ?></th>
            <td><?= h($cotizacion->cliente) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contacto') ?></th>
            <td><?= h($cotizacion->contacto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Centro') ?></th>
            <td><?= $cotizacion->has('centro') ? $this->Html->link($cotizacion->centro->nombre, ['controller' => 'Centros', 'action' => 'view', $cotizacion->centro->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Correo') ?></th>
            <td><?= h($cotizacion->correo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telefono') ?></th>
            <td><?= h($cotizacion->telefono) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telefono Movil') ?></th>
            <td><?= h($cotizacion->telefono_movil) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Moneda') ?></th>
            <td><?= h($cotizacion->moneda) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($cotizacion->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= $cotizacion->has('usuario') ? $this->Html->link($cotizacion->usuario->username, ['controller' => 'Usuarios', 'action' => 'view', $cotizacion->usuario->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cotizacion->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Vigencia') ?></th>
            <td><?= $this->Number->format($cotizacion->vigencia) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo Cambio') ?></th>
            <td><?= $this->Number->format($cotizacion->tipo_cambio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Vence') ?></th>
            <td><?= h($cotizacion->vence) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creada') ?></th>
            <td><?= h($cotizacion->creada) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Aplica Promocion') ?></th>
            <td><?= $cotizacion->aplica_promocion ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Layout Centro') ?></th>
            <td><?= $cotizacion->layout_centro ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Eliminado') ?></th>
            <td><?= $cotizacion->eliminado ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Texto Inicial') ?></h4>
        <?= $this->Text->autoParagraph(h($cotizacion->texto_inicial)); ?>
    </div>
    <div class="row">
        <h4><?= __('Texto Final') ?></h4>
        <?= $this->Text->autoParagraph(h($cotizacion->texto_final)); ?>
    </div>
</div>

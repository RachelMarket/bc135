<h2>Arq. Celia Isabel Fuentes Matus</h2>
<div>
	<?php echo h($cotizacion->texto_inicio); ?>
</div>
<table>
<thead>
	<tr>
		<th>Número</th>
		<th>Capacidad</th>
		<th>Precio normal</th>
		<th>Precio promocional</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($cotizacion->oficinas as $cotioficina): ?>
		<tr>
			<td><?= h($cotioficina->numero) ?></td>
			<td><?= h($cotioficina->capacidad) ?></td>
			<td>$<?= h($cotioficina->_joinData->precio_regular) ?></td>
			<td>$<?= h($cotioficina->_joinData->precio_promocional) ?></td>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>
<div>
	<?php echo h($cotizacion->texto_fin); ?>
</div>

<h2><?php echo __('Cotización'); ?></h2>
	<dl>
		<dt><?php echo __('Cliente'); ?></dt>
		<dd>
			<?php echo h($cotizacion->cliente); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contacto'); ?></dt>
		<dd>
			<?php echo h($cotizacion->contacto); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Centro'); ?></dt>
		<dd>
			<?php echo h($cotizacion->centro_id); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telefono'); ?></dt>
		<dd>
			<?php echo h($cotizacion->telefono); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vence'); ?></dt>
		<dd>
			<?php echo h($cotizacion->vence); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($cotizacion->status); ?>
			&nbsp;
		</dd>
	</dl>
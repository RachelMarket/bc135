<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cotizacion $cotizacion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cotizacion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cotizacion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Cotizaciones'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Oficinas'), ['controller' => 'Oficinas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Oficina'), ['controller' => 'Oficinas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Centros'), ['controller' => 'Centros', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Centro'), ['controller' => 'Centros', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cotizaciones form large-9 medium-8 columns content">
    <?= $this->Form->create($cotizacion) ?>
    <fieldset>
        <legend><?= __('Edit Cotizacion') ?></legend>
        <?php
            echo $this->Form->control('cliente');
            echo $this->Form->control('contacto');
            echo $this->Form->control('centro_id', ['options' => $centros]);
            echo $this->Form->control('correo');
            echo $this->Form->control('telefono');
            echo $this->Form->control('telefono_movil');
            echo $this->Form->control('vigencia');
            echo $this->Form->control('vence');
            echo $this->Form->control('moneda');
            echo $this->Form->control('status');
            echo $this->Form->control('usuario_id', ['options' => $usuarios]);
            echo $this->Form->control('tipo_cambio');
            echo $this->Form->control('aplica_promocion');
            echo $this->Form->control('texto_inicial');
            echo $this->Form->control('texto_final');
            echo $this->Form->control('layout_centro');
            echo $this->Form->control('creada');
            echo $this->Form->control('eliminado');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<div class="modal inmodal" id="modal_cambiar_formapago" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-clock-o modal-icon"></i> -->
                <h4 class="modal-title"> <?= __("Cambiar Forma Pago / Cuenta Banco") ?></h4>
                
            </div>
            <div id="modal_cambiar_forma" class="modal-body">

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function() {
	

	$('.cambiar_forma_pago').click( function(){

		var factura_id = $(this).data('factura_id');
		
		$('#modal_cambiar_forma').html('Buscando...');
		$('#modal_cambiar_forma').load(  urlForJs + "Facturas/cambiarformapagocuentabanco/"+factura_id  );
	});

} );

</script>
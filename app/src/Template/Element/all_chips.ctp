<?php echo $this->Html->css('../css/modal_styles.css'); ?>

<div id="updateChipsIndex">
<div class="ibox float-e-margins">
  <div class="ibox-title">
    <h2><?php echo __('Chips')?> </h2>
    <?php if($this->UserAuth->isAdmin()):?>
    <?= $this->Html->link('Agregar Chip', '/Chips/add/', ['class' => 'btn btn-cancel btn-back','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?>
    <?php endif; ?>
  </div>
  <div class="ibox-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox float-e-margins no-mar">
          <div class="ibox-content">
            <?php echo $this->Search->searchForm('Chips', ['legend'=>false, 'updateDivId'=>'updateChipsIndex']); ?>
            <div class="table-responsive">
              <table class="table ca-table">
                <thead>
                  <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                    <th><h5><?= $this->Paginator->sort('Chips.numero',__('Número de Chip')) ?></h5></th>
                    <th><h5><?= $this->Paginator->sort('Contactos.nombre',__('Asignado')) ?></h5></th>
                    <th><h5>Acciones</h5></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($chips as $chip): ?>
                  <tr>
                    <td><p><?= h($chip->numero) ?></p></td>
                    <?php if(!empty($chip['contactos_join']['contacto']['nombre'])): ?>
                    <td><p><?= h($chip['contactos_join']['contacto']['nombre'].' '.$chip['contactos_join']['contacto']['apellido_paterno'].' '.$chip['contactos_join']['contacto']['apellido_materno']) ?></p></td>
                    <?php else: ?>
                    <td><p>N/A</p></td>
                    <?php endif; ?>
                    <td>
                      <div class="dropdown mx-100">
                        <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                          <?php if($this->UserAuth->isAdmin()):?>
                          <li><?= $this->Html->link('Editar', '/Chips/edit/'.$chip->id, ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?></li>
                          <?php endif; ?>
                          <?php if($this->UserAuth->isAdmin()):?>
                          <li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $chip['id']], ['confirm' => __('Seguro que quiere borrar el registro #{0}?', $chip['id']), 'title' => __('Delete'), "escape" => false]) ?></li>
                          <?php endif; ?>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              <?php if (!empty($chips)) {
                echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de Chips')]);
                } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php echo $this->element('modalURL'); ?>
<script type="text/javascript">
  $(document).ready(function(){
      $('.modal').insertBefore($('body'));
  });
</script>
<div class="modal inmodal" id="modalURL" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content animated fadeIn panel panel-info">
      </div>
    </div>
</div>

<script type="text/javascript">
	$('a[data-target="#modalURL"]').on('click', function () {

          var spinner = "<div class='text-center'><i class='fa fa-spinner fa-spin fa-5x fa-fw' style='margin:40px; font-size:20px;'></i></div>";
          $("#modalURL .modal-body").html(spinner);
        
          $("#modalURL .modal-content").load($(this).attr('href'));
      });
</script>

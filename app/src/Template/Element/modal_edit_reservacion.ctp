<div class="modal inmodal" id="modal_reservacion" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-clock-o modal-icon"></i> -->
                <h4 class="modal-title"> <?= __("Agregar Reservación") ?> </h4>
                <h3 id="lbl_recurso"></h3>
                
            </div>
            <div id="modal_cambiar_forma" class="modal-body">
              <?php echo $this->element('Usermgmt.ajax_validation', ['formId'=>'addFrom', 'submitButtonId'=>'btn_crear']); ?>

              <?php echo $this->Form->create($reservacion, [ 'url' => [  'controller' => 'Reservaciones','action'=> 'add'], 'id'=>'addFrom', 'class'=>'form-horizontal', 'novalidate'=>true]); ?>

              <div class="row">

                  <?= $this->Form->input('Reservaciones.recurso_id', [ 'type' => 'hidden' ] ); ?>
                  
                      <?php $user =   $this->UserAuth->getUser(); ?>
                      
                      
                      <?php if(  in_array( $this->UserAuth->getGroupId()   , [5,6] ) ): ?>
                        
                        <?= $this->Form->input('Reservaciones.cliente_id', [ 'value' => $user['User']['cliente_id'] ,  'type' => 'hidden' ] ); ?>

                        <div class="col-md-offset-1 col-md-4">
                            <?php $tmp =  $clientes->toArray(); ?>
                           <?= $this->Form->input('Reservaciones.cliente', ['type'=>"text", 'readonly' =>'readonly' , 'label' => 'Cliente', 'value' => $tmp[ $user['User']['cliente_id']  ] ] ); ?>


                        </div>

                          <div class="col-md-offset-2 col-md-4">

                          <?= $this->Form->input('Reservaciones.saldo', ['readonly' =>'readonly' , 'label' => 'Saldo'] ); ?>
                       </div>


                      <?php  else:?>
                        <div class="col-md-offset-1 col-md-4">
                          <?= $this->Form->input('Reservaciones.cliente_id', [ 'options' => $clientes  ] ); ?>
                          </div>

                          <div class="col-md-offset-2 col-md-4">

                          <?= $this->Form->input('Reservaciones.saldo', ['readonly' =>'readonly' , 'label' => 'Saldo'] ); ?>
                       </div>

                      <?php endif;?>
                    

              </div>

              <div class="row">

                  <div class="col-md-offset-1 col-md-4">

                    <?= $this->Form->input('Reservaciones.asunto', [ 'label' => __('Asunto')] ); ?>

                  </div>

                  <div class="col-md-offset-2 col-md-4">
                  <?= $this->Form->input('Reservaciones.cantidad', [ 'label' => __('Número de asistentes')] ); ?>
                  </div>

              </div>

              <div class="row">
                    <div class="col-md-offset-1 col-md-4">
                    <?= $this->Form->input('Reservaciones.fecha_inicio', [ 'label' => __('Desde'), 'class'=>'datetimepicker', 'type'=> 'text' ] ); ?>

                    </div>
                    <div class="col-md-offset-2 col-md-4">
                    <?= $this->Form->input('Reservaciones.fecha_fin', [ 'label' => __('Hasta') ,  'class' => 'datetimepicker', 'type'=> 'text'] ); ?>
                    </div>
              </div>


              <div class="row">
                  <div class="col-md-offset-1 col-md-4">
                      <?= $this->Form->input('Reservaciones.recurrente', ['label' => __('Reservación recurrente'),'type' => 'checkbox' ] ) ?>
                  </div>

                  <div class="col-md-offset-2 col-md-4">

                    <?= $this->Form->radio('Reservaciones.tipo' ,  [ 
                          [ 'value' =>  1 , 'text' => __('Día') ],
                          [ 'value' =>  2 , 'text' => __('Semana') ],
                          [ 'value' =>  3 , 'text' => __('Mes') ],
                        ] ) ?>
                  </div>

              </div>
              <?php echo $this->Form->end(); ?>
            </div>

            <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_crear" class="btn btn-primary">Agregar</button>
      </div>

        </div>
    </div>
</div>


<div class="modal fade" id="editCentroModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title"><?= __('Editar Centro') ?></h4>
        <div class="body"></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#editCentroModal").on("show.bs.modal", function(e) {
      var link = $(e.relatedTarget);
      $(this).find(".body").load(link.attr("href"), function(){
        
      });
  });
  $("#editCentroModal").on("hidden.bs.modal", function(e) {
      $(this).find(".body").html("<br>");
  });
</script>
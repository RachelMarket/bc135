<div id="updateContactosIndex" class="table-responsive" >

    <?php echo $this->Search->searchForm('Contactos', ['legend'=>false, 'updateDivId'=>'updateContactosIndex']); ?>
    <?php echo $this->element('Usermgmt.paginator', ['updateDivId'=>'updateContactosIndex']); ?>

    <table class="table table-striped table-bordered table-condensed table-hover">
        <thead>
            <tr>
            	                
                <th class="psorting"><?php echo $this->Paginator->sort('Contactos.nombre', __('Nombre')); ?></th>
                <th class="psorting"><?php echo $this->Paginator->sort('Contactos.apellido_paterno', __('Apellido Paterno')); ?></th>
                <th class="psorting"><?php echo $this->Paginator->sort('Contactos.apellido_materno', __('Apellido Materno')); ?></th>

                <th class="psorting"><?php echo $this->Paginator->sort('Contactos.correo', __('Correo Electrónico')); ?></th>
                <th class="psorting"><?php echo $this->Paginator->sort('Contactos.telefono_movil', __('Teléfono Móvil')); ?></th>
                <th class="psorting"><?php echo $this->Paginator->sort('Contactos.telefono_fijo', __('Teléfono Fijo')); ?></th>
               
                <th align="center" class="actions">Acciones</th>    
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contactos as $contacto): ?>
                <tr>
                                            
                    <td><?= h($contacto->nombre) ?></td>
                    <td><?= h($contacto->apellido_paterno) ?></td>
                    <td><?= h($contacto->apellido_materno) ?></td>
                    <td><?= h($contacto->correo) ?></td>
                    <td><?= h($contacto->telefono_movil) ?></td>
                    <td><?= h($contacto->telefono_fijo) ?></td>
                    
                    <td>
                        <div class="dropdown">
                           <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            
                            <li role="presentation"><a title="View" class="view-contact" data-value="<?= $contacto->id ?>"><i class="fa fa-eye"></i>&nbsp;View</a></li>

                             <li role="presentation"><a title="Editar" class="list-contact" data-value="<?= $contacto->id ?>"><i class="fa fa-pencil"></i>&nbsp;Editar</a></li>
                         
                              <li role="presentation"><?= $this->Form->postLink('<i class="fa fa-trash"></i>&nbsp;Borrar', ['controller'=>'Contactos','action' => 'delete', $contacto->id], ['confirm' => __('¿Esta seguro de querer borrar el contacto #{0}?', $contacto->id), 'title' => __('Delete'), "escape" => false]) ?></li>
                            </ul>
                        </div>
                    </td>
                </tr>

            <?php endforeach; ?>
        </tbody>
    </table>

    <?php if (!empty($contactos)){
            echo $this->element('Usermgmt.pagination', ['paginationText'=>__('Número de Contactos')]);?>
        <?php } ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#addContacto").hide();
        $("#editContacto").hide();
        $("#listButtonContact").hide();
        $("#span").hide();
        //resetCheckbox();
        //resetRadio();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
        });

        $('.minToDay').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy',
          startDate: new Date()
        });

        $("#addButtonContact").on('click', function(){
            $("#contentContactos").fadeOut(500, function(){
                $("#addContacto").fadeIn(300, function(){
                    $("#addButtonContact").fadeOut(500);
                    var cliente_id = <?php echo $cliente_id; ?>;
                    $("#addContacto").load("/Contactos/add/"+cliente_id+"");
                });
            });
        });

        $("#cancelButtonContact").on('click', function(){
            $("#addContacto").fadeOut(500, function(){
                $("#addButtonContact").fadeIn(300, function(){
                    $("#contentContactos").fadeIn(300);
                });
            });
        });

        $(".list-contact").on('click', function(){
                var cliente_id = $(this).data('value');
                $("#contentContactos").fadeOut(500, function(){
                    $("#addButtonContact").fadeOut(500);
                    $("#editContacto").fadeIn(300, function(){
                        $("#editContacto").load("/Contactos/edit/"+cliente_id+"");
                    });
                });
                
           
        });

        $('.view-contact').on('click', function(){

            var cliente_id = $(this).data('value');
            console.log(cliente_id);
            $("#addButtonContact").fadeOut(500, function(){
                $("#contentContactos").fadeOut(500, function(){
                    $("#viewContact").load("/Contactos/view/"+cliente_id+"");
                    $("#listButtonContact").fadeIn(300, function(){
                        $("#viewContact").fadeIn(300);
                    });
                });
            }); 
        });
        
        $("#listButtonContact").on('click', function(){
            $("#viewContact").fadeOut(500, function(){
                $("#listButtonContact").fadeOut(500, function(){
                   $("#addButtonContact").fadeIn(300, function(){
                       $("#contentContactos").fadeIn(300);
                    });
                });
            });
        });
        function resetSelect(){
            $('input[type=select]').each(function() 
            { 
                    this.selected = false; 
            }); 
        }
        function resetRadio(){
            $('input[type=radio]').each(function() 
            { 
                    this.checked = false; 
            }); 
        }

       // $(".searchBlock").addClass('col-lg-4', 'col-md-12');
        $(".searchSubmit").css('padding-right','0px');
        $(".radio").addClass('radio-inline');
        $(".checkbox").addClass('checkbox-inline');

        $("#saveContact").on('click', function(){
            
            $("#formContacto").submit();
        });

        $("#add").on('click',function(){
            $('.list-ext').append('<li value="'+$('#extension-id').find("option:selected").val()+'">'+$('#extension-id').find("option:selected").text()+'  <i class="fa fa-times-circle" aria-hidden="true"></i></li>'); 
        });
    });

</script>

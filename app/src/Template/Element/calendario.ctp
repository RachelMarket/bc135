<?php  use Cake\Routing\Router;  ?>


 <div class="ibox">
               
                <div class="ibox-content">

<div id="full">



</div>
	</div>
	</div>

<script type="text/javascript">
	

	$(document).ready(function(){


		var postAdd = '<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'add']) ?>';
		var postEdit = '<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'edit']) ?>/';
		var postDel = '<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'delete']) ?>/';
		

		var calcula_saldo = function( cliente_id, is_edit){

			$.post( '<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'saldo']) ?>/'+cliente_id+'/'+is_edit,{ 
					fecha_inicio:$('#reservaciones-fecha-inicio').val(),
					fecha_fin: $('#reservaciones-fecha-fin').val()
					 } ,function(data){

					 	var data = JSON.parse(data);

					 	if( data.saldo !== undefined){

					 		$('#reservaciones-saldo').val(data.saldo);
					 		$('#reservaciones-horas').val(data.horas);

					 		$('#reservaciones-cargo-tiempo-extra').removeAttr('checked');
					 		$('#cargo_tiempo_extra').css('display','none');
					 		
					 		$('#btn_crear').prop('disabled', false);
					 		$('#btn_replicar').prop('disabled', false);

					 		if( data.saldo < 0){
					 			$('#cargo_tiempo_extra').css('display','block');
					 			$('#btn_crear').prop('disabled', true);
					 			$('#btn_replicar').prop('disabled', true);
					 		}
					 	}
					
			}  );

		}


		// Si doy click en guardar y replicar
		$('#btn_replicar').click(function(){

			$('#reservaciones-guardar-replicar').val(1);
			$('#btn_crear').trigger('click');

		});

		$('#btn_cancelar_replicar').click(function(){


			$('#reservaciones-cancelar-replicar').val(1);

			$('#btn_cancelar').trigger('click');

		});

		
		$('#btn_cancelar').click(function(){


			var confir = confirm( '¿Realmente deseas cancelar la reservación ?' );
			if( confir){
			var reservacion_id = $('#btn_cancelar').data('reservacion_id');

				if( reservacion_id !== undefined ){
					$('#deleteFrom').attr('action' , postDel+reservacion_id);
					$('#deleteFrom').submit();


				}
			}else{
				$('#reservaciones-cancelar-replicar').val(0);
			}


		});


		$('.datetimepicker').datetimepicker({
		    startView:1,
		    format: 'dd/mm/yyyy HH:ii P',
		    showMeridian: true,
	        autoclose: true,
	        todayBtn: true

		  }).change( function(){

		  	calcula_saldo($("#reservaciones-cliente-id").val(), false);
		  });

		
		var last  = null;
		
		$('.calendario').click(function(){

			var recurso_id = $(this).data('recurso_id');
			var recurso = $(this).data('recurso');

			console.log(recurso);
			$('#reservaciones-recurso-id').val(recurso_id);
			$('#lbl_recurso').html( recurso.name );

			$('#full').fullCalendar({
				
				select:function(start, end, jsEvent, view){

					console.log(start);
					console.log(end);
					console.log(jsEvent);
					console.log(view);

					var is_cliente = <?= (in_array( $this->UserAuth->getGroupId(), [5,6]))? 1 : 0 ?>;
						
		    		if(is_cliente){

		    			$('#reservaciones-cliente').prop('disabled', false);
		    			$('#reservaciones-saldo').prop('disabled', false);
		    			
		    			$('#reservaciones-asunto').prop('disabled', false);
		    			$('#reservaciones-cantidad').prop('disabled', false);

		    			$("#reservaciones-fecha-inicio").prop('disabled', false);
		    			$("#reservaciones-fecha-fin").prop('disabled', false);

		    			$('.editbCliente').css('display','block');
		    		}

					$('#addFrom').trigger("reset");
					$('#reservaciones-fecha-inicio').val(start.format('DD/MM/YYYY hh:mm A') );

					$('#reservaciones-fecha-fin').val(end.format('DD/MM/YYYY hh:mm A') );


					$('#btn_crear').html('Agregar');
					$('#reservaciones-guardar-replicar').val(0);
					$('#addFrom').attr('action' , postAdd);
					$('#modal_reservacion .modal-title').html( '<?= __("Agregar Reservación") ?>' );
					$('.editb').css('display','none');

					console.log('Quitmos los check');
					$('#reservaciones-tipo-1').removeAttr('checked');
					$('#reservaciones-tipo-2').removeAttr('checked');
					$('#reservaciones-tipo-3').removeAttr('checked');
					$('#reservaciones-recurrente').removeAttr('checked');
					$('#reservaciones-cliente-id').trigger("chosen:updated");
					$('.error-message').css('display', 'none');

					$('#selectchosen').show(); 
					$('#spanname').hide(); 

					
					$('#modal_reservacion').modal('show');

					
					<?php if(  !in_array( $this->UserAuth->getGroupId()   , [5,6] ) ): ?>
						
						if($.fn.chosen) {
							$("#reservaciones-cliente-id").chosen().change(function(){

								calcula_saldo($("#reservaciones-cliente-id").val(), false);
							});
						}
						
					<?php endif;?>


					// busco el saldo
					calcula_saldo($("#reservaciones-cliente-id").val(), false);


				},
				 eventRender: function(event, element) {
			        
			        $(element).addClass( event.class  );

			        var newElement = '<div class="nameClient">'+event.cliente+'</div>';

			        $(element).append(  newElement );

			        console.log( element );
			        console.log(event);

			    },
			    eventClick: function (calEvent, jsEvent, view) {           
				   	

			    	if(calEvent.reservacion_id > 0){

			    		$('#reservaciones-guardar-replicar').val(0);
			    		$('#reservaciones-cancelar-replicar').val(0);
			    		$('#reservaciones-cliente-id').val( calEvent.cliente_id );
			    		$('#reservaciones-cantidad').val( calEvent.cantidad );

			    		$('#reservaciones-asunto').val( calEvent.asunto );

			    		

						$('#reservaciones-fecha-inicio').val(calEvent.start.format('DD/MM/YYYY hh:mm A') );

						$('#reservaciones-fecha-fin').val(calEvent.end.format('DD/MM/YYYY hh:mm A') );

						calcula_saldo($("#reservaciones-cliente-id").val(), true);
						
						$('#reservaciones-tipo-'+calEvent.tipo).trigger('click');


						$('.editb').css('display','block');


						var is_cliente = <?= (in_array( $this->UserAuth->getGroupId(), [5,6]))? 1 : 0 ?>;
						
			    		if(is_cliente){

			    			$('#reservaciones-cliente').prop('disabled', true);
			    			$('#reservaciones-saldo').prop('disabled', true);

			    			$('#reservaciones-asunto').prop('disabled', true);
			    			$('#reservaciones-cantidad').prop('disabled', true);

			    			$("#reservaciones-fecha-inicio").prop('disabled', true);
			    			$("#reservaciones-fecha-fin").prop('disabled', true);

			    			$('.editbCliente').css('display','none');
			    		}


						if( calEvent.padre == null ){
							$('#reservaciones-cliente-id').trigger("chosen:updated");

							$('.error-message').css('display', 'none');
							$('#btn_replicar').css('display','none');
							$('#btn_cancelar_replicar').css('display','none');
							$('#reservaciones-recurrente').removeAttr('checked');
						}else{
							$('#reservaciones-recurrente').trigger('click');

						}
						console.log('ponemos los check');
			    		$('#modal_reservacion .modal-title').html( '<?= __("Editar Reservación") ?>' );
			    		$('#btn_crear').html('Guardar');
			    		$('#addFrom').attr('action' , postEdit+calEvent.reservacion_id);
		            	$('#btn_cancelar').data('reservacion_id',calEvent.reservacion_id);						   

		            	$('#spancliente').text($('#reservaciones-cliente-id option:selected').text());

				   		$('#selectchosen').hide(); 
				   		$('#spanname').show(); 
					   	
		            	
		            	$('#modal_reservacion').modal('show');



		        	}
				},
				selectable:true,
				selectOverlap: false,
				defaultView:'agendaWeek', 
				header: { center: 'agendaWeek,month' } });


			$('#full').fullCalendar('removeEvents');

			if( last != null){
				$('#full').fullCalendar( 'removeEventSource', last );
				
			}
			
			last = {  type:'POST', url:'<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'events']) ?>/' + recurso_id } ;

			$('#full').fullCalendar( 'addEventSource', last );

		});

		 
		$('#reservaciones-cargo-tiempo-extra').click(function(){

			if ($(this).is(":checked")) {
				$('#btn_crear').prop('disabled', false);
				$('#btn_replicar').prop('disabled', false);
			}else{
				$('#btn_crear').prop('disabled', true);
				$('#btn_replicar').prop('disabled', true);
			}
			
		});
	});
</script>
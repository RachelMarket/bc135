<?php
  $hoy = new DateTime();
  ?>
<div id="updateProductosIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2> Clientes</h2>
      <a href="/clientes/add" class="btn btn-back btn-cancel">Agregar Cliente</a>
    </div>
    <div class="ibox-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins no-mar">
            <div class="ibox-content">
              <?php echo $this->Search->searchForm('Clientes', ['legend'=>false, 'updateDivId'=>'updateProductosIndex','custom'=>true, 'buttons'=>[]]); ?>
              <div class="table-responsive">
                <table class="table ca-table">
                  <thead>
                    <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                      <th><h5><?= $this->Paginator->sort('nombre','Nombre');?></h5></th>
                      <th><h5><?= $this->Paginator->sort('primario_persona_de_contacto','Contacto');?></h5></th>
                      <th><h5><?= $this->Paginator->sort('primario_correo_electronico','Correo Electrónico');?></h5></th>
                      <th><h5><?= $this->Paginator->sort('primario_telefono_oficina','Teléfono Oficina');?></h5></th>
                      <th><h5>Acciones</h5></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($clientes as $cliente): ?>
                    <tr class="<?php $style='';
                      if(!$cliente->activo){
                          $style='background-color: #F8AC59;'; 
                          echo 'danger" style="background-color: #F8AC59;';
                      }?>">
                      <td style="<?php echo $style; ?>"><?= $this->Html->link($cliente->nombre, ['action' => 'view', $cliente->id], ['title' => $cliente->nombre, "escape" => false]) ?>
                      </td>
                      <td style="<?php echo $style; ?>"><?= h($cliente->primario_persona_de_contacto) ?></td>
                      <td style="<?php echo $style; ?>"><?= h($cliente->primario_correo_electronico) ?></td>
                      <td style="<?php echo $style; ?>"><?= h($cliente->primario_telefono_oficina) ?></td>
                      <td class="actions" style="<?php echo $style; ?>">
                        <div class="dropdown">
                          <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <li role="presentation"><?= $this->Html->link('Ver', ['action' => 'view', $cliente->id], ['title' => __('Editar'), "escape" => false]) ?></li>
                            <?php if($this->UserAuth->isAdmin() && $cliente->activo):?>
                            <li role="presentation"><?= $this->Html->link('Agregar Factura Manual', ['controller' => 'facturas','action' => 'add', $cliente->id], ['title' => __('Agregar Factura Manual'), "escape" => false]) ?></li>
                            <li role="presentation"><?= $this->Html->link('Editar', ['action' => 'edit', $cliente->id], ['title' => __('Editar'), "escape" => false]) ?></li>
                            <!--li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $cliente->id], ['confirm' => __('Seguro que quiere borrar # {0}?', $cliente->id), 'title' => __('Delete'), "escape" => false]) ?></li -->
                            <li role="presentation"><?= $this->Html->link('Mis reservaciones', ['controller' => 'Reservaciones','action' => 'misreservaciones', $cliente->id], ['title' => __('Mis reservaciones'), "escape" => false]) ?></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            <?php echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de Clientes')]);?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php  $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio',
                      'Agosto','Septiembre','Octubre','Noviembre','Diciembre');?>
<style type="text/css">
<!--
-->
</style>
<page pageset="old">		
<!-- empieza header-->
<div style="display:inline">
<div id="page_1">

<div style="float:left; width:750px; margin-left:10px; font-size: 10px; font-face:'Arial';">
  <div style="float:left; width:100%;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial'; background-color: #000000; color: #FFFFFF;" >
    <tbody>
        <tr>
        <td style="width:110px;"><img height="100" src="<?php echo WWW_ROOT ?>/img/cropped-bc-135logo.jpg"></td>
      
        <td align="left" style="width:240px;">
          <h4>| FACTURA |</h4><br>
          <p style="font-size: 14px; font-face:'Arial';"> www.bc-mx.com</p>
        </td>
        <td style="width:320px;">
        <div style="padding: 10px;height: 80px; float: right;" align="right" >
          <table><tbody>
            <tr>
              <td><strong> Folio: </strong></td>
              <td><strong><?php echo $factura->folio; ?></strong></td>
            </tr>
            <tr>
              <td><strong> Serie: </strong></td>
              <td><strong><?php echo $factura->serie; ?></strong></td>
            </tr>
            <tr>
              <td><strong>  Número y Año de Aprobación: </strong></td>
              <td><?php echo $factura->ano_y_no_aprobacion; ?></td>
            </tr>
            <tr>
              <td><strong> Fecha: </strong></td>
              <td><?php echo date('Y-m-d H:i',strtotime($factura->fecha_timbrado)); ?></td>
            </tr>
            <tr>
              <td><strong> Versión y No. de Certificado: </strong></td>
              <td><?php echo $factura->certificado; ?></td>
            </tr>
            <tr>
              <td><strong>  Folio Fiscal : </strong></td>
              <td><?php echo $factura->uuid; ?></td>
            </tr>
            </tbody>
          </table>
          </div>
        </td>
        </tr>
    </tbody>
  </table>
  </div>
  <br>
<?php if($factura->festado_id == 3):?>
   <div style="float:center; width:100%;"  align="center">
   <p style="font-size: 20px; font-face:'Arial'; color: #AA0000;">CANCELADA</p>
   </div>
     <br>
<?php endif;?>
  <div style="float:left; width:100%;">
  <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
    <tbody>
      <tr>
      <td  style="width:45%;"> 
        
        <div style="border-radius: 25px;border: 1px solid #999999;padding: 10px;height: 80px;" >
          <strong>EMISOR:</strong><br><br>
          <strong><?php echo $datos['nombre_fiscal']; ?></strong><br>
              <?php echo $datos['colonia_fiscal']; ?>, 
              <?php echo $datos['ciudad_fiscal'];?><br>
              <?php echo $datos['estado_fiscal']; ?>, 
              <?php echo $datos['pais_fiscal']; ?>, 
              C.P. <?php echo $datos['cp_fiscal']; ?><br>
              RFC <?php echo $datos['rfc']; ?><br>
          </div>

      </td>
      <td style="width:10%">&nbsp;</td>
      <td style="width:45%;">
        <div style="border-radius: 25px;border: 1px solid #999999;padding: 10px;height: 80px;" >
              <strong>RECEPTOR:</strong><br><br>
              <strong><?php echo $factura->cliente->razon_social; ?></strong><br>
              <?php echo $factura->cliente->calle.' '.$factura->cliente->numero.' '.$factura->cliente->numero_interior; ?><br>
              <?php echo $factura->cliente->colonia; ?><br>
              <?php echo $factura->estado; ?>, 
              <?php echo $factura->municipio; ?>,
              <?php echo $factura->pais; ?>
              C.P. <?php echo $factura->cliente->codigo_postal; ?><br>
              RFC <?php echo $factura->cliente->rfc; ?>
        </div>
      </td>
        </tr>
      </tbody>
    </table>
  </div>
<br><
  <div style="float:left; width:100%; margin-top:20px;">
    <table width="100%" cellpadding="5" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
    <tbody>
      <tr>
        <td style="width:10%; border: #999999 1px solid; color: #000000; background-color: #999999; align: center; padding: 2px;" align="center"> 
          <strong>Cantidad</strong>
        </td>
        <td style="width:10%; border: #999999 1px solid; border-left:0px;  color: #000000; background-color: #999999; align: center;padding: 2px;" align="center"> 
          <strong>Unidad</strong>
        </td>
        <td style="width:40%; border: #999999 1px solid; border-left:0px;  color: #000000; background-color: #999999; align: center;padding: 2px;" align="center"> 
          <strong>Descripci&oacute;n</strong></td>
        <td style="width:20%; border: #999999 1px solid; border-left:0px;  color: #000000; background-color: #999999; align: center;padding: 2px;" align="center"> 
          <strong>Precio Unitario</strong>
        </td>
        <td style="width:20%; border: #999999 1px solid;  border-left:0px;  color: #000000; background-color: #999999; align: center;padding: 2px;" align="center"> 
          Importe
        </td>
      </tr>

      <?php foreach ($factura->clientes_servicios as $servicio): ?>
      <tr>
        <td style="width:10%; border: #999999 1px solid; border-top:0px;padding: 2px;" align="center"> 
          <?php echo $servicio->cantidad;?>
        </td>
        <td style="width:10%; border: #999999 1px solid; border-left:0px; border-top:0px;padding: 2px;" align="center"> 
          <?php echo $factura->unidades[$servicio->unidad_id];?>
        </td>
        <td style="width:40%; border: #999999 1px solid; border-left:0px; border-top:0px;padding: 2px;" align="center"> 
          <?php echo $servicio->nombre;?><br> <?php echo $servicio->descripcion;?>
        </td>
        <td style="width:20%; border: #999999 1px solid; border-left:0px; border-top:0px;padding: 2px;" align="right"> 
          $<?php 
            if($factura->moneda_id == 1){ // 1-USD 2-MXN

                echo number_format($servicio->precio_usd,2);

            }else{ 

                echo number_format($servicio->precio_mxn,2); 

            } ?> &nbsp;&nbsp;
        </td>
        <td style="width:20%; border: #999999 1px solid;  border-left:0px; border-top:0px;padding: 2px;" align="right"> 
          $<?php if($factura->moneda_id == 1){ echo number_format($servicio->precio_usd * $servicio->cantidad,2); }else{ echo number_format($servicio->precio_mxn * $servicio->cantidad,2); } ?> &nbsp;&nbsp;
        </td>
      </tr>

      <?php endforeach; ?>


    </tbody>
    </table>
  </div>

  <div style="float:left; width:100%; margin-top:5px;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
    <tbody>
      <tr>
        <td style="width:60%;" align="right">&nbsp; </td>
        <td style="width:20%; border: #999999 1px solid; border-left:0px;  color: #000000; background-color: #999999; align: center;padding: 2px;" align="right"> <strong>Subtotal: </strong>&nbsp;&nbsp; </td>
        <td style="width:20%; border: #999999 1px solid;padding: 2px;" align="right"><strong> $<?php echo number_format($factura->subtotal,2); ?>&nbsp;&nbsp;</strong></td>
      </tr>
      <tr>
        <td style="width:60%;" align="right">&nbsp; </td>
        <td style="width:20%; border: #999999 1px solid; border-left:0px;  color: #000000; background-color: #999999; align: center;padding: 2px;" align="right"> <strong>IVA: </strong>&nbsp;&nbsp; </td>
        <td style="width:20%; border: #999999 1px solid; border-top: 0px;padding: 2px;" align="right"><strong> $<?php echo number_format($factura->iva,2); ?>&nbsp;&nbsp;</strong></td>
      </tr>
      <tr>
        <td style="width:60%;" align="right">&nbsp; </td>
        <td style="width:20%; border: #999999 1px solid; border-left:0px;  color: #000000; background-color: #999999; align: center;padding: 2px;" align="right"> <strong>Total: </strong>&nbsp;&nbsp; </td>
        <td style="width:20%; border: #999999 1px solid; border-top: 0px;padding: 2px;" align="right"><strong> $<?php echo number_format($factura->total,2); ?>&nbsp;&nbsp;</strong></td>
      </tr>
    </tbody>
    </table>
  </div>


  <div style="float:left; width:100%; margin-top:20px;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
    <tbody>
      <tr>
      <td style="width:100%; border: #999999 1px solid; border-bottom: 0px; color: #000000; background-color: #999999;padding: 2px;"> <strong>Importe Total con Letra </strong></td>
      </tr>
      <tr>

      <td style="width:100%; border: #999999 1px solid; border-top: 0px;padding: 2px;" align="center"> <?php echo $factura->cantidad_letra; ?> </td>
      </tr>
    </tbody>
    </table>
  </div>

  <div style="float:left; width:100%; margin-top:20px;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
    <tbody>
      <tr>
      <td style="width:100%; border: #999999 1px solid; border-bottom: 0px; color: #000000; background-color: #999999;padding: 2px;"> <strong>Referencias Bancarias </strong></td>
      </tr>
      <tr>

      <td style="width:100%; border: #999999 1px solid; border-top: 0px;padding: 2px;" align="center"> <?php echo $datos['referencias_fiscales']; ?> </td>
      </tr>
    </tbody>
    </table>
  </div>

  <div style="float:left; width:100%; margin-top:20px;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
    <tbody>
      <tr>
      <td style="width:100%; border: #999999 1px solid; border-bottom: 0px; color: #000000; background-color: #999999;padding: 2px;"> <strong>Observaciones</strong> </td>
      </tr>
      <tr>

      <td style="width:100%; border: #999999 1px solid; border-top: 0px; padding: 2px;">

        

       <?php  echo $factura->observaciones."<br>";

       ?>
  Método de pago: [<?php echo $factura->formasdepago->clave  ?>] <?php echo $factura->formasdepago->nombre ?> <br>
        Cuenta de banco: <?php echo $factura->cuentabanco ?><br>

       <?php
                      echo "Tipo de cambio utilizado: ".number_format($factura['tipo_de_cambio'],2)."<br>";
                      echo "Fecha límite de pago: ".date_format($factura['fecha'],"d")." de ".$meses[date_format($factura['fecha'],"n")]." de ".date_format($factura['fecha'],"Y");

        
        ?> 
        </td>
      </tr>
    </tbody>
    </table>
  </div>

  <div style="float:left; width:100%; margin-top:20px;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';" >
    <tbody>
      <tr>
      <td style="width:100%; border: #999999 1px solid; border-bottom: 0px; color: #000000; background-color: #999999;padding: 2px;"> <strong>Cadena Original</strong> </td>
      </tr>
      <tr>

      <td style="width:100%; border: #999999 1px solid; border-top: 0px; padding: 2px;"> <?php  echo $factura->cadena_original;?> 
        </td>
      </tr>
    </tbody>
    </table>
  </div>

  <div style="float:left; width:100%; margin-top:20px;">
    <table width="100%" cellpadding="0" cellspacing="0" style="font-size: 10px; font-face:'Arial';table-layout: fixed;" >
    <tbody>
      <tr>
      <td style="width:100%; border: #999999 1px solid; border-bottom: 0px; color: #000000; background-color: #999999;padding: 2px;"> <strong>Sello Digital</strong> </td>
      </tr>
      <tr>

      <td style="width:100%; border: #999999 1px solid; border-top: 0px; padding: 2px;"><?php  echo chunk_split($factura->sello_digital, 120, '<br>');?>
        </td>
      </tr>
    </tbody>
    </table>
  </div>

</div>


</div>


</div>
</page>

<?php echo $this->Html->css('../css/modal_styles.css'); ?>
<div id="updateLugaresDIDsIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2> <?php echo __('Type DID')?> </h2>
      <?php if($this->UserAuth->isAdmin()):?>
      <?= $this->Html->link('Agregar Tipo DID', '/LugaresDid/add/', ['class' => 'btn btn-cancel btn-back','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?>
      <?php endif; ?>
    </div>
    <div class="ibox-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins no-mar">
            <div class="ibox-content">
              <div class="table-responsive">
                <?php echo $this->Search->searchForm('LugaresDid', ['legend'=>false, 'updateDivId'=>'updateLugaresDIDsIndex']); ?>  
                <table class="table ca-table">
                  <thead>
                    <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                      <th><h5><?= $this->Paginator->sort('lugar',__('Type')) ?></h5></th>
                      <th><h5><?= $this->Paginator->sort('created',__('Created')) ?></h5></th>
                      <th><h5><?= __('Actions') ?></h5></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($lugaresDid as $did): ?>
                    <tr>
                      <td><p><?= h($did->lugar) ?></p></td>
                      <td><p><?= $did->created->format('d-m-Y h:i A') ?></p></td>
                      <td>
                        <div class="dropdown mx-100">
                          <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <?php if($this->UserAuth->isAdmin()):?>
                            <li><?= $this->Html->link('Editar', '/LugaresDid/edit/'.$did->id, ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?></li>
                            <?php endif; ?>
                            <?php if($this->UserAuth->isAdmin()):?>
                            <li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $did['id']], ['confirm' => __('Seguro que quiere borrar el registro #{0}?', $did['id']), 'title' => __('Delete'), "escape" => false]) ?></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <?php if (!empty($lugaresDid)) {
                  echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de Lugares DID')]);
                  } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('modalURL'); ?>
<script type="text/javascript">
  $(document).ready(function(){
      $('.modal').insertBefore($('body'));
  });
</script>
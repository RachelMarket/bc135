<?php $activo=array("NO","SI");?>
<div class="row">
                <div class="col-md-12">
                    <h6>Información General</h6>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-2">
                    <h5><?= __('Tipo de Cliente') ?></h5>
                    <?= h($cliente->tipo_cliente->nombre) ?>
                </div>
                <div class="col-md-2">
                    <h5><?= __('Dia de Pago') ?></h5>
                    <?= h($cliente->dia_pago) ?>
                </div>
                <div class="col-md-2">
                    <h5><?= __('Moneda') ?></h5>
                    <?= h($cliente->moneda->nombre) ?>
                </div>
                <div class="col-md-2">
                    <h5><?= __('Activo') ?></h5>
                    <?= h($activo[$cliente->activo]) ?>
                </div>
                <div class="col-md-2">
                    <h5><?= __('Centro') ?></h5>
                    <?= h($cliente->centro->nombre) ?>
                </div>
                <div class="col-md-2">
                    <h5><?= __('Oficina') ?></h5>
                    <?= h($cliente->oficina) ?>
                </div>
                
            </div>
            <br>
            <div class="row">
                <div class="col-md-2">
                    <h5><?= __('Forma de pago') ?></h5>
                    <?= h(  $cliente->formasdepago->clave . " " .  $cliente->formasdepago->nombre) ?>
                </div>
                <div class="col-md-2">
                    <h5><?= __('Cuenta Banco') ?></h5>
                    <?= h(  $cliente->cuentabanco) ?>
                </div>
            </div>
            <br>
            <legend></legend>  
            <div class="row">
                <div class="col-md-12">
                    <h6>Información de Contacto</h6>
                </div>
            </div>
            
                
            <br>
            <div class="row">
                <div class="col-md-2">
                    <h5><?= __('Contacto Primario') ?></h5>
                    <?= h($cliente->primario_persona_de_contacto) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Correo Electrónico') ?></h5>
                    <?= h($cliente->primario_correo_electronico) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Teléfono') ?></h5>
                    <?= h($cliente->primario_telefono_oficina) ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-2">
                    <h5><?= __('Contacto Secundario') ?></h5>
                    <?= h($cliente->secundario_persona_de_contacto) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Correo Electrónico') ?></h5>
                    <?= h($cliente->secundario_correo_electronico) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Teléfono') ?></h5>
                    <?= h($cliente->secundario_telefono_oficina) ?>
                </div>
            </div>
            <br>

            <legend></legend>


            <div class="row">
                <div class="col-md-2">
                    <h6>Datos Fiscales</h6>
                </div>
            </div>
                      
                
            <br>
            <div class="row">
                <div class="col-md-2">
                    <h5><?= __('Razón Social') ?></h5>
                    <?= h($cliente->razon_social) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('RFC') ?></h5>
                    <?= h($cliente->rfc) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Código Postal') ?></h5>
                    <?= h($cliente->codigo_postal) ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-2">
                    <h5><?= __('Calle') ?></h5>
                    <?= h($cliente->calle) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Número') ?></h5>
                    <?= h($cliente->numero) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Interior') ?></h5>
                    <?= h($cliente->numero_interior) ?>
                </div>
                
                
            </div>
            <br>
            <div class="row">
                <div class="col-md-2">
                    <h5><?= __('Colonia') ?></h5>
                    <?= h($cliente->colonia) ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Municipio') ?></h5>
                    <?= $cliente->has('municipio') ? h($cliente->municipio->municipio) :''  ?>
                </div>
                <div class="col-md-offset-2 col-md-2">
                    <h5><?= __('Estado') ?></h5>
                    <?= $cliente->has('estado') ? h($cliente->estado->estado) :''  ?>
                </div>
            </div>

            
    <br>
<?php if(in_array($this->UserAuth->getGroupId(),[1,4,8,9])):?>

    <legend></legend>
    <div class="row">
        <div class="col-md-4"> 
                <h2><?= __('Estado de Cuenta') ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2"> 
              Estado de cuenta:
            <?php echo $this->Form->input('meses',array('class ' => 'form-control', 'type'=>'text', 'label' => false, 'value' => date('m-Y'))); ?>
        </div>
        <div class="col-md-4 pull-right"><span style="display:inline-block;"><p style="display:inline-block;">Estado de cuenta mensual: </p><a href="#" class="btn" id="edo-cuenta"><i class="fa fa-file-pdf-o fa-lg" style="font-size:2em;"></i></a></span></div>
    </div>
    <div class="row">
        <div class="col-md-4 pull-right"><span style="display:inline-block;"><p style="display:inline-block;"> Reservaciones del mes: </p><a href="#" class="btn" id="pdf-reservaciones">
        <span class="fa-stack ">
  <i class="fa fa-file-pdf-o fa-stack-1x fa-sm"></i>
  <i class="fa fa-calendar-o fa-stack-2x fa-lg" ></i>
</span>
        </a></span></div>
    </div>
    
    <br>
    <br>

            
<ul id="myTab" class="nav nav-tabs" role="tablist">
    
    <li role="presentation" class="active">
        <a href="#Servicios" id="Servicios-tab" role="tab" data-toggle="tab" aria-controls="Servicios" aria-expanded="true">Movimientos del mes</a>
      </li>

    <li role="presentation" class="">
        <a href="#Facturas" id="Facturas-tab" role="tab" data-toggle="tab" aria-controls="Facturas" aria-expanded="true">Facturas</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in " id="Facturas" aria-labelledBy="Facturas-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($cliente->facturas)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?= __('Factura') ?></th>
                        <th><?= __('Fecha de vencimiento') ?></th>
                        <th><?= __('Días Vencidos') ?></th>
                        <th><?= __('Subtotal') ?></th>
                        <th><?= __('IVA') ?></th>
                        <th><?= __('Total') ?></th>
                        <th><?= __('Fecha de pago') ?></th>
                        <th><?= __('Timbrada') ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php $hoy = new DateTime(); ?>
                    <?php foreach ($cliente->facturas as $facturas): ?>
                    <tr>
                        <td><?php if($facturas->folio != ''){ echo $facturas->serie."-".$facturas->folio; } ?>

                        </td>
                        <td>
                        <?php $accion='download'; 
                        if(($facturas->festado_id == 2) || ($facturas->festado_id == 3 && $facturas->uuid != '') ){$accion='download';} ?>
                        <?php //if ($facturas->festado_id <> 3):?>
                        <?= $this->Html->link($facturas->fecha->i18nFormat('YYYY-MM-dd'), ['controller' => 'Facturas', 'action' => $accion, $facturas->id],['target'=>"_blank"]); ?>
                    <?php //endif;?>
                        </td>
                        <td>&nbsp;
                            <?php 
                                if ($facturas->festado_id != 3){
                                    if($facturas->fecha_pago == ''){
                                        $fecha = new DateTime($facturas->fecha->i18nFormat('YYYY-MM-dd HH:mm:ss'));

                                        $interval = $fecha->diff($hoy);
                                        echo $interval->format('%a días');

                                    }else{
                                         $fecha = new DateTime($facturas->fecha->i18nFormat('YYYY-MM-dd HH:mm:ss'));
                                         $pago = new DateTime($facturas->fecha_pago->i18nFormat('YYYY-MM-dd HH:mm:ss'));

                                        
                                        if($fecha <= $pago){
                                            $interval = $fecha->diff($pago);
                                            echo $interval->format('%a días');
                                        }
                                        
                                    }

                                }?>
                        </td>
                        <td>$<?= h(number_format($facturas->subtotal,2)); ?></td>
                        <td>$<?= h(number_format($facturas->iva,2)); ?></td>
                        <td>$<?= h(number_format($facturas->total,2)); ?></td>
                        <td><?php if($facturas->fecha_pago != ''){ echo date_format($facturas->fecha_pago,"Y-m-d"); } ?></td>
                        <td><?php if($facturas->festado_id == 1){ ?> 
                                 &nbsp;
                            <?php }elseif ($facturas->festado_id == 2){
                            ?> <i class="fa fa-check"></i> <?php
                            }elseif ($facturas->festado_id == 3){
                            ?> Cancelada <?php
                            } 
                         ?></td>
                         
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Facturas asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in active" id="Servicios" aria-labelledBy="Servicios-tab">
    <div class="row pull-right">
        <div style="display:inline-block;" class="col-lg-12">
         <?php 
                $total_precio = 0;
                $moneda = "";
                $subtotal = 0;
                foreach ($cliente_servicios as $cliente_servicio): 
                if($cliente->moneda_id == 1){
                    $subtotal = $cliente_servicio->cantidad*$cliente_servicio->precio_usd;
                    $moneda = "USD";
                }
                if($cliente->moneda_id == 2){
                    $subtotal = $cliente_servicio->cantidad*$cliente_servicio->precio_mxn;
                    $moneda = "MXN";
                }
                    $total_precio += $subtotal;
                endforeach;
        ?>
        <br>
        <p style="width:300px; display:inline-block;">Saldo del mes: &nbsp; <strong id="saldo_mes" style="font-size:1.5em;"><?= "$".number_format($total_precio, 2).$moneda; ?></strong></p>        
        </div>
    </div>
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($cliente_servicios)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?= __('Fecha') ?></th>
                        <th><?= __('Concepto de pago') ?></th>
                        <?php if($cliente->moneda_id == 1): ?>
                            <th><?= __('Precio USD') ?></th>
                        <?php endif; ?>
                         <?php if($cliente->moneda_id == 2): ?>
                            <th><?= __('Precio MXN') ?></th>
                        <?php endif; ?>
                        <th><?= __('Cantidad') ?></th>
                        <th><?= __('Subtotal') ?></th>
                        <th><?= __('Factura') ?></th>
                        <th><?= __('Acciones') ?></th>

                       <!--  <th class="actions"><?= __('Actions') ?></th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cliente_servicios as $cliente_servicio): 
                    //debug($cliente_servicio);
                        $subtotal = 0;
                        $precio = 0;
                        if($cliente->moneda_id == 1){
                            $subtotal = $cliente_servicio->cantidad*$cliente_servicio->precio_usd;
                            $precio = $cliente_servicio->precio_usd;
                            $moneda = "USD";
                        }
                        if($cliente->moneda_id == 2){
                            $subtotal = $cliente_servicio->cantidad*$cliente_servicio->precio_mxn;
                            $precio = $cliente_servicio->precio_mxn;
                            $moneda = "MXN";
                        }
                    ?>
                    <tr>
                        <td><?= h(date_format($cliente_servicio->fecha, "Y-m-d H:i")) ?></td>
                        <td><?= h($cliente_servicio->nombre) ?></td>
                        <td><?= h("$".number_format($precio, 2)) ?></td>
                        <td><?= h($cliente_servicio->cantidad) ?></td>
                        <td><?= h("$".number_format($subtotal, 2)." ".$moneda) ?></td>
                        <td> <?php if(!is_null($cliente_servicio->factura_id)){
                            $accion='download'; 
                            if($cliente_servicio->factura->festado_id == 2){$accion='download';}  
                            if ($cliente_servicio->factura->festado_id <> 3){
                                echo $this->Html->link($cliente_servicio->factura->serie."-".$cliente_servicio->factura->folio, ['controller' => 'Facturas', 'action' => $accion, $cliente_servicio->factura->id],['target'=>"_blank"]); 
                            }else{
                                echo $this->Html->link('Cancelada', ['controller' => 'Facturas', 'action' => $accion, $cliente_servicio->factura->id],['target'=>"_blank"]); 
                            }
                            
                            } ?></td>
                        <td>
                            <?php if(is_null($cliente_servicio->factura_id)){
                            echo $this->Form->postLink('<i class="fa fa-trash-o"></i>&nbsp;Eliminar', ['action' => 'delete_servicio', $cliente_servicio->id,$cliente->id], ['confirm' => '¿Deseas eliminar el servicio?', 'title' => __('Eliminar Servicio'), "escape" => false]);
                            }
                            ?>
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?= __('Fecha') ?></th>
                        <th><?= __('Concepto de pago') ?></th>
                        <?php if($cliente->moneda_id == 1): ?>
                            <th><?= __('Precio USD') ?></th>
                        <?php endif; ?>
                         <?php if($cliente->moneda_id == 2): ?>
                            <th><?= __('Precio MXN') ?></th>
                        <?php endif; ?>
                        <th><?= __('Cantidad') ?></th>
                        <th><?= __('Subtotal') ?></th>
                        <th><?= __('Factura') ?></th>
                        <th><?= __('Acciones') ?></th>

                       <!--  <th class="actions"><?= __('Actions') ?></th> -->
                    </tr>
                </thead>
                <tbody><tr><td colspan="8"><h4><?= __('No existen Servicios asociados') ?></h4></td></tr></tbody>
            </table>
        <?php endif; ?>

        </div>
    </div>
</div>

</div>
<?php endif; ?>
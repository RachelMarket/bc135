<?php

use Cake\Utility\Inflector;
$actionUrl = Inflector::camelize($this->request['controller']).'/'.$this->request['action'];
$activeClass = 'dropdown active';
$inactiveClass = '';
$activeSubmenu = 'active';
$inactiveSubmenu = '';

?>

<?php if(in_array($this->UserAuth->getGroupId(),[1,9])):?>
  <li class="<?php echo (($actionUrl == 'Oficinas/index' || $actionUrl == 'Oficinas/add' )? $activeClass : $inactiveClass ) ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-cutlery"></i>
    <span class="nav-label">Cotizaciones</span>
    <span class="fa arrow"></span></a>
    <ul class='nav nav-second-level'>
      <li class="<?php echo (($actionUrl == 'Oficinas/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
        <a href="/oficinas/index">Oficinas</a></li>
      <li class="<?php echo (($actionUrl == 'Cotizacciones/add')? $activeSubmenu : $inactiveSubmenu ) ?>">
        <a href="/cotizaciones/add">Agregar Cotización</a></li>
    </ul>
  </li>
<?php endif;?>

<?php if($this->UserAuth->isAdmin()):?>
   <li class="<?php echo (($actionUrl == 'Centros/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
    <a href="/Centros/index">
    <i class="fa fa-building-o"></i>
    <span class="nav-label">Centros</span></a>  
  </li>
<?php endif;?>
<?php if($this->UserAuth->getGroupId() != 5 && $this->UserAuth->getGroupId() != 6):?>
  <li class="<?php echo (($actionUrl == 'Clientes/index' || $actionUrl == 'Clientes/add' )? $activeClass : $inactiveClass ) ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-child"></i>
       <span class="nav-label">Clientes</span>
      <span class="fa arrow"></span>
    </a>
    <ul class='nav nav-second-level'>
      <li class="<?php echo (($actionUrl == 'Clientes/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
        <a href="/clientes/index">Listado de Clientes</a></li>
      <li class="<?php echo (($actionUrl == 'Clientes/add')? $activeSubmenu : $inactiveSubmenu ) ?>">
        <a href="/clientes/add">Agregar Cliente</a>
      </li>
    </ul>
  </li>
<?php endif;?>
<?php if(in_array($this->UserAuth->getGroupId(),[1,9])):?>
  <li class="<?php echo (($actionUrl == 'Servicios/index' || $actionUrl == 'Servicios/add' )? $activeClass : $inactiveClass ) ?>">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-cutlery"></i>
    <span class="nav-label">Servicios</span>
    <span class="fa arrow"></span></a>
    <ul class='nav nav-second-level'>
      <li class="<?php echo (($actionUrl == 'Servicios/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
        <a href="/servicios/index">Listado de Servicios</a></li>
      <li class="<?php echo (($actionUrl == 'Servicios/add')? $activeSubmenu : $inactiveSubmenu ) ?>">
        <a href="/servicios/add">Agregar Servicio</a>
      </li>
    </ul>
  </li>
<?php endif;?>
<?php if(in_array($this->UserAuth->getGroupId(),[1,9])):?>
  <li class="<?php echo (($actionUrl == 'Facturas/index' || $actionUrl == 'Facturas/prefacturas' )? $activeClass : $inactiveClass ) ?>">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
  <i class="fa fa-credit-card"></i>
  <span class="nav-label">Facturas</span>
  <span class="fa arrow"></span></a>
  <ul class='nav nav-second-level'>
    <li class="<?php echo (($actionUrl == 'Facturas/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
      <a href="/Facturas/index">Listado de Facturas</a></li>
    <li class="<?php echo (($actionUrl == 'Facturas/prefacturas')? $activeSubmenu : $inactiveSubmenu ) ?>">
      <a href="/Facturas/prefacturas">Prefactura Mensual</a>
    </li>
  </ul>
</li>
<?php endif;?>

<?php if($this->UserAuth->getGroupId() != 5 && $this->UserAuth->getGroupId() != 6):?>
  <li class="<?php echo (($actionUrl == 'Mensajes/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
    <a href="/Mensajes/index">
    <i class="fa fa-envelope-o"></i>
    <span class="nav-label">Mensajes</span></a>  
  </li>
<?php endif;?>

<?php if($this->UserAuth->isAdmin()):?>

<li class="<?php echo (($actionUrl == 'TarjetasAcceso/index' || $actionUrl == 'Chips/index' )? $activeClass : $inactiveClass ) ?>">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
  <i class="fa fa-key"></i>
  <span class="nav-label">Control de Acceso</span>
  <span class="fa arrow"></span></a>
  <ul class='nav nav-second-level'>
    <li class="<?php echo (($actionUrl == 'TarjetasAcceso/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
      <a href="/TarjetasAcceso/index">Tarjeta de Acceso</a>
    </li>
    <li class="<?php echo (($actionUrl == 'Chips/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
      <a href="/Chips/index">Chip</a>
    </li>
  </ul>
</li>

<!-- validation -->
<?php
  $bandera = false;

  if( $actionUrl == 'Extensiones/index' ||
      $actionUrl == 'Dids/index' ||
      $actionUrl == 'LugaresDid/index' || 
      $actionUrl == 'Ladas/index' ){
      $bandera = true;
  }
?>

<li class="<?php echo (($bandera == true))? $activeClass : $inactiveClass ?>" >
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
  <i class="fa fa-phone"></i>
  <span class="nav-label">Telefonía</span>
  <span class="fa arrow"></span></a>
  <ul class='nav nav-second-level'>
    <li class="<?php echo (($actionUrl == 'Extensiones/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
      <a href="/Extensiones/index">Extensiones</a>
    </li>
    <li class="<?php echo (($actionUrl == 'Dids/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
      <a href="/Dids/index">DIDs</a>
    </li>
    <li class="<?php echo (($actionUrl == 'LugaresDid/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
      <a href="/LugaresDid/index">Tipos DID</a>
    </li>
    <li class="<?php echo (($actionUrl == 'Ladas/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
      <a href="/Ladas/index">Ladas</a>
    </li>
  </ul>
</li>

<?php endif; ?>

<?php if($this->UserAuth->isAdmin()):?>
  <li class="<?php echo (($actionUrl == 'Recursos/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
    <a href="/Recursos/index">
    <i class="fa fa-slideshare"></i>
    <span class="nav-label">Salas</span></a>  
  </li>
<?php endif; ?>


<?php if($this->UserAuth->isAdmin()):?>
  <li class="<?php echo (($actionUrl == 'Asuetos/index')? $activeSubmenu : $inactiveSubmenu ) ?>">
    <a href="/Asuetos">
    <i class="fa fa-calendar"></i>
    <span class="nav-label">Días Inhábiles</span></a>  
  </li>
<?php endif; ?>

 <li class="<?php echo (($actionUrl == 'Reservaciones/calendario')? $activeSubmenu : $inactiveSubmenu ) ?>">
    <a href="/Reservaciones/calendario">
    <i class="fa fa-plus"></i>
    <span class="nav-label">Reservaciones</span></a>  
  </li>

<?php if($this->UserAuth->getGroupId() == 5 || $this->UserAuth->getGroupId() == 6): ?>
  <li class="<?php echo (($actionUrl == 'Reservaciones/misreservaciones')? $activeSubmenu : $inactiveSubmenu ) ?>">
    <a href="/Reservaciones/misreservaciones">
    <i class="fa fa-calendar"></i>
    <span class="nav-label">Mis reservaciones</span></a>  
  </li>
<?php endif; ?>
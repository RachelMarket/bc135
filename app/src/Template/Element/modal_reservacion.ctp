
<div class="modal inmodal" id="modal_reservacion" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn panel panel-info">
            <div class="panel-heading">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <!-- <i class="fa fa-clock-o modal-icon"></i> -->
                <h4 class="modal-title"> <?= __("Agregar Reservación") ?> </h4>
                <h3 id="lbl_recurso"></h3>
                
            </div>
            <div id="modal_cambiar_forma" class="modal-body">
            	<?php echo $this->element('Usermgmt.ajax_validation', ['formId'=>'addFrom', 'submitButtonId'=>'btn_crear']); ?>

              <?php echo $this->Form->create($reservacion, [ 'url' => [  'controller' => 'Reservaciones','action'=> 'add'], 'id'=>'addFrom', 'class'=>'form-horizontal', 'novalidate'=>true]); ?>

              <div class="row">


              <?php  if( $show_select_recurs ):  ?>
                		    <?php 
                        $list = [];
                        foreach ($recursos as $key => $value) {
                           $list[ $value->id ] = $value->name;
                        }

                        ?>
                       <div class="col-md-offset-1 col-md-10">
                    <?= $this->Form->input('Reservaciones.recurso_id', [ 'options' => $list, 'label' => 'Sala'  ]); ?>
                    </div>

              <?php else: ?>
                  
                  <?= $this->Form->input('Reservaciones.recurso_id', [ 'type' => 'hidden' ] ); ?>
            
              <?php endif; ?>

                  <?= $this->Form->input('Reservaciones.guardar_replicar', ['value' => 0, 'type' => 'hidden' ] ); ?>
                  
                  
              				<?php 
                            $user =   $this->UserAuth->getUser(); 
                            $is_cliente = (in_array( $this->UserAuth->getGroupId(), [5,6]))? true : false;
                      ?>
              				
              				
              				<?php if(  $is_cliente ): ?>
                        
              					<?= $this->Form->input('Reservaciones.cliente_id', [ 'value' => $user['User']['cliente_id'] ,  'type' => 'hidden' ] ); ?>

                        <div class="col-md-offset-1 col-md-10">
                            <?php $tmp =  $clientes->toArray(); ?>
                           <?= $this->Form->input('Reservaciones.cliente', ['type'=>"text", 'readonly' =>'readonly' , 'label' => 'Cliente', 'value' => $tmp[ $user['User']['cliente_id']  ] ] ); ?>
                        </div>

              				<?php  else:?>
                        <div class="col-md-offset-1 col-md-10">
                        <div id="selectchosen">
                          <?= $this->Form->input('Reservaciones.cliente_id', [ 'options' => $clientes  ] ); ?>
                        </div>
                        <div id="spanname" style="display: none; margin-left: -15px; margin-bottom: 10px !important;">
                            <label class="control-label">Cliente</label><br>
                            <span style="font-size: 13px; padding-left: 0px;" id="spancliente"></span>
                        </div>
                        </div>

              				<?php endif;?>
<!-- //Saldo -->
              </div>

              <div class="row">

              		<div class="col-md-offset-1 col-md-4">

              			<?= $this->Form->input('Reservaciones.asunto', [ 'label' => __('Asunto'),'class'=>'input-sm'] ); ?>

              		</div>

              		<div class="col-md-offset-1 col-md-2">
              		<?= $this->Form->input('Reservaciones.cantidad', [ 'label' => __('No. asistentes'),'class'=>'input-sm'] ); ?>
              		</div>

                  <div class="col-md-offset-1 col-md-2">
                    <?= $this->Form->input('Reservaciones.saldo', ['readonly' =>'readonly' , 'label' => 'Saldo','class'=>'input-sm'] ); ?>
                  </div>

              </div>

              <div class="row">
              			<div class="col-sm-offset-1 col-sm-3">
              			<?= $this->Form->input('Reservaciones.fecha_inicio', [ 'label' => __('Desde'), 'style' => 'width: 150px' ,'class'=>'datetimepicker', 'type'=> 'text','class'=>'input-sm'] ); ?>

                    <input tpye="text" disabled="disabled" class="input-sm form-control " id="fecha_inicio_dia" stlye="display:none;width: 150px" />
              			
                    </div>
              			<div class="col-md-offset-1 col-md-3"> 
	              		<?= $this->Form->input('Reservaciones.fecha_fin', [ 'label' => __('Hasta'), 'style' => 'width: 150px', 'class' =>'datetimepicker', 'type'=> 'text','class'=>'input-sm'] ); ?>

                    <input tpye="text" disabled="disabled"  class="input-sm form-control "   id="fecha_fin_dia" stlye="display:none;width: 150px" />
	              		</div>
                    <div class="col-md-offset-1 col-md-2">
                    <?= $this->Form->input('Reservaciones.horas', ['readonly' =>'readonly' , 'label' => 'Hrs a usar','class'=>'input-sm'] ); ?>
                  </div>
              </div>

              <?php if(  !$is_cliente ){ ?>
              <div id="div_recurente" class="row">
                  
              		<div class="col-md-offset-1 col-md-4">
              				<?= $this->Form->input('Reservaciones.recurrente', ['label' => __('Recurrente'),'type' => 'checkbox' ] ) ?>
              		</div>
              		<div class="col-md-offset-1 col-md-5">
                  <ul class="list-inline"><li>

                    <li><input name="Reservaciones[tipo]" value="1" id="reservaciones-tipo-1" type="radio">&nbsp;<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Cada día a la misma hora">
                    &nbsp;Día</span>
                    </li>
                    <li><input name="Reservaciones[tipo]" value="2" id="reservaciones-tipo-2" type="radio"><span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Cada semana el mismo día">
                    &nbsp;Semana</span></li>
                    <li><input name="Reservaciones[tipo]" value="3" id="reservaciones-tipo-3" type="radio"><span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Cada mes el mismo día.">
                    &nbsp;Mes</span></li>
                    </ul>

              		</div>

              </div>
              <?php } ?>
              
              <?php echo $this->Form->end(); ?>


              <div class="row" id="cargo_tiempo_extra" style="display:none;">
                <div class="col-md-offset-1 col-md-10 cargo-tiempo-extra"><br>
                  <?= $this->Form->input('Reservaciones.cargo_tiempo_extra', ['label' => ' '.__('Su reservación sobrepasa las horas disponibles, se le realizará un cargo por el tiempo extra  '),'type' => 'checkbox'] ) ?>
                        
                </div>
              </div>

              <div class="row" id="erroroficinavirtual" style="display:none;">
                <div class="col-md-offset-1 col-md-10 "><br>
                <div class="form-group">
                    <label><?= __('No cuenta con saldo para realizar la reservación  ')  ?>
                    </label>
                  </div>    
                </div>
              </div>

            </div>

            <div class="modal-footer">
            

                  <button type="button" id="btn_cancelar" class="btn btn-danger pull-left editb editbCliente">Eliminar</button>

                  <button type="button" id="btn_cancelar_replicar"  class="btn btn-danger  pull-left editb editbCliente">Eliminar todas</button>
                  

                  <button type="button" id="btn_crear" class="btn btn-primary pull-right editbCliente">Agregar</button>
                  <button type="button" id="btn_replicar"  class="btn btn-primary  pull-right editb editbCliente">Guardar y replicar</button>
                 
                  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </div>
</div>


 <?php echo $this->Form->create($reservacion, [ 'url' => [  'controller' => 'Reservaciones','action'=> 'delete'], 'id'=>'deleteFrom', 'class'=>'form-horizontal', 'novalidate'=>true]); ?>


  <?= $this->Form->input('Reservaciones.cancelar_replicar', ['value' => 0, 'type' => 'hidden' ] ); ?>


 <?php echo $this->Form->end(); ?>
 <script type="text/javascript">
   $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
 </script>

<?php echo $this->Html->css('../css/modal_styles.css'); ?>
<div id="updateDidsIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2> <?php echo __('DIDs')?> </h2>
      <?php if($this->UserAuth->isAdmin()):?>
      <?= $this->Html->link('Agregar DID', '/Dids/add/', ['class' => 'btn btn-cancel btn-back','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?>
      <?php endif; ?>
    </div>
    <div class="ibox-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins no-mar">
            <div class="ibox-content">
              <div class="table-responsive">
                <?php echo $this->Search->searchForm('Dids', ['legend'=>false, 'updateDivId'=>'updateDidsIndex']); ?> 
                <table class="table ca-table">
                  <thead>
                    <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                      <th>
                        <h5><?= $this->Paginator->sort('Ladas.lada',__('Lada')) ?></h5>
                      </th>
                      <th>
                        <h5><?= $this->Paginator->sort('Dids.did',__('DID')) ?></h5>
                      </th>
                      <th>
                        <h5><?= $this->Paginator->sort('Contactos.nombre',__('Asignado')) ?></h5>
                      </th>
                      <th>
                        <h5>Acciones</h5>
                      </th>
                  </thead>
                  <tbody>
                    <?php foreach ($dids as $row): ?>
                    <tr>
                      <td><p><?= h($row->lada->lada) ?></p></td>
                      <td><p><?= h($row->did) ?></p></td>
                      <?php if(!empty($row['contactos_dids_join']['contacto']['nombre'])): ?>
                      <td><p><?= h($row['contactos_dids_join']['contacto']['nombre'].' '.$row['contactos_dids_join']['contacto']['apellido_paterno'].' '.$row['contactos_dids_join']['contacto']['apellido_materno']) ?></p></td>
                      <?php else: ?>
                      <td><p>N/A</p></td>
                      <?php endif; ?>
                      <td align="center">
                        <div class="dropdown">
                          <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <?php if($this->UserAuth->isAdmin()):?>
                            <li><?= $this->Html->link('Editar', '/Dids/edit/'.$row->id, ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?></li>
                            <?php endif; ?>
                            <?php if($this->UserAuth->isAdmin()):?>
                            <li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $row['id']], ['confirm' => __('Seguro que quiere borrar el registro #{0}?', $row['id']), 'title' => __('Delete'), "escape" => false]) ?></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      </p></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <?php if(!empty($dids)) {
                  echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de DIDs')]);
                  }?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('modalURL'); ?>
<script type="text/javascript">
  $(document).ready(function(){
      $('.modal').insertBefore($('body'));
  });
</script>
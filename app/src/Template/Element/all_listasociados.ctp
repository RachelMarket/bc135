<br><br>
<div id="updateClientesAsociadosIndex" class="col-md-12">
    <div style="top: 10px;" class="ibox-tools col-md-4 pull-right">
        <button id="saveButton" style="width: 210px" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar</button>
    </div>
        <?php echo $this->Search->searchForm('ClientesAsociados', ['legend'=>false, 'updateDivId'=>'updateClientesAsociadosIndex']); ?>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('nombre','Nombre');?></th>
                        <th><?= $this->Paginator->sort('comentario','Comentario');?></th>
                        <th><?= $this->Paginator->sort('created','Fecha');?></th>
                        <th><?= __('Accciones') ?></th>
                    </tr>
                </thead>
                <?php foreach ($clientesAsociados as $cliente): ?>
                    <tr>
                        <td><?= $cliente['nombre'] ?></td>
                        <td><?= $cliente['comentario'] ?></td>
                        <td><?= $cliente['created']->format('d-m-Y g:i A') ?></td>
                        <td>
                            <div class="dropdown">
                               <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                 <li role="presentation"><a title="Editar" class="list" data-value="<?= $cliente['id'] ?>"><i class="fa fa-pencil"></i>&nbsp;Editar</a></li>
                             
                                  <li role="presentation"><?= $this->Form->postLink('<i class="fa fa-trash"></i>&nbsp;Borrar', ['action' => 'deleteAsociados', $cliente['id']], ['confirm' => __('Seguro que quiere borrar # {0}?', $cliente['id']), 'title' => __('Delete'), "escape" => false]) ?></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
        </table>
        <?php if (!empty($clientesAsociados)){
            echo $this->element('Usermgmt.pagination', ['paginationText'=>__('Número de Clientes Asociados')]);?>
        <?php } ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#addClienteAsociado").hide();
        $("#editClienteAsociado").hide();

        $("#saveButton").on('click', function(){
            $("#updateClientesAsociadosIndex").fadeOut(2000, function(){
                $("#addClienteAsociado").fadeIn(3000);
            });
        });

        $("#cancelButton").on('click', function(){
            $("#addClienteAsociado").fadeOut(2000, function(){
                $("#updateClientesAsociadosIndex").fadeIn(3000);
            });
        });

        $(".list").on('click', function(){
            var cliente_id = $(this).data('value');

            $.get(urlForJs+'/clientes/editAsociados/'+cliente_id+'.json',function(data){
                $("#updateClientesAsociadosIndex").fadeOut(2000, function(){
                $("#editClienteAsociado").fadeIn(3000);
                $('#nombre-asociado').val(data.cliente.nombre);
                $('#comentario-asociado').val(data.cliente.comentario);
                $("#formEdit").attr('action','/clientes/editAsociados/'+data.cliente.id+'');
            });
            });
        });

        $('#btnCancel').on('click',function(){
            $("#editClienteAsociado").fadeOut(2000, function(){
                $("#updateClientesAsociadosIndex").fadeIn(3000);
            });
        });

        $(".searchBlock").addClass('col-lg-4', 'col-md-12');
        $(".searchSubmit").css('padding-right','0px');
    });

</script>
<?php  use Cake\Routing\Router;  ?>


<div class="ibox">       
    <div class="ibox-content">
		<div id="full">
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	    </div>
	    <?php echo $this->Form->create(null, [ 'url' => [  'controller' => 'Reservaciones','action'=> 'delete'], 'class'=>'form-horizontal']); ?>
	    <div class="modal-body">
	      	<p>¿Está seguro de querer cancelar la reservación?</p>
	      	<?= $this->Form->input('reservacion_id', [ 'type' => 'hidden'] ); ?>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
	      <button type="submit" id="cancel" class="btn btn-primary pull-right">Cancelar</button>
	    </div>
	    <?php echo $this->Form->end(); ?>
	  </div>
	</div>
</div>


<script type="text/javascript">
	

	$(document).ready(function(){

		
		var last  = null;

			var cliente_id = <?= $cliente->id ?>;
			

			$('#full').fullCalendar({
				select:function(start, end, jsEvent, view){

				},
				 eventRender: function(event, element) {
			        
			        $(element).addClass( event.class  );
			        
			    },
			    eventClick: function (calEvent, jsEvent, view) {           
				    $('#myModal').modal('show');
				    //var event_id = $('#full').fullCalendar('removeEvents', calEvent._id);
				    console.log(calEvent);
				    $('#reservacion-id').val(calEvent.reservacion_id);
				},
				selectable:true,
				selectOverlap: false,
				defaultView:'agendaWeek', 
				header: { center: 'agendaWeek,month' } });


			$('#full').fullCalendar('removeEvents');

			if( last != null){
				$('#full').fullCalendar( 'removeEventSource', last );
				
			}
			
			last = { type:'POST', url:'<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'eventsClients']) ?>/' + cliente_id } ;
			
			$('#full').fullCalendar( 'addEventSource', last );
	});
</script>
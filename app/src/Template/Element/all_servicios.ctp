<?php
  $hoy = new DateTime();
  ?>

<div class="ibox float-e-margins">
  <div class="ibox-title">
    <h2> Servicios</h2>
    <a href="/servicios/add" class="btn btn-cancel btn-back">Agregar Servicio</a>
  </div>
  <div class="ibox-content">
    <div id="updateProductosIndex">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins no-mar">
            <div class="ibox-content">
              <?php echo $this->Search->searchForm('Servicios', ['legend'=>false, 'updateDivId'=>'updateProductosIndex', 'custom'=>true, 'buttons'=>[]]); ?>
              <div class="table-responsive">
                <table class="table ca-table">
                  <thead>
                    <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                      <th><h5><?= $this->Paginator->sort('nombre') ?></h5></th>
                      <th><h5><?= $this->Paginator->sort('descripcion','Descripción') ?></h5></th>
                      <th><h5>Tipo</h5></th>
                      <th><h5>Unidad</h5></th>
                      <th><h5><?= $this->Paginator->sort('precio_mxn','Precio MXN') ?></h5></th>
                      <th><h5><?= $this->Paginator->sort('precio_usd','Precio USD') ?></h5></th>
                      <th><h5>Acciones</h5></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($servicios as $servicio): ?>
                    <tr>
                      <td><p><?= h($servicio->nombre) ?></p></td>
                      <td><p><?= h($servicio->descripcion) ?></p></td>
                      <td><p>
                        <?= $servicio->has('tipo') ? $this->Html->link($servicio->tipo->nombre, ['controller' => 'Tipos', 'action' => 'view', $servicio->tipo->id]) : '' ?>
                      </p></td>
                      <td><p>
                        <?= $servicio->has('unidad') ? $this->Html->link($servicio->unidad->nombre, ['controller' => 'Unidades', 'action' => 'view', $servicio->unidad->id]) : '' ?>
                      </p></td>
                      <td><p>$<?= $this->Number->format($servicio->precio_mxn) ?></p></td>
                      <td><p>$<?= $this->Number->format($servicio->precio_usd) ?></p></td>
                      <td class="actions">
                        <div class="dropdown">
                          <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <li role="presentation"><?= $this->Html->link('Editar', ['action' => 'edit', $servicio->id], ['title' => __('Editar'), "escape" => false]) ?></li>
                            <li role="presentation"><?= $this->Form->postLink('<Borrar', ['controller' => 'servicios', 'action' => 'delete', $servicio->id], ['confirm' => __('Seguro que quiere borrar # {0}?', $servicio->id), 'title' => __('Delete'), "escape" => false]) ?></li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <?php echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de Servicios')]);?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
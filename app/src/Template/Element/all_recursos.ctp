<div id="updateRecusosIndex">
  
  <?php
    echo $this->Search->searchForm('Recursos', ['legend'=>false, 'updateDivId'=>'updateRecusosIndex', 'custom'=>true, 'buttons'=>[]]); ?>

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins no-mar">
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table ca-table">
              <thead>
                <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                  <th>
                    <h5><?= $this->Paginator->sort('Recursos.id',__('#')) ?></h5>
                  </th>
                  <th>
                    <h5><?= $this->Paginator->sort('Centros.nombre',__('Centro')) ?></h5>
                  </th>
                  <th>
                    <h5><?= $this->Paginator->sort('Recursos.name',__('Nombre')) ?></h5>
                  </th>
                  <th>
                    <h5><?= $this->Paginator->sort('RecursosTipos.name',__('Tipo')) ?></h5>
                  </th>
                  <th>
                    <h5><?= $this->Paginator->sort('Recursos.capacidad',__('Capacidad')) ?></h5>
                  </th>
                  <th>
                    <h5><?= $this->Paginator->sort('Recursos.hora_inicio',__('Horario hábil')) ?></h5>
                  </th>
                  <th>
                    <h5>Acciones</h5>
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($recursos as $row): ?>
                <tr>
                  <td><p><?= h($row->id) ?></p></td>
                  <td><p><?= h($row->centro->nombre) ?></p></td>
                  <td><p><?= h($row->name) ?></p></td>
                  <td><p><span class="label"><?= h($row->recursos_tipo->name) ?></span></p></td>
                  <td><p><?= h($row->capacidad) ?> Personas</p></td>
                  <td><p><?= $this->Time->format($row['hora_inicio'], 'HH:mm a').' '.$this->Time->format($row['hora_fin'], 'HH:mm a')?></p></td>
                  <td><p>
                    <?php if( !$modo_calendario ): ?>
                    <div class="dropdown">
                      <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                        <li role="presentation"><?= $this->Html->link('Editar', ['action' => 'edit', $row['id']], ['title' => __('Editar'), "escape" => false]) ?></li>
                        <li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $row['id']], ['confirm' => __('¿Esta seguro de borrar el registro #{0}?', $row['id']), 'title' => __('Delete'), "escape" => false]) ?></li>
                      </ul>
                    </div>
                    <?php else:?>
                    <a data-recurso='<?= json_encode( $row ) ?>' class="calendario" data-recurso_id="<?= $row['id'] ?>" href="#">Ver Calendario</a>
                    <?php endif; ?>
                  </p></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
              <?php if (!empty($recursos)) {
                $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de recursos')]);
                } ?>
              <?php if( $modo_calendario ): ?>
              <?= $this->element('calendario'); ?>
              <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->Html->css('../css/modal_styles.css'); ?>
<div id="updateCentrosIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2> Centros</h2>
      <?php if($this->UserAuth->isAdmin()):?>
      <?= $this->Html->link('Agregar Centro', '/Centros/add', ['class' => 'btn btn-back btn-cancel']); ?>
      <?php endif; ?>
    </div>
      <div class="ibox-content">
        <?php echo $this->Search->searchForm('Centros', ['legend'=>false, 'updateDivId'=>'updateCentrosIndex']); ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins no-mar">
              <div class="ibox-content">
                <div class="table-responsive">
                  <table class="table ca-table">
                    <thead>
                      <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                        <th scope="col"><h5><?= $this->Paginator->sort('id') ?></h5></th>
                        <th scope="col"><h5><?= $this->Paginator->sort('nombre') ?></h5></th>
                        <th scope="col"><h5><?= __('Actions') ?></h5></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($centros as $centro): ?>
                      <tr>
                        <td><p><?= $this->Number->format($centro->id) ?></p></td>
                        <td><p><?= h($centro->nombre) ?></p></td>
                        <td>
                          <div class="dropdown mx-100">
                            <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                              <?php if($this->UserAuth->isAdmin()):?>
                              <li><?= $this->Html->link('Editar', '/Centros/edit/'.$centro->id, ['escape' => false]); ?></li>
                              <li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $centro->id], ['confirm' => __('Seguro que quiere borrar el registro #{0}?', $centro->id), 'title' => __('Delete'), "escape" => false]) ?></li>
                              <?php endif; ?>
                            </ul>
                          </div>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                </table>
                <?php echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de Centros')]);?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('modalURL'); ?>
<script type="text/javascript">
  $(document).ready(function(){
      $('.modal').insertBefore($('body'));
  });
</script>
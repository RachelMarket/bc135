<?php echo $this->Html->css('/css/modal_styles.css'); ?>
<div id="updatecotizacionesIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2>Cotizaciones</h2>

      <?php if($this->UserAuth->isAdmin()):?>
      <?= $this->Html->link('Agregar cotizacion', '/cotizaciones/add', ['class' => 'btn btn-back btn-cancel']); ?>
      <?php endif; ?>
    </div>
      <div class="ibox-content">
        <?php echo $this->Search->searchForm('Cotizaciones', ['legend'=>false, 'updateDivId'=>'updatecotizacionesIndex','custom'=>true, 'buttons'=>[]]); ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins no-mar">
              <div class="ibox-content">
                <div class="table-responsive">
                  <table class="table ca-table">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id', '#') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('cliente') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('contacto') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('centro_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('vence') ?></th>
                            <th scope="col" align="center"><?= $this->Paginator->sort('status') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('usuario_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('aplica_promocion', 'promoción') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('creada') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($cotizaciones as $cotizacion): ?>
                        <tr>
                            <td><?= $this->Html->link(__($cotizacion->id), ['action' => 'pdf', $cotizacion->id, '_ext' => 'pdf'], ['target', '_blank']); ?></td>
                            <td><?= h($cotizacion->cliente) ?></td>
                            <td><?= h($cotizacion->contacto) ?></td>
                            <td><?= $cotizacion->centro->nombre ?></td>
                            <td><?= $this->Time->format($cotizacion->vence, 'Y-M-d') ?></td>
                            <td align="center"><span title="Cambiar status" data-toggle="popover" data-trigger="click" data-placement="top" data-html="true" class="active_popover" id="change_status_<?= $cotizacion->id ?>">
                            <?php 
                              switch ($cotizacion->status) {
                                case 'enviada':
                                    echo '<span class="label label-primary">'. $cotizacion->status.'</span>';
                                  break;
                                case 'aceptada':
                                    echo '<span class="label label-success">'. $cotizacion->status.'</span>';
                                    break;
                                case 'cancelada':
                                    echo '<span class="label label-danger">'. $cotizacion->status.'</span>';
                                    break;
                                default:
                                    echo '<span class="label label-default">'. $cotizacion->status.'</span>';
                                  break;
                              }
                            ?>
                            </span>
                            <div class="pop_content hidden">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <textarea class="comentario form-control" name="comentario" placeholder="Comentario" rows="3"><?= $cotizacion->comentario ?></textarea>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="col-md-4">
                                  <button class="label label-primary" onclick="moveStatus(this)" data-status='enviada' data-cotizacion="<?= $cotizacion->id ?>">Enviada</button>
                                  </div>
                                  <div class="col-md-4">
                                  <button class="label label-success" onclick="moveStatus(this)" data-status='aceptada' data-cotizacion="<?= $cotizacion->id ?>">Aceptada</button>
                                  </div>
                                  <div class="col-md-4">
                                  <button class="label label-danger" onclick="moveStatus(this)" data-status='cancelada' data-cotizacion="<?= $cotizacion->id ?>">Cancelada</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            </td>
                            <td><?= $cotizacion->usuario->username ?></td>
                            <td align="center"><?= $cotizacion->aplica_promocion== 1 ? '✓' : '' ?></td>
                            <td><?= $this->Time->format($cotizacion->creada, 'Y-M-d') ?></td>

                            <td>
                              <div class="dropdown mx-100">
                                <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                  <?php if($this->UserAuth->isAdmin()):?>
                                  <li><?= $this->Html->link('Ver', ['action' => 'view', $cotizacion->id]) ?></li>
                                  <li><?= $this->Html->link(__('Editar'), ['action' => 'edit', $cotizacion->id]) ?></li>
                                  <li role="presentation"><?=  $this->Form->postLink(__('Delete'), ['action' => 'delete', $cotizacion->id], ['confirm' => __('Seguro que quiere borrar el registro #{0}?', $cotizacion->id), 'title' => __('Delete'), "escape" => false]) ?></li>
                                  <?php endif; ?>
                                </ul>
                              </div>
                            </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                </table>
                <!--MODAL.... CAMBIO STATUS-->
      
                <?php echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de cotizaciones')]);?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('modalURL'); ?>
<script type="text/javascript">

  function moveStatus(button){
    var id = $(button).data('cotizacion'),
        status = $(button).data('status'),
        comentario = $(button).closest('.row').find('textarea.comentario').val();
    $('.active_popover').each(function(){
      $(this).popover('hide');
    });
    $.ajax({
        type: 'POST',
        url: '<?php echo $this->Url->build(["controller" => "Cotizaciones","action" => "changeStatus"]); ?>',
        data: {id_cotizacion:id, status:status, comentario:comentario},
        dataType: 'json',
        success: function(data){
            if(data['result'] === true){
              switch(status){
                case 'enviada':
                    $('span#change_status_'+id).html('<span class="label label-primary">'+status+'</span>');
                    break;
                case 'aceptada':
                    $('span#change_status_'+id).html('<span class="label label-success">'+status+'</span>');
                    break;
                case 'cancelada':
                    $('span#change_status_'+id).html('<span class="label label-danger">'+status+'</span>');
                    break;
                default:
                    $('span#change_status_'+id).html('<span class="label label-default">'+status+'</span>');
                    break;
              }
            }else{
              alert('Hubo un error en el sistema!');
            }
        }
    });
  }

	$(document).ready(function(){

    $('.pop_content').each(function(){
      var content = $(this).html();
      $(this).siblings('.active_popover').attr('data-content', content);
      $(this).remove();
    });

    $('.active_popover').click(function(){
      $(this).popover('show');
      $('.active_popover').not(this).popover('hide');
    });

		$('.modal').insertBefore('body');

		$('#fecha_inicio').datepicker({
			'setDate': new Date(),
			'format': 'yyyy-mm-dd',
			'autoclose': true 
		});

		$('#fecha_fin').datepicker({
			'setDate': new Date(),
			'format': 'yyyy-mm-dd',
			'autoclose': true 
		});
    $('#fecha_inicio').val('<?php echo $fecha_inicio; ?>');
    $('#fecha_fin').val('<?php echo $fecha_fin; ?>');

	});
</script>
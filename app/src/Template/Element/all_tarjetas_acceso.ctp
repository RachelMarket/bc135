<?php echo $this->Html->css('../css/modal_styles.css'); ?>
<div id="updateProductosIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2> Tarjetas de Acceso</h2>
      <?php if($this->UserAuth->isAdmin()):?>
      <?= $this->Html->link('Agregar Tarjeta de Acceso', '/TarjetasAcceso/add/', ['class' => 'btn btn-cancel btn-back','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?>
      <?php endif; ?>
    </div>
    <div class="ibox-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins no-mar">
            <div class="ibox-content">
              <div class="table-responsive">
                <?php echo $this->Search->searchForm('TarjetasAcceso', ['legend'=>false, 'updateDivId'=>'updateProductosIndex']); ?>    
                <table class="table ca-table">
                  <thead>
                    <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                      <th><h5><?= $this->Paginator->sort('TarjetasAcceso.num_tarjeta',__('Número de Tarjeta')) ?></h5></th>
                      <th><h5><?= $this->Paginator->sort('Contactos.nombre',__('Asignado')) ?></h5></th>
                      <th><h5><?= $this->Paginator->sort('TarjetasAcceso.created',__('Creado')) ?></h5></th>
                      <th><h5><?= $this->Paginator->sort('TarjetasAcceso.modified',__('Modificado')) ?></h5></th>
                      <th><h5>Acciones</h5></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($tarjetasAcceso as $acceso): ?>
                    <tr>
                      <td><p><?= h($acceso->num_tarjeta) ?></p></td>
                      <?php if(!empty($acceso['tarjetas_acceso_join']['contacto']['nombre'])): ?>
                      <td><p><?= h($acceso['tarjetas_acceso_join']['contacto']['nombre'].' '.$acceso['tarjetas_acceso_join']['contacto']['apellido_paterno'].' '.$acceso['tarjetas_acceso_join']['contacto']['apellido_materno']) ?></p></td>
                      <?php else: ?>
                      <td><p>N/A</p></td>
                      <?php endif; ?>
                      <td><p><?= $this->Time->format($acceso->created,'YYY-MM-dd HH:mm a') ?></p></td>
                      <td><p><?= $this->Time->format($acceso->modified,'YYY-MM-dd HH:mm a') ?></p></td>
                      <td>
                        <div class="dropdown">
                          <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <?php if($this->UserAuth->isAdmin()):?>
                            <li><?= $this->Html->link('Editar', '/TarjetasAcceso/edit/'.$acceso->id, ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?></li>
                            <?php endif; ?>
                            <?php if($this->UserAuth->isAdmin()):?>
                            <li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $acceso['id']], ['confirm' => __('Seguro que quiere borrar el registro #{0}?', $acceso['id']), 'title' => __('Delete'), "escape" => false]) ?></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <?php if (!empty($tarjetasAcceso)) {
                  echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de Tarjetas de Accesso')]);
                  } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('modalURL'); ?>
<script type="text/javascript">
  $(document).ready(function(){
      $('.modal').insertBefore($('body'));
  });
</script>
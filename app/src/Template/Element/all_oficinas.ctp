<?php echo $this->Html->css('/css/modal_styles.css'); ?>
<div id="updateoficinasIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2>Oficinas</h2>

      <?php if($this->UserAuth->isAdmin()):?>
      <?= $this->Html->link('Agregar oficina', '/oficinas/add', ['class' => 'btn btn-back btn-cancel']); ?>
      <?php endif; ?>
    </div>
      <div class="ibox-content">
        <?php echo $this->Search->searchForm('Oficinas', ['legend'=>false, 'updateDivId'=>'updateoficinasIndex','custom'=>true, 'buttons'=>[]]); ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins no-mar">
              <div class="ibox-content">
                <div class="table-responsive">
                  <table class="table ca-table">
                    <thead>
                      <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                        <th scope="col"><h5><?= $this->Paginator->sort('id') ?></h5></th>
                        <th scope="col"><h5><?= $this->Paginator->sort('centro_id') ?></h5></th>
                        <th scope="col"><h5><?= $this->Paginator->sort('numero') ?></h5></th>
                        <th scope="col"><h5><?= $this->Paginator->sort('capacidad') ?></h5></th>
                        <th scope="col"><h5><?= $this->Paginator->sort('precio_minimo') ?></h5></th>
                        <th scope="col"><h5><?= $this->Paginator->sort('precio_medio') ?></h5></th>
                        <th scope="col"><h5><?= $this->Paginator->sort('precio_maximo') ?></h5></th>
                        <?php 
                          if($flag){
                            echo "<th scope='col'><h5>".$this->Paginator->sort('eliminado')."</h5></th>";
                          }
                        ?>
                        <th scope="col"><h5><?= __('Actions') ?></h5></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($oficinas as $oficina): ?>
                      <tr>
                        <td><p><?= $this->Number->format($oficina->id) ?></p></td>
                        <td><p><?= $oficina->centro->nombre ?></p></td>
                        <td><p><?= $this->Number->format($oficina->numero) ?></p></td>
                        <td><p><?= $this->Number->format($oficina->capacidad) ?></p></td>
                        <td><p><?= $this->Number->format($oficina->precio_minimo) ?></p></td>
                        <td><p><?= $this->Number->format($oficina->precio_medio) ?></p></td>
                        <td><p><?= $this->Number->format($oficina->precio_maximo) ?></p></td>
                        <?php
                          if($flag){
                            echo "<td align='center'><p>";
                            echo $oficina->eliminado == 1 ? '✓' : '';
                            echo "</p></td>";
                          }
                        ?>
                        <td>
                          <div class="dropdown mx-100">
                            <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                              <?php if($this->UserAuth->isAdmin()):?>
                              <li><?= $this->Html->link('Editar', '/oficinas/edit/'.$oficina->id, ['escape' => false]); ?></li>
                              <li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $oficina->id], ['confirm' => __('Seguro que quiere borrar el registro #{0}?', $oficina->id), 'title' => __('Delete'), "escape" => false]) ?></li>
                              <?php endif; ?>
                            </ul>
                          </div>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                </table>
                <?php echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de oficinas')]);?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('modalURL'); ?>
<script type="text/javascript">
  $(document).ready(function(){
      $('.modal').insertBefore($('body'));
  });
</script>
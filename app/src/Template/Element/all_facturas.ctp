<?php
  $hoy = new DateTime();
  ?>
</pre>
<div id="updateProductosIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2>Facturas</h2>
      <div class="join-btn">
        <?php $url=$this->Paginator->generateUrl(['action'=>'index','_ext'=>'xlsx']); ?>
        <a href="<?php echo html_entity_decode($url);?>" class="btn btn-save" style="margin-right:5px;">Exportar</a>
        <div class="dropdown pull-right">
          <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Opciones</button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="/facturas/prefacturas">Facturación Mensual</a></li>
            <li><a href="#" onclick="timbrar_seleccion();">Timbrar selección</a></li>
            <li><a href="#" onclick="enviar_seleccion();">Enviar selección</a></li>
            <li><a href="#" onclick="descargar_seleccion();">Descargar selección</a></li>
            <!--
              <li role="separator" class="divider"></li>
              <li><a href="#">Eliminar selección</a></li>
              -->
          </ul>
        </div>
      </div>
    </div>
    <div class="ibox-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins no-mar">
            <div class="ibox-content">
              <?php echo $this->Search->searchForm('Facturas', ['legend'=>false, 'updateDivId'=>'updateProductosIndex', 'custom'=>true, 'buttons'=>[]]); ?>
              <?= $this->Form->create($facturas,['id'=>'formFacturas', 'method' => 'POST']); ?>
              <div class="table-responsive">
                <table class="table ca-table">
                  <thead>
                    <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                      <th><h5><input type="checkbox" value="0" class='checkall'></h5></th>
                      <th><h5><?= $this->Paginator->sort('folio','Factura') ?></h5></th>
                      <th><h5><?= $this->Paginator->sort('created','Creación') ?></h5></th>
                      <th><h5><?// $this->Paginator->sort('Clientes.id','Cliente');?>Cliente</h5></th>
                      <th><h5><?// $this->Paginator->sort('Clientes.rfc','RFC');?>RFC</h5></th>
                      <th><h5><?= $this->Paginator->sort('fecha','Fecha de vencimiento');?></h5></th>
                      <th><h5>D&iacute;as vencidos</h5></th>
                      <th><h5><?= $this->Paginator->sort('total','Monto');?></h5></th>
                      <th><h5><?= $this->Paginator->sort('fecha_pago','Fecha de pago');?></h5></th>
                      <th><h5><?= $this->Paginator->sort('festado_id','Timbre');?></h5></th>
                      <th><h5>Acciones</h5></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($facturas as $factura): ?>
                    <tr>
                      <td><p><input name="factura_id[]" type="checkbox" value="<?php echo $factura->id; ?>" data-id="<?php echo $factura->id; ?>"></p></td>
                      <!-- <td><p><?= $factura->has('cliente') ? $this->Html->link($factura->cliente->nombre, ['controller' => 'Clientes', 'action' => 'view', $factura->cliente->id]) : '' ?></p></td> -->
                      <td><p><?php if($factura->folio != ''){ echo $factura->serie."-".$factura->folio; } ?></p></td>
                      <td><p><?php if($factura->created != ''){ echo date_format($factura->created,"d-m-Y");} ?></p></td>
                      <td><p><?= $factura->has('cliente') ? $this->Html->link($factura->cliente->nombre, ['controller' => 'Clientes', 'action' => 'view', $factura->cliente->id]) : '' ?></p></td>
                      <td><p><?= h($factura->cliente->rfc) ?></p></td>
                      <td><p><? echo date_format($factura->fecha,"d-m-Y"); ?></p></td>
                      <td><p><span class="label label-danger"><?php 
                        if ($factura->festado_id != 3){
                            if($factura->fecha_pago == ''){
                                $fecha = new DateTime($factura->fecha->i18nFormat('YYYY-MM-dd HH:mm:ss'));
                        
                                 if($fecha <= $hoy){
                                    $interval = $fecha->diff($hoy);
                                    echo $interval->format('%a días');
                                }
                        
                            }else{
                                 $fecha = new DateTime($factura->fecha->i18nFormat('YYYY-MM-dd HH:mm:ss'));
                                 $pago = new DateTime($factura->fecha_pago->i18nFormat('YYYY-MM-dd HH:mm:ss'));
                        
                                
                                if($fecha <= $pago){
                                    $interval = $fecha->diff($pago);
                                    echo $interval->format('%a días');
                                }
                                
                            }
                        
                        }
                        ?></span></p></td>
                      <td><p><?php   echo "$".number_format($factura->subtotal, 2);
                        echo "&nbsp;".$factura->moneda->nombre;
                        //debug($factura);
                        ?></p></td>
                      <td id="pago_<?php echo $factura->id; ?>"><?php if($factura->fecha_pago != ''){ echo date_format($factura->fecha_pago,"d-m-Y"); } ?></p></td>
                      <td id="timbre_<?php echo $factura->id; ?>">
                        <?php if($factura->festado_id == 2): ?> 
                        <i class="fa fa-check"></i>
                        <?php endif; ?>
                        <?php if($factura->festado_id == 3): ?> 
                        <i class="fa fa-circle-o"></i>
                        <?php endif; ?>
                      </p></td>
                      <td><p>
                        <div class="dropdown">
                          <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                          <ul class="dropdown-menu" id="dropdown-menu-<?= $factura->id;?>" role="menu" aria-labelledby="menu1">
                            <?php //$this->Html->link('<i class="fa fa-money"></i>&nbsp;Registrar Pago', ['action' => 'javascript:void(0);'], ['onclick'=> "paguito(this)", 'class'=> "pagar_factura", 'data-id'=> $factura->id, 'title' => __('Registrar pago'), "escape" => false]) ?>
                            <?php if($factura->festado_id == 3): ?>
                            <li role="presentation"><?= $this->Html->link('Descargar PDF', ['action' => 'download', $factura->id,0], ['data-id'=> $factura->id, 'title' => __('Descargar PDF'), "escape" => false,  'target' => '_blank']) ?></li>
                            <!--li role="presentation"><?= $this->Html->link('Prefactura PDF', ['action' => 'descargarpdf', $factura->id], ['target'=>"_blank", 'data-id'=> $factura->id, 'title' => __('Prefactura PDF'), "escape" => false]) ?></li-->
                            <!--li role="separator" class="divider"></li>
                              <li role="presentation"><?= $this->Html->link('Enviar Prefactura PDF', ['action' => 'enviarprefacturapdf', $factura->id], ['data-id'=> $factura->id, 'title' => __('Enviar Prefactura PDF'), "escape" => false]) ?></li-->
                            <?php endif; ?>
                            <?php if($factura->festado_id != 2 ){ ?>
                            <li role="presentation">
                              <a data-toggle="modal" data-target="#modal_cambiar_formapago"  class="cambiar_forma_pago" data-factura_id="<?php echo $factura->id ?>" href="javascript:void(0);">
                              Cambiar Forma Pago
                              </a>
                            </li>
                            <?php } ?>
                            <?php if($factura->fecha_pago == '' && $factura->festado_id != 3){ ?> 
                            <li role="presentation"><a href="javascript:void(0);" onclick="pago(this);" class="pagar_factura" data-id="<?= $factura->id; ?>" data-metodop="<?php echo $factura->formasdepago->nombre ?>" data-folio="<?= $factura->serie.'-'.$factura->folio; ?>">Registrar Pago</a></li>
                            <li role="separator" class="divider"></li>
                            <?php } ?>
                            <?php if($factura->festado_id == 1): ?>
                            <li role="presentation"><?= $this->Html->link('Timbrar', '#', ['class'=>"timbrar_factura", 'data-id'=> $factura->id, 'title' => __('Timbrar Prefactura'), "escape" => false]) ?></li>
                            <li role="presentation"><?= $this->Html->link('Prefactura PDF', ['action' => 'download', $factura->id,0], ['target'=>"_blank", 'data-id'=> $factura->id, 'title' => __('Prefactura PDF'), "escape" => false]) ?></li>
                            <li role="presentation"><?= $this->Html->link('Eliminar', '#',['title' => __('Eliminar Prefactura'),'onclick' =>'delete_prefactura('.$factura->id.')', "escape" => false]) ?></li>
                            <li role="separator" class="divider"></li>
                            <li role="presentation"><?= $this->Html->link('Enviar Prefactura PDF', ['action' => 'enviarprefacturapdf', $factura->id], ['data-id'=> $factura->id, 'title' => __('Enviar Prefactura PDF'), "escape" => false]) ?></li>
                            <?php endif; if($factura->festado_id == 2): ?>
                            <li role="presentation"><?= $this->Html->link('Descargar PDF', ['action' => 'download', $factura->id,0], ['data-id'=> $factura->id, 'title' => __('Descargar PDF'), "escape" => false,  'target' => '_blank']) ?></li>
                            <li role="presentation"><?= $this->Html->link('Descargar XML', ['action' => 'download', $factura->id,1], ['data-id'=> $factura->id, 'title' => __('Descargar XML'), "escape" => false,  'target' => '_blank']) ?></li>
                            <li role="presentation"><a href="javascript:void(0);" onclick="cancelar(this);" class="cancelar_factura" data-id="<?= $factura->id; ?>">Cancelar Factura</a></li>
                            <li role="separator" class="divider"></li>
                            <li role="presentation"><?= $this->Html->link('Enviar Factura PDF/XML', ['action' => 'enviarfacturapdf', $factura->id], ['data-id'=> $factura->id, 'title' => __('Enviar Factura PDF'), "escape" => false]) ?></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      </p></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <?= $this->Form->end() ?>
                <?php echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de Facturas')]);?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $('#datepicker1').datepicker({
      "setDate": new Date(),
      "format": 'yyyy-mm-dd',
      "autoclose": true
      });
  
      $('#datepicker2').datepicker({
      "setDate": new Date(),
      "format": 'yyyy-mm-dd',
      "autoclose": true
      }); 
      $('#datepicker_pago').datepicker({
          "setDate": new Date(),
          "autoclose": true
      });
      $('#datepicker_pago').datepicker('update');
      $('#datepicker1').change(function (){
          $('#facturas-fecha').val($('#datepicker1').val());
          //alert($('#facturas-fecha').val());
  
      })
  
      $('#datepicker1').val('<?php echo $fecha_inicio; ?>');
      $('#datepicker2').val('<?php echo $fecha_fin; ?>');
  
      $('.checkall').click(function(e) {
  
            var chk = $(this).prop('checked');
            $(':checkbox').each(function() {
                  this.checked = chk;                        
              });
  
            //$('input', dTable.fnGetNodes()).prop('checked',chk);
          });
  
</script>
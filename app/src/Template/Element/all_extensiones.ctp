<?php echo $this->Html->css('../css/modal_styles.css'); ?>
<div id="updateProductosIndex">
  <div class="ibox float-e-margins">
    <div class="ibox-title">
      <h2>Extensiones</h2>
      <?php if($this->UserAuth->isAdmin()):?>
      <?= $this->Html->link('Agregar Extensión', '/Extensiones/add/', ['class' => 'btn btn-cancel btn-back','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?>
      <?php endif; ?>
    </div>
    <div class="ibox-content">
      <?php echo $this->Search->searchForm('Extensiones', ['legend'=>false, 'updateDivId'=>'updateProductosIndex','custom'=>true, 'buttons'=>[]]); ?>
      <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins no-mar">
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table ca-table">
              <thead>
                <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                    <th>
                      <h5><?= $this->Paginator->sort('Extensiones.extension',__('Extensión'));?></h5>
                    </th>
                    <th>
                      <h5><?= $this->Paginator->sort('Extensiones.ip',__('IP'));?></h5>
                    </th>
                    <th>
                      <h5><?= $this->Paginator->sort('Extensiones.mac',__('MAC'));?></h5>
                    </th>
                    <th>
                      <h5><?= $this->Paginator->sort('Contactos.nombre',__('Asignado')) ?></h5>
                    </th>
                    <th>
                      <h5>Acciones</h5>
                    </th>
                  </thead>
                  <tbody>
                    <?php foreach ($extensiones as $extension): ?>
                    <tr>
                      <td><p><?= h($extension->extension) ?></p></td>
                      <td><p><?= h($extension->ip) ?></p></td>
                      <td><p><?= h($extension->mac) ?></p></td>
                      <?php if(!empty($extension['contactos_extensiones_join']['contacto']['nombre'])): ?>
                      <td><p><?= h($extension['contactos_extensiones_join']['contacto']['nombre'].' '.$extension['contactos_extensiones_join']['contacto']['apellido_paterno'].' '.$extension['contactos_extensiones_join']['contacto']['apellido_materno']) ?></p></td>
                      <?php else: ?>
                      <td><p>N/A</p></td>
                      <?php endif; ?>
                      <td><p>
                        <div class="dropdown mx-100">
                          <button class="btn btn-actions dropdown-toggle" data-toggle="dropdown">Acciones</button>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <?php if($this->UserAuth->isAdmin()):?>
                            <li><?= $this->Html->link('Editar', '/Extensiones/edit/'.$extension->id, ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modalURL']); ?></li>
                            <?php endif; ?>
                            <?php if($this->UserAuth->isAdmin()):?>
                            <li role="presentation"><?= $this->Form->postLink('Borrar', ['action' => 'delete', $extension->id], ['confirm' => __('Seguro que quiere borrar el registro #{0}?', $extension->id), 'title' => __('Delete'), "escape" => false]) ?></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      </p></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <?php echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Número de Extensiones')]);?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('modalURL'); ?>
<script type="text/javascript">
  $(document).ready(function(){
      $('.modal').insertBefore($('body'));
  });
</script>
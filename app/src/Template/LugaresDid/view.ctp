
<div class="ibox">
    <div class="ibox-title">

        <h5> <?php echo __('lugaresDid Details'); ?> </h5>

        <span class="ibox-tools">
            

            <?= $this->Html->link(__('PDF'), ['action' => 'view', '_ext'=>'pdf' , $lugaresDid->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

            <?= $this->Html->link(__('Edit Lugares Did'), ['action' => 'edit', $lugaresDid->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

        </span>

    </div>

    <div class="ibox-content">
        
  

<dl class="dl-horizontal">

        
                       
                        <dt><?= __('Lugar') ?>:</dt> 
                    <dd><?= h($lugaresDid->lugar) ?></dd>
                
          
                
    


        
                       
        <dt><?= __('Id') ?>:</dt> 
            <dd><?= $this->Number->format($lugaresDid->id) ?></dd>
         
               
            
                       
        <dt><?= __('Created') ?>:</dt> 

                <dd><?= h($lugaresDid->created) ?></dd>
          
                       
        <dt><?= __('Modified') ?>:</dt> 

                <dd><?= h($lugaresDid->modified) ?></dd>
          
            
            
                  
        <dt><?= __('Deleted') ?>:</dt>
                <dd><?= $lugaresDid->deleted ? __('Yes') : __('No'); ?></dd>
             
                  
        <dt><?= __('Assigned') ?>:</dt>
                <dd><?= $lugaresDid->assigned ? __('Yes') : __('No'); ?></dd>
             
               
    


</dl>

<ul id="myTab" class="nav nav-tabs" role="tablist">
   
</ul>

<div id="myTabContent" class="tab-content">
</div>
</div>
</div>


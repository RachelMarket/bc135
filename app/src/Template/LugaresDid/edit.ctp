<div class="panel-heading">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title"> <?= __('Editar Tipo DID') ?> </h4>
</div>

<?= $this->Form->create($lugaresDid , [ 'class'=>'form-horizontal'] ); ?>

<div class="modal-body">

    <div class="um-form-row form-group">
        <label class="col-sm-2 control-label"><?php echo __('Type'); ?></label>
        <div class="col-sm-10">
             <?= $this->Form->input('LugaresDid.lugar', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => false]); ?>
        </div>
    </div>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel') ?></button>
    <?= $this->Form->button( __('Save' ) , ['class'=>'btn btn-primary'  ]) ?> 
</div>

<?= $this->Form->end() ?>

<div class="panel-heading">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title"> <?= __('Editar Extensión') ?> </h4>
</div>

<?= $this->Form->create($extension , [ 'class'=>'form-horizontal'] ); ?>

<div class="modal-body">

    <div class="um-form-row form-group">
        <label class="col-sm-2 control-label"><?php echo __('Extensión'); ?></label>
        <div class="col-sm-10">
            <?php echo $this->Form->input('Extensiones.extension', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control', 'required' => 'required']); ?>
        </div>
    </div>

    <div class="um-form-row form-group">
        <label class="col-sm-2 control-label"><?php echo __('IP'); ?></label>
        <div class="col-sm-10">
            <?php echo $this->Form->input('Extensiones.ip', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control', 'required' => 'required']); ?>
        </div>
    </div>
    <div class="um-form-row form-group">
        <label class="col-sm-2 control-label"><?php echo __('MAC'); ?></label>
        <div class="col-sm-10">
            <?php echo $this->Form->input('Extensiones.mac', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control', 'required' => 'required']); ?>
        </div>
    </div>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel') ?></button>
    <?= $this->Form->button( __('Save' ) , ['class'=>'btn btn-primary'  ]) ?> 
</div>

<?= $this->Form->end() ?>

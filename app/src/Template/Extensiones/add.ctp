<div class="panel-heading">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title"> <?= __('Agregar Extensión') ?> </h4>
</div>

<?= $this->Form->create($extension , [ 'class'=>''] ); ?>
<div class="modal-body">
  <div class="row ca-forms">
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label class="required"><?php echo __('Extensión'); ?></label>
            <div>
              <?php echo $this->Form->input('Extensiones.extension', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control', 'required' => 'required']); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label class="required"><?php echo __('IP'); ?></label>
            <div>
              <?php echo $this->Form->input('Extensiones.ip', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control', 'required' => 'required']); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label class="required"><?php echo __('MAC'); ?></label>
            <div>
              <?php echo $this->Form->input('Extensiones.mac', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control', 'required' => 'required']); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="last-btns">
            <button type="button" class="btn btn-cancel" data-dismiss="modal"><?php echo __('Cancel') ?></button>
            <?= $this->Form->button( __('Save' ) , ['class'=>'btn btn-save'  ]) ?>
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


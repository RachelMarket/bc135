<div class="um-panel">
    <div class="ibox-title">
        <div class="col-md-4">
        <span class="um-panel-title">
                <h2><?php echo __('Detalle de la Extensión'); ?></h2>
        </span>
        </div>
        <span class="um-panel-title-right col-md-4" style="text-align:right;">
            <?php echo $this->Html->link(__('<i class="fa fa-undo"></i> Listado'), ['action'=>'index'], ['class'=>'btn btn-primary pull-right', 'escape' => false]); ?>
        </span>
    </div><br><br>
    <div class="um-panel-content">

        <div class="um-form-row form-group">
            <label class="col-sm-2 control-label"><?php echo __('Clave'); ?></label>
            <div class="col-sm-3">
                 <p><?= h($extension->id) ?></p>
            </div>
        </div>

        <div class="um-form-row form-group">
            <label class="col-sm-2 control-label"><?php echo __('Extensión'); ?></label>
            <div class="col-sm-3">
                 <p><?= h($extension->extension) ?></p>
            </div>
        </div>


         <div class="um-form-row form-group">
            <label class="col-sm-2 control-label"><?php echo __('IP'); ?></label>
            <div class="col-sm-3">
                 <p><?= h($extension->ip) ?></p>
            </div>
        </div>

         <div class="um-form-row form-group">
            <label class="col-sm-2 control-label"><?php echo __('MAC'); ?></label>
            <div class="col-sm-3">
                 <p><?= h($extension->mac) ?></p>
            </div>
        </div>

    </div>
    
</div>
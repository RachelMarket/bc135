<div class="ibox float-e-margins">
  <?= $this->Form->create($clientesServicio); ?>
  <div class="ibox-title">
    <h2><?= __('Registrar {0}', [$tipo_name]) ?></h2>
    <a class="btn btn-cancel btn-back" href="<?= $this->request->referer(); ?>">Cancelar</a>
  </div>
  <div class="ibox-content">
    <div class="row ca-forms mtp-40">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
          <div class="col-md-12">
            <?= $this->Form->input('nombre'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('unidad_id', ['options' => $unidades]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('precio_mxn'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('precio_usd'); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('cantidad'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?= $this->Form->input('descripcion'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?php echo $this->Form->input('fecha', array('class ' => 'form-control', 'type'=>'text',  'value' => date('d-m-Y')));?>
          </div>
          <div class="col-md-6">
            <label>Notificar via correo a socio</label><br>
              <?= $this->Form->checkbox('notificacion', array( 'checked' => true)); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="last-btns">
              <?= $this->Form->button(__('Registrar'), ['class' => 'btn btn-save']) ?>
              <a class="btn btn-cancel" href="<?= $this->request->referer(); ?>">Cancelar</a>
              <?= $this->Form->end() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $('#fecha').datepicker({ 
      autoclose: true,
      format: "dd-mm-yyyy"
  
      });
  
</script>
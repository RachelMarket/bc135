<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Editar Clientes Servicio'), ['action' => 'edit', $clientesServicio->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Eliminar Clientes Servicio'), ['action' => 'delete', $clientesServicio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientesServicio->id)]) ?> </li>
    <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('Nuevo Clientes Servicio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => 'Servicios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Facturas'), ['controller' => 'Facturas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Factura'), ['controller' => 'Facturas', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Unidades'), ['controller' => 'Unidades', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Unidad'), ['controller' => 'Unidades', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>

<h2><?= h($clientesServicio->nombre) ?></h2>
<div class="row">
        <div class="col-lg-5">
                                    <h6><?= __('Cliente') ?></h6>
                    <p><?= $clientesServicio->has('cliente') ? $this->Html->link($clientesServicio->cliente->nombre, ['controller' => 'Clientes', 'action' => 'view', $clientesServicio->cliente->id]) : '' ?></p>
                                                    <h6><?= __('Servicio') ?></h6>
                    <p><?= $clientesServicio->has('servicio') ? $this->Html->link($clientesServicio->servicio->nombre, ['controller' => 'Servicios', 'action' => 'view', $clientesServicio->servicio->id]) : '' ?></p>
                                                    <h6><?= __('Factura') ?></h6>
                    <p><?= $clientesServicio->has('factura') ? $this->Html->link($clientesServicio->factura->id, ['controller' => 'Facturas', 'action' => 'view', $clientesServicio->factura->id]) : '' ?></p>
                                                    <h6><?= __('Usuario') ?></h6>
                    <p><?= $clientesServicio->has('usuario') ? $this->Html->link($clientesServicio->usuario->nombre, ['controller' => 'Usuarios', 'action' => 'view', $clientesServicio->usuario->id]) : '' ?></p>
                                                    <h6><?= __('Tipo') ?></h6>
                    <p><?= $clientesServicio->has('tipo') ? $this->Html->link($clientesServicio->tipo->nombre, ['controller' => 'Tipos', 'action' => 'view', $clientesServicio->tipo->id]) : '' ?></p>
                                                    <h6><?= __('Nombre') ?></h6>
                    <p><?= h($clientesServicio->nombre) ?></p>
                                                    <h6><?= __('Unidad') ?></h6>
                    <p><?= $clientesServicio->has('unidad') ? $this->Html->link($clientesServicio->unidad->nombre, ['controller' => 'Unidades', 'action' => 'view', $clientesServicio->unidad->id]) : '' ?></p>
                                </div>
            <div class="col-lg-2">
                    <h6><?= __('Id') ?></h6>
                <p><?= $this->Number->format($clientesServicio->id) ?></p>
                    <h6><?= __('Precio Mxn') ?></h6>
                <p><?= $this->Number->format($clientesServicio->precio_mxn) ?></p>
                    <h6><?= __('Precio Usd') ?></h6>
                <p><?= $this->Number->format($clientesServicio->precio_usd) ?></p>
                    <h6><?= __('Cantidad') ?></h6>
                <p><?= $this->Number->format($clientesServicio->cantidad) ?></p>
                    <h6><?= __('Notificacion') ?></h6>
                <p><?= $this->Number->format($clientesServicio->notificacion) ?></p>
                </div>
            <div class="col-lg-2">
                    <h6><?= __('Fecha') ?></h6>
                <p><?= h($clientesServicio->fecha) ?></p>
                </div>
        </div>
    <div class="row texts">
            <div class="col-lg-9">
                <h6><?= __('Descripcion') ?></h6>
                <?= $this->Text->autoParagraph(h($clientesServicio->descripcion)); ?>
            </div>
        </div>
    <ul id="myTab" class="nav nav-tabs" role="tablist">
   
</ul>

<div id="myTabContent" class="tab-content">
</div>


<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  $this->start('tb_sidebar');
  ?>
<ul class="nav nav-sidebar">
  <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['action' => 'index']) ?></li>
  <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => 'Servicios', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Facturas'), ['controller' => 'Facturas', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Factura'), ['controller' => 'Facturas', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Unidades'), ['controller' => 'Unidades', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Unidad'), ['controller' => 'Unidades', 'action' => 'add']) ?> </li>
</ul>
<?php $this->end(); ?>

<div class="ibox float-e-margins">
  <div class="ibox-title">
    <h2><?= __('Registrar {0}', [$tipo_name]) ?></h2>
    <a class="btn btn-back btn-cancel" href="<?= $this->request->referer(); ?>">Cancelar</a>
  </div>
  <div class="ibox-content">
    <?= $this->Form->create($clientesServicio); ?>
    <div class="row ca-forms mtp-40">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
          <div class="col-md-12">
            <p>Cliente:</p>
            <h3><?= $cliente->nombre; ?></h3>
            <?= $this->Form->hidden('cliente_id', ['value' => $cliente->id]); ?>
            <?= $this->Form->hidden('tipo_id', ['value' => 2]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label class="">Servicios</label>
            <select class="" name="servicio_id" onchange="whatsup(this)" id="servicios-select">
              <option>Seleccione un servicio a registrar</option>
              ;
              <?php foreach ($servicios as $servicio): ?>
              <option data-type="<?= $servicio['tipo_id'] ?>" value="<?= $servicio['id'] ?>"><?= $servicio['nombre'] ?></option>
              ";
              <? endforeach; ?>    
            </select>
          </div>
          <div class="col-md-6">
            <?php if($tipo_id == 2): ?>
            <label class="">Recordatorio vigencia precio anual</label><br>
            <?php endif; ?>
            <?php if($tipo_id == 3): ?>
            <label class="">Notificar via correo a socio</label><br>
            <?php endif; ?>
            <?= $this->Form->checkbox('notificacion', array( 'checked' => true)); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <br>
            <label class="">Descripción</label>
            <textarea class="descripcion txs"></textarea>
          </div>
        </div>
        <div class="row" style="display: flex; align-items: center; height:6em;">
            <div class="col-md-2">
              <p>Precio MXN:</p>
              <h2 id="precio-mxn">$0</h2>
            </div>
            <div class="col-md-offset-4 col-md-4">
              <p>Precio USD:</p>
              <h2 id="precio-usd">$0</h2>
            </div>
            <br>
          </div>
          <?php if($tipo_id == 3): ?>
          <div class="row" style="display: flex; align-items: center; height:6em;">
            <div style="" class="col-md-1">
              Cantidad:
            </div>
            <div class="col-md-1">
              <input name="cantidad" type="number" class="form-control" step="any" min="1" value="1" />
            </div>
            <br>
          </div>
          <?php endif; ?>
          <?php if($tipo_id == 2): ?>
          <?= $this->Form->hidden('cantidad', ['value' => 1]); ?>
          <?php endif; ?>  
          <div class="row">
            <legend></legend>
            <div class="col-md-12">
              <div class="last-btns">
                <a class="btn btn-cancel" href="<?= $this->request->referer(); ?>">Cancelar</a>
                <?= $this->Form->button(__('Registrar'), ['class' => 'btn btn-save submit']) ?>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
  <?= $this->Form->end() ?>
<script>
  $("#servicios-select").chosen({disable_search_threshold: 3});
  
  var servicios = <?= $servicios_json ?>;
  
  function whatsup(e){
      $('.descripcion').html(servicios[e.selectedIndex-1].descripcion);
      $('#precio-mxn').html('$'+servicios[e.selectedIndex-1].precio_mxn.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $('#precio-usd').html('$'+servicios[e.selectedIndex-1].precio_usd.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
  }
  
</script>
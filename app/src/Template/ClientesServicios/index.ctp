<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['action' => 'add']); ?></li>
        <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => ' Clientes', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => ' Servicios', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Facturas'), ['controller' => 'Facturas', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Factura'), ['controller' => ' Facturas', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Usuario'), ['controller' => ' Usuarios', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Tipos'), ['controller' => 'Tipos', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Tipo'), ['controller' => ' Tipos', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Unidades'), ['controller' => 'Unidades', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Unidad'), ['controller' => ' Unidades', 'action' => 'add']); ?></li>
                </ul>
<?php $this->end(); ?>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="col-md-4">
                        <h2> Clientes Servicios</h2>
                    </div>
                    <div class="ibox-tools col-md-4 pull-right">
                        <a href="/clientesServicios/add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Agregar Clientes Servicio</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table ata-order='[[ 1, "asc" ]]' data-page-length='50' class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                                                <th>id</th>
                                                                <th>cliente_id</th>
                                                                <th>servicio_id</th>
                                                                <th>factura_id</th>
                                                                <th>usuario_id</th>
                                                                <th>tipo_id</th>
                                                                <th>fecha</th>
                                                                <th class="actions">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($clientesServicios as $clientesServicio): ?>
                            <tr>
                                                                <td><?= $this->Number->format($clientesServicio->id) ?></td>
                                                                                            <td>
                                                    <?= $clientesServicio->has('cliente') ? $this->Html->link($clientesServicio->cliente->nombre, ['controller' => 'Clientes', 'action' => 'view', $clientesServicio->cliente->id]) : '' ?>
                                                </td>
                                                                                                <td>
                                                    <?= $clientesServicio->has('servicio') ? $this->Html->link($clientesServicio->servicio->nombre, ['controller' => 'Servicios', 'action' => 'view', $clientesServicio->servicio->id]) : '' ?>
                                                </td>
                                                                                                <td>
                                                    <?= $clientesServicio->has('factura') ? $this->Html->link($clientesServicio->factura->id, ['controller' => 'Facturas', 'action' => 'view', $clientesServicio->factura->id]) : '' ?>
                                                </td>
                                                                                                <td>
                                                    <?= $clientesServicio->has('usuario') ? $this->Html->link($clientesServicio->usuario->nombre, ['controller' => 'Usuarios', 'action' => 'view', $clientesServicio->usuario->id]) : '' ?>
                                                </td>
                                                                                                <td>
                                                    <?= $clientesServicio->has('tipo') ? $this->Html->link($clientesServicio->tipo->nombre, ['controller' => 'Tipos', 'action' => 'view', $clientesServicio->tipo->id]) : '' ?>
                                                </td>
                                                                                <td><?= h($clientesServicio->fecha) ?></td>
                                                                            <td class="actions">
                                    <div class="dropdown">
                                       <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                          <li role="presentation"><?= $this->Html->link('<i class="fa fa-pencil"></i>&nbsp;Editar', ['action' => 'edit', $clientesServicio->id], ['title' => __('Editar'), "escape" => false]) ?></li>
                                          <li role="presentation"><?= $this->Form->postLink('<i class="fa fa-trash"></i>&nbsp;Borrar', ['action' => 'delete', $clientesServicio->id], ['confirm' => __('Seguro que quiere borrar # {0}?', $clientesServicio->id), 'title' => __('Delete'), "escape" => false]) ?></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>

                                <?php endforeach; ?>
                             </tbody>
</table>
    </div></div></div></div></div>


<script type="text/javascript">
        $(document).ready(function() {
            $('.dataTables-example').dataTable({
                responsive: true,
                "dom": 'T<"clear">lfrtip',
                "lengthChange": false,
                 "footerCallback": function( tfoot, data, start, end, display ) {
                    $(tfoot).html( "" );
                }
            });
        });

</script>
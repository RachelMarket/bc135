<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  $this->start('tb_sidebar');
  ?>
<ul class="nav nav-sidebar">
  <li><?=
    $this->Form->postLink(
    __('Eliminar'),
    ['action' => 'delete', $clientesServicio->id],
    ['confirm' => __('Are you sure you want to delete # {0}?', $clientesServicio->id)]
    )
    ?></li>
  <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['action' => 'index']) ?></li>
  <li><?= $this->Html->link(__('Listar Clientes'), ['controller' => 'Clientes', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Cliente'), ['controller' => 'Clientes', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => 'Servicios', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Facturas'), ['controller' => 'Facturas', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Factura'), ['controller' => 'Facturas', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Tipos'), ['controller' => 'Tipos', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Tipo'), ['controller' => 'Tipos', 'action' => 'add']) ?> </li>
  <li><?= $this->Html->link(__('Listar Unidades'), ['controller' => 'Unidades', 'action' => 'index']) ?> </li>
  <li><?= $this->Html->link(__('Agregar Unidad'), ['controller' => 'Unidades', 'action' => 'add']) ?> </li>
</ul>
<?php $this->end(); ?>
<div class="page-padding">
  <?= $this->Form->create($clientesServicio); ?>
  <fieldset>
    <legend><?= __('Edit {0}', ['Clientes Servicio']) ?></legend>
    <?php
      echo $this->Form->input('cliente_id', ['options' => $clientes]);
              echo $this->Form->input('servicio_id', ['options' => $servicios]);
              echo $this->Form->input('factura_id', ['options' => $facturas]);
              echo $this->Form->input('usuario_id', ['options' => $usuarios]);
              echo $this->Form->input('tipo_id', ['options' => $tipos]);
              echo $this->Form->input('fecha');
              echo $this->Form->input('nombre');
              echo $this->Form->input('unidad_id', ['options' => $unidades]);
              echo $this->Form->input('precio_mxn');
              echo $this->Form->input('precio_usd');
              echo $this->Form->input('descripcion');
              echo $this->Form->input('cantidad');
              echo $this->Form->input('notificacion');
              ?>
  </fieldset>
  <?= $this->Form->button(__('Submit')) ?>
  <?= $this->Form->end() ?>
</div>
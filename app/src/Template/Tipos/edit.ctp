<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
        <li><?=
            $this->Form->postLink(
            __('Eliminar'),
            ['action' => 'delete', $tipo->id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $tipo->id)]
            )
            ?></li>
        <li><?= $this->Html->link(__('Listar Tipos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Clientes Servicios'), ['controller' => 'ClientesServicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Clientes Servicio'), ['controller' => 'ClientesServicios', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Servicios'), ['controller' => 'Servicios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Servicio'), ['controller' => 'Servicios', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>
<?= $this->Form->create($tipo); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Tipo']) ?></legend>
    <?php
        echo $this->Form->input('nombre');
                ?>
</fieldset>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>
<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
        <li><?=
            $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $formasdepago->id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $formasdepago->id)]
            )
            ?></li>
        <li><?= $this->Html->link(__('List Formasdepagos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Facturas'), ['controller' => 'Facturas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('New Factura'), ['controller' => 'Facturas', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>
<?= $this->Form->create($formasdepago); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Formasdepago']) ?></legend>
    <?php
        echo $this->Form->input('nombre');
                ?>
</fieldset>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>
<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Edit Formasdepago'), ['action' => 'edit', $formasdepago->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Delete Formasdepago'), ['action' => 'delete', $formasdepago->id], ['confirm' => __('Are you sure you want to delete # {0}?', $formasdepago->id)]) ?> </li>
    <li><?= $this->Html->link(__('List Formasdepagos'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('New Formasdepago'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Facturas'), ['controller' => 'Facturas', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('New Factura'), ['controller' => 'Facturas', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>

<h2><?= h($formasdepago->nombre) ?></h2>
<div class="row">
        <div class="col-lg-5">
                                    <h6><?= __('Nombre') ?></h6>
                    <p><?= h($formasdepago->nombre) ?></p>
                                </div>
            <div class="col-lg-2">
                    <h6><?= __('Id') ?></h6>
                <p><?= $this->Number->format($formasdepago->id) ?></p>
                </div>
            </div>
<ul id="myTab" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#Facturas" id="Facturas-tab" role="tab" data-toggle="tab" aria-controls="Facturas" aria-expanded="true">Facturas</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="Facturas" aria-labelledBy="Facturas-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($formasdepago->facturas)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Cliente Id') ?></th>
                                            <th><?= __('Folio') ?></th>
                                            <th><?= __('Serie') ?></th>
                                            <th><?= __('Fecha') ?></th>
                                            <th><?= __('Subtotal') ?></th>
                                            <th><?= __('Iva') ?></th>
                                            <th><?= __('Total') ?></th>
                                            <th><?= __('Cantidad Letra') ?></th>
                                            <th><?= __('Festado Id') ?></th>
                                            <th><?= __('Moneda Id') ?></th>
                                            <th><?= __('Observaciones') ?></th>
                                            <th><?= __('Cfdi') ?></th>
                                            <th><?= __('Response') ?></th>
                                            <th><?= __('Xml') ?></th>
                                            <th><?= __('AddRetencion') ?></th>
                                            <th><?= __('Retencion') ?></th>
                                            <th><?= __('Cfdicancelado') ?></th>
                                            <th><?= __('Tipo De Cambio') ?></th>
                                            <th><?= __('Fecha Pago') ?></th>
                                            <th><?= __('Referencia De Pago') ?></th>
                                            <th><?= __('Formasdepago Id') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($formasdepago->facturas as $facturas): ?>
                    <tr>
                                            <td><?= h($facturas->id) ?></td>
                                            <td><?= h($facturas->cliente_id) ?></td>
                                            <td><?= h($facturas->folio) ?></td>
                                            <td><?= h($facturas->serie) ?></td>
                                            <td><?= h($facturas->fecha) ?></td>
                                            <td><?= h($facturas->subtotal) ?></td>
                                            <td><?= h($facturas->iva) ?></td>
                                            <td><?= h($facturas->total) ?></td>
                                            <td><?= h($facturas->cantidad_letra) ?></td>
                                            <td><?= h($facturas->festado_id) ?></td>
                                            <td><?= h($facturas->moneda_id) ?></td>
                                            <td><?= h($facturas->observaciones) ?></td>
                                            <td><?= h($facturas->cfdi) ?></td>
                                            <td><?= h($facturas->response) ?></td>
                                            <td><?= h($facturas->xml) ?></td>
                                            <td><?= h($facturas->addRetencion) ?></td>
                                            <td><?= h($facturas->retencion) ?></td>
                                            <td><?= h($facturas->cfdicancelado) ?></td>
                                            <td><?= h($facturas->tipo_de_cambio) ?></td>
                                            <td><?= h($facturas->fecha_pago) ?></td>
                                            <td><?= h($facturas->referencia_de_pago) ?></td>
                                            <td><?= h($facturas->formasdepago_id) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Facturas', 'action' => 'view', $facturas->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Facturas', 'action' => 'edit', $facturas->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Facturas', 'action' => 'delete', $facturas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $facturas->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Facturas asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
</div>


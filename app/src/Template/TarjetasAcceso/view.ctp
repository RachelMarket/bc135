<style type="text/css">
    .um-button-row{
        margin-top: 100px;
    }
</style>
<div class="um-panel">
    <div class="ibox-title">
        <div class="col-md-4">
        <span class="um-panel-title">
                <h2><?php echo __('Detalle de la Tarjeta de Acesso'); ?></h2>
        </span>
        </div>
        <span class="um-panel-title-right col-md-4" style="text-align:right;">
            <?php echo $this->Html->link(__('Listado'), ['action'=>'index'], ['class'=>'btn btn-primary pull-right']); ?>
        </span>
    </div><br><br>
    <div class="um-panel-content">

        <?php echo $this->Form->create($tarjetasAcceso, ['class'=>'form-horizontal']); ?>

        <div class="um-form-row form-group">
            <label class="col-sm-2 control-label"><?php echo __('Clave'); ?></label>
            <div class="col-sm-3">
                 <p><?= h($tarjetasAcceso->id) ?></p>
            </div>
        </div>

        <div class="um-form-row form-group">
            <label class="col-sm-2 control-label"><?php echo __('Numero de la Tarjeta'); ?></label>
            <div class="col-sm-3">
                 <p><?= h($tarjetasAcceso->num_tarjeta) ?></p>
            </div>
        </div>

        <div class="um-form-row form-group">
            <label class="col-sm-2 control-label"><?php echo __('Fecha de Creación'); ?></label>
            <div class="col-sm-3">
                 <p><?= $tarjetasAcceso->created->format('d-m-Y h:i A') ?></p>
            </div>
        </div>

        <div class="um-form-row form-group">
            <label class="col-sm-2 control-label"><?php echo __('Fecha Modificada'); ?></label>
            <div class="col-sm-3">
                 <p><?= $tarjetasAcceso->modified->format('d-m-Y h:i A') ?></p>
            </div>
        </div>
        
        <?php echo $this->Form->end(); ?>
    </div>
    
</div>
<div class="panel-heading">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title"> <?= __('Editar Tarjeta de Acesso') ?> </h4>
</div>

<?= $this->Form->create($tarjetasAcceso , [ 'class'=>'form-horizontal'] ); ?>

<div class="modal-body">
            
    <div class="um-form-row form-group">
        <label class="col-sm-4 control-label"><?php echo __('Número de la Tarjeta'); ?></label>
        <div class="col-sm-8">
            <?php echo $this->Form->input('TarjetasAcceso.num_tarjeta', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']); ?>
        </div>
    </div>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel') ?></button>
    <?= $this->Form->button( __('Save' ) , ['class'=>'btn btn-primary'  ]) ?>
</div>

<?= $this->Form->end() ?>
<div class="panel-heading">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
  <h4 class="modal-title"> <?= __('Agregar Tarjeta de Acceso') ?> </h4>
</div>

<?= $this->Form->create($tarjetasAcceso , [ 'class'=>''] ); ?>
<div class="modal-body">
  <div class="row ca-forms">
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <div>
              <?php echo $this->Form->radio('TarjetasAcceso.rango', [ ['value' => '0', 'text' => __(' Record One'), 'style' => 'color:red;', 'checked' => true] ]); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label class="required"><?php echo __('Número de la Tarjeta'); ?></label>
            <div>
              <?php echo $this->Form->input('TarjetasAcceso.num_tarjeta', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <div>
              <?php echo $this->Form->radio('TarjetasAcceso.rango', [ ['value' => '1', 'text' => __(' Record Interval'), 'style' => 'color:red;'] ] ); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label class="required"><?php echo __('Start'); ?></label>
            <div>
              <?php echo $this->Form->input('TarjetasAcceso.inicial', ['type' => 'text','label'=>false, 'div'=>false, 'class'=>'form-control']); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label class="required"><?php echo __('End'); ?></label>
            <div>
              <?php echo $this->Form->input('TarjetasAcceso.final', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="last-btns">
            <button type="button" class="btn btn-cancel" data-dismiss="modal"><?php echo __('Cancel') ?></button>
            <?= $this->Form->button( __('Save' ) , ['class'=>'btn btn-save'  ]) ?> 
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
  
      var inicial = $('#tarjetasacceso-inicial'); var final = $('#tarjetasacceso-final'); var num = $('#tarjetasacceso-num-tarjeta');
  
      if($('#tarjetasacceso-rango-0').is(':checked')){ inicial.prop('disabled',true); final.prop('disabled',true); }
  
      $('#tarjetasacceso-rango-1').change(function() { inicial.prop('disabled',false); final.prop('disabled',false);
          num.prop('disabled', true);
          $('#tarjetasacceso-num-tarjeta').prop('required', false);
      });
  
      $('#tarjetasacceso-rango-0').change(function() { inicial.prop('disabled',true); final.prop('disabled',true);
          num.prop('disabled', false);
      });
  });
</script>
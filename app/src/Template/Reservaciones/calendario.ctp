<?php  use Cake\Routing\Router; use Cake\I18n\Time;  ?>

<?php
  	echo $this->Html->css('plugins/fullcalendar/fullcalendar.css?q='.QRDN);

    echo $this->Html->css('bootstrap-datetimepicker.min.css?q='.QRDN);

    echo $this->Html->css('/plugins/lightslider/dist/css/lightslider.min.css?q='.QRDN);

        
    echo $this->Html->script('plugins/fullcalendar/moment.min.js');

    echo $this->Html->script('plugins/fullcalendar/fullcalendar.min.js');
    
    echo $this->Html->script('plugins/fullcalendar/es.js');

    echo $this->Html->script('plugins/fullcalendar/fullcalendar-columns.js');

    

    echo $this->Html->script('bootstrap-datetimepicker.js');

    echo $this->Html->script('/plugins/lightslider/dist/js/lightslider.min.js');

$this->extend('../Layout/TwitterBootstrap/dashboard');
?>
	
<?php

	
	if(!empty($this->request->session()->read('startDate'))){
    
        $session = explode('-', $this->Time->format($this->request->session()->read('startDate'), 'yyyy-MM-dd'));
    }

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="col-md-4">
                        <h2> <?php echo __('Calendario')?> </h2>
                    </div>
                    <div class="ibox-tools col-md-4 pull-right">
                    </div>
                </div>
                <div class="ibox-content">
                	<div class="col-md-12" style="margin-bottom: 30px;">
                		<?php echo $this->Form->create(null, ['url' => ['controller' => 'Reservaciones', 'action' => 'calendario', 'plugin' => false], 'type' => 'get', 'class'=>'form-horizontal']); ?>
				            
				            <div class="col-sm-3">
				                <?php echo $this->Form->input('centro_id', ['type'=>'select', 'label'=>'Centros', 'div'=>false, 'class'=>'form-control', 'options' => $centros, 'empty' => 'Todos', 'default' => (!empty($centro_id))? $centro_id : ""]); ?>
				            </div>
				            <div class="col-sm-3">
				                <?php echo $this->Form->input('Buscar', ['type'=>'submit', 'label'=>false, 'div'=>false, 'class'=>'btn lambs',
				                'style' => 'width:32px !important;']); ?>
				            </div>
                		<?php echo $this->Form->end(); ?>
                	</div>

					<div id="full">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?= $this->element('modal_reservacion',array('reservacion' => $reservacion,'clientes' => $clientes ,'recursos' => $recursos,'show_select_recurs'=> true) )?>

             

<?php 

$listaRecursos = [];
$listaRecursosId = [];
$listaRecursosImg = [ ];


foreach ($recursos as $key => $value) {

	if( count($value->recursos_fotos ) < 1 )
		continue;

	if( !file_exists(  WWW_ROOT.'files/fotos/'.$value->recursos_fotos[0]->name  ) )
		continue;
	$color="#FFFFFF";
	if($value->color !=''){
		$color=$value->color;
	}
	$listaRecursos[] = '<button class="btn btn-sala btn-xs" style="background-color: '.$color.';">'.$value->name.'</button>';
	$listaRecursosId[] = $value->id;
	$listaRecursosImg[]  = $this->Image->resize('files/fotos/', $value->recursos_fotos[0]->name, 150, 150, false);
}
?>
<script type="text/javascript">
	


	$(document).ready(function(){

		var hours = <?= json_encode($rangohoras)  ?>;

		var centro_id = "<?= $centro_id ?>";

		var postAdd = '<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'add']) ?>';
		var postEdit = '<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'edit']) ?>/';
		var postDel = '<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'delete']) ?>/';
		
		var recurso_id = null;

		var flag_is_edit = false;


		var calcula_saldo = function( cliente_id, is_edit){
			
			flag_is_edit = is_edit;


		


			$.post( '<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'saldo']) ?>/'+cliente_id+'/'+is_edit ,{ 
					fecha_inicio:$('#reservaciones-fecha-inicio').val(),
					fecha_fin: $('#reservaciones-fecha-fin').val(),
					recurso_id:recurso_id
					 } ,function(data){

					 	var data = JSON.parse(data);

					 	$('#erroroficinavirtual').css('display','none');

					 	if( data.saldo !== undefined){

					 		$('#reservaciones-saldo').val(data.saldo);
					 		$('#reservaciones-horas').val(data.horas);

					 		$('#reservaciones-cargo-tiempo-extra').removeAttr('checked');
					 		$('#cargo_tiempo_extra').css('display','none');
					 		
					 		$('#btn_crear').prop('disabled', false);
					 		$('#btn_replicar').prop('disabled', false);

					 		if( data.saldo < 0){

					 			if( data.isModOficinaVirtual ){

					 			}else{
						 			$('#cargo_tiempo_extra').css('display','block');
						 			$('#btn_crear').prop('disabled', true);
						 			$('#btn_replicar').prop('disabled', true);
					 			}
					 		}
					 	}

					 	if(data.isModOficinaVirtual){


					 		var start = moment(data.fecha_inicio);
					 		var end = moment(data.fecha_fin);

						 
							if( data.saldodias < 1){

								$('#btn_crear').prop('disabled', true);
								$('#erroroficinavirtual').css('display','block');
								$('#erroroficinavirtual').css('color','red');
								$('#erroroficinavirtual').css('font-size','12px');
							}

							$('#reservaciones-saldo').parent().find('label').html('Saldo en días');
							$('#reservaciones-saldo').val(data.saldodias);


					 		$('#reservaciones-horas').val(1);
					 		$('#reservaciones-horas').parent().find('label').html('Día a usar');

							$('#reservaciones-fecha-inicio').css('display','none');

							$('#fecha_inicio_dia').val( start.format('DD/MM/YYYY hh:mm A')   );

							$('#fecha_inicio_dia').css('display','block');
							$('#fecha_inicio_dia').css('padding','4px');
							$('#fecha_inicio_dia').css('width','150px');

							$('#reservaciones-fecha-inicio').before($('#fecha_inicio_dia'));

							$('#reservaciones-fecha-fin').css('display','none');

							$('#reservaciones-fecha-fin').before($('#fecha_fin_dia'));

							$('#fecha_fin_dia').val( end.format('DD/MM/YYYY hh:mm A')   );
							
							$('#fecha_fin_dia').css('display','block');

							$('#fecha_fin_dia').css('padding','4px');
							$('#fecha_fin_dia').css('width','150px');




							$('#reservaciones-recurrente').removeAttr('checked');

							$('#div_recurente').css('display','none');

						}else{

							$('#reservaciones-saldo').parent().find('label').html('Saldo');
							$('#div_recurente').css('display','block');
							$('#reservaciones-fecha-inicio').css('display','block');

							$('#reservaciones-fecha-fin').css('display','block');

							$('#fecha_inicio_dia').css('display','none');
							$('#fecha_fin_dia').css('display','none');
							$('#reservaciones-horas').parent().find('label').html('Hrs a usar');
							
						}

					 	
					
			}  );

		}


		$('#reservaciones-recurso-id').change(function(){
			recurso_id = $(this).val();

			calcula_saldo($("#reservaciones-cliente-id").val(), flag_is_edit );

		});


		// Si doy click en guardar y replicar
		$('#btn_replicar').click(function(){

			$('#reservaciones-guardar-replicar').val(1);
			$('#btn_crear').trigger('click');

		});

		$('#btn_cancelar_replicar').click(function(){


			$('#reservaciones-cancelar-replicar').val(1);

			$('#btn_cancelar').trigger('click');

		});

		
		$('#btn_cancelar').click(function(){


			var confir = confirm( '¿Realmente deseas cancelar la reservación ?' );
			if( confir){
			var reservacion_id = $('#btn_cancelar').data('reservacion_id');

				if( reservacion_id !== undefined ){
					$('#deleteFrom').attr('action' , postDel+reservacion_id);
					$('#deleteFrom').submit();


				}
			}else{
				$('#reservaciones-cancelar-replicar').val(0);
			}


		});

		$('#reservaciones-fecha-inicio').addClass('datetimepicker');
		$('#reservaciones-fecha-fin').addClass('datetimepicker');

		$('.datetimepicker').datetimepicker({
		    startView:1,
		    format: 'dd/mm/yyyy HH:ii P',
		    showMeridian: true,
	        autoclose: true,
	        todayBtn: true

		  }).change( function(){

		  	calcula_saldo($("#reservaciones-cliente-id").val(), false);
		  });

		
		var last  = null;
		
	
		//$('#reservaciones-recurso-id').val(recurso_id);
		//$('#lbl_recurso').html( recurso.name );

		$('#full').fullCalendar({
			 lang: 'es',
			views: {
		        multiColAgendaDay: {
		            type: 'multiColAgenda',
		            duration: { days: 1 },
		            numColumns: <?= count($listaRecursos) ?>,
		            columnHeaders: <?= json_encode($listaRecursos) ?>,
		            columnImgHeaders: <?= json_encode($listaRecursosImg) ?>
		        }
    		},
			businessHours: {
			    // days of week. an array of zero-based day of week integers (0=Sunday)
			    dow: [0,1,2,3,4,5,6], 
			    /*
<?php 
$inicio='08:00';
$fin='18:00';
foreach($horas as $hora){
	$inicio=$hora->inicio;
	$fin=$hora->fin;
}

			    
	?>
	*/
			    start: '<?php echo $inicio; ?>', // a start time (10am in this example)
			    end: '<?php echo $fin; ?>', // an end time (6pm in this example)
			},
    
			
			select:function(start, end, jsEvent, view){

				
				var columns = <?= json_encode($listaRecursosId) ?>;


				


				var is_cliente = <?= (in_array( $this->UserAuth->getGroupId(), [5,6]))? 1 : 0 ?>;
					
	    		if(is_cliente){

	    			$('#reservaciones-cliente').prop('disabled', false);
	    			$('#reservaciones-saldo').prop('disabled', false);
	    			
	    			$('#reservaciones-asunto').prop('disabled', false);
	    			$('#reservaciones-cantidad').prop('disabled', false);

	    			$("#reservaciones-fecha-inicio").prop('disabled', false);
	    			$("#reservaciones-fecha-fin").prop('disabled', false);

	    			$('.editbCliente').css('display','block');
	    		}

				$('#addFrom').trigger("reset");
				$('#reservaciones-fecha-inicio').val(start.format('DD/MM/YYYY hh:mm A') );
				$('#reservaciones-fecha-fin').val(start.format('DD/MM/YYYY') +" " +end.format('hh:mm A') );
				//$('#reservaciones-fecha-fin').val(start.format('DD/MM/YYYY hh:mm A') );


				$('#btn_crear').html('Agregar');
				$('#reservaciones-guardar-replicar').val(0);
				$('#addFrom').attr('action' , postAdd);
				$('#modal_reservacion .modal-title').html( '<?= __("Agregar Reservación") ?>' );
				$('.editb').css('display','none');

				console.log('Quitmos los check');
				$('#reservaciones-tipo-1').removeAttr('checked');
				$('#reservaciones-tipo-2').removeAttr('checked');
				$('#reservaciones-tipo-3').removeAttr('checked');
				$('#reservaciones-recurrente').removeAttr('checked');
				$('#reservaciones-cliente-id').trigger("chosen:updated");
				$('.error-message').css('display', 'none');

				$('#selectchosen').show(); 
				$('#spanname').hide(); 
				
				$('#modal_reservacion').modal('show');

				
				<?php if(  !in_array( $this->UserAuth->getGroupId()   , [5,6] ) ): ?>
					
					if($.fn.chosen) {
						$("#reservaciones-cliente-id").chosen().change(function(){

							calcula_saldo($("#reservaciones-cliente-id").val(), false);
						});
					}
					
				<?php endif;?>

				$('#reservaciones-recurso-id').val(columns[ start.column ]);
				recurso_id = columns[ start.column ];

				// busco el saldo
				calcula_saldo($("#reservaciones-cliente-id").val(), false);


			},
			 eventRender: function(event, element) {
		        
		        $(element).addClass( event.class  );

		        var newElement = '<div class="nameClient">'+event.cliente+'</div>';

			    $(element).append(  newElement );

			    if(event) {
				   timeformat = event.start.format('hh:mm A') + ' - ' + event.end.format('hh:mm A');
				   element.find( '.fc-time' ).html( timeformat );
				}
		    },
		    viewRender: function(view, element) {

		    	var startDate = <?php echo (isset($session))? json_encode($session) : 0 ?>;
		    	
		    	if(startDate != 0){

		    		jQuery('#full').fullCalendar('gotoDate', new Date(startDate[0], startDate[1] - 1, startDate[2]));
		    	}
			   
			},
		    eventClick: function (calEvent, jsEvent, view) {           
			   	

		    	if(calEvent.reservacion_id > 0){

		    		$('#reservaciones-recurso-id').val(calEvent.recurso_id);
		    		recurso_id = calEvent.recurso_id;

		    		$('#reservaciones-guardar-replicar').val(0);
		    		$('#reservaciones-cancelar-replicar').val(0);
		    		$('#reservaciones-cliente-id').val( calEvent.cliente_id );
		    		$('#reservaciones-cantidad').val( calEvent.cantidad );

		    		$('#reservaciones-asunto').val( calEvent.asunto );

		    		

					$('#reservaciones-fecha-inicio').val(calEvent.start.format('DD/MM/YYYY hh:mm A') );

					$('#reservaciones-fecha-fin').val(calEvent.end.format('DD/MM/YYYY hh:mm A') );

					calcula_saldo($("#reservaciones-cliente-id").val(), true);
					
					$('#reservaciones-tipo-'+calEvent.tipo).trigger('click');


					$('.editb').css('display','block');


					var is_cliente = <?= (in_array( $this->UserAuth->getGroupId(), [5,6]))? 1 : 0 ?>;
					
		    		if(is_cliente){

		    			$('#reservaciones-cliente').prop('disabled', true);
		    			$('#reservaciones-saldo').prop('disabled', true);

		    			$('#reservaciones-asunto').prop('disabled', true);
		    			$('#reservaciones-cantidad').prop('disabled', true);

		    			$("#reservaciones-fecha-inicio").prop('disabled', true);
		    			$("#reservaciones-fecha-fin").prop('disabled', true);

		    			$('.editbCliente').css('display','none');
		    		}


					if( calEvent.padre == null ){

						$('#btn_replicar').css('display','none');
						$('#btn_cancelar_replicar').css('display','none');
						$('#reservaciones-recurrente').removeAttr('checked');
					}else{
						$('#reservaciones-recurrente').trigger('click');

					}


		    		$('#modal_reservacion .modal-title').html( '<?= __("Editar Reservación") ?>' );
		    		$('#btn_crear').html('Guardar');
		    		$('#addFrom').attr('action' , postEdit+calEvent.reservacion_id);
	            	$('#btn_cancelar').data('reservacion_id',calEvent.reservacion_id);
	            	$('#reservaciones-cliente-id').trigger("chosen:updated");
	            	$('.error-message').css('display', 'none');

	            	$('#spancliente').text($('#reservaciones-cliente-id option:selected').text());
	            	
	            	$('#selectchosen').hide(); 
				   	$('#spanname').show(); 
	            	
	            	$('#modal_reservacion').modal('show');



	        	}
			},
			selectable:true,
			selectOverlap: true,
			allDaySlot:false,
			defaultView:'multiColAgendaDay',
			minTime: moment(hours['inicio']), //8am
  			maxTime: moment(hours['fin']), //9am
  			axisFormat: 'hh:mm A',  
			header: {
			 center: 'multiColAgendaDay,agendaWeek,month' 
			} });


		$('#full').fullCalendar('removeEvents');

		if( last != null){
			$('#full').fullCalendar( 'removeEventSource', last );
			
		}
		
		last = {  type:'POST', url:'<?= $url = Router::url(['controller'=>'Reservaciones', 'action'=> 'events']) ?>' + '?color=1&centro_id='+centro_id } ;

		$('#full').fullCalendar( 'addEventSource', last );

		

		 
		$('#reservaciones-cargo-tiempo-extra').click(function(){

			if ($(this).is(":checked")) {
				$('#btn_crear').prop('disabled', false);
				$('#btn_replicar').prop('disabled', false);
			}else{
				$('#btn_crear').prop('disabled', true);
				$('#btn_replicar').prop('disabled', true);
			}
			
		});

		
		applyCssFullCalendar();

		$( '.fc-multiColAgendaDay-button' ).click(function(){ applyCssFullCalendar(); });


	});

	function applyCssFullCalendar(){
		$( ".fc-widget-header" ).find( "div" ).css( "color", "transparent" );
		$( ".fce-col-header" ).find( "p" ).css( "color", "#676a6c");
	}

	jQuery(document).ready(function() {
	$( ".fc-multiColAgendaDay-button").html("D&iacute;a");
		$( ".fc-agendaWeek-button").html("Semana");
		$( ".fc-month-button").html("Mes");
	});

	$(document).on('click', '.fc-multiColAgendaDay-button, .fc-prev-button, .fc-next-button', function(){
		
		applyCssFullCalendar();

		var startDate = <?php echo (isset($session))? json_encode($session) : 0 ?>;
		    	
    	if(startDate != 0){
	    	$.get( "deletedSession", function(data) {
	    		var mensaje = JSON.parse(data);
	    		mensaje.error = mensaje.error*1;
		  		if(mensaje.error == 1){
			  		window.location.reload(true);
			  	}
			});
    	}
	});

</script>

<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $reservacion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $reservacion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Reservaciones'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recursos'), ['controller' => 'Recursos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recursos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="reservaciones form large-10 medium-9 columns">
    <?= $this->Form->create($reservacion) ?>
    <fieldset>
        <legend><?= __('Edit Reservacion') ?></legend>
        <?php
            echo $this->Form->input('recurso_id', ['options' => $recursos, 'empty' => true]);
            echo $this->Form->input('fecha_inicio');
            echo $this->Form->input('fecha_fin');
            echo $this->Form->input('cancelado');
            echo $this->Form->input('deleted');
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  ?>
<?php
  /* Chosen is taken from https://github.com/harvesthq/chosen/releases/ */
          echo $this->Html->css('plugins/fullcalendar/fullcalendar.css?q='.QRDN);
  
          
          echo $this->Html->script('plugins/fullcalendar/moment.min.js');
  
          echo $this->Html->script('plugins/fullcalendar/fullcalendar.min.js');
          echo $this->Html->script('plugins/fullcalendar/es.js');
  
  ?>
<style type="text/css">
  .mireservacion{
  background-color:#1ab394;
  }
  .mireservacion
</style>

<div class="ibox float-e-margins">
  <div class="ibox-title">
    <h2><?php echo __('Mis Reservaciones'); ?>: <?= $cliente->nombre; ?></h2>
    <h3>Horas restantes del mes: <?= ($dias_retantes > 0)? $dias_retantes:0 ?></h3>
  </div>
  <div class="ibox-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox float-e-margins no-mar">
          <div class="ibox-content">
            <div class="table-responsive">
              <table class="table ca-table">
                <thead>
                  <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                    <th><h5><?= $this->Paginator->sort('Recursos.id',__('#')) ?></h5></th>
                    <th><h5><?= $this->Paginator->sort('Centros.nombre',__('Centro')) ?></h5></th>
                    <th><h5><?= $this->Paginator->sort('Recursos.name',__('Nombre')) ?></h5></th>
                    <th><h5><?= $this->Paginator->sort('RecursosTipos.name',__('Tipo')) ?></h5></th>
                    <th><h5><?= $this->Paginator->sort('Recursos.created',__('Fecha')) ?></h5></th>
                    <th><h5><?= $this->Paginator->sort('Recursos.hora_inicio',__('Horario')) ?></h5></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($reservations as $row): ?>
                  <tr>
                    <td><p><?= h($row->id) ?></p></td>
                    <td><p><?= h($row->recurso->centro->nombre) ?></p></td>
                    <td><p><?= h($row->recurso->name) ?></p></td>
                    <td><p><span class="label"><?= h($row->recurso->recursos_tipo->name) ?></span></p></td>
                    <td><p><?= $this->Time->format($row['fecha_inicio'], 'YYYY-MM-dd') ?></p></td>
                    <td><p><?= $this->Time->format($row['fecha_inicio'], 'HH:mm a').' '.$this->Time->format($row['fecha_fin'], 'HH:mm a')?></p></td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <hr>
            <div class="row">
              <div class="col-lg-12">
                <?= $this->element('all_calendario'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
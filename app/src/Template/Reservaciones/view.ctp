<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Reservacion'), ['action' => 'edit', $reservacion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Reservacion'), ['action' => 'delete', $reservacion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $reservacion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Reservaciones'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reservacion'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recursos'), ['controller' => 'Recursos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recurso'), ['controller' => 'Recursos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="reservaciones view large-10 medium-9 columns">
    <h2><?= h($reservacion->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Recurso') ?></h6>
            <p><?= $reservacion->has('recurso') ? $this->Html->link($reservacion->recurso->name, ['controller' => 'Recursos', 'action' => 'view', $reservacion->recurso->id]) : '' ?></p>
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $reservacion->has('user') ? $this->Html->link($reservacion->user->id, ['controller' => 'Users', 'action' => 'view', $reservacion->user->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($reservacion->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Fecha Inicio') ?></h6>
            <p><?= h($reservacion->fecha_inicio) ?></p>
            <h6 class="subheader"><?= __('Fecha Fin') ?></h6>
            <p><?= h($reservacion->fecha_fin) ?></p>
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($reservacion->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($reservacion->modified) ?></p>
        </div>
        <div class="large-2 columns booleans end">
            <h6 class="subheader"><?= __('Cancelado') ?></h6>
            <p><?= $reservacion->cancelado ? __('Yes') : __('No'); ?></p>
            <h6 class="subheader"><?= __('Deleted') ?></h6>
            <p><?= $reservacion->deleted ? __('Yes') : __('No'); ?></p>
        </div>
    </div>
</div>

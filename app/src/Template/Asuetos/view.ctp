
<div class="ibox">
    <div class="ibox-title">

        <h5> <?php echo __('asueto Details'); ?> </h5>

        <span class="ibox-tools">
            

            <?= $this->Html->link(__('PDF'), ['action' => 'view', '_ext'=>'pdf' , $asueto->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

            <?= $this->Html->link(__('Edit Asueto'), ['action' => 'edit', $asueto->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

        </span>

    </div>

    <div class="ibox-content">
        
  

<dl class="dl-horizontal">

    


        
                       
        <dt><?= __('Id') ?>:</dt> 
            <dd><?= $this->Number->format($asueto->id) ?></dd>
         
                       
        <dt><?= __('User Id') ?>:</dt> 
            <dd><?= $this->Number->format($asueto->user_id) ?></dd>
         
               
            
                       
        <dt><?= __('Dia') ?>:</dt> 

                <dd><?= h($asueto->dia) ?></dd>
          
                       
        <dt><?= __('Created') ?>:</dt> 

                <dd><?= h($asueto->created) ?></dd>
          
            
        


</dl>

<ul id="myTab" class="nav nav-tabs" role="tablist">
   
</ul>

<div id="myTabContent" class="tab-content">
</div>
</div>
</div>


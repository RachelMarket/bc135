<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  use Cake\Routing\Router;
  ?>
<?php
  /* Chosen is taken from https://github.com/harvesthq/chosen/releases/ */
          echo $this->Html->css('plugins/fullcalendar/fullcalendar.css?q='.QRDN);
  
          
          echo $this->Html->script('plugins/fullcalendar/moment.min.js');
  
          echo $this->Html->script('plugins/fullcalendar/fullcalendar.min.js');
           echo $this->Html->script('plugins/fullcalendar/es.js');
  
  ?>
<style type="text/css">
  .fc-content{
  padding:30px;
  }
  .ca-bins .fc-widget-header {
    background-color: #dde4ea;
    padding: 3px;
    color: #404040;
    border-color: transparent;
  }
  .ca-bins .fc-row .fc-bg {
    background-color: #f5f7f9;
  }
  .fc-unthemed th, 
  .fc-unthemed td, 
  .fc-unthemed hr, 
  .fc-unthemed thead, 
  .fc-unthemed tbody, 
  .fc-unthemed .fc-row, 
  .fc-unthemed .fc-popover {
    border-color: #fff !important;
  }
.ca-bins .fc-event {
    position: relative;
    display: block;
    font-size: 0.9em;
    line-height: initial;
    border-radius: 0;
    border: none;
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f5f7f9+0,d9d6d1+100 */
background: #f5f7f9; /* Old browsers */
background: -moz-linear-gradient(top, #f5f7f9 0%, #d9d6d1 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #f5f7f9 0%,#d9d6d1 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #f5f7f9 0%,#d9d6d1 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f5f7f9', endColorstr='#d9d6d1',GradientType=0 ); /* IE6-9 */
    font-weight: 600;
    text-align: center;
    padding: 15px 0 !important;
    margin: 0 !important;
}
.fc-content .fc-title {
    display: block;
    position: absolute;
    margin: auto;
    width: 75px;
    text-align: center;
    padding-top: 25px;
    left: 0;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    background-image: url(../img/check-ico.png);
    background-repeat: no-repeat;
    background-position: top center;
    background-size: 27px;
}
.fc-basic-view td.fc-week-number span, .fc-basic-view td.fc-day-number {
    padding: 10px;
    color: #000;
    opacity: .5;
    padding-bottom: 0;
}
.fc-basic-view td.fc-day-number::first-letter { 
  text-decoration: underline;
}

.ca-bins .fc-state-default {
    background-color: transparent !important;
    background-image: none !important;
    border-color: #ceac71 !important;
    border-color: #ceac71 !important;
    color: #ceac71 !important;
    text-shadow: none !important;
    box-shadow: none !important;
    border-radius: 50%;
    width: 40px;
    height: 40px;
    margin: 5px;
  }
.fc .fc-toolbar h2 {
    margin-left: 0;
    text-transform: uppercase;
    font-weight: 600;
    font-size: 18px;
    margin-top: 17px;
}
</style>


<div class="ibox float-e-margins">
  <div class="ibox-title">
    <h2> Días Inhábiles</h2>
  </div>
  <div class="ibox-content">
    <div class="calendar ca-bins">
    </div>
  </div>
</div>



<div class="modal fade" id="modal_dia">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Agregar día inhábil</h4>
      </div>
      <div class="modal-body">
        <?php echo $this->element('Usermgmt.ajax_validation', ['formId'=>'addFrom', 'submitButtonId'=>'btn_crear']); ?>
        <?php echo $this->Form->create($asueto, [ 'url' => [  'controller' => 'Asuetos','action'=> 'add'], 'id'=>'addFrom', 'class'=>'form-horizontal', 'novalidate'=>true]); ?>
        <div class="form-content">
          <?php echo $this->Form->input('Asuetos.dia', [ 'label' => 'Día', 'readonly' => 'readonly', 'type'=>'text'] ); ?>
        </div>
        <?php echo $this->Form->end(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_crear" class="btn btn-primary">Registrar Día</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
  $(document).ready(function(){
  
      $('.calendar').fullCalendar({
          
          eventClick: function(calEvent, jsEvent, view) {
  
              var c = confirm('¿Estas seguro de borrar este día ?');
  
              if(c){
                  //borrams el evento.
                  calEvent.asueto_id;
  
                  $.post( '<?= $url = Router::url(['controller'=>'Asuetos', 'action'=> 'delete']) ?>/'+calEvent.asueto_id , function(data){
  
                      console.log(data);
  
                      if( data == 'ok' ){
                          alert('Día eliminado');
                      }
                      $('.calendar').fullCalendar( 'refetchEvents' )
                  });
  
              }
              
          },
          dayClick: function(date, jsEvent, view) { 
                  
                
                  
                  $('#asuetos-dia').val( date.format('YYYY-MM-DD') );
                  $('#modal_dia').modal('show');
          },
           events: {
              url: '<?= $url = Router::url(['controller'=>'Asuetos', 'action'=> 'index']) ?>',
              type: 'POST'
          }
      });
  })
</script>
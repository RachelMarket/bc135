<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $folio->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $folio->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Folio'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="folio form large-10 medium-9 columns">
    <?= $this->Form->create($folio) ?>
    <fieldset>
        <legend><?= __('Edit Folio') ?></legend>
        <?php
            echo $this->Form->input('serie');
            echo $this->Form->input('numero');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

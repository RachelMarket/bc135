<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Folio'), ['action' => 'edit', $folio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Folio'), ['action' => 'delete', $folio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $folio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Folio'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Folio'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="folio view large-10 medium-9 columns">
    <h2><?= h($folio->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Serie') ?></h6>
            <p><?= h($folio->serie) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($folio->id) ?></p>
            <h6 class="subheader"><?= __('Numero') ?></h6>
            <p><?= $this->Number->format($folio->numero) ?></p>
        </div>
    </div>
</div>

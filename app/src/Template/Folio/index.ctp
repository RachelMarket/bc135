<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Folio'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="folio index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('serie') ?></th>
            <th><?= $this->Paginator->sort('numero') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($folio as $folio): ?>
        <tr>
            <td><?= $this->Number->format($folio->id) ?></td>
            <td><?= h($folio->serie) ?></td>
            <td><?= $this->Number->format($folio->numero) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $folio->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $folio->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $folio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $folio->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

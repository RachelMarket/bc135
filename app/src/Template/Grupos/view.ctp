<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Editar Grupo'), ['action' => 'edit', $grupo->id]) ?> </li>
    <li><?= $this->Form->postLink(__('Eliminar Grupo'), ['action' => 'delete', $grupo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $grupo->id)]) ?> </li>
    <li><?= $this->Html->link(__('Listar Grupos'), ['action' => 'index']) ?> </li>
    <li><?= $this->Html->link(__('Nuevo Grupo'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Listar Permisos'), ['controller' => 'Permisos', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Permiso'), ['controller' => 'Permisos', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>

<h2><?= h($grupo->nombre) ?></h2>
<div class="row">
        <div class="col-lg-5">
                                    <h6><?= __('Nombre') ?></h6>
                    <p><?= h($grupo->nombre) ?></p>
                                                    <h6><?= __('Url') ?></h6>
                    <p><?= h($grupo->url) ?></p>
                                </div>
            <div class="col-lg-2">
                    <h6><?= __('Id') ?></h6>
                <p><?= $this->Number->format($grupo->id) ?></p>
                </div>
            </div>
<ul id="myTab" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#Permisos" id="Permisos-tab" role="tab" data-toggle="tab" aria-controls="Permisos" aria-expanded="true">Permisos</a>
      </li>
          <li role="presentation" class="">
        <a href="#Usuarios" id="Usuarios-tab" role="tab" data-toggle="tab" aria-controls="Usuarios" aria-expanded="true">Usuarios</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="Permisos" aria-labelledBy="Permisos-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($grupo->permisos)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Grupo Id') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Orden') ?></th>
                                            <th><?= __('Accion') ?></th>
                                            <th><?= __('Permitir') ?></th>
                                            <th><?= __('Padre') ?></th>
                                            <th><?= __('Mostrar') ?></th>
                                            <th><?= __('Icono') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($grupo->permisos as $permisos): ?>
                    <tr>
                                            <td><?= h($permisos->id) ?></td>
                                            <td><?= h($permisos->grupo_id) ?></td>
                                            <td><?= h($permisos->nombre) ?></td>
                                            <td><?= h($permisos->orden) ?></td>
                                            <td><?= h($permisos->accion) ?></td>
                                            <td><?= h($permisos->permitir) ?></td>
                                            <td><?= h($permisos->padre) ?></td>
                                            <td><?= h($permisos->mostrar) ?></td>
                                            <td><?= h($permisos->icono) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Permisos', 'action' => 'view', $permisos->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Permisos', 'action' => 'edit', $permisos->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Permisos', 'action' => 'delete', $permisos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $permisos->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Permisos asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade in " id="Usuarios" aria-labelledBy="Usuarios-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($grupo->usuarios)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Uid') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Telefono') ?></th>
                                            <th><?= __('Correo') ?></th>
                                            <th><?= __('Password') ?></th>
                                            <th><?= __('Grupo Id') ?></th>
                                            <th><?= __('Activo') ?></th>
                                            <th><?= __('Foto') ?></th>
                                            <th><?= __('Randfoto') ?></th>
                                            <th><?= __('Filepath') ?></th>
                                            <th><?= __('Created') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($grupo->usuarios as $usuarios): ?>
                    <tr>
                                            <td><?= h($usuarios->id) ?></td>
                                            <td><?= h($usuarios->uid) ?></td>
                                            <td><?= h($usuarios->nombre) ?></td>
                                            <td><?= h($usuarios->telefono) ?></td>
                                            <td><?= h($usuarios->correo) ?></td>
                                            <td><?= h($usuarios->password) ?></td>
                                            <td><?= h($usuarios->grupo_id) ?></td>
                                            <td><?= h($usuarios->activo) ?></td>
                                            <td><?= h($usuarios->foto) ?></td>
                                            <td><?= h($usuarios->randfoto) ?></td>
                                            <td><?= h($usuarios->filepath) ?></td>
                                            <td><?= h($usuarios->created) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Usuarios', 'action' => 'view', $usuarios->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Usuarios', 'action' => 'edit', $usuarios->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Usuarios', 'action' => 'delete', $usuarios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuarios->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Usuarios asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
</div>


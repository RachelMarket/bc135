<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
    <li><?= $this->Html->link(__('Agregar Grupo'), ['action' => 'add']); ?></li>
        <li><?= $this->Html->link(__('Listar Permisos'), ['controller' => 'Permisos', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Permiso'), ['controller' => ' Permisos', 'action' => 'add']); ?></li>
                    <li><?= $this->Html->link(__('Listar Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']); ?></li>
                <li><?= $this->Html->link(__('Agregar Usuario'), ['controller' => ' Usuarios', 'action' => 'add']); ?></li>
                </ul>
<?php $this->end(); ?>
<table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
                        <th><?= $this->Paginator->sort('id'); ?></th>
                        <th><?= $this->Paginator->sort('nombre'); ?></th>
                        <th><?= $this->Paginator->sort('url'); ?></th>
                        <th class="actions"><?= __('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($grupos as $grupo): ?>
        <tr>
                        <td><?= $this->Number->format($grupo->id) ?></td>
                                    <td><?= h($grupo->nombre) ?></td>
                                    <td><?= h($grupo->url) ?></td>
                                    <td class="actions">
                <?= $this->Html->link('', ['action' => 'view', $grupo->id], ['title' => __('Ver'), 'class' => 'btn btn-default fa fa-eye']) ?>
                <?= $this->Html->link('', ['action' => 'edit', $grupo->id], ['title' => __('Editar'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                <?= $this->Form->postLink('', ['action' => 'delete', $grupo->id], ['confirm' => __('Seguro que quiere borrar # {0}?', $grupo->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>
            </td>
        </tr>

        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
$this->start('tb_sidebar');
?>
<ul class="nav nav-sidebar">
        <li><?=
            $this->Form->postLink(
            __('Eliminar'),
            ['action' => 'delete', $grupo->id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $grupo->id)]
            )
            ?></li>
        <li><?= $this->Html->link(__('Listar Grupos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Listar Permisos'), ['controller' => 'Permisos', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Permiso'), ['controller' => 'Permisos', 'action' => 'add']) ?> </li>
                    <li><?= $this->Html->link(__('Listar Usuarios'), ['controller' => 'Usuarios', 'action' => 'index']) ?> </li>
                <li><?= $this->Html->link(__('Agregar Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?> </li>
                </ul>
<?php $this->end(); ?>
<?= $this->Form->create($grupo); ?>
<fieldset>
    <legend><?= __('Edit {0}', ['Grupo']) ?></legend>
    <?php
        echo $this->Form->input('nombre');
                echo $this->Form->input('url');
                ?>
</fieldset>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>
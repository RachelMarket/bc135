<div class="ibox">
        <div class="ibox-title">

            <h5> <?php echo __('Chip Details'); ?> </h5>

            <span class="ibox-tools">

                <?= $this->Html->link(__('Edit Did'), ['action' => 'edit', $chip->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

            </span>

        </div>

        <div class="ibox-content">
                
            <dl class="dl-horizontal">
                
                <dt><?= __('Clave') ?>:</dt> 
                <dd><?= $this->Number->format($chip->id) ?></dd>

                <dt><?= __('Número') ?>:</dt> 
                <dd><?= h($chip->numero) ?></dd>
                                 
                <dt><?= __('Created') ?>:</dt> 
                <dd><?= date('d/m/Y h:i A',strtotime($chip->created)) ?></dd>
                                   
                <dt><?= __('Modified') ?>:</dt> 
                <dd><?= date('d/m/Y h:i A',strtotime($chip->modified)) ?></dd>

            </dl>

        <ul id="myTab" class="nav nav-tabs" role="tablist">
           
        </ul>

        <div id="myTabContent" class="tab-content">
        </div>
    </div>
</div>


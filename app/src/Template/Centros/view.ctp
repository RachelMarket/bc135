
<div class="ibox">
    <div class="ibox-title">

        <h5> <?php echo __('centro Details'); ?> </h5>

        <span class="ibox-tools">
            

            <?= $this->Html->link(__('PDF'), ['action' => 'view', '_ext'=>'pdf' , $centro->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

            <?= $this->Html->link(__('Edit Centro'), ['action' => 'edit', $centro->id] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

        </span>

    </div>

    <div class="ibox-content">
        
  

<dl class="dl-horizontal">

        
                       
                        <dt><?= __('Nombre') ?>:</dt> 
                    <dd><?= h($centro->nombre) ?></dd>
                
          
                
    


        
                       
        <dt><?= __('Id') ?>:</dt> 
            <dd><?= $this->Number->format($centro->id) ?></dd>
         
               
            


</dl>

<ul id="myTab" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#Clientes" id="Clientes-tab" role="tab" data-toggle="tab" aria-controls="Clientes" aria-expanded="true">Clientes</a>
      </li>
         
</ul>

<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="Clientes" aria-labelledBy="Clientes-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($centro->clientes)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                                            <th><?= __('Id') ?></th>
                                            <th><?= __('Nombre') ?></th>
                                            <th><?= __('Primario Persona De Contacto') ?></th>
                                            <th><?= __('Primario Correo Electronico') ?></th>
                                            <th><?= __('Primario Telefono Oficina') ?></th>
                                            <th><?= __('Primario Telefono Movil') ?></th>
                                            <th><?= __('Secundario Persona De Contacto') ?></th>
                                            <th><?= __('Secundario Correo Electronico') ?></th>
                                            <th><?= __('Secundario Telefono Oficina') ?></th>
                                            <th><?= __('Secundario Telefono Movil') ?></th>
                                            <th><?= __('Razon Social') ?></th>
                                            <th><?= __('Rfc') ?></th>
                                            <th><?= __('Calle') ?></th>
                                            <th><?= __('Numero') ?></th>
                                            <th><?= __('Colonia') ?></th>
                                            <th><?= __('Pais Id') ?></th>
                                            <th><?= __('Estado Id') ?></th>
                                            <th><?= __('Municipio Id') ?></th>
                                            <th><?= __('Codigo Postal') ?></th>
                                            <th><?= __('Moneda Id') ?></th>
                                            <th><?= __('Activo') ?></th>
                                            <th><?= __('Created') ?></th>
                                            <th><?= __('Tipo Cliente Id') ?></th>
                                            <th><?= __('Dia Pago') ?></th>
                                            <th><?= __('Numero Interior') ?></th>
                                            <th><?= __('Oficina') ?></th>
                                            <th><?= __('Centro Id') ?></th>
                                            <th><?= __('Formasdepago Id') ?></th>
                                            <th><?= __('Cuentabanco') ?></th>
                                            <th><?= __('Saldo') ?></th>
                                            <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($centro->clientes as $clientes): ?>
                    <tr>
                                            <td><?= h($clientes->id) ?></td>
                                            <td><?= h($clientes->nombre) ?></td>
                                            <td><?= h($clientes->primario_persona_de_contacto) ?></td>
                                            <td><?= h($clientes->primario_correo_electronico) ?></td>
                                            <td><?= h($clientes->primario_telefono_oficina) ?></td>
                                            <td><?= h($clientes->primario_telefono_movil) ?></td>
                                            <td><?= h($clientes->secundario_persona_de_contacto) ?></td>
                                            <td><?= h($clientes->secundario_correo_electronico) ?></td>
                                            <td><?= h($clientes->secundario_telefono_oficina) ?></td>
                                            <td><?= h($clientes->secundario_telefono_movil) ?></td>
                                            <td><?= h($clientes->razon_social) ?></td>
                                            <td><?= h($clientes->rfc) ?></td>
                                            <td><?= h($clientes->calle) ?></td>
                                            <td><?= h($clientes->numero) ?></td>
                                            <td><?= h($clientes->colonia) ?></td>
                                            <td><?= h($clientes->pais_id) ?></td>
                                            <td><?= h($clientes->estado_id) ?></td>
                                            <td><?= h($clientes->municipio_id) ?></td>
                                            <td><?= h($clientes->codigo_postal) ?></td>
                                            <td><?= h($clientes->moneda_id) ?></td>
                                            <td><?= h($clientes->activo) ?></td>
                                            <td><?= h($clientes->created) ?></td>
                                            <td><?= h($clientes->tipo_cliente_id) ?></td>
                                            <td><?= h($clientes->dia_pago) ?></td>
                                            <td><?= h($clientes->numero_interior) ?></td>
                                            <td><?= h($clientes->oficina) ?></td>
                                            <td><?= h($clientes->centro_id) ?></td>
                                            <td><?= h($clientes->formasdepago_id) ?></td>
                                            <td><?= h($clientes->cuentabanco) ?></td>
                                            <td><?= h($clientes->saldo) ?></td>
                                                                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => 'Clientes', 'action' => 'view', $clientes->id],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => 'Clientes', 'action' => 'edit', $clientes->id], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => 'Clientes', 'action' => 'delete', $clientes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clientes->id), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen Clientes asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


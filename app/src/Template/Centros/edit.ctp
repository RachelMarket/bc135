<?php
$this->extend('../Layout/TwitterBootstrap/dashboard');
?>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=cq4j15aidftyr52ebdshv22txiqduet110ds4jvrjp9tk6ha"></script>

<script>tinymce.init({ selector:'#referencias-fiscales' });</script>

<style type="text/css"> .form-group { margin-right: 0px !important; }  .form-control { border-radius: 2px !important; }
    form .required:after { content: "*" !important; }
    #mceu_29 { display: none; }
</style>
<div class="ibox float-e-margins">
  <?= $this->Form->create($centro , [ 'class'=>'', 'novalidate'] ); ?>
  <div class="ibox-title">
    <h2><?= __('Editar {0}', ['Centro']) ?></h2>
    <a class="btn btn-cancel btn-back" href="<?= $this->request->referer(); ?>">Cancelar</a>
  </div>
  <div class="ibox-content">
    <div class="row ca-forms mtp-40">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
          <div class="col-md-12">
            <?= $this->Form->input('nombre', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Nombre') ]); ?>
          </div>
        </div>
        <h5 class="div-line"><?=__('Datos de Ubicación')?></h5>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('calle', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Calle') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('numero', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Número') ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('colonia', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Colonia') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('ciudad', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Ciudad') ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('cp', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Cp') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('estado', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Estado') ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('pais', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('País') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('email', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Email') ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('telefono', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Teléfono') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('telefono2', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Teléfono2') ]); ?>
          </div>
        </div>
        <h5 class="div-line"><?=__('Datos Fiscales')?></h5>

        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('nombre_fiscal', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Nombre Fiscal') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('rfc', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('RFC') ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('direccion_fiscal', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Dirección Fiscal') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('colonia_fiscal', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Colonia Fiscal') ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('ciudad_fiscal', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Ciudad Fiscal') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('cp_fiscal', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Cp Fiscal') ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('estado_fiscal', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Estado Fiscal') ]); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('pais_fiscal', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('País Fiscal') ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?= $this->Form->input('referencias_fiscales', ['type' => 'textarea', 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Referencias Fiscales') ]); ?>
          </div>
          <div class="col-md-12">
            <?= $this->Form->input('url_timbrado', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('URL de Timbrado') ]); ?>
          </div>
        </div>
        <h5 class="div-line"><?=__('Datos Fiscales')?></h5>

        <div class="row">
          <div class="col-md-6">
            <?= $this->Form->input('username', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Username'), 'value' => '']); ?>
          </div>
          <div class="col-md-6">
            <?= $this->Form->input('password', ['type' => 'password', 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Password'), 'value' => '' ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="last-btns">
              <?= $this->Form->button( __('Save' ) , ['class'=>'btn btn-save'  ]) ?>
              <a class="btn btn-cancel" href="<?= $this->request->referer(); ?>">Cancelar</a>
              <?= $this->Form->end() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


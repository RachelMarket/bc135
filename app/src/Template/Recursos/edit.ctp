
<?php 

echo $this->Html->css('plugins/colorpicker/bootstrap-colorpicker.min.css');


echo $this->Html->script('plugins/colorpicker/bootstrap-colorpicker.min.js');

?>
<div class="ibox">
        <div class="ibox-title">
            
            <div class="col-md-4">
                <span class="panel-title">
                        <h2><?= __('Editar {0}', ['Sala']) ?></h2>
                </span>
            </div>
            <span class="um-panel-title-right col-md-4" style="text-align:right;">
            <?php echo $this->Html->link(__('<i class="fa fa-undo"></i> Cancelar'), ['action'=>'index'], ['class'=>'btn btn-primary pull-right', 'escape' => false]); ?>
        </span>
        </div>
        <div class="ibox-content">
        <?= $this->Form->create($recurso , [ 'class'=>'form-horizontal'] ); ?>
        
        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    
                    <div class="col-sm-12">
                        <?= $this->Form->input('centro_id', [ 'div'=>false, 'label' => __('Centro'), 'options' => $centros]); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    
                    <div class="col-sm-12">
                        <?= $this->Form->input('name', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Nombre')]); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group"> 
                    
                    <div class="col-sm-12">
                        <?= $this->Form->input('capacidad', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Capacidad'), 'default' => '1']); ?>
                    </div>
                </div>
            </div>
        </div>
            

        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                        <?= $this->Form->input('tipo_id', [ 'div'=>false, 'label' => __('Tipo'), 'options' => $tipos]); ?>
                    </div>
                </div>
            </div> 
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                        <?= $this->Form->input('inicio', [ 'div'=>false, 'class' => 'form-control datetimepicker', 'label' => __('Horario Hábil (inicio)'), 'value' => ( !empty($recurso->hora_inicio) )? $recurso->hora_inicio->format('d/m/Y g:i A') : "" ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                        <?= $this->Form->input('fin', [ 'div'=>false, 'class' => 'form-control datetimepicker', 'label' => __('Horario Hábil (fin)'), 'value' => ( !empty($recurso->hora_fin) )? $recurso->hora_fin->format('d/m/Y g:i A') : "" ]); ?>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                        <?= $this->Form->input('tiempo_minimo', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Tiempo Minimo (horas)'), 'default' => '1']); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                    <?= $this->Form->input('minutos_espera', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Minutos de espera'), 'default' => '15']); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                    <?= $this->Form->input('color', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Color')]); ?>
                    
                    </div>
                </div>
            </div>
        </div>
            
        <div class="col-sm-12">
            <div class="um-form-row form-group">
                <div class="col-sm-12">
                    <?= $this->Form->input('descripcion', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Descripción')]); ?>
                </div>
            </div>
        </div>
                        
        <div class="um-button-row">
            <?= $this->Form->button( __('<i class="fa fa-save"></i> Guardar') , ['class'=>'btn btn-primary pull-right'  ]) ?>
        </div>
        <?= $this->Form->end() ?>
        <div class="um-button-row" style="border-top: none;">
            <?= $this->Form->create($recurso , [ 'id'=>'evidencias', 'class'=>'dropzone', 'update'=>'GuiaFileDiv'] ); ?>
            
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<style type="text/css"> 

    .dz-message { background: none !important; }

    .dz-message span { display: block !important; text-align: center; font-size: x-large; font-family: inherit; }
    
    .previous { margin-right: 10px; }
    
    .next { margin-left: 10px; }
    
    .upload { margin-top: 15px; margin-right: 15px; }

</style>
<script type="text/javascript">
    $(document).ready(function(){


        $('#color').colorpicker( );

        
        $('.datetimepicker').datetimepicker({
            startView:1,
            format: 'dd/mm/yyyy HH:ii P',
            showMeridian: true,
            autoclose: true,
            todayBtn: true
        });
        var fileList = new Array;

        var archivos = <?= json_encode($recurso->recursos_fotos)?>;
        var myDropzone = new Dropzone("#evidencias");

        var recurso_id = <?= $recurso->id?>;

        $.each( archivos, function( key, value) {

            var mockFile = { name: value.name_real, size: value.size };
            myDropzone.options.addedfile.call(myDropzone, mockFile);
            myDropzone.options.thumbnail.call(myDropzone, mockFile, '/files/fotos/'+value.name);
            mockFile.previewTemplate.id = 'evidencia-'+value.id;
            
            var a = document.createElement('a');
            a.setAttribute('href','/files/fotos/' + value.name);
            a.setAttribute('target','_blank');
            a.setAttribute('download',value.name_real);
            a.setAttribute('class','btn btn-primary');
            a.setAttribute('style','width:100%;');
            a.innerHTML = 'Descargar';
            mockFile.previewTemplate.appendChild(a);

            var a = document.createElement('a');
            a.setAttribute('class','eliminar-archivo btn btn-danger');
            a.setAttribute('style','width:100%; margin-top:2px;');
            a.setAttribute('data-archivo',value.id);
            a.setAttribute('data-recurso',recurso_id);
            a.innerHTML = 'Eliminar';
            mockFile.previewTemplate.appendChild(a);

            fileList[value.id] = mockFile;

        });

        myDropzone.on("success", function(file, serverFileName) {
            
            file.previewTemplate.id = 'evidencia-'+serverFileName;

            var a = document.createElement('a');
            a.setAttribute('href',"/files/fotos/" + recurso_id+'-'+file.name);
            a.setAttribute('target','_blank');
            a.setAttribute('download',file.name);
            a.setAttribute('class','btn btn-primary');
            a.setAttribute('style','width:100%;');
            a.innerHTML = 'Descargar';
            file.previewTemplate.appendChild(a);
            

            var a = document.createElement('a');
            a.setAttribute('class','eliminar-archivo btn btn-danger');
            a.setAttribute('style','width:100%; margin-top:2px;');
            a.setAttribute('data-archivo',serverFileName);
            a.setAttribute('data-recurso',recurso_id);
            a.innerHTML = 'Eliminar';
            file.previewTemplate.appendChild(a);

            fileList[serverFileName] = file;

        });


        $(document).on({click: function(e){ 
        
            var foto_id = $(this).data('archivo');

            x = confirm('¿Realmente desea eliminar la foto?');
            if(x){
                $.ajax({
                    type: 'POST',
                    url: '/recursos/removeFile',
                    data: {foto_id:foto_id},
                    dataType: 'json',
                    success: function(data){

                        if(data.resultado == 200){
                            alert(data.msg);
                            $('div').remove('#evidencia-'+foto_id);
                        }else{alert(data.msg)}
                    }
                });
            }else{return false;} 

        }},'.eliminar-archivo');
    });
</script>
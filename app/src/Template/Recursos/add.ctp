
<?php 

echo $this->Html->css('plugins/colorpicker/bootstrap-colorpicker.min.css');


echo $this->Html->script('plugins/colorpicker/bootstrap-colorpicker.min.js');

?>
<div class="ibox">
        <div class="ibox-title">
            
            <div class="col-md-4">
                <span class="panel-title">
                        <h2><?= __('Agregar {0}', ['Sala']) ?></h2>
                </span>
            </div>
            <span class="um-panel-title-right col-md-4" style="text-align:right;">
            <?php echo $this->Html->link(__('<i class="fa fa-undo"></i> Cancelar'), ['action'=>'index'], ['class'=>'btn btn-primary pull-right', 'escape' => false]); ?>
        </span>
        </div>
        <div class="ibox-content">
        <?= $this->Form->create($recurso , [ 'class'=>'form-horizontal'] ); ?>
        
        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    
                    <div class="col-sm-12">
                        <?= $this->Form->input('centro_id', [ 'div'=>false, 'label' => __('Centro'), 'options' => $centros]); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    
                    <div class="col-sm-12">
                        <?= $this->Form->input('name', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Nombre')]); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group"> 
                    
                    <div class="col-sm-12">
                        <?= $this->Form->input('capacidad', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Capacidad'), 'default' => '1']); ?>
                    </div>
                </div>
            </div>
        </div>
            

        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                        <?= $this->Form->input('tipo_id', [ 'div'=>false, 'label' => __('Tipo'), 'options' => $tipos]); ?>
                    </div>
                </div>
            </div> 
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                        <?= $this->Form->input('inicio', [ 'div'=>false, 'class' => 'form-control datetimepicker', 'label' => __('Horario Hábil (inicio)'), 'default' => date('d/m/Y').' 08:00 AM' ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                        <?= $this->Form->input('fin', [ 'div'=>false, 'class' => 'form-control datetimepicker', 'label' => __('Horario Hábil (fin)'), 'default' => date('d/m/Y').' 08:00 PM' ]); ?>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                        <?= $this->Form->input('tiempo_minimo', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Tiempo Minimo (horas)'), 'default' => '1']); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                    <?= $this->Form->input('minutos_espera', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Minutos de espera'), 'default' => '15']); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="um-form-row form-group">
                    <div class="col-sm-12">
                    <?= $this->Form->input('color', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Color')]); ?>
                    
                    </div>
                </div>
            </div>
        </div>
            
        <div class="col-sm-12">
            <div class="um-form-row form-group">
                <div class="col-sm-12">
                    <?= $this->Form->input('descripcion', [ 'div'=>false, 'class' => 'form-control  ' , 'label' => __('Descripción')]); ?>
                </div>
            </div>
        </div>
                        
        <div class="um-button-row">
            <?= $this->Form->button( __('<i class="fa fa-save"></i> Guardar') , ['class'=>'btn btn-primary pull-right'  ]) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
         $('#color').colorpicker( );
         
        $('.datetimepicker').datetimepicker({
            startView:1,
            format: 'dd/mm/yyyy HH:ii P',
            showMeridian: true,
            autoclose: true,
            todayBtn: true
        });
    });
</script>
<?php
  $this->extend('../Layout/TwitterBootstrap/dashboard');
  ?>


<div class="ibox float-e-margins">
  <div class="ibox-title">
    <h2><?php echo __('Salas')?> </h2>
    <a href="/recursos/add" class="btn btn-cancel btn-back"><?php echo __('Agregar Sala')?></a>
  </div>
  <div class="ibox-content">
    <?php echo $this->element('all_recursos',[ 'modo_calendario' => false ] );?>
  </div>
</div>

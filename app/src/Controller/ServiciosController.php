<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Servicios Controller
 *
 * @property \App\Model\Table\ServiciosTable $Servicios
 */
class ServiciosController extends AppController
{


/**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '25'];

    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Servicios'=>[
                'Servicios'=>[
                    'position'=>'busqueda',
                    'type'=>'text',
                    'label'=>'Buscar',
                    'tagline'=>'<br>Busca por Nombre, Descripción, Precio',
                    'condition'=>'multiple',
                    'searchFields'=>['Servicios.nombre','Servicios.descripcion','Servicios.precio_usd','Servicios.precio_mxn',],
                    'inputOptions'=>['style'=>'width:300px;']
                ],

                'Servicios.tipo_id'=>[
                    'type'=>'select',
                    'label'=>'Tipo',
                    'model'=>'Tipos',
                    'selector'=>'GetTipos'
                 
                   
                    
                ],
                'Servicios.unidad_id'=>[
                    'type'=>'select',
                    'label'=>'Unidad',
                    'model'=>'Unidades',
                    'selector'=>'getUnidades'
                    
                ],

                
                              
            ]
        ]
    ];

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tipos', 'Unidades']
        ];
        $usuario = $this->UserAuth->getUser();

        $conditions = [];

        if( $this->UserAuth->getGroupId() != 1 ){
            $conditions[] = ['Servicios.centro_id' =>  $usuario['User']['centro_id'] ];
        }

        $this->Search->applySearch($conditions);
        $this->set('servicios', $this->paginate($this->Servicios));
        $this->set('_serialize', ['servicios']);

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_servicios');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Servicio id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $servicio = $this->Servicios->get($id, [
            'contain' => ['Tipos', 'Unidades', 'Clientes']
        ]);
        $this->set('servicio', $servicio);
        $this->set('_serialize', ['servicio']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $servicio = $this->Servicios->newEntity();
        if ($this->request->is('post')) {
            
            $servicio = $this->Servicios->patchEntity($servicio, $this->request->data);
            $servicio['centro_id'] = $this->request->data['centro_id'];
            if ($this->Servicios->save($servicio)) {
                $this->Flash->success(__('The servicio has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The servicio could not be saved. Please, try again.'));
            }
        }else{
            $this->request->data['unidad_id']=7;
        }
        $tipos = $this->Servicios->Tipos->find('list', ['limit' => 200]);
        $unidades = $this->Servicios->Unidades->find('list', ['limit' => 200]);
        $clientes = $this->Servicios->Clientes->find('list', ['limit' => 200]);
        $centros = $this->Servicios->Centros->find('list', ['conditions' => ['Centros.deleted' => FALSE]]);
        $this->set(compact('servicio', 'tipos', 'unidades', 'clientes','centros'));
        $this->set('_serialize', ['servicio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Servicio id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $servicio = $this->Servicios->get($id, [
            'contain' => ['Clientes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $servicio = $this->Servicios->patchEntity($servicio, $this->request->data);
            $servicio['centro_id'] = $this->request->data['centro_id'];
            if ($this->Servicios->save($servicio)) {
                $this->Flash->success(__('The servicio has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The servicio could not be saved. Please, try again.'));
            }
        }
        $tipos = $this->Servicios->Tipos->find('list', ['limit' => 200]);
        $unidades = $this->Servicios->Unidades->find('list', ['limit' => 200]);
        $clientes = $this->Servicios->Clientes->find('list', ['limit' => 200]);
        $centros = $this->Servicios->Centros->find('list', ['conditions' => ['Centros.deleted' => FALSE]]);
        $this->set(compact('servicio', 'tipos', 'unidades', 'clientes','centros'));
        $this->set('_serialize', ['servicio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Servicio id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $servicio = $this->Servicios->get($id);
        if ($this->Servicios->delete($servicio)) {
            $this->Flash->success(__('The servicio has been deleted.'));
        } else {
            $this->Flash->error(__('The servicio could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

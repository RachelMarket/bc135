<?php
namespace App\Controller;

use App\Controller\AppController;

use Usermgmt\Controller\UsermgmtAppController;
use Cake\View\CellTrait; 
use Cake\Mailer\Email;

/**
 * Oficinas Controller
 *
 * @property \App\Model\Table\OficinasTable $Oficinas
 */
class OficinasController extends AppController
{
    use CellTrait;

     /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '100'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Oficinas'=>[
                'Oficinas'=>[
                    'position' => 'busqueda',
                    'type'=>'text',
                    'label'=>'Buscar',
                    'tagline'=>false,
                    'condition'=>'multiple',
                    'searchFields'=>['Oficinas.numero', 'Oficinas.capacidad', 'Oficinas.precio_medio', 'Oficinas.precio_maximo', 'Centros.nombre'],
                    'inputOptions'=>['style'=>'width:300px;']
                ],
                'Centros.nombre'=>[
                    'type'=>'select',
                    'label'=>'Centros',
                    'model'=>'Centros',
                    'selector'=>'getCentrosSelect'
                ],
                'Oficinas.eliminado'=>[
                    'type'=>'select',
                    'label'=>'Estatus',
                    'options'=>['' => 'Seleccione', '1' => 'Eliminado', '0' => 'Activo'],
                    'inputOptions'=>['style'=>'width:150px;']
                ]

            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = ['limit'=>10, 'order'=>['Oficinas.id'=>'DESC']];
        $this->Search->applySearch();
        $this->paginate = [
            'contain' => ['Centros']
        ];
        $oficinas = $this->paginate($this->Oficinas)->toArray();
        $centros = $this->Oficinas->Centros->find('list', ['limit' => 200]);
        $flag = in_array(1, array_column($oficinas, 'eliminado'));
        $this->set(compact('oficinas','centros','flag'));
        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_oficinas');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Oficina id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $oficina = $this->Oficinas->get($id, [
            'contain' => ['Centros']
        ]);
        $this->set('oficina', $oficina);
        $this->set('_serialize', ['oficina']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $oficina = $this->Oficinas->newEntity();
        if ($this->request->is('post')) {
            $oficina = $this->Oficinas->patchEntity($oficina, $this->request->data);
            if ($this->Oficinas->save($oficina)) {
                $this->Flash->success(__('The oficina has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The oficina could not be saved. Please, try again.'));
            }
        }
        $centros = $this->Oficinas->Centros->find('list', ['limit' => 200]);
        $this->set(compact('oficina', 'centros'));
        $this->set('_serialize', ['oficina']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Oficina id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $oficina = $this->Oficinas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $oficina = $this->Oficinas->patchEntity($oficina, $this->request->data);
            if ($this->Oficinas->save($oficina)) {
                $this->Flash->success(__('The oficina has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The oficina could not be saved. Please, try again.'));
            }
        }
        $centros = $this->Oficinas->Centros->find('list', ['limit' => 200]);
        $this->set(compact('oficina', 'centros'));
        $this->set('_serialize', ['oficina']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Oficina id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $oficina = $this->Oficinas->get($id);
        $oficina->eliminado = 1;
        if ($this->Oficinas->save($oficina)) {
            $this->Flash->success(__('The oficina has been deleted.'));
        } else {
            $this->Flash->error(__('The oficina could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\View\CellTrait; 
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

use Usermgmt\Controller\UsermgmtAppController;
use Cake\Event\Event;
use Cake\Utility\Inflector;


/**
 * Cotizaciones Controller
 *
 * @property \App\Model\Table\CotizacionesTable $Cotizaciones
 *
 * @method \App\Model\Entity\Cotizacion[] paginate($object = null, array $settings = [])
 */
class CotizacionesController extends AppController
{
    use CellTrait;

     /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search', 'RequestHandler'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '100'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Cotizaciones'=>[
                'Cotizaciones'=>[
                    'position' => 'busqueda',
                    'type'=>'text',
                    'label'=>'Buscar',
                    'tagline'=>false,
                    'condition'=>'multiple',
                    'searchFields'=>['Cotizaciones.cliente', 'Cotizaciones.contacto', 'Centros.nombre', 'Cotizaciones.vence', 'Cotizaciones.creada', 'Cotizaciones.status'],
                    'inputOptions'=>['style'=>'width:300px;']
                ],
                'Centros.nombre'=>[
                    'type'=>'select',
                    'label'=>'Centros',
                    'model'=>'Centros',
                    'selector'=>'getCentrosSelect'
                ],
                'Cotizaciones.status'=>[
                    'type'=>'select',
                    'label'=>'Status',
                    'options'=>['' => 'Seleccione', 'creada' => 'creada', 'enviada' => 'enviada', 'aceptada'=>'aceptada', 'cancelada' => 'cancelada']
                ],
                'Cotizaciones.aplica_promocion'=>[
                    'type'=>'select',
                    'label'=>'Promoción',
                    'options'=>['' => 'Seleccione', '1' => 'Si', '0' => 'No']
                ],
                'Cotizaciones.fecha_inicio'=>[
                    'type'=>'text',
                    'label'=>'Creación desde',
                    'model'=>'Cotizaciones',
                    'inputOptions'=>['id'=>"fecha_inicio"]
                ],
                'Cotizaciones.fecha_fin'=>[
                    'type'=>'text',
                    'label'=>'Creación hasta',
                    'model'=>'Cotizaciones',
                    'inputOptions'=>['id'=>"fecha_fin"]
                ]
            ]
        ]
    ];

    public $fecha_inicio=null;
    public $fecha_fin=null;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }


    public function beforeFilter(Event $event){

        if($this->request->params['action']=='index'){
            $sesion=$this->request->session();
            $this->fecha_inicio='';
            if(isset($this->request->data['Cotizaciones']['fecha_inicio'])){
                $this->fecha_inicio=$this->request->data['Cotizaciones']['fecha_inicio'];
                unset($this->request->data['Cotizaciones']['fecha_inicio']);
            }else{
                $fecha_inicio=$sesion->read('UserAuth.Search.Cotizaciones.index.Cotizaciones.fecha_inicio');
                if(!is_null($fecha_inicio)){
                    $this->fecha_inicio=$fecha_inicio;
                    $sesion->delete('UserAuth.Search.Cotizaciones.index.Cotizaciones.fecha_inicio');
                }
            }
            $this->fecha_fin='';
            if(isset($this->request->data['Cotizaciones']['fecha_fin'])){
                $this->fecha_fin=$this->request->data['Cotizaciones']['fecha_fin'];
                unset($this->request->data['Cotizaciones']['fecha_fin']);
            }else{
                $fecha_fin=$sesion->read('UserAuth.Search.Cotizaciones.index.Cotizaciones.fecha_fin');
                if(!is_null($fecha_fin)){
                    $this->fecha_fin=$fecha_fin;
                    $sesion->delete('UserAuth.Search.Cotizaciones.index.Cotizaciones.fecha_fin');
                }
            }
            if(isset($this->request->data['search_clear']) && $this->request->data['search_clear'] ==1){
                $this->fecha_inicio='';
                $this->fecha_fin='';
            }
        }

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if(isset($this->request->data['start'])){
            $fecha_inicio = date("Y-m-d", strtotime($this->request->data['start']));
        }else{
            $this->request->data['start']='';
        }
        if(isset($this->request->data['end'])){
            $fecha_fin = date("Y-m-d", strtotime($this->request->data['end']));
        }else{
            $this->request->data['end']='';
        }
        if(isset($fecha_inicio)&&isset($fecha_fin)){
            $this->paginate = [
                'contain' => ['Oficinas', 'Centros', 'Usuarios'],
                'conditions' => ['Cotizaciones.creada >=' => $fecha_inicio, 'Cotizaciones.creada <=' => date('Y-m-d', strtotime($fecha_fin.' + 1 days'))],
                'order' =>['fecha' => 'DESC']
            ];
            $this->Search->applySearch();
            $cotizaciones = $this->paginate($this->Cotizaciones)->toArray();
            $this->set(compact('cotizaciones'));
        }
        else{
             $fecha_fin="";
             $this->paginate = ['limit'=>10, 'order'=>['Cotizaciones.id'=>'DESC']];
            $conditions=[];
            if(!is_null($this->fecha_inicio) && $this->fecha_inicio !='' && !is_null($this->fecha_fin) && $this->fecha_fin !=''){
                $conditions=['Cotizaciones.creada >=' => $this->fecha_inicio, 'Cotizaciones.creada <=' => date('Y-m-d', strtotime($this->fecha_fin.' + 1 days'))];       
            }

            $this->Search->applySearch($conditions);
            $this->paginate = [
                'contain' => ['Oficinas', 'Centros', 'Usuarios']
            ];
            $this->request->data['fecha_inicio']='';
            $this->request->data['fecha_fin']='';
            if(!is_null($this->fecha_inicio) && $this->fecha_inicio !='' && !is_null($this->fecha_fin) && $this->fecha_fin !=''){
                $sesion=$this->request->session();
                $sesion->write('UserAuth.Search.Cotizaciones.index.Cotizaciones.fecha_inicio',$this->fecha_inicio);
                $sesion->write('UserAuth.Search.Cotizaciones.index.Cotizaciones.fecha_fin',$this->fecha_fin);
                $this->request->data['fecha_inicio']=$this->fecha_inicio;
                $this->request->data['fecha_fin']=$this->fecha_fin;
            }
            $cotizaciones = $this->paginate($this->Cotizaciones)->toArray();
            $this->set(compact('cotizaciones'));
        }
        $this->set('fecha_fin',$this->request->data['fecha_fin']);
        $this->set('fecha_inicio',$this->request->data['fecha_inicio']);
        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_cotizaciones');
        }
    }

    /**
     * ShowPDF method
     *
     * @param string|null $id Cotizacion id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function pdf($id = null)
    {
        $cotizacion = $this->Cotizaciones->get($id, [
            'contain' => ['Oficinas', 'Centros']
        ]);
        $this->viewBuilder()->options([
            'pdfConfig' => [
                'orientation' => 'portrait',
                'filename' => 'cotizacion' . $id . '.pdf'
            ]
        ]);
        $this->set('cotizacion', $cotizacion);
    }

    /**
     * View method
     *
     * @param string|null $id Cotizacion id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cotizacion = $this->Cotizaciones->get($id, [
            'contain' => ['Oficinas', 'Centros']
        ]);

        $this->set('cotizacion', $cotizacion);
        $this->set('_serialize', ['cotizacion']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $allOficinas = TableRegistry::get('oficinas');
        $allOficinas = $allOficinas->find();
        $cotizacion = $this->Cotizaciones->newEntity();
        if ($this->request->is('post')) {
            $cotizacion = $this->Cotizaciones->patchEntity($cotizacion, $this->request->data);
            $cotizacion->creada =date("Y-m-d H:i:s");
            $cotizacion->vence = date("Y-m-d H:i:s", strtotime($cotizacion->creada.' + '.strval($cotizacion->vigencia).'days'));
            $cotizacion->status = 'creada';
            $cotizacion->usuario_id = $this->UserAuth->getUserId();
            $cotizacion->eliminado =0;
            if ($this->Cotizaciones->save($cotizacion)) {
                $cotizacion_id = $cotizacion->id;
                foreach (json_decode($this->request->data['oficinas_array']) as $cot_ofi) {
                    $this->loadModel('CotizacionesOficinas');
                    $cotizacionesOficinas = $this->CotizacionesOficinas->newEntity();
                    $cotizacionesOficinas->cotizacion_id = $cotizacion_id;
                    $cotizacionesOficinas->oficina_id = $cot_ofi->id;
                    $cotizacionesOficinas->precio_regular = $cot_ofi->precio_regular;
                    $cotizacionesOficinas->precio_promocional = $cot_ofi->precio_promocional;
                    $this->CotizacionesOficinas->save($cotizacionesOficinas);
                }
                $this->Flash->success(__('The cotizacion has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cotizacion could not be saved. Please, try again.'));
        }
        $centros = $this->Cotizaciones->Centros->find('list', ['limit' => 200]);
        $monedas = $this->Cotizaciones->Monedas->find('list', ['limit' => 200]);
        $this->set(compact('cotizacion', 'centros', 'monedas', 'allOficinas'));
        $this->set('_serialize', ['cotizacion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cotizacion id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cotizacion = $this->Cotizaciones->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cotizacion = $this->Cotizaciones->patchEntity($cotizacion, $this->request->getData());
            if ($this->Cotizaciones->save($cotizacion)) {
                $this->Flash->success(__('The cotizacion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cotizacion could not be saved. Please, try again.'));
        }
        $oficinas = $this->Cotizaciones->Oficinas->find('list', ['limit' => 200]);
        $centros = $this->Cotizaciones->Centros->find('list', ['limit' => 200]);
        $this->set(compact('cotizacion', 'oficinas', 'centros'));
        $this->set('_serialize', ['cotizacion']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cotizacion id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cotizacion = $this->Cotizaciones->get($id);
        if ($this->Cotizaciones->delete($cotizacion)) {
            $this->Flash->success(__('The cotizacion has been deleted.'));
        } else {
            $this->Flash->error(__('The cotizacion could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

     /**
     * getInfoOficinas method
     *
     * @param string|null $id Oficina id.
     * @return json
     */

    public function getInfoOficinas()
    {
        if ($this->request->is('ajax')) {
            $oficina_id = $this->request->data['oficina_id'];
            $oficinas = TableRegistry::get('oficinas')->find()->where(['id' => $oficina_id]);
            die(json_encode($oficinas)); 
        }
    }

     /**
     * getOficinas method
     *
     * @param string|null $id Centro id.
     * @return json
     */

    public function getOficinas()
    {
        if ($this->request->is('ajax')) {
            $centro_id = $this->request->data['centro_id'];
            $oficinas = TableRegistry::get('oficinas')->find()->where(['centro_id' => $centro_id]);
            die(json_encode($oficinas)); 
        }
    }

     /**
     * changeStatus method
     *
     * @param string|null $id Oficina id.
     * @return json
     */

    public function changeStatus()
    {
        if ($this->request->is('ajax')) {
            $id_cotizacion = $this->request->data['id_cotizacion'];
            $status = $this->request->data['status'];
            $comentario = $this->request->data['comentario'];
            $cotizaciones= TableRegistry::get('Cotizaciones');
            $cotizacion = $cotizaciones->get($id_cotizacion);
            $result = [];
            $cotizacion->comentario = $comentario;
            $cotizacion->status = $status;
            if($cotizaciones->save($cotizacion)){
                $result['result'] = true;
            } else {
                $result['result'] = false;
            }
            die(json_encode($result));
        }
    }



}

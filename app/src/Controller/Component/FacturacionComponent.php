<?php
/**
 * Facturacion component
 *
 * FacturaciÃ³n con solucionFactible.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Controller.Component
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller\Component;

//use Cake\Controller\Component;
use Cake\Core\App;
use SoapClient;

class FacturacionComponent extends \Cake\Controller\Component
{

	public $usuario = 'testing@solucionfactible.com';//'admin@135-bc.com'; //
    public $password = 'a0123456789';//'solrol14'; //

    public $ws_url='https://testing.solucionfactible.com/ws/services/CFDI?wsdl';//'https://solucionfactible.com/ws/services/CFDI?wsdl';//


	public function descargarXML($factura_id, $centro = null) {
			// $this->loadModel('Timbrarfactura');
			// $facturaTimbrada = $this->Timbrarfactura->find('first',array('conditions'=>array('Timbrarfactura.factura_id'=>$factura_id)));
		$url 		= isset($centro) ? $centro['url_timbrado'] 	: $this->ws_url;
		$username 	= isset($centro) ? $centro['username'] 		: $this->usuario;
		$password 	= isset($centro) ? $centro['password'] 		: $this->password;
			
			try {
					$client = new SoapClient($url,array('trace' => true));
			        $params = array('usuario' => $username, 'password' => $password, 'uuid'=>'475C3876-8DB1-4950-B7F3-5D8CBF2D34FC','folio'=>"1006",'serie'=>"A",'diseno'=>2);
			        $response2 = $client->__soapCall('obtenerDatos', array('parameters' => $params));

			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}
			$datosfactura = $response2->return;
			die(debug($datosfactura));
			// $this->response->body($client->__getLastResponse());
 		//     $this->response->type('xml');
 		//     $this->response->download('factura'.$facturaTimbrada['Timbrarfactura']['uuid'].'.xml');
  	// 		return $this->response;
	}


		public function descargarPDF($factura_id) {
			$this->loadModel('Timbrarfactura');
			$this->loadModel('MetodoPago');
			$facturaTimbrada = $this->Timbrarfactura->find('first',array('conditions'=>array('Timbrarfactura.factura_id'=>$factura_id)));
			$metodospago = $this->MetodoPago->find('list');
			
			
			try {
					$client = new \SoapClient($this->ws_url);
			        $params = array('usuario' => $this->usuario, 'password' => $this->password, 'uuid'=>"",'folio'=>"1006",'serie'=>"A",'diseno'=>2);
			        $response2 = $client->__soapCall('obtenerDatos', array('parameters' => $params));
			        /*echo '<pre>';
			        print_r($response2->return->comprobantes);
			        echo '</pre>';*/
			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}

			$this->Factura->Behaviors->load('Containable');
			$factura = $this->Factura->find('first',array(
					'conditions'=>array(
							'Factura.id'=>$factura_id
						),
					'contain'=>array(
							'Cliente'=>array('Sucursal')
						)
				));

			 // debug($factura);die;

			$datosfactura = $response2->return->comprobantes;
			//generamos el pdf
			App::import('Vendor', 'html2pdf', array('file' => 'html2pdf'.DS.'html2pdf.class.php'));
			$html2pdf = new HTML2PDF('P','LETTER','fr');
			//print_r($datosfactura);
			$body = $this->_pdf_content(compact('datosfactura','metodospago','factura'), 'facturas/factura');
		    $html2pdf->WriteHTML($body);
		   	$html2pdf->Output('factura.pdf');
	}
	
	// @params usuario: required
	// @params usuario: required
	// @params data: required, ver https://solucionfactible.com/sfic/capitulos/emision/ws-cfdi-metodos/crear.jsp

	public function crear($comprobantes = null, $centro = null){

		$url 		= isset($centro) ? $centro['url_timbrado'] 	: $this->ws_url;
		$username 	= isset($centro) ? $centro['username'] 		: $this->usuario;
		$password 	= isset($centro) ? $centro['password'] 		: $this->password;

	//public function crear($usuario = null, $password = null, $comprobantes = null){
		try {
		        $client = new SoapClient($url);
		        $params = array('usuario' => $username, 'password' => $password, 'comprobantes'=>$comprobantes, 'zip'=>False);
		        return $client->__soapCall('crear', array('parameters' => $params));
			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}
	}


	public function obtenerDatos($uuid = null, $folio = null, $serie = null, $centro = null){
	//public function obtenerDatos($usuario = null, $password = null, $uuid = null, $folio = null, $serie = null){
		$url 		= isset($centro) ? $centro['url_timbrado'] 	: $this->ws_url;
		$username 	= isset($centro) ? $centro['username'] 		: $this->usuario;
		$password 	= isset($centro) ? $centro['password'] 		: $this->password;

		$client = new SoapClient($url);

			try {
			        $params = array('usuario' => $username, 'password' => $password, 'uuid'=>$uuid,'folio'=>$folio, 'serie'=>$serie);
			        $response = $client->__soapCall('obtenerDatos', array('parameters' => $params));
					
					$responseData = $response->return;
					return $responseData->comprobantes->xml;
			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}
	}

	public function datosFactura($uuid = null, $folio = null, $serie = null, $centro = null){
	//public function datosFactura($usuario = null, $password = null, $uuid = null, $folio = null, $serie = null){	
		
		$url 		= isset($centro) ? $centro['url_timbrado'] 	: $this->ws_url;
		$username 	= isset($centro) ? $centro['username'] 		: $this->usuario;
		$password 	= isset($centro) ? $centro['password'] 		: $this->password;

		$client = new SoapClient($url);
		

			try {
			        $params = array('usuario' => $username, 'password' => $password, 'uuid'=>$uuid,'folio'=>$folio, 'serie'=>$serie);
			        $response = $client->__soapCall('obtenerDatos', array('parameters' => $params));
					
					$responseData = $response->return;
					return $responseData->comprobantes;
			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}
	}


	public function cancelar($solicitudesCancelacion = null, $centro){
	//public function cancelar($usuario = null, $password = null, $solicitudesCancelacion = null){
		$url 		= isset($centro) ? $centro['url_timbrado'] 	: $this->ws_url;
		$username 	= isset($centro) ? $centro['username'] 		: $this->usuario;
		$password 	= isset($centro) ? $centro['password'] 		: $this->password;

		$client = new SoapClient($url);

			try {
			        $params = array('usuario' => $username, 'password' => $password, 'solicitudesCancelacion'=> $solicitudesCancelacion);
			        return $client->__soapCall('cancelar', array('parameters' => $params));
				
			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}
	}

	public function pdf($usuario=null, $password=null, $uuid = null, $folio=null, $serie=null,$download=1){

		$client = new SoapClient($this->ws_url);

			try {
			        $params = array('usuario' => $this->usuario, 'password' => $this->password, 'uuid'=>$uuid,'folio'=>$folio, 'serie'=>$serie,'diseno'=>2);
			        $response2 = $client->__soapCall('generarPDF', array('parameters' => $params));
			        if($download){
				        header("Content-Description: File Transfer");
						header("Content-Type: application/octet-stream");
						header("Content-Transfer-Encoding: binary");
						header("Content-Disposition: attachment; filename=factura.pdf");
						header('Content-Length: '.  strlen($response2->return->pdf));
						header('Expires: 0');
						header('Cache-Control: must-revalidate');
						header('Pragma: public');
						echo $response2->return->pdf;
						return;// $this->redirect(array('action'=>'index'));
					}else{
						return $response2->return->pdf;
					}

			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}
	}


	public function timbrar($factura_id = null) {
			//CARGAMOS LOS MODELOS
			$this->loadModel('Cliente');
			$this->loadModel('Factura');
			$this->loadModel('MetodoPago');
			$this->loadModel('Temptable');
			$this->loadModel('Timbrarfactura');
			//
			$factura = $this->Factura->find('first',array('conditions'=>array('Factura.id'=>$factura_id)));
			$cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$factura['Factura']['cliente_id'])));
			$metodopago = $this->MetodoPago->find('first',array('conditions'=>array('MetodoPago.id'=>$factura['Factura']['metodo_pago_id'])));
			
			//$b64 = base64_encode($sData);
			//debug($metodopago);
			//cargamos el ultimo folio
			$ultimofolio = $this->Temptable->find('first',array('order'=>array('id DESC')));
			$response = '';
			$data['autorizada'] = true;
			$data['cancelada'] = false;
			$data['fechaPago'] = '2012-12-18';
			$data['folio'] = $ultimofolio['Temptable']['folio']+1;
			$this->Temptable->create();
			$this->Temptable->save(array('folio'=>$data['folio']));
			$data['formaPago'] = $metodopago['MetodoPago']['nombre'];
			$data['metodoPago'] = '<table border="0" cellpadding="1" cellspacing="1" style="width: 100%;">
									<tr>
										<td style="width: 25%; background: black; color: white;">Banco</td>
										<td style="width: 25%; background: black; color: white;">NÃºmero de cuenta</td>
									</tr>

									<tr>
										<td style=" width: ; ">
							   						 '.$factura['Factura']['banco'].'
										</td>

										<td style=" width: ; ">
							   						 '.$factura['Factura']['no_cuenta'].'
										</td>

									</tr>
							</table>';
			$data['nombreCliente'] = $cliente['Cliente']['nombre'].' '.$cliente['Cliente']['apellido_paterno'].' '.$cliente['Cliente']['apellido_materno'];
			$data['rfcCliente'] = $cliente['Cliente']['rfc'];
			$data['detalleCFDI']['cantidad'] = 1;
			$data['detalleCFDI']['comment'] = 'Comentario';
			$data['detalleCFDI']['concepto'] = "Periodo del {$factura['Factura']['periodo_inicio']} al {$factura['Factura']['periodo_final']} con renta diaria de {$factura['Factura']['renta_diaria']}  en un total de {$factura['Factura']['dias']} dÃ­as";
			$data['detalleCFDI']['importe'] = $factura['Factura']['subtotal'];
			$data['detalleCFDI']['precioUnitario'] = $factura['Factura']['total'];
			$data['detalleCFDI']['tasaIva'] = 16;
			$data['detalleCFDI']['unidad'] = 'PZA';
         
			/*
			Generamos CFDI */
			try {
			        $client = new SoapClient($this->ws_url);
			        $params = array('usuario' => $this->usuario, 'password' => $this->password, 'comprobantes'=>$data, 'zip'=>False);
			        $response = $client->__soapCall('crear', array('parameters' => $params));
			        $this->Factura->save(array('id'=>$factura['Factura']['id'],'timbrada'=>1));
			        /*echo '<pre>';
			        print_r($response->return);
			        echo '</pre>';
			        die();*/
			        $this->Timbrarfactura->create();
			        $this->Timbrarfactura->save(array(
			        	'serie'=>$response->return->resultadosCreacion->serie,
			        	'uuid'=>$response->return->resultadosCreacion->uuid,
			        	'folio'=>$response->return->resultadosCreacion->folio,
			        	'factura_id'=>$factura_id));
			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}
			 
			//debug($response);	
			$ret = $response->return;

			try {
			        $params = array('usuario' => $this->usuario, 'password' => $this->password, 'uuid'=>$ret->resultadosCreacion->uuid,'folio'=>$ret->resultadosCreacion->folio,'serie'=>$ret->resultadosCreacion->serie,'diseno'=>2);
			        $response2 = $client->__soapCall('generarPDF', array('parameters' => $params));
			        header("Content-Description: File Transfer");
					header("Content-Type: application/octet-stream");
					header("Content-Transfer-Encoding: binary");
					header("Content-Disposition: attachment; filename=hex.pdf");
					header('Content-Length: '.  strlen($response2->return->pdf));
					header('Expires: 0');
					header('Cache-Control: must-revalidate');
					header('Pragma: public');
					echo $response2->return->pdf;
					return $this->redirect(array('action'=>'index'));
			} catch (SoapFault $fault) {
			        echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
			}
		
	}


}

?>
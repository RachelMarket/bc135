<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

use Cake\View\CellTrait;

/**
 * Reservaciones Controller
 *
 * @property \App\Model\Table\ReservacionesTable $Reservaciones
 */
class ReservacionesController extends AppController
{

    use CellTrait;

     /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '100'];

    public $searchFields = [
        'index'=>[
            'Recursos'=>[
                'Recursos.centro_id'=>[
                    'type'=>'select',
                    'label'=>'Centro',
                    'model'=>'Centros',
                    'selector'=>'getCentros'
                    
                ],
                'Recursos'=>[
                    'position'=>'busqueda',
                    'type'=>'text',
                    'label'=>'Nombre',
                    'condition'=>'multiple',
                    'searchFields'=>['Recursos.id'],
                    'inputOptions'=>['style'=>'width:300px;']
                ],
                'Recursos.tipo_id'=>[
                    'type'=>'select',
                    'label'=>'Tipo',
                    'model'=>'RecursosTipos',
                    'selector'=>'getTiposRecursos'
                    
                ],
            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {   

      
        $this->loadModel('Recursos');
        $this->loadModel('Clientes');
        $clientes = $this->Clientes->find('list');
        $this->set('clientes',$clientes);

        $reservacion = $this->Reservaciones->newEntity();

        $this->paginate = [
                'sortWhitelist' => [ 
                    'Recursos.id',
                    'Centros.nombre',
                    'Recursos.name',
                    'RecursosTipos.name',
                    'Recursos.capacidad',
                    'Recursos.hora_inicio'
                ],
                'limit'=>10, 
                'contain' => ['Centros', 'RecursosTipos'], 
                'order'=>['Recursos.id'=>'DESC']];

        $this->Search->applySearch(['Recursos.deleted' => false]);
        $recursos = $this->paginate($this->Recursos)->toArray();
        


        $this->set(compact('recursos','reservacion','cliente'));

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->set('modo_calendario',true);
            $this->render('/Element/all_recursos' );
        }

    }

    public function events( $recurso_id = null , $resto = null){

        $userId = $this->UserAuth->getUserId();
        $groupId = $this->UserAuth->getGroupId();
        $user = $this->UserAuth->getUser();



        //grupos 5 ,6 ven en gris pobrecitos
        $grupoLectura = array( 5 ,6 );

        $options['modo_protegido']  = false;
        $options['userId'] = $userId;
        
        $options['user'] = $user;

        if( isset( $this->request->query['color'] ) ) {
            $options['modo_color'] = true;
        }

        if( isset( $this->request->query['centro_id'] ) ) {
            $options['centro_id'] = $this->request->query['centro_id'];
            
        }

        if((in_array( $this->UserAuth->getGroupId(), [5,6]))? 1 : 0){
            $options['mostrar_tiempo_espera'] = true;
        }

        if( in_array($groupId, $grupoLectura)  ){
            $options['modo_protegido'] = true ;
        }


        $options['conditions'][] = array('Reservaciones.fecha_inicio >=' => $this->request->data['start']);

        $options['conditions'][] = array('Reservaciones.fecha_inicio <=' => $this->request->data['end']);

        if( !is_null( $recurso_id ) ){
            $options['recurso_id'] = $recurso_id;
            $options['conditions'][] = array('Reservaciones.recurso_id' => $recurso_id );
        }

       

        $events = $this->Reservaciones->find('event',$options);


        echo json_encode($events);

        die();

    }

    /**
     * View method
     *
     * @param string|null $id Reservacion id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reservacion = $this->Reservaciones->get($id, [
            'contain' => ['Recursos', 'Users']
        ]);
        $this->set('reservacion', $reservacion);
        $this->set('_serialize', ['reservacion']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {   

        $reservacion = $this->Reservaciones->newEntity();
        if ($this->request->is('post')) {

            $user = $this->UserAuth->getUser();

            $fi=explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['Reservaciones']['fecha_inicio']));
            
            $inicio= $fi[0] . date(' H:i:s',strtotime($fi[1].' '.$fi[2]));
            
            $f=explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['Reservaciones']['fecha_fin']));
            
            $fin= $f[0] . date(' H:i:s',strtotime($f[1].' '.$f[2]));

            $this->request->data['Reservaciones']['fecha_inicio'] = $inicio;
            $this->request->data['Reservaciones']['fecha_fin'] = $fin;

            $this->request->session()->write('startDate', $this->request->data['Reservaciones']['fecha_inicio'] ); 

            $reservacion = $this->Reservaciones->patchEntity($reservacion, $this->request->data);
            $reservacion->user_id =  $this->UserAuth->getUserId();
            if( in_array(  $this->UserAuth->getGroupId()  , [1,4,8] )){
            // soy admin  o Encargado tengo mas privilegios
                $this->Reservaciones->addBehavior('AdminStaffReservacion');  

            }else{
                 $this->Reservaciones->addBehavior('ClientesReservacion');
            
            }


            

            $this->Reservaciones->canCreate($reservacion, null);
            $this->Reservaciones->validD( $reservacion,null );
            

            $errors = $reservacion->errors();
            if($this->request->is('ajax')) {
                if(empty($errors)) {
                    $response = ['error'=>0, 'message'=>'success'];
                } else {
                    $response = ['error'=>1, 'message'=>'failure'];
                    $response['data']['Reservaciones'] = $errors;
                }
                echo json_encode($response);exit;
            } else {

                $connection = ConnectionManager::get('default');
                $connection->begin();


                try{
                    
                    //debug($this->request->session()->read('startDate'));
                    //debug($reservacion);die();
                    if ($this->Reservaciones->save($reservacion) ) {

                        $this->Reservaciones->repetir( $reservacion , $this->request->data );
                        
                        $connection->commit();
                        
                        $this->Reservaciones->enviar_correo( $reservacion );

                        $this->Flash->success(__('Reservación creada'));
                        return  $this->redirect(  $_SERVER['HTTP_REFERER']  );
                    } else {

                        $this->Flash->error(__('Ocurrio un error al intentar crear la reservación'));

                        return  $this->redirect(  $_SERVER['HTTP_REFERER']  );

                        
                    }

                } catch (\Exception $e) {

                     

                    $connection->rollback();

                    $this->Flash->error($e->getMessage());

                    return $this->redirect(  $_SERVER['HTTP_REFERER']  );
               }
            }
        }
        $recursos = $this->Reservaciones->Recursos->find('list', ['limit' => 200]);
        $users = $this->Reservaciones->Users->find('list', ['limit' => 200]);
        $this->set(compact('reservacion', 'recursos', 'users'));
        $this->set('_serialize', ['reservacion']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Reservacion id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Clientes');
        
        $reservacion = $this->Reservaciones->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $fi=explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['Reservaciones']['fecha_inicio']));
            
            $inicio= $fi[0] . date(' H:i:s',strtotime($fi[1].' '.$fi[2]));
            
            $f=explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['Reservaciones']['fecha_fin']));
            
            $fin= $f[0] . date(' H:i:s',strtotime($f[1].' '.$f[2]));

            $this->request->data['Reservaciones']['fecha_inicio'] = $inicio;
            $this->request->data['Reservaciones']['fecha_fin'] = $fin;

            $user = $this->UserAuth->getUser();

            $tipo = $reservacion->tipo;
            if(  intval($this->request->data['Reservaciones']['guardar_replicar'])  == 0   ){
                // Di click en guardar es una edición manual
                $this->request->data['Reservaciones']['reservacion_id'] = null;

            }

            if( $this->request->data['Reservaciones']['recurrente'] == 0 ){
                $this->request->data['Reservaciones']['tipo'] = null;
            }

            $reservacion = $this->Reservaciones->patchEntity($reservacion, $this->request->data);
            $reservacion->user_id =  $this->UserAuth->getUserId();
            

            if( in_array(  $this->UserAuth->getGroupId()  , [1,4,8] )){
            // soy admin  o Encargado tengo mas privilegios
                $this->Reservaciones->addBehavior('AdminStaffReservacion');  

            }else{
                 $this->Reservaciones->addBehavior('ClientesReservacion');
            
            }


            

            $this->Reservaciones->canCreate($reservacion, null);
            $this->Reservaciones->validD( $reservacion,null );
            

            $errors = $reservacion->errors();
            if($this->request->is('ajax')) {
                if(empty($errors)) {
                    $response = ['error'=>0, 'message'=>'success'];
                } else {
                    $response = ['error'=>1, 'message'=>'failure'];
                    $response['data']['Reservaciones'] = $errors;
                }
                echo json_encode($response);exit;
            } else {

                $connection = ConnectionManager::get('default');
                $connection->begin();


                try{

                    $this->request->session()->write('startDate', $reservacion['fecha_inicio']);

                    if ($this->Reservaciones->save($reservacion)) {
                        $this->Reservaciones->guardar_replicar( $reservacion , $this->request->data , $tipo  );


                        $connection->commit();
                        $this->Reservaciones->enviar_correo( $reservacion );
                        
                        $this->Flash->success(__('Reservación guardada'));

                        

                        return  $this->redirect(  $_SERVER['HTTP_REFERER']  );
                    } else {
                        $connection->rollback();
                        $this->Flash->error(__('Ocurrio un error al intentar guardar la reservación'));
                        return  $this->redirect(  $_SERVER['HTTP_REFERER']  );
                    }
                
                 } catch (\Exception $e) {

                     

                    $connection->rollback();

                    $this->Flash->error($e->getMessage());

                    return  $this->redirect(  $_SERVER['HTTP_REFERER']  );
               }

            }



        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Reservacion id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        
        if( in_array(  $this->UserAuth->getGroupId()  , [1,4,8] )){
            // soy admin  o Encargado tengo mas privilegios
            $this->Reservaciones->addBehavior('AdminStaffReservacion');  

        }else{
             $this->Reservaciones->addBehavior('ClientesReservacion');
        
        }

        if(in_array( $this->UserAuth->getGroupId(), [1,4,5,6,8])){
            

            try {
                
                $connection = ConnectionManager::get('default');
                $connection->begin();
                $this->Reservaciones->cancelar_reservacion( $id , $this->request->data );

                $this->Flash->success(__('La reservación ha sido cancelada.'));
                
                $connection->commit();
            } catch (\Exception $e) {

                $connection->rollback();
                $this->Flash->error( $e->getMessage() );
            }

        }else{
            $this->Flash->error(__('No tienes pemiso para cancelar esta reservación'));
        }
        return $this->redirect($this->referer());
    }

    public function misreservaciones($id = null){
        
        $this->loadModel('Reservaciones');
        $this->loadModel('Clientes');
        
        $user = $this->UserAuth->getUser();
        
        $cliente_id = 0;

        if(!empty($id)){ $cliente_id = $id;
        
        }else{
            $cliente_id = $user['User']['cliente_id'];
        }

        $this->paginate = [
                'sortWhitelist' => [ 
                    'Reservaciones.id',
                    'Centros.nombre',
                    'Recursos.name',
                    'RecursosTipos.name',
                    'Reservaciones.fecha_inicio',
                    'Reservaciones.fecha_inicio'
                ], 
                'contain' => ['Recursos.Centros', 'Recursos.RecursosTipos', 'Clientes', 'Recursos'],
                'conditions' => ['Reservaciones.cliente_id' => $cliente_id], 
                'order'=>['Reservaciones.fecha_inicio'=>'DESC']];

        $this->Search->applySearch();
        $reservations = $this->paginate($this->Reservaciones)->toArray();

        $cliente = $this->Clientes->get($cliente_id);

        $interval = [];

        foreach ($reservations as $key => $row) {
            $one_day = new \DateTime($row->fecha_inicio->format('Y-m-d H:i'));
            $tow_day = new \DateTime($row->fecha_fin->format('Y-m-d H:i'));
            
            $interval[] = $one_day->diff($tow_day);
        }

        $horas = 0;

        if ($interval > 0) {
            foreach ($interval as $key => $value) {
                $horas = $horas + $value->h;
            }
        }

        $dias_retantes = $cliente->saldo - $horas; 

        $this->set(compact('reservations', 'cliente', 'dias_retantes'));

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/reservaciones' );
        }
    }
    
    public function eventsClients( $id = null){

        $userId = $this->UserAuth->getUserId();
        $groupId = $this->UserAuth->getGroupId();
        $user = $this->UserAuth->getUser();

        $cliente_id = 0;

        if(!empty($id)){ $cliente_id = $id; 

        }else{
            $cliente_id = $user['User']['cliente_id'];
        }

        $grupoLectura = array( 5 ,6 );

        $options['modo_protegido']  = false;
        $options['userId'] = $userId;
        $options['user'] = $user;

        if( in_array($groupId, $grupoLectura)  ){
            $options['modo_protegido'] = true ;
        }

       
        $options['conditions'][] = array('Reservaciones.cliente_id' => $cliente_id );

       

        $events = $this->Reservaciones->find('event',$options);


        echo json_encode($events);

        die();

    }




    public function saldo($cliente_id, $is_edit){

        $this->loadModel('Clientes');



        //

        $result['isModOficinaVirtual']  = $this->Reservaciones->isModOficinaVirtual($cliente_id ,  $this->request->data['recurso_id'] );
        
        


        $date = explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['fecha_inicio']));
        $begin = $date[0] . date(' H:i:s',strtotime($date[1].' '.$date[2]));

        $_date = explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['fecha_fin']));
        $end = $_date[0] . date(' H:i:s',strtotime($_date[1].' '.$_date[2]));



        if($result['isModOficinaVirtual'] ){

            $this->loadModel('Recursos');

            $recurso = $this->Recursos->get( $this->request->data['recurso_id']  );


            $begin = $date[0] .  $recurso->hora_inicio->format(' H:i:s') ;

            $end = $_date[0] . $recurso->hora_fin->format( ' H:i:s'  );
            
            $result['fecha_inicio'] = $begin;

            $result['fecha_fin'] = $end ;

            
            
        }


        $one_day = new \DateTime($begin);
        $tow_day = new \DateTime($end);
        
        $saldo = $this->Clientes->saldo($cliente_id, $begin);

        $value = $one_day->diff($tow_day);

        if ($is_edit == 'true'){ $result['saldo'] = $saldo; } else {

            if($value->i > 0){
                $value->h = $value->h + 1;
            }

            $result['saldo'] = $saldo - $value->h;
        }

        $result['saldodias'] =  $this->Clientes->saldodias($cliente_id, $begin);
        

        $result['horas'] = $value->h;

        echo json_encode($result);

        die();
    }

    public function calendario(){

        $this->loadModel('Recursos');
        $this->loadModel('Clientes');
        $this->loadModel('Centros');


        $user = $this->UserAuth->getUser();

        $clientes = $this->Clientes->find('list', [ 'conditions' => [ 'activo' => 1 ] ]);
        $centros = $this->Centros->find('list', ['conditions' => ['deleted' => FALSE]]);

        $reservacion = $this->Reservaciones->newEntity();

        $conditions = [];

        $conditions = ['Recursos.deleted' => false];

        $centro_id = $user['User']['centro_id'];
        $conditions = [ 'Recursos.centro_id' => $user['User']['centro_id']];

        if($this->request->is('get') && $this->request->query){

            $centro_id = $this->request->query['centro_id'];

            if ( !empty($centro_id) ) { $conditions['Recursos.centro_id'] = $centro_id; }

        }else {

            $default = $this->Clientes->find('all')->where([ 'id' => $user['User']['cliente_id'] ])->toArray();



            if ( !empty($default) ) { 
                $centro_id  =  $default[0]['centro_id'];
                $conditions['Recursos.centro_id'] = $centro_id;
            }

        }
        
       
        $recursos = $this->Recursos->find('all', [ 
            'order' => ['Recursos.id' => 'ASC'],
            'conditions' => $conditions,  
            'contain' => ['RecursosFotos'] ] ) ;

        //Tomo las horas de inicio y fin
        $horas = $this->Recursos->find()->where([ 'Recursos.deleted' => false]);
        $horas->select(['inicio' => $horas->func()->min('Recursos.hora_inicio'),'fin' => $horas->func()->max('Recursos.hora_fin')]);

        $rangohoras = ['inicio' => new Time( date('Y-m-d')." 00:00:00" ), 'fin' => new Time( date('Y-m-d')." 23:59:59") ];
        
        //if((in_array( $this->UserAuth->getGroupId(), [5,6]))? 1 : 0){

            foreach ($horas as $key => $row) {
                
                if($key == 0){
                    $rangohoras['inicio'] = $row->inicio;
                    $rangohoras['fin'] = $row->fin;
                    break;
                } 
            }
        //}

        $this->set( compact( 'clientes','reservacion','recursos','horas', 'centros', 'centro_id', 'rangohoras') );

    }

    public function reservacionespdf($cliente_id = null,$month=null,$year=null){
        
        $this->loadModel('Reservaciones');
        $this->loadModel('Clientes');
        
        if(empty($cliente_id)){
            return $this->redirect($this->referer());
        }
        if(is_null($year)){
            $year=date('Y');
        }

        if(is_null($month)){
            $month=date('m');
        }

        $fecha=date_create('01-'.$month.'-'.$year);
        //debug($fecha);
        
        $inicio=date_format($fecha,'Y-m-01');
        $fin=date_format($fecha,'Y-m-t');
        $ultimo_dia = date_format($fecha,'t');

        $this->paginate = [
                'sortWhitelist' => [ 
                    'Reservaciones.id',
                    'Centros.nombre',
                    'Recursos.name',
                    'RecursosTipos.name',
                    'Reservaciones.fecha_inicio',
                    'Reservaciones.fecha_inicio'
                ], 
                'contain' => ['Recursos.Centros', 'Recursos.RecursosTipos', 'Clientes', 'Recursos','Users'],
                'conditions' => ['Reservaciones.cliente_id' => $cliente_id,
                                'fecha_inicio BETWEEN "'.$inicio.'" AND "'.$fin.' 23:59:59"'], 
                'order'=>['Reservaciones.fecha_inicio'=>'DESC'],
                ];

        $this->Search->applySearch();
        $reservations = $this->paginate($this->Reservaciones)->toArray();

        //debug($reservations);
        $cliente = $this->Clientes->get($cliente_id);

           //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        //App::import('Vendor', 'html2pdf', array('file' => 'html2pdf'.DS.'html2pdf.class.php'));
        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');

        $mes_espanol=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
        $periodo = "1 de ".$mes_espanol[$month-1]." al $ultimo_dia de ".$mes_espanol[$month-1]." de $year";
        
        $body = $this->cell('Pdf::reservaciones', ['reservations' => $reservations, 'cliente' => $cliente, 'periodo' => $periodo]);
        $html2pdf->WriteHTML($body);

        $html2pdf->Output('reservaciones_'.$year.'_'.$month.'.pdf');
        exit;

        
    }

    public function deletedSession(){
        $reponse=array('error'=>0);
        try{
            if ($this->request->session()->read('startDate')) {
                $this->request->session()->delete('startDate');
                $reponse['error']=1;
            }
        }catch (\Exception $e) {
            $this->Flash->error($e->getMessage());
        }
        die(json_encode($reponse));
    }
}

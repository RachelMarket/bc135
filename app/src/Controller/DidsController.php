<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Usermgmt\Controller\UsermgmtAppController;
use Cake\View\CellTrait; 
use Cake\Mailer\Email;

/**
 * Dids Controller
 *
 * @property \App\Model\Table\DidsTable $Dids
 */
class DidsController extends AppController
{
	use CellTrait;

	 /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '10'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Dids'=>[
                'Dids.did'=>[
                    'type'=>'text',
                    'label'=>'Buscar',
                    'condition'=>'multiple',
                    'searchFields'=>['Dids.did'],
                    'inputOptions'=>['style'=>'width:300px;', 'placeholder' => 'por DID']
                ]
            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'sortWhitelist' => [
                'Ladas.lada',
                'Dids.did',
                'Contactos.nombre'
            ],
            'limit'=>10, 'contain' => ['ContactosDidsJoin.Contactos','Ladas','ContactosDidsJoin.LugaresDid'],
            'order'=>[
                'Dids.did'=>'ASC'
            ],
        ];

        $this->Search->applySearch(['Dids.deleted' => false]);
        $this->loadModel('Ladas');
        $ladas = $this->Ladas->find('list',['keyField' => 'id', 'valueField' => 'lada'])->where(['deleted' => false]);
        $this->set('ladas', $ladas);

        //debug($this->paginate($this->Dids)->toArray());
        $this->set('dids', $this->paginate($this->Dids)->toArray());
        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_dids');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Did id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $did = $this->Dids->get($id, [
            'contain' => []
        ]);
        $this->set('did', $did);
        $this->set('_serialize', ['did']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Ladas');

        $did = $this->Dids->newEntity();
        if ($this->request->is('post')) {
            $connection = ConnectionManager::get('default');
            $connection->begin();
            
            try{
                
                $did = $this->Dids->patchEntity($did, $this->request->data);

                $errors = $did->errors();

                $did['lada_id'] = $this->request->data['lada_id'];
            
                if ($this->Dids->save($did)) {
                    $this->Flash->success(__('The DID has been saved.'));
                    $connection->commit();
                    return $this->redirect(['action' => 'index']);
                } else {

                    $isUniqueError = $did->errors();

                    if ( isset($isUniqueError['did']) && isset($isUniqueError['did']['_isUnique'] ) ) {

                        $this->Flash->error(__('El DID no se pudo guardar. El número ya existe. '));

                    }else if( isset($isUniqueError['lada_id']) && isset($isUniqueError['lada_id']['_isUnique'] ) ) {
                        $this->Flash->error(__('El DID no se pudo guardar. La lada ya existe. '));
                    }else {
                        throw new \Exception(__('The DID could not be saved. Please, try again.') , 1);
                    }
                    return $this->redirect(['action' => 'index']);
                }
                
            } catch (\Exception $e) {
                
                $connection->rollback();
                $this->Flash->error($e->getMessage());
                return $this->redirect(['action' => 'index']);
            }
        }
        $ladas = $this->Ladas->find('list',['keyField' => 'id', 'valueField' => 'lada'])->where(['deleted' => false]);

        $this->set(compact('did','ladas'));
        $this->set('_serialize', ['did']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Did id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Ladas');

        $did = $this->Dids->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $did = $this->Dids->patchEntity($did, $this->request->data);

            $errors = $did->errors();

            $did['lada_id'] = $this->request->data['lada_id'];
            //debug($this->request->data);die();
            if ($this->Dids->save($did)) {
                $this->Flash->success(__('The DID has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $isUniqueError = $did->errors();
                if ( isset($isUniqueError['did']) && isset($isUniqueError['did']['_isUnique'] ) ) {

                    $this->Flash->error(__('El DID no se pudo guardar. El número ya existe. '));

                }else if( isset($isUniqueError['lada_id']) && isset($isUniqueError['lada_id']['_isUnique'] ) ) {
                    $this->Flash->error(__('El DID no se pudo guardar. La lada ya existe. '));
                }else {
                    throw new \Exception(__('The DID could not be saved. Please, try again.') , 1);
                }
                return $this->redirect(['action' => 'index']);
            }
            
        }

        $ladas = $this->Ladas->find('list',['keyField' => 'id', 'valueField' => 'lada'])->where(['deleted' => false]);

        $this->set(compact('did','ladas'));
        $this->set('_serialize', ['did']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Did id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $did = $this->Dids->get($id);
        $did->deleted = true;
        if ($this->Dids->save($did)) {
            $this->Flash->success(__('The did has been deleted.'));
        } else {
            $this->Flash->error(__('The did could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\View\CellTrait;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Email\Email;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class ClientesController extends AppController
{
    use CellTrait;

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '25'];

    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Clientes'=>[
                'Clientes'=>[
                    'position'=>'busqueda',
                    'type'=>'text',
                    'label'=>'Buscar',
                    'tagline'=>'<br>Busca por Nombre, Razón Social, RFC, Contactos',
                    'condition'=>'multiple',
                    'searchFields'=>['Clientes.nombre','Clientes.rfc','Clientes.primario_persona_de_contacto','Clientes.secundario_persona_de_contacto','Clientes.razon_social'],
                    'inputOptions'=>['style'=>'width:300px;']
                ],

                'Clientes.tipo_cliente_id'=>[
                    'type'=>'select',
                    'label'=>'Tipo Cliente',
                    'model'=>'TipoClientes',
                    'selector'=>'getTipoClientes'
                    
                ],
                'Clientes.activo'=>[
                    'type'=>'select',
                    'label'=>'Estatus',
                    'options'=>['1' => 'Activo', '0' => 'Suspendidos'],
                    'inputOptions'=>['style'=>'width:150px;']
                ],            
            ]
        ],
        'view'=>[
            'ClientesAsociados'=>[
                'ClientesAsociados.nombre'=>[
                    'position'=>'busqueda',
                    'type'=>'text',
                    'label'=>false,
                    'condition'=>'multiple',
                    'searchFields'=>["ClientesAsociados.nombre"],
                    'inputOptions'=>['style'=>'width:300px; border-radius: 3px', 'placeholder' => 'Nombre']
                ],
                'ClientesAsociados.comentario'=>[
                    'position'=>'busqueda',
                    'type'=>'text',
                    'label'=>false,
                    'condition'=>'multiple',
                    'searchFields'=>["ClientesAsociados.comentario"],
                    'inputOptions'=>['style'=>'width:300px; border-radius: 3px', 'placeholder' => 'Comentario']
                ]
            ]
        ]
    ];

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $usuario = $this->UserAuth->getUser();

        $conditions = [];

        $this->paginate = [
            'contain' => ['Paises', 'Estados', 'Municipios', 'Monedas']
        ];
        $conditions = ['Clientes.activo' => true];

        if( $this->UserAuth->getGroupId() != 1 ){
            $conditions[] = ['Clientes.centro_id' =>  $usuario['User']['centro_id'] ];
        }

        $this->Search->applySearch($conditions);
        $this->set('clientes', $this->paginate($this->Clientes));
        $this->set('_serialize', ['clientes']);

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_clientes');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Cliente id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        //ver mes actual
        $this->loadModel('Servicios');
        $this->loadModel('ClientesServicios');
        
        $now = new \DateTime('now');
        $mes = $now->format('m');
        $año = $now->format('Y');
        $ultimo_dia = cal_days_in_month(CAL_GREGORIAN, $mes, $año);

        $fechaInicial = $año.'-'.$mes.'-01';
        $fechaFinal = $año.'-'.$mes.'-'.$ultimo_dia." 23:59:59";

        //die(debug($fechaFinal));

        $clienteServicio = $this->ClientesServicios->find('all', ['conditions'=> ["ClientesServicios.cliente_id = $id", 'AND' => ['ClientesServicios.fecha >='=>$fechaInicial,'ClientesServicios.fecha <='=>$fechaFinal]], 
                                                                    'order' => ["ClientesServicios.id DESC"],
                                                                    'contain' => ['Facturas']]);
        
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Paises', 'Estados', 'Municipios', 'Monedas', 'Servicios', 'Facturas','TipoClientes','Centros','Formasdepagos']
        ]);

        $this->loadModel('ClientesAsociados');
        $this->loadModel('Extensiones');
        $this->loadModel('TarjetasAcceso');
        
        $this->paginate = ['contain' => ['Clientes'],
            'limit' => 10,
            'order' => [
                'ClientesAsociados.id' => 'DESC'
            ],
        ];
        
        $this->Search->applySearch(['ClientesAsociados.cliente_id' => $id, 'ClientesAsociados.deleted' => false]);

        $clientesAsociados = $this->paginate($this->ClientesAsociados)->toArray();
        
        $options = ['M' => 'Masculino', 'F' => 'Femenino'];
        
        $extends = $this->Extensiones->find('list')->where(['Extensiones.assigned' => false, 'Extensiones.deleted' => false]);
        $tajects = $this->TarjetasAcceso->find('list')->where(['TarjetasAcceso.assigned' => false, 'TarjetasAcceso.deleted' => false]);
        
        $activeTab = true;
        $this->set(compact('clientesAsociados', 'id', 'options', 'extends', 'tajects', 'contacts','activeTab'));
        $this->set('cliente', $cliente);
        $this->set('cliente_servicios', $clienteServicio->toArray());
        $this->set('_serialize', ['cliente']);

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_listasociados');
        }
    }

     public function viewedodecuenta($id = null, $mes, $año)
    {
        $this->loadModel('ClientesServicios');
        $ultimo_dia = cal_days_in_month(CAL_GREGORIAN, $mes, $año);

        $fechaInicial = $año.'-'.$mes.'-01 00:00:00';
        $fechaFinal = $año.'-'.$mes.'-'.$ultimo_dia.' 23:59:59';

        //die(debug($fechaFinal));
        $clienteServicio = $this->ClientesServicios->find('all', ['conditions'=> 
            ["ClientesServicios.cliente_id = $id", 
            'AND' => ['ClientesServicios.fecha >='=>$fechaInicial,'ClientesServicios.fecha <='=>$fechaFinal]], 
            'order' => ["ClientesServicios.id DESC"],
            'contain'=>['Facturas']]);
        $movimientos = $clienteServicio;
        
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Paises', 'Estados', 'Municipios', 'Monedas']
        ]);

             //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        //App::import('Vendor', 'html2pdf', array('file' => 'html2pdf'.DS.'html2pdf.class.php'));
        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');

        $mes_espanol=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
        //$body = $this->_pdf_content(compact('factura'), 'Facturas/prefactura');
        $periodo = "1ero de ".$mes_espanol[$mes-1]." al $ultimo_dia de ".$mes_espanol[$mes-1]." de $año";
        //die(debug($movimientos));
        $body = $this->cell('Pdf::edocuenta', ['movimientos' => $movimientos, 'cliente' => $cliente, 'periodo' => $periodo]);
        $html2pdf->WriteHTML($body);

        $html2pdf->Output('movimientos.pdf');
        exit;
    }
    /**
     * View method
     *
     * @param 
     * @return array of clientes_servicios
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function filtroClientesServiciosMensual()
     {
        if ($this->request->is('ajax')) { 
            $this->loadModel('ClientesServicios');
            //$this->ClientesServicios->recursive= -1;

            $cliente_id = $this->request->data['cliente_id'];
            $mes = date('m', strtotime('1-'.$this->request->data['mes']));
            $año = date('Y', strtotime('1-'.$this->request->data['mes']));
            $ultimo_dia = cal_days_in_month(CAL_GREGORIAN, $mes, $año);

            //$fechaInicial = '01/'.$mes.'/'.$año;
            //$fechaFinal = $ultimo_dia.'/'.$mes.'/'.$año;
            $fechaInicial = $año.'-'.$mes.'-01';
            $fechaFinal = $año.'-'.$mes.'-'.$ultimo_dia." 23:59:59";

            $serviciosMensuales = $this->ClientesServicios->find('all',array('order'=>'ClientesServicios.fecha DESC' ,
                                                                                'contain' => ['Facturas'],
                                                                                'conditions'=>array('AND' => array('ClientesServicios.cliente_id'=>$cliente_id, 
                                                                                                                    array('ClientesServicios.fecha >='=>$fechaInicial,
                                                                                                                            'ClientesServicios.fecha <='=>$fechaFinal)
                                                                                                                    )
                                                                                                    )
                                                                            )
                                                                );

            //die(debug($serviciosMensuales->toArray()));


            die(json_encode($serviciosMensuales->toArray())); 
        }
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //debug($this->request->data);
        //die();
        $cliente = $this->Clientes->newEntity();
        if ($this->request->is('post')) {
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            $cliente->saldo = $this->request->data['saldo'];
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('The cliente has been saved.'));

                //Genero los usuarios desde los contactos.
                $this->loadModel('Usuarios');
                $date = Time::now();
                
                $group=$this->request->data['tipo_cliente_id'];

                $name=explode(' ',$this->request->data['primario_persona_de_contacto']);
                $first=$name[0];
                unset($name[0]);
                $last=implode(' ',$name);

                $email=$this->request->data['primario_correo_electronico'];

                $usr_data = [
                    'user_group_id'=> $group,
                    'first_name'=> $first,
                    'last_name'=>$last,
                    'username'=> $email,
                    'email'=>$email,
                    'created'=> $date,
                    'modified'=> $date,
                    'active'=>1,
                    'created_by'=>1,
                    'last_login'=>'',
                    'email_verified'=>1,
                    'modified_by'=>1,
                    'cliente_id' => $cliente->id,
                    'centro_id' => $this->request->data('centro_id'),
                ];

                $usuario = $this->Usuarios->newEntity();
                $usuario = $this->Usuarios->patchEntity($usuario, $usr_data);
                
                $this->Usuarios->save($usuario);

                //Segundo usuario
                //Valido si viene vacío
                if( ($this->request->data['secundario_persona_de_contacto'] !="" && !is_null($this->request->data['secundario_persona_de_contacto']))
                    ||
                    ($this->request->data['secundario_correo_electronico'] !="" && !is_null($this->request->data['secundario_correo_electronico']))
                )
                {

                    $name=explode(' ',$this->request->data['secundario_persona_de_contacto']);
                    $first=$name[0];
                    unset($name[0]);
                    $last=implode(' ',$name);

                    $email=$this->request->data['secundario_correo_electronico'];

                    $usr_data=[
                        'user_group_id'=> $group,
                        'first_name'=> $first,
                        'last_name'=>$last,
                        'username'=> $email,
                        'email'=>$email,
                        //'created'=> $date,
                        //'modified'=> $date,
                        'active'=>1,
                        'created_by'=>1,
                        'last_login'=>'',
                        'email_verified'=>1,
                        'modified_by'=>1,
                        'cliente_id' => $cliente->id,
                        'centro_id' => $this->request->data('centro_id'),
                    ];

                    $usuario = $this->Usuarios->newEntity();
                    $usuario = $this->Usuarios->patchEntity($usuario, $usr_data);
                    $this->Usuarios->save($usuario);
                }

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cliente could not be saved. Please, try again.'));
            }
        }
        $paises = $this->Clientes->Paises->find('list');
        $estados = $this->Clientes->Estados->find('list');
        $tipoclientes = $this->Clientes->TipoClientes->find('list');

        $this->loadModel('Formasdepagos');

        $formasdepagos = $this->Formasdepagos->find('list');

        //$estados->find('list')->where(['id' => 1])->toArray();
        //$id_estados = $estados[0];
        //$municipios = $this->Clientes->Municipios->find('list', ['limit' => 200]);
        $monedas = $this->Clientes->Monedas->find('list', ['limit' => 200]);
        //$servicios = $this->Clientes->Servicios->find('list', ['limit' => 200]);
        $centros = $this->Clientes->Centros->find('list', ['conditions' => ['deleted' => false] ]);
        $this->set(compact('cliente', 'paises', 'estados', 'tipoclientes', 'monedas', 'centros' ,'formasdepagos'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

    public function edit($id = null)
    {
        //modelos
        $this->loadModel('Chips');
        $this->loadModel('Extensiones');
        $this->loadModel('TarjetasAcceso');
        $this->loadModel('Dids');

        //submodelos
        $this->loadModel('Contactos');
        $this->loadModel('ContactosChips');
        $this->loadModel('ContactosExtensiones');
        $this->loadModel('ContactosTarjetasAcceso');
        $this->loadModel('ContactosDids');



        //die(debug(locale_get_default()));
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Servicios']
        ]);

        //Verificamos que si los contactos de este cliente tengan recursos asignados
        $mycontacts = $this->Contactos->find('all', ['conditions' => ['cliente_id' => $id, 'deleted' => false]])->toArray();
        //debug($mycontacts);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            $cliente->saldo = $this->request->data['saldo'];

            if($this->request->data['activo'] == FALSE && !empty($mycontacts)){
                $modelos    = [ 'chip_id' => 'Chips', 'extension_id' => 'Extensiones', 'tarjetas_acceso_id' => 'TarjetasAcceso', 'did_id' => 'Dids'];
                $submodelos = [ 'chip_id' => 'ContactosChips', 'extension_id' => 'ContactosExtensiones', 'tarjetas_acceso_id' => 'ContactosTarjetasAcceso', 'did_id' => 'ContactosDids'];
                
                foreach ($mycontacts as $row) {
                    foreach ($submodelos as $key => $value) {
                        $valor = $this->$value->find('all', ['conditions' => ['contacto_id' => $row['id']]])->toArray();
                        foreach ($valor as $x) {
                            $model = $this->$modelos[$key]->get($x[$key]); 
                            $model->assigned = FALSE;
                            $this->$modelos[$key]->save($model);
                        }
                        $this->$value->deleteAll(['contacto_id' => $row['id']]);
                    }   
                } 
            }
            //debug($mycontacts);die();
            if ($this->Clientes->save($cliente)) {
                $this->Flash->success(__('The cliente has been saved.'));
                return $this->redirect(['action' => 'view',$id]);
            } else {
                $this->Flash->error(__('The cliente could not be saved. Please, try again.'));
            }
        }
        $paises = $this->Clientes->Paises->find('list');
        $estados = $this->Clientes->Estados->find('list');
        $municipios = $this->Clientes->Municipios->find('list', [ 'conditions'=> ["Municipios.estado_id = $cliente->estado_id"]]);
        $tipoclientes = $this->Clientes->TipoClientes->find('list');
        $this->loadModel('Formasdepagos');

        $formasdepagos = $this->Formasdepagos->find('list');

        $monedas = $this->Clientes->Monedas->find('list');
        //$servicios = $this->Clientes->Servicios->find('list', ['limit' => 200]);
        $centros = $this->Clientes->Centros->find('list', ['conditions' => ['deleted' => false] ]);
        $this->set(compact('cliente', 'paises', 'estados', 'municipios', 'monedas', 'centros','tipoclientes','formasdepagos'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Suspender method
     *
     * @param string|null $id Cliente id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function suspender($id = null,$activo=0)
    {
        // $this->request->allowMethod(['post', 'delete']);
        $cliente = $this->Clientes->get($id);
        $clientes = TableRegistry::get('Clientes');
        $query = $clientes->query();
        $result = $query->update()
        ->set(['activo' => $activo])
        ->where(['id' => $id])
        ->execute();

        if ($result) {
            if($activo){
                $this->Flash->success(__('El cliente se ha sido activado.'));
            }else{
                $this->desasociar($id);
                $this->Flash->success(__('El cliente se ha sido suspendido.'));
            }
        } else {
            $this->Flash->error(__('El cliente no pudo modificarse, por favor intente de nuevo.'));
        }
        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->loadModel('ClientesAsociados');
        $this->loadModel('Contactos');

        $this->request->allowMethod(['post', 'delete']);
        $cliente = $this->Clientes->get($id);
        
        $this->desasociar($id);

        $mycontacts = $this->Contactos->find('all', ['conditions' => ['cliente_id' => $id, 'deleted' => false]])->toArray();

        $asociados = $this->ClientesAsociados->find('all', ['conditions' => ['cliente_id' => $id, 'deleted' => false]])->toArray();
        
        try {

            if ($this->Clientes->delete($cliente)) {
            
            if(isset($mycontacts)){
                foreach ($mycontacts as $value) {

                    $contacto = $this->Contactos->get($value['id']); 
                    $contacto->deleted = TRUE;
                    $this->Contactos->save($contacto);
                }
            }
            
            if(isset($asociados)){
                foreach ($asociados as $value) {
                    $asociado = $this->ClientesAsociados->get($value['id']); 
                    $asociado->deleted = TRUE;
                    $this->ClientesAsociados->save($asociado);
                }
            }

            $this->Flash->success(__('Se ha eliminado el cliente.'));

            } else {
                $this->Flash->error($this->Clientes->error);
            }
            return $this->redirect(['action' => 'index']);

        } catch (\Exception $e) {
            $this->Flash->error($e->getMessage());
            return $this->redirect(['action' => 'index']);
        }
    }
    
    public function delete_servicio($cliente_servicio_id=null,$id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('ClientesServicios');

        $cs = $this->ClientesServicios->get($cliente_servicio_id);
        
        if(!is_null($cs['factura_id'])){
            $this->Flash->error(__('El servicio no pudo ser eliminado, intente de nuevo'));
        }else{
            if ($this->ClientesServicios->delete($cs)) {
                $this->Flash->success(__('El servicio fue eliminado'));
            } else {
                $this->Flash->error(__('El servicio no pudo ser eliminado, intente de nuevo'));
            }
        }
        return $this->redirect(['action' => 'view', $id]);
    }

    public function addAsociados(){
        $this->loadModel('ClientesAsociados');

        $asociado = $this->ClientesAsociados->newEntity();
        if ($this->request->is('post')) {
            $clienteAsociado = $this->ClientesAsociados->patchEntity($asociado, $this->request->data);
            if ($this->ClientesAsociados->save($clienteAsociado)) {
                $this->Flash->success(__('El contacto asociado se ha guardado.'));
                return $this->redirect(['action' => 'view', $this->request->data['cliente_id'], 'Asociados']);
            }else{
                $this->Flash->error(__('El contacto asociado no se pudo guardar. Por favor, intente nuevamente.'));
            }
        }
    }

    public function editAsociados($id = null){

        $this->loadModel('ClientesAsociados');

        $cliente = $this->ClientesAsociados->get($id);
        
        if ($this->request->is(['patch', 'post', 'put'])) {

            $cliente['nombre'] = $this->request->data['nombre-asociado'];
            $cliente['comentario'] = $this->request->data['comentario-asociado'];
    
            $clienteAsociado = $this->ClientesAsociados->patchEntity($cliente, $this->request->data);
            if ($this->ClientesAsociados->save($clienteAsociado)) {
                $this->Flash->success(__('El contacto asociado se ha guardado.'));
            } else {
                $this->Flash->error(__('El contacto asociado no se pudo guardar. Por favor, intente nuevamente.'));
            }
        }
        $this->set(compact('cliente'));
        $this->set('_serialize', ['cliente']);

        return $this->redirect(['action' => 'view',$cliente->cliente_id, 'Asociados']);
    }

    public function deleteAsociados($id = null)
    {
        $this->loadModel('ClientesAsociados');

        $this->request->allowMethod(['post', 'delete']);
        $cliente = $this->ClientesAsociados->get($id);
        //debug($cliente);die();
        $cliente->deleted = true;
        if ($this->ClientesAsociados->save($cliente)) {
            $this->Flash->success(__('El contacto asociado ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El cliente asociado no pudo ser eliminado, por favor intente de nuevo.'));
        }
        return $this->redirect(['action' => 'view', $cliente->cliente_id, 'Asociados']);
    }
    public function search(){

        $this->loadModel('ClientesAsociados');
        
        $filtro = $this->request->data['search'];

        $status = ['0' => 'Suspendido', '1' => 'Activo'];

        $datos1 = $this->ClientesAsociados->find('all')->contain(['Clientes', 'Clientes.Centros'])->where(['ClientesAsociados.nombre LIKE' => "%".$filtro."%", 'ClientesAsociados.deleted' => false])->toArray();

        $this->loadModel('Contactos');
        $datos2 = $this->Contactos->find('all')->contain(['Clientes', 'Clientes.Centros'])->where(['Contactos.deleted' => false ])->where(

            function ($exp, $query) use($filtro) {

                $concat = $query->func()->concat([ 'Contactos.nombre' => 'literal' , 'Contactos.apellido_paterno' => 'literal' ]);


                $li = $exp->like($concat, "%".str_replace(' ', "", $filtro)."%"  );
                
                $concat2 = $query->func()->concat([ 'Contactos.nombre' => 'literal' , 'Contactos.apellido_paterno' => 'literal','Contactos.apellido_materno' => 'literal' ]);

                $li2 = $exp->like($concat, "%".str_replace(' ', "", $filtro)."%"  );

                $or = $exp->or_([
                        'Contactos.nombre LIKE' => "%".$filtro."%",
                        'Contactos.apellido_paterno LIKE' => "%".$filtro."%",
                        'Contactos.apellido_materno LIKE' => "%".$filtro."%",
                        $li,
                        $li2
                ]);



                return $or;


        }
        )->toArray();

        $this->loadModel('Clientes');
        $datos3 = $this->Clientes->find('all')->contain('Centros')->where(['OR' => [['Clientes.nombre LIKE' => "%".$filtro."%"],
                                                                ['Clientes.primario_persona_de_contacto LIKE' => "%".$filtro."%"],
                                                                ['Clientes.secundario_persona_de_contacto LIKE' => "%".$filtro."%"]]])->toArray();

        $datos = [];

        //debug($datos2);

        foreach ($datos1 as $value) {
            
            if(!is_null($value['cliente'])){
                $datos[]=['id' =>$value['cliente']->id ,'nombre'=>$value['nombre'],'cliente'=>$value['cliente']['nombre'], 'centro' => $value['cliente']['centro']['nombre'],'comentario'=>$value['comentario'],'tipo'=>'Asociado', 'primer_contacto' => $value['primario_persona_de_contacto'], 'segundo_contacto' => $value['secundario_persona_de_contacto'], 'status' => $status[$value['cliente']['activo']] ];
            }
        }

        foreach ($datos2 as $value) {
            if(!is_null($value['cliente'])){
                $datos[]=['id' => $value['cliente']->id, 'nombre'=> $value['nombre'].' '.$value['apellido_paterno'].' '.$value['apellido_materno'], 'cliente'=>$value['cliente']['nombre'], 'centro' => $value['cliente']['centro']['nombre'],'comentario'=>'','tipo'=>'Contacto', 'primer_contacto' => $value['primario_persona_de_contacto'], 'segundo_contacto' => $value['secundario_persona_de_contacto'], 'status' => $status[$value['cliente']['activo']] ];
            }
        }

        foreach ($datos3 as $value) {
            if (!is_null($value['nombre'])) {
                $datos[]=['id' =>$value['id'], 'nombre'=>$value['nombre'],'cliente'=>$value['nombre'], 'centro' => $value['centro']['nombre'],'comentario'=>'','tipo'=>'Cliente', 'primer_contacto' => $value['primario_persona_de_contacto'], 'segundo_contacto' => $value['secundario_persona_de_contacto'], 'status' => $status[$value['activo']]];
            }
        }
       
        $this->set(compact('datos', 'filtro'));
        
    }

    public function getContactos($id = null){
        $this->loadModel('Contactos');
        $this->paginate = ['contain' => ['Clientes', 'Extensiones','TarjetasAcceso'],
            'conditions' => ['Contactos.cliente_id' => $id, 'Contactos.deleted' => false], 
            'limit' => 10,
            'order' => [
            'Contactos.id DESC'
            ]
        ];

        $this->Search->applySearch();

        $contact = $this->paginate($this->Contactos)->toArray();

        return $contact;
    }

    public function desasociar($id=null){
        if (empty($id)) {
            return true;
        }

        //modelos
        $this->loadModel('Chips');
        $this->loadModel('Extensiones');
        $this->loadModel('TarjetasAcceso');
        $this->loadModel('Dids');

        //submodelos
        $this->loadModel('Contactos');
        $this->loadModel('ContactosChips');
        $this->loadModel('ContactosExtensiones');
        $this->loadModel('ContactosTarjetasAcceso');
        $this->loadModel('ContactosDids');


        //Verificamos que si los contactos de este cliente tengan recursos asignados
        $mycontacts = $this->Contactos->find('all', ['conditions' => ['cliente_id' => $id, 'deleted' => false]])->toArray();

        $modelos    = [ 'chip_id'               =>  'Chips',
                        'extension_id'          =>  'Extensiones',
                        'tarjetas_acceso_id'    =>  'TarjetasAcceso',
                        'did_id'                =>  'Dids'];
        
        $submodelos = [ 'chip_id'               => 'ContactosChips',
                        'extension_id'          => 'ContactosExtensiones',
                        'tarjetas_acceso_id'    => 'ContactosTarjetasAcceso',
                        'did_id'                => 'ContactosDids'];
                
        foreach ($mycontacts as $row) {
            foreach ($submodelos as $key => $value) {
                $valor = $this->$value->find('all', ['conditions' => ['contacto_id' => $row['id']]])->toArray();
                foreach ($valor as $x) {
                    $m=$modelos[$key];
                    $i=$x[$key];
                    $model = $this->$m->get($i); 
                    $model->assigned = FALSE;
                    $this->$modelos[$key]->save($model);
                }
                $this->$value->deleteAll(['contacto_id' => $row['id']]);
            }   
        } 

        return true;


    }

    public function sendNotificationsMail($cliente_id = null){
       
        $email = new Email('default');

        $emails = TableRegistry::get('Users')
                    ->find('list', ['valueField' => 'email'])
                    ->where(['Users.cliente_id' => $cliente_id ])
                    ->toArray();

        if( $emails > 0 ){
            
            foreach ($emails as $key => $row) {
                $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                    ->emailFormat('html')
                    ->viewVars(['titulo'=>'Nuevo sistema de reservaciones', 'email' => $row])
                    ->template('envianotificaciones')
                    ->to($row)
                    ->subject('Nuevo sistema de reservaciones')
                    ->send();
            }

            $query = TableRegistry::get('Clientes')
                    ->query()
                    ->update()
                    ->set(['sendEmail' => TRUE])
                    ->where(['id' => $cliente_id ])
                    ->execute();

            $this->Flash->success(__('La invitación se envio correctamente.'));

        }else{
            $this->Flash->error(__('No tiene usuarios para enviar correo.'));
        }

        return $this->redirect($this->referer());
    }
}

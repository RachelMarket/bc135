<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CotizacionesOficinas Controller
 *
 * @property \App\Model\Table\CotizacionesOficinasTable $CotizacionesOficinas
 *
 * @method \App\Model\Entity\CotizacionesOficina[] paginate($object = null, array $settings = [])
 */
class CotizacionesOficinasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cotizacion', 'Oficina']
        ];
        $cotizacionesOficinas = $this->paginate($this->CotizacionesOficinas);

        $this->set(compact('cotizacionesOficinas'));
        $this->set('_serialize', ['cotizacionesOficinas']);
    }

    /**
     * View method
     *
     * @param string|null $id Cotizaciones Oficina id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cotizacionesOficina = $this->CotizacionesOficinas->get($id, [
            'contain' => ['Cotizacion', 'Oficina']
        ]);

        $this->set('cotizacionesOficina', $cotizacionesOficina);
        $this->set('_serialize', ['cotizacionesOficina']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cotizacionesOficina = $this->CotizacionesOficinas->newEntity();
        if ($this->request->is('post')) {
            $cotizacionesOficina = $this->CotizacionesOficinas->patchEntity($cotizacionesOficina, $this->request->getData());
            if ($this->CotizacionesOficinas->save($cotizacionesOficina)) {
                $this->Flash->success(__('The cotizaciones oficina has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cotizaciones oficina could not be saved. Please, try again.'));
        }
        $cotizacion = $this->CotizacionesOficinas->Cotizacion->find('list', ['limit' => 200]);
        $oficina = $this->CotizacionesOficinas->Oficina->find('list', ['limit' => 200]);
        $this->set(compact('cotizacionesOficina', 'cotizacion', 'oficina'));
        $this->set('_serialize', ['cotizacionesOficina']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cotizaciones Oficina id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cotizacionesOficina = $this->CotizacionesOficinas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cotizacionesOficina = $this->CotizacionesOficinas->patchEntity($cotizacionesOficina, $this->request->getData());
            if ($this->CotizacionesOficinas->save($cotizacionesOficina)) {
                $this->Flash->success(__('The cotizaciones oficina has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cotizaciones oficina could not be saved. Please, try again.'));
        }
        $cotizacion = $this->CotizacionesOficinas->Cotizacion->find('list', ['limit' => 200]);
        $oficina = $this->CotizacionesOficinas->Oficina->find('list', ['limit' => 200]);
        $this->set(compact('cotizacionesOficina', 'cotizacion', 'oficina'));
        $this->set('_serialize', ['cotizacionesOficina']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cotizaciones Oficina id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cotizacionesOficina = $this->CotizacionesOficinas->get($id);
        if ($this->CotizacionesOficinas->delete($cotizacionesOficina)) {
            $this->Flash->success(__('The cotizaciones oficina has been deleted.'));
        } else {
            $this->Flash->error(__('The cotizaciones oficina could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

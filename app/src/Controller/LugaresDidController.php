<?php
namespace App\Controller;

use App\Controller\AppController;

use Usermgmt\Controller\UsermgmtAppController;
use Cake\View\CellTrait; 
use Cake\Mailer\Email;

/**
 * LugaresDid Controller
 *
 * @property \App\Model\Table\LugaresDidTable $LugaresDid
 */
class LugaresDidController extends AppController
{
	use CellTrait;

	 /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '10'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'LugaresDid'=>[
                'LugaresDid'=>[
                    'type'=>'text',
                    'label'=>'Buscar',
                    'condition'=>'multiple',
                    'searchFields'=>['LugaresDid.lugar'],
                    'inputOptions'=>['style'=>'width:300px;']
                ]
            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = ['limit'=>10, 'order'=>['LugaresDid.lugar']];
        $this->Search->applySearch(['LugaresDid.deleted' => false]);
        $lugaresDid = $this->paginate($this->LugaresDid)->toArray();
        $this->set(compact('lugaresDid'));

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_lugares_did');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Lugares Did id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('LugaresDid');
        $lugaresDid = $this->LugaresDid->get($id, [
            'contain' => []
        ]);
        $this->set('lugaresDid', $lugaresDid);
        $this->set('_serialize', ['lugaresDid']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('LugaresDid');
        
        $lugaresDid = $this->LugaresDid->newEntity();
        
        if ($this->request->is('post')) {

            $lugaresDid = $this->LugaresDid->patchEntity($lugaresDid, $this->request->data);
                    
            if ($this->LugaresDid->save($lugaresDid)) {
                $this->Flash->success(__('The type did has been saved.'));
                return $this->redirect(['action' => 'index']);
            
            }else{
                
                $errors = $lugaresDid->errors();
                
                if( isset($errors['lugar']) && isset($errors['lugar']['_empty']) ){
                    
                    $this->Flash->error(__('Campo requerido, agrega un tipo.'));

                }else if( isset($errors['lugar']) && isset($errors['lugar']['_isUnique']) ){
                    
                    $this->Flash->error(__('Ya hay un tipo con este nombre.'));

                }else{
                    throw new \Exception(__('The type did could not be saved. Please, try again.')); 
                }
                return $this->redirect(['action' => 'index']);
            }
                
        }
        $this->set(compact('lugaresDid'));
        $this->set('_serialize', ['lugaresDid']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lugares Did id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('LugaresDid');
        $lugaresDid = $this->LugaresDid->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $lugaresDid = $this->LugaresDid->patchEntity($lugaresDid, $this->request->data);
      
            if ($this->LugaresDid->save($lugaresDid)) {
                $this->Flash->success(__('The type did has been saved.'));
                return $this->redirect(['action' => 'index']);
            }else{

                $errors = $lugaresDid->errors();
                
                if(isset($errors['lugar']) && isset($errors['lugar']['_empty'])){
                    
                    $this->Flash->error(__('Campo requerido, agrega un tipo.'));
                    
                }else if( isset($errors['lugar']) && isset($errors['lugar']['_isUnique']) ){
                    
                    $this->Flash->error(__('Ya hay un tipo con este nombre.'));

                }else{
                    throw new \Exception(__('The type did could not be saved. Please, try again.'));
                }
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('lugaresDid'));
        $this->set('_serialize', ['lugaresDid']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lugares Did id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->loadModel('LugaresDid');
        $this->request->allowMethod(['post', 'delete']);
        $lugaresDid = $this->LugaresDid->get($id);
        $lugaresDid->deleted = true;
        if ($this->LugaresDid->save($lugaresDid)) {
            $this->Flash->success(__('The type did has been deleted.'));
        } else {
            $this->Flash->error(__('The type did could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

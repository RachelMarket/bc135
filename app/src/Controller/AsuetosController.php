<?php
namespace App\Controller;

use App\Controller\AppController;


/**
 * Asuetos Controller
 *
 * @property \App\Model\Table\AsuetosTable $Asuetos
 */
class AsuetosController extends AppController
{
	

	 /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '100'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Asuetos'=>[
                'Asuetos'=>[
                    'type'=>'text',
                    'label'=>'Buscar',
                    'tagline'=>'<br>Busca por ID',
                    'condition'=>'multiple',
                    'searchFields'=>['Asuetos.id'],
                    'inputOptions'=>['style'=>'width:300px;']
                ]
            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        
        $asueto = $this->Asuetos->newEntity();

       $this->set(compact('asueto'));
    
        if($this->request->is('ajax')) {
           
                

            $options['conditions'][] = array('Asuetos.dia >=' => $this->request->data['start']);

            $options['conditions'][] = array('Asuetos.dia <=' => $this->request->data['end']);

            $events = $this->Asuetos->find('event',$options);


            echo json_encode($events);

            die();
        }
        
    }

    /**
     * View method
     *
     * @param string|null $id Asueto id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $asueto = $this->Asuetos->get($id, [
            'contain' => ['Useres']
        ]);
        $this->set('asueto', $asueto);
        $this->set('_serialize', ['asueto']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $asueto = $this->Asuetos->newEntity();
        if ($this->request->is('post')) {
            $asueto = $this->Asuetos->patchEntity($asueto, $this->request->data);
            $asueto->user_id =  $this->UserAuth->getUserId();
            $errors = $asueto->errors();
            if($this->request->is('ajax')) {
                if(empty($errors)) {
                    $response = ['error'=>0, 'message'=>'success'];
                } else {
                    $response = ['error'=>1, 'message'=>'failure'];
                    $response['data']['Asuetos'] = $errors;
                }
                echo json_encode($response);exit;
            } else {

                if ($this->Asuetos->save($asueto)) {
                    $this->Flash->success(__('Día Inhábil agregado'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The asueto could not be saved. Please, try again.'));
                    return $this->redirect(['action' => 'index']);
                }
            }
        }
        $useres = $this->Asuetos->Users->find('list', ['limit' => 200]);
        $this->set(compact('asueto', 'useres'));
        $this->set('_serialize', ['asueto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Asueto id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $asueto = $this->Asuetos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $asueto = $this->Asuetos->patchEntity($asueto, $this->request->data);
            if ($this->Asuetos->save($asueto)) {
                $this->Flash->success(__('The asueto has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The asueto could not be saved. Please, try again.'));
            }
        }
        $useres = $this->Asuetos->Useres->find('list', ['limit' => 200]);
        $this->set(compact('asueto', 'useres'));
        $this->set('_serialize', ['asueto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Asueto id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $asueto = $this->Asuetos->get($id);
        if ($this->Asuetos->delete($asueto)) {

            if($this->request->is('ajax')) {

                die('ok');

            }else{
                $this->Flash->success(__('The asueto has been deleted.'));
            }
        } else {

            if($this->request->is('ajax')) {
                die('ko');
            }else
            $this->Flash->error(__('The asueto could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

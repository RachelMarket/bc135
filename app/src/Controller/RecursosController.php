<?php
namespace App\Controller;

use App\Controller\AppController;

use Usermgmt\Controller\UsermgmtAppController;
use Cake\View\CellTrait; 
use Cake\Mailer\Email;
use Cake\I18n\Time;

/**
 * Recursos Controller
 *
 * @property \App\Model\Table\RecursosTable $Recursos
 */
class RecursosController extends AppController
{
	use CellTrait;

	 /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '100'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Recursos'=>[
                'Recursos.centro_id'=>[
                    'type'=>'select',
                    'label'=>'Centro',
                    'model'=>'Centros',
                    'selector'=>'getCentros'
                    
                ],
                'Recursos'=>[
                    'position'=>'busqueda',
                    'type'=>'text',
                    'label'=>'Nombre',
                    'condition'=>'multiple',
                    'searchFields'=>['Recursos.id'],
                    'inputOptions'=>['style'=>'width:300px;']
                ],
                'Recursos.tipo_id'=>[
                    'type'=>'select',
                    'label'=>'Tipo',
                    'model'=>'RecursosTipos',
                    'selector'=>'getTiposRecursos'
                    
                ],
            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
                'sortWhitelist' => [ 
                    'Recursos.id',
                    'Centros.nombre',
                    'Recursos.name',
                    'RecursosTipos.name',
                    'Recursos.capacidad',
                    'Recursos.hora_inicio'
                ],
                'limit'=>10, 
                'contain' => ['Centros', 'RecursosTipos'], 
                'order'=>['Recursos.id'=>'DESC']];
        $this->Search->applySearch(['Recursos.deleted' => false]);
        $recursos = $this->paginate($this->Recursos)->toArray();
        
        $this->set(compact('recursos'));

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
             $this->set('modo_calendario',false);
            $this->render('/Element/all_recursos' );
        }
    }

    /**
     * View method
     *
     * @param string|null $id Recurso id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $recurso = $this->Recursos->get($id, [
            'contain' => ['Centros', 'RecursosTipos']
        ]);
        $this->set('recurso', $recurso);
        $this->set('_serialize', ['recurso']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $recurso = $this->Recursos->newEntity();
        if ($this->request->is('post')) {

            $inicio = explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['inicio']));

            $beginDate = $inicio[0] . date(' H:i:s',strtotime($inicio[1].' '.$inicio[2]));

            $fin = explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['fin']));

            $endDate = $fin[0] . date(' H:i:s',strtotime($fin[1].' '.$fin[2]));

            $recurso = $this->Recursos->patchEntity($recurso, $this->request->data);

            $recurso->hora_inicio = $beginDate;
            $recurso->hora_fin = $endDate;

            if ($this->Recursos->save($recurso)) {
                $this->Flash->success(__('The resource has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The resource could not be saved. Please, try again.'));
            }
        }
        $centros = $this->Recursos->Centros->find('list', ['limit' => 200, 'conditions' => ['Centros.deleted' => FALSE]]);
        $tipos = $this->Recursos->RecursosTipos->find('list', ['limit' => 200]);
        $this->set(compact('recurso', 'centros', 'tipos'));
        $this->set('_serialize', ['recurso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Recurso id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $recurso = $this->Recursos->get($id, [
            'contain' => ['RecursosFotos']
        ]);
        //debug($recurso);die();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $inicio = explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['inicio']));

            $beginDate = $inicio[0] . date(' H:i:s',strtotime($inicio[1].' '.$inicio[2]));

            $fin = explode(' ',preg_replace('~(\d{2})/(\d{2})/(\d{2,4})~', '$3-$2-$1', $this->request->data['fin']));

            $endDate = $fin[0] . date(' H:i:s',strtotime($fin[1].' '.$fin[2]));

            $recurso = $this->Recursos->patchEntity($recurso, $this->request->data);

            $recurso->hora_inicio = $beginDate;
            $recurso->hora_fin = $endDate;
            
            if ($this->Recursos->save($recurso)) {

                if($this->request->data['file']['error'] == 0 &&  $this->request->data['file']['size'] > 0){

                    $ext = explode(".", $this->request->data['file']['name']);
                    $nombre =  $id.'-'.$this->request->data['file']['name']; 
                    $destino = WWW_ROOT.'files/fotos/'.$nombre;

                    $tmp = $this->request->data['file']['tmp_name'];

                    if(move_uploaded_file($tmp, $destino)){

                        $this->loadModel('RecursosFotos');
                        $recursosFotos = $this->RecursosFotos->newEntity();
                        $recursosFotos->recurso_id = $id;
                        $recursosFotos->user_id = $this->UserAuth->getUserId();
                        $recursosFotos->name = $nombre;
                        $recursosFotos->name_real = $this->request->data['file']['name'];
                        $recursosFotos->size = $this->request->data['file']['size'];
                        $this->RecursosFotos->save($recursosFotos);
                        die(json_encode($recursosFotos->id));
                    }
                }

                $this->Flash->success(__('The resource has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The resource could not be saved. Please, try again.'));
            }
        }
        
        $centros = $this->Recursos->Centros->find('list', ['limit' => 200]);
        $tipos = $this->Recursos->RecursosTipos->find('list', ['limit' => 200]);
        $this->set(compact('recurso', 'centros', 'tipos'));
        $this->set('_serialize', ['recurso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Recurso id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->loadModel('Reservaciones');
        $this->request->allowMethod(['post', 'delete']);
        $recurso = $this->Recursos->get($id);
        $recurso->deleted = true;

        //Verificamos si tienes reservaciones porterioes 
        $reservaciones = $this->Reservaciones->find('all', ['conditions' => ['recurso_id' => $id]])->toArray();
        
        $hoy  = date('Y-m-d G:i:s');

        $resultado = ['cantidad' => 0, 'flag' => FALSE];
        
        if (!empty($reservaciones)) {
           foreach ($reservaciones as $row) {
                if ($hoy <= date_format($row['fecha_inicio'],'Y-m-d H:i:s')) {
                    $resultado['flag'] =  TRUE; 
                    $resultado['cantidad'] += 1;
                } 
            } 
        }
         
        if($resultado['flag'] == FALSE){
            if ($this->Recursos->save($recurso)) {
                $this->Flash->success(__('The recurso has been deleted.'));
            } else {
                $this->Flash->error(__('The recurso could not be deleted. Please, try again.'));
            }
        }else{
            $this->Flash->error(__('El registro no puede ser borrado porque tiene '.$resultado['cantidad'].' reservaciones vigentes.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function removeFile(){

        if($this->request->is('post')){

            $this->loadModel('RecursosFotos');
            $recursosFotos = $this->RecursosFotos->get($this->request->data['foto_id']);
            $respuesta = [];
            if ($this->RecursosFotos->delete($recursosFotos)) {
                $respuesta['resultado'] = 200;
                $respuesta['msg'] = 'La foto ha sido borrado.';
            } else {
                $respuesta['resultado'] = 404;
                $respuesta['msg'] = 'La foto no se pudo borrar. Por favor, intente nuevamente.';
            }
            die(json_encode($respuesta));
        } 
    }

    public function calendario(){

        
    }
}

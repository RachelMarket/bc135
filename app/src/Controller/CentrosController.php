<?php
namespace App\Controller;

use App\Controller\AppController;

use Usermgmt\Controller\UsermgmtAppController;
use Cake\View\CellTrait; 
use Cake\Mailer\Email;

/**
 * Centros Controller
 *
 * @property \App\Model\Table\CentrosTable $Centros
 */
class CentrosController extends AppController
{
	use CellTrait;

	 /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '100'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Centros'=>[
                'Centros'=>[
                    'type'=>'text',
                    'label'=>'Buscar',
                    'tagline'=>'<br>Busca por nombre',
                    'condition'=>'multiple',
                    'searchFields'=>['Centros.nombre'],
                    'inputOptions'=>['style'=>'width:300px;']
                ]
            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = ['limit'=>10, 'order'=>['Centros.id'=>'DESC']];
        $this->Search->applySearch(['Centros.deleted' => FALSE]);
        $centros = $this->paginate($this->Centros)->toArray();
        $this->set(compact('centros'));

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_centros');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Centro id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $centro = $this->Centros->get($id, [
            'contain' => ['Clientes']
        ]);
        $this->set('centro', $centro);
        $this->set('_serialize', ['centro']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $centro = $this->Centros->newEntity();
        
        if ($this->request->is('post')) {

            try {

                $centro = $this->Centros->patchEntity($centro, $this->request->data);
                $centro['flag'] = false;
                if ($this->Centros->save($centro)) {
                    $this->Flash->success(__('The centro has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }else{

                    $this->Flash->error(__('The centro could not be saved. Please, try again.'));
                }

            }catch (\Exception $e) {
                $this->Flash->error($e->getMessage());
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('centro'));
        $this->set('_serialize', ['centro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Centro id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $centro = $this->Centros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            try {
                $centro = $this->Centros->patchEntity($centro, $this->request->data);
                $centro['flag'] = false;
                if ($this->Centros->save($centro)) {
                    $this->Flash->success(__('The centro has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }else{
                    
                    $this->Flash->error(__('The centro could not be saved. Please, try again.'));
                }
            }catch (\Exception $e) {
                $this->Flash->error($e->getMessage());
                return $this->redirect(['action' => 'index']);
            }
            
        }
        $this->set(compact('centro'));
        $this->set('_serialize', ['centro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Centro id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $centro = $this->Centros->get($id);
        $centro->deleted = TRUE;
        $centro['flag'] = true;
        if ($this->Centros->save($centro)) {
            $this->Flash->success(__('The centro has been deleted.'));
        } else {
            $this->Flash->error(__($this->Centros->error));
        }
        return $this->redirect(['action' => 'index']);
    }
}

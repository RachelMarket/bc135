<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\View\CellTrait;
use Cake\Datasource\ConnectionManager;

/**
 * Chips Controller
 *
 * @property \App\Model\Table\ChipsTable $Chips
 */
class ChipsController extends AppController
{

    use CellTrait;
    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '10'];

    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Chips'=>[
                'Chips.numero'=>[
                    'type'=>'text',
                    'label'=>'Buscar',
                    'condition'=> 'multiple',
                    'searchFields'=>['Chips.numero'],
                    'inputOptions'=>['style'=>'width:300px; border-radius: 3px;', 'placeholder' => 'Número']
                ],         
            ]
        ]];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {

        $this->paginate = [
            'sortWhitelist' => [
                'Chips.numero',
                'Contactos.nombre'
            ],
            'limit' => 10, 'contain' => ['ContactosJoin.Contactos'],
            'order' => [
                'Chips.numero' => 'ASC'
            ],
        ];

        //debug($this->paginate($this->Chips)->toArray());die();
        
        $this->Search->applySearch(['Chips.deleted' => false]);
       
        $this->set('chips', $this->paginate($this->Chips)->toArray());
        $this->set('_serialize', ['chips']);

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_chips');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Chip id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $chip = $this->Chips->get($id, [
            'contain' => []
        ]);
        $this->set('chip', $chip);
        $this->set('_serialize', ['chip']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $chip = $this->Chips->newEntity();
        if ($this->request->is('post')) {
            $connection = ConnectionManager::get('default');
            $connection->begin();

            try {

                $chip = $this->Chips->patchEntity($chip, $this->request->data);

                $errors = $chip->errors();

                if ($this->Chips->save($chip)) {
                   
                    $this->Flash->success(__('The chip has been saved.'));
                    $connection->commit();
                    return $this->redirect(['action' => 'index']);
                
                }else{

                    $isUniqueError = $chip->errors();
                    //debug($isUniqueError);
                    if (isset($isUniqueError['numero']) && isset($isUniqueError['numero']['_isUnique']) ) {

                        $this->Flash->error(__('El chip no se pudo guardar. El número ya existe. '));

                    }else if( isset($isUniqueError['numero']) && isset($isUniqueError['numero']['_empty']) ){
                        
                        $this->Flash->error(__('Campo requerido, agrega un número de chip.'));

                    }else {
                        throw new \Exception(__('The chip could not be saved. Please, try again.'), 1); 
                    }
                    return $this->redirect(['action' => 'index']);   
                }
                
            } catch (\Exception $e) {
                $connection->rollback();
                $this->Flash->error($e->getMessage());
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('chip'));
        $this->set('_serialize', ['chip']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chip id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $chip = $this->Chips->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chip = $this->Chips->patchEntity($chip, $this->request->data);

            $errors = $chip->errors();
            
            if ($this->Chips->save($chip)) {
                $this->Flash->success(__('The chip has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {

                $isUniqueError = $chip->errors();
                if (isset($isUniqueError['numero']) && isset($isUniqueError['numero']['_isUnique']) ) {

                    $this->Flash->error(__('El chip no se pudo guardar. El número ya existe. '));

                }else if( isset($isUniqueError['numero']) && isset($isUniqueError['numero']['_empty']) ){
                        
                        $this->Flash->error(__('Campo requerido, agrega un número de chip.'));

                }else{
                    throw new \Exception(__('The chip could not be saved. Please, try again.'), 1); 
                } 
                return $this->redirect(['action' => 'index']);
            }
          
        }
        $this->set(compact('chip'));
        $this->set('_serialize', ['chip']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Chip id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $chip = $this->Chips->get($id);
        $chip->deleted = true;
        if ($this->Chips->save($chip)) {
            $this->Flash->success(__('The chip has been deleted.'));
        } else {
            $this->Flash->error(__('The chip could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

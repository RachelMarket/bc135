<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Unidades Controller
 *
 * @property \App\Model\Table\UnidadesTable $Unidades
 */
class UnidadesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('unidades', $this->Unidades->find('all'));
        $this->set('_serialize', ['unidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Unidad id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $unidad = $this->Unidades->get($id, [
            'contain' => ['ClientesServicios', 'Partidas', 'Servicios']
        ]);
        $this->set('unidad', $unidad);
        $this->set('_serialize', ['unidad']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $unidad = $this->Unidades->newEntity();
        if ($this->request->is('post')) {
            $unidad = $this->Unidades->patchEntity($unidad, $this->request->data);
            if ($this->Unidades->save($unidad)) {
                $this->Flash->success(__('The unidad has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The unidad could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('unidad'));
        $this->set('_serialize', ['unidad']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Unidad id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $unidad = $this->Unidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $unidad = $this->Unidades->patchEntity($unidad, $this->request->data);
            if ($this->Unidades->save($unidad)) {
                $this->Flash->success(__('The unidad has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The unidad could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('unidad'));
        $this->set('_serialize', ['unidad']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Unidad id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $unidad = $this->Unidades->get($id);
        if ($this->Unidades->delete($unidad)) {
            $this->Flash->success(__('The unidad has been deleted.'));
        } else {
            $this->Flash->error(__('The unidad could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

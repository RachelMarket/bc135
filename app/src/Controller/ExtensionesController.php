<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Extensiones Controller
 *
 * @property \App\Model\Table\ExtensionesTable $Extensiones
 */
class ExtensionesController extends AppController
{
    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '10'];

    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Extensiones'=>[
                'Extensiones.extension'=>[
                    'position'=>'busqueda',
                    'type'=>'text',
                    'condition'=>'multiple',
                    'searchFields'=>['Extensiones.extension'],
                    'inputOptions'=>['style'=>'width:200px; border-radius:3px;', 'placeholder' => 'Extensión']
                ],
                'Extensiones.ip'=>[
                    'type'=>'text',
                    'condition'=>'multiple',
                    'searchFields'=>['Extensiones.ip'],
                    'inputOptions'=>['style'=>'width:200px; border-radius:3px;', 'placeholder' => 'IP']
                ],
                'Extensiones.mac'=>[
                    'type'=>'text',
                    'condition'=>'multiple',
                    'searchFields'=>['Extensiones.mac'],
                    'inputOptions'=>['style'=>'width:200px; border-radius:3px;', 'placeholder' => 'MAC']
                ],
            ]
        ]
    ];

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
        'sortWhitelist' => [
            'Extensiones.extension',
            'Extensiones.ip',
            'Extensiones.mac',
            'Contactos.nombre'
        ],
        'contain' => ['ContactosExtensionesJoin.Contactos'],
        'limit' => 10,
            'order' => [
                'Extensiones.extension' => 'ASC'
            ],
        ];

        $this->Search->applySearch(['Extensiones.deleted' => false]);
        $extensiones = $this->paginate($this->Extensiones)->toArray();
        //debug($extensiones);die();
        $this->set(compact('extensiones'));
        $this->set('_serialize', ['extensiones']);

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_extensiones');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Extension id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $extension = $this->Extensiones->get($id);
        $this->set('extension', $extension);
        $this->set('_serialize', ['extension']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $extension = $this->Extensiones->newEntity();
        if ($this->request->is('post')) {
            $extension = $this->Extensiones->patchEntity($extension, $this->request->data);
            
            if ($this->Extensiones->save($extension)) {
        
                $this->Flash->success(__('The extension has been saved.'));
                return $this->redirect(['action' => 'index']);
            
            }else{

                $isUniqueErrors = $extension->errors();
                
                if( isset( $isUniqueErrors['extension'] ) && isset( $isUniqueErrors['extension']['_isUnique'] ) ){

                    $this->Flash->error(__('La extensión no pudo guardar. La extensión ya existe.'));
               
                }else if(isset( $isUniqueErrors['mac'] ) && isset( $isUniqueErrors['mac']['_isUnique'] )) {
                    
                    $this->Flash->error(__('La extensión no pudo guardar. La MAC Address ya existe.'));

                }else if(isset( $isUniqueErrors['ip'] ) && isset( $isUniqueErrors['ip']['_isUnique'] )) {

                    $this->Flash->error(__('La extensión no pudo guardar. La IP ya existe.'));

                }else {

                    $this->Flash->error(__('The extension could not be saved. Please, try again.'));
                }

                return $this->redirect(['action' => 'index']);
                
            }  
        }
        

        $this->set(compact('extension', 'arrayRecepciones', 'arrayDirecto'));
        $this->set('_serialize', ['extension']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Extension id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $extension = $this->Extensiones->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $extension = $this->Extensiones->patchEntity($extension, $this->request->data);
            
            if ($this->Extensiones->save($extension)) {
                $this->Flash->success(__('The extension has been saved.'));
                return $this->redirect(['action' => 'index']);
            }else{
               $errorsUnique = $extension->errors();
                
                if( isset( $errorsUnique['extension'] ) && isset( $errorsUnique['extension']['_isUnique'] ) ){

                    $this->Flash->error(__('La extensión no pudo guardar. La extensión ya existe.'));
               
                }else if(isset( $errorsUnique['mac'] ) && isset( $errorsUnique['mac']['_isUnique'] )) {
                    
                    $this->Flash->error(__('La extensión no pudo guardar. La MAC Address ya existe.'));

                }else if(isset( $errorsUnique['ip'] ) && isset( $errorsUnique['ip']['_isUnique'] )) {

                    $this->Flash->error(__('La extensión no pudo guardar. La IP ya existe.'));

                }else {

                    $this->Flash->error(__('The extension could not be saved. Please, try again.'));
                }

                return $this->redirect(['action' => 'index']);
            }
            
        }
        

        $this->set(compact('extension', 'arrayRecepciones', 'arrayDirecto'));
        $this->set('_serialize', ['extension']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Extension id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $extension = $this->Extensiones->get($id);
 
        $extension->deleted = true;
        if ($this->Extensiones->save($extension)) {
            $this->Flash->success(__('The extension has been deleted.'));
        } else {
            $this->Flash->error(__('The extension could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

<?php
namespace App\Controller;

use App\Controller\AppController;

use Usermgmt\Controller\UsermgmtAppController;
use Cake\View\CellTrait; 
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Contactos Controller
 *
 * @property \App\Model\Table\ContactosTable $Contactos
 */
class ContactosController extends AppController
{
	use CellTrait;

	 /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '100'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Contactos'=>[
                'Contactos'=>[
                    'type'=>'text',
                    'label'=>false,
                    'condition'=>'multiple',
                    'searchFields'=>['Contactos.nombre','Contactos.apellido_paterno','Contactos.apellido_materno'],
                    'inputOptions'=>['style'=>'width:400px; border-radius:3px', 'placeholder' => 'Nombre, Apellido Paterno, Apellido Materno']
                ],
                'Extensiones'=>[
                    'type'=>'text',
                    'label'=>false,
                    'condition'=>'multiple',
                    'searchFields'=>['Extensiones.extension'],
                    'inputOptions'=>['style'=>'width:200px; border-radius:3px', 'placeholder' => 'Extensión']
                ],
                'TarjetasAcceso'=>[
                    'type'=>'text',
                    'label'=>false,
                    'condition'=>'multiple',
                    'searchFields'=>['TarjetasAcceso.num_tarjeta'],
                    'inputOptions'=>['style'=>'width:200px; border-radius:3px', 'placeholder' => 'Tarjeta Magnética']
                ]
            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index($cliente_id = null)
    {
        $this->paginate = ['contain' => ['Clientes'], 'limit'=>10, 'order'=>['Contactos.id'=>'DESC']];
        $conditions = ['Contactos.cliente_id' => $cliente_id,'Contactos.deleted' => false];
        $this->Search->applySearch($conditions);

        $contactos = $this->paginate($this->Contactos)->toArray();
        
        $this->set(compact('contactos','cliente_id'));

        if($this->request->is('ajax')) {
            
            $this->layout = 'ajax';
            $this->render('/Element/all_contactos');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Contacto id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('ContactosDids');
        $this->loadModel('ContactosExtensiones');
        $this->loadModel('ContactosTarjetasAcceso');
        $this->loadModel('ContactosChips');
        
        $contacto = $this->Contactos->get($id);
        
        $dids = $this->ContactosDids->find('all')->contain(['Dids','Dids.Ladas','LugaresDid'])->where(['contacto_id' => $id])->toArray();

        $extensiones = $this->ContactosExtensiones->find('all')->contain('Extensiones')->where(['contacto_id' => $id])->toArray();

        $tarjetas = $this->ContactosTarjetasAcceso->find('all')->contain('TarjetasAcceso')->where(['contacto_id' => $id])->toArray();

        $chips = $this->ContactosChips->find('all')->contain('Chips')->where(['contacto_id' => $id])->toArray();
       
        $options = ['M' => 'Masculino', 'F' => 'Femenino'];
        $this->set(compact('contacto','dids', 'options', 'extensiones', 'tarjetas', 'chips'));

        $this->set('_serialize', ['contacto']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($cliente_id = null){
        
        $this->loadModel('Contactos');
        

        $contacto = $this->Contactos->newEntity();
        
        if(!empty($this->request->data['fecha_ingreso'])){
                $contacto['fecha_ingreso'] = new Time(str_replace('/','-', @$this->request->data['fecha_ingreso']));
            }
        if(!empty($this->request->data['contrasenia_alarma'])){
            $contacto['contrasenia_alarma'] = $this->request->data['contrasenia_alarma'];
        }
        $contacto['cliente_id'] = $cliente_id;
        
        if ($this->request->is('post')) {

            $contacto = $this->Contactos->patchEntity($contacto, $this->request->data);

            if ($this->Contactos->save($contacto)) {
                $this->Flash->success(__('El contacto se ha guardado.'));
                return $this->redirect(['controller' => 'Clientes','action' => 'view', $cliente_id, 'Contactos']);
            } else {
                
                $error = $contacto->errors();

                if( sizeof($error) > 0){

                    $this->return_errors($error);
                    
                }else{
                    $this->Flash->error(__('El contacto no se pudo guardar. Por favor, intente nuevamente.'));
                } 
                
                return $this->redirect(['controller' => 'Clientes','action' => 'view', $cliente_id, 'Contactos']);
            }

        }
        $options = ['M' => 'Masculino', 'F' => 'Femenino'];

        $this->set(compact('contacto','options'));
        $this->set('_serialize', ['contacto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contacto id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Contactos');
        $this->loadModel('Extensiones');
        $this->loadModel('TarjetasAcceso');
        $this->loadModel('Dids');
        $this->loadModel('Ladas');
        $this->loadModel('Chips');
        $this->loadModel('LugaresDid');

        $contacto = $this->Contactos->get($id);
        
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            

            if(!empty($this->request->data['fecha_ingreso'])){
                $contacto['fecha_ingreso'] = new Time(str_replace('/','-', @$this->request->data['fecha_ingreso']));
            }

            $contacto['contrasenia_alarma'] = $this->request->data['contrasenia_alarma'];
        
            $contactoCliente = $this->Contactos->patchEntity($contacto, $this->request->data);
            

            if ($this->Contactos->save($contactoCliente)) {
                $this->Flash->success(__('El contacto se ha guardado.'));
   
                return $this->redirect(['controller' => 'Clientes','action' => 'view',$contacto->cliente_id, 'Contactos']);

            } else {

                $error = $contacto->errors();

                if( sizeof($error) > 0){

                    $this->return_errors($error);
                    
                }else{
                    $this->Flash->error(__('El contacto no se pudo guardar. Por favor, intente nuevamente.'));
                }
                
                return $this->redirect(['controller' => 'Clientes','action' => 'view', $contacto->cliente_id, 'Contactos' ]);
            }

        }
        $options = ['M' => 'Masculino', 'F' => 'Femenino'];
        
        $extends = $this->Extensiones->find('list')
        ->where(['Extensiones.assigned' => false, 'Extensiones.deleted' => false])
        ->order(['extension' => 'ASC'])
        ->toArray();

        $tajects = $this->TarjetasAcceso->find('list')
        ->where(['TarjetasAcceso.assigned' => false, 'TarjetasAcceso.deleted' => false])
        ->order(['num_tarjeta' => 'ASC'])
        ->toArray();;

        $didsNumber = $this->Dids->find('list', ['keyField' => 'id', 'valueField' => 'did'])
        ->where(['Dids.assigned' => false,'Dids.deleted' => false])
        ->order(['did' => 'ASC'])
        ->toArray();

        $didsLada = $this->Ladas->find('list', ['keyField' => 'id', 'valueField' => 'lada'])
        ->where(['Ladas.deleted' => false])
        ->order(['lada' => 'ASC'])
        ->toArray();

        $chips = $this->Chips->find('list', ['keyField' => 'id', 'valueField' => 'numero'])
        ->where(['Chips.assigned' => false,'Chips.deleted' => false])
        ->order(['numero' => 'ASC'])
        ->toArray();

        $lugaresDID = $this->LugaresDid->find('list', ['keyField' => 'id', 'valueField' => 'lugar'])
        ->where(['LugaresDid.deleted' => false])
        ->order(['lugar' => 'ASC'])
        ->toArray();
        
        $this->set(compact('contacto', 'options', 'extends', 'tajects','didsNumber', 'didsLada', 'chips','lugaresDID'));
        $this->set('_serialize', ['contacto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contacto id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //Configure::write('debug', 2); 
        $this->request->allowMethod(['post', 'delete']);
        $contacto = $this->Contactos->get($id);
        $contacto->deleted = true;  
        $this->desasociar($id);

        if ($this->Contactos->save($contacto)) {

            $this->Flash->success(__('El contacto ha sido borrado.'));

        } else {
            $this->Flash->error(__('El contacto no se pudo eliminar. Por favor, intente nuevamente.'));
        }
        return $this->redirect(['controller' => 'clientes', 'action' => 'view', $contacto->cliente_id, 'Contactos']);
    }

    public function getLada(){
        
        $this->loadModel('Dids');
        
        if ($this->request->is('post')) {

            $id = $this->request->data['id'];

            $result = $this->Dids->find('list', ['keyField' => 'id', 'valueField' => 'did'])->contain('Ladas')->where(['Dids.lada_id' => $id])->toArray();

            $respuesta = [];

            if (!empty($result)) {
                $respuesta['resultado'] = 200;
                $respuesta['msn'] = 'OK.';
                $respuesta['array'] = $result;
                
            }else{
                $respuesta['error'][] = $result;
                $respuesta['resultado'] = 100;
                $respuesta['msn'] = 'Error.';
            }
            die(json_encode($respuesta));
        }
    }
    public function saveDID(){
        
        $this->loadModel('ContactosDids');
        $this->loadModel('Dids');
        $this->loadModel('Ladas');
        $this->loadModel('LugaresDid');

        $contacto = $this->ContactosDids->newEntity();
        $respuesta = [];

        if ($this->request->is('post')) {

            //$exists = $this->ContactosDids->exists(['did_id' => $this->request->data['did_id']]);

            $contacto['contacto_id'] = $this->request->data['id'];
            $contacto['did_id'] = $this->request->data['did_id'];
            $contacto['lugar_id'] = $this->request->data['lugar'];

            if($this->ContactosDids->save($contacto)){

                $respuesta['resultado'] = 200;
                $respuesta['msn'] = 'OK';

                $did = $this->Dids->get($this->request->data['did_id']);
                $did->assigned = true;
                $this->Dids->save($did);
                $resultado = $this->ContactosDids->find('all',['fields'=>'id'])->last();
                $query = $this->Dids->find('all',['fields'=>'did'])->where(['id' => $this->request->data['did_id']])->toArray();
                $lada = $this->Ladas->find('all',['fields'=>'lada'])->where(['id' => $this->request->data['lada']])->toArray();
                $lugar = $this->LugaresDid->find('all',['fields'=>'lugar'])->where(['id' => $this->request->data['lugar']])->toArray();

                $respuesta['contacto_id'] = $resultado->id;
                $respuesta['numberDID'] = $query[0]['did'];
                $respuesta['numberLada'] = $lada[0]['lada'];
                $respuesta['nameLugar'] = $lugar[0]['lugar'];
            
            }else{
                $respuesta['resultado'] = 500;
                $respuesta['msn'] = 'ERROR EXIST DATA';
            }
        }
        die(json_encode($respuesta));
    }


    public function removeDID(){
        $this->loadModel('ContactosDids');
        $this->loadModel('Dids');
        $this->request->allowMethod(['post', 'delete']);
        
        $contacto = $this->ContactosDids->get($this->request->data['id']);
        $did = $this->Dids->get($contacto->did_id);
        $did['assigned'] = false;

        if ($this->ContactosDids->delete($contacto)) {
            $this->Dids->save($did);
            $respuesta['resultado'] = 200;
            $respuesta['msn'] = 'OK';
        }
        

        die(json_encode($respuesta));
    }

    public function addChips(){

        $this->loadModel('ContactosChips');
        $this->loadModel('Chips');

        $chip = $this->ContactosChips->newEntity();

        if ($this->request->is('post')) {

           $exists = $this->ContactosChips->exists(['chip_id' => $this->request->data['chip_id']]);

           $respuesta = [];

           if($exists == false){
                $chip['contacto_id'] = $this->request->data['contacto_id'];
                $chip['chip_id'] = $this->request->data['chip_id'];

                if($this->ContactosChips->save($chip)){

                    $chip = $this->Chips->get($this->request->data['chip_id']);
                    $chip->assigned = true;
                    $this->Chips->save($chip);

                    $respuesta['resultado'] = 200;
                    $respuesta['msn'] = 'OK';

                    $query = $this->ContactosChips->find('all',['fields'=>'id'])->last();
                    $chip = $this->Chips->find('all',['fields'=>'numero'])->where(['id' => $this->request->data['chip_id']]);
                    $numberChip = '';
                    foreach ($chip as $value) {
                        $numberChip = $value['numero'];
                    }
                    $chip_id = $query->id;
                    $respuesta['chip_id'] = $chip_id;
                    $respuesta['numberChip'] = $numberChip;
                }
           }else{
                $respuesta['resultado'] = 500;
                $respuesta['msn'] = 'ERROR EXIST DATA';
           }
        }

        die(json_encode($respuesta));
    }

    public function removeChips(){

        $this->loadModel('ContactosChips');
        $this->loadModel('Chips');
        $this->request->allowMethod(['post', 'delete']);
        
        $chip = $this->ContactosChips->get($this->request->data['id']);
        $chipp = $this->Chips->get($chip->chip_id);
        $chipp['assigned'] = false;

        if ($this->ContactosChips->delete($chip)) {
            $this->Chips->save($chipp);
            $respuesta['resultado'] = 200;
            $respuesta['msn'] = 'OK';
        }

        die(json_encode($respuesta));
    }

    public function addTarjetas(){

        $this->loadModel('ContactosTarjetasAcceso');
        $this->loadModel('TarjetasAcceso');

        $tarjeta = $this->ContactosTarjetasAcceso->newEntity();
      
        if ($this->request->is('post')) {
           

           $exists = $this->ContactosTarjetasAcceso->exists(['tarjetas_acceso_id' => $this->request->data['tarjeta_id']]);

           $respuesta = [];

           if($exists == false){

                $tarjeta['contacto_id'] = $this->request->data['contacto_id'];
                $tarjeta['tarjetas_acceso_id'] = $this->request->data['tarjeta_id'];

                if($this->ContactosTarjetasAcceso->save($tarjeta)){

                    $tarjetasAcceso = $this->TarjetasAcceso->get($this->request->data['tarjeta_id']);
                    $tarjetasAcceso->assigned = true;
                    $this->TarjetasAcceso->save($tarjetasAcceso);

                    $respuesta['resultado'] = 200;
                    $respuesta['msn'] = 'OK';
                    $query = $this->ContactosTarjetasAcceso->find('all',['fields'=>'id'])->last();
                    $num_tarjeta = $this->TarjetasAcceso->find('all',['fields'=>'num_tarjeta'])->where(['id' => $this->request->data['tarjeta_id']]);
                    $numTarjeta = '';
                    foreach ($num_tarjeta as $value) {
                        $numTarjeta = $value['num_tarjeta'];
                    }
                    $tarjeta_id = $query->id;
                    $respuesta['tarjeta_id'] = $tarjeta_id;
                    $respuesta['numTarjeta'] = $numTarjeta;
                }
           }else{
                $respuesta['resultado'] = 500;
                $respuesta['msn'] = 'ERROR EXIST DATA';
           }
           
        }

        die(json_encode($respuesta));
    }

    public function removeTarjetas(){

        $this->loadModel('ContactosTarjetasAcceso');
        $this->loadModel('TarjetasAcceso');
        $this->request->allowMethod(['post', 'delete']);
        
        $tarjeta = $this->ContactosTarjetasAcceso->get($this->request->data['id']);
        $card = $this->TarjetasAcceso->get($tarjeta->tarjetas_acceso_id);
        $card['assigned'] = false;

        if ($this->ContactosTarjetasAcceso->delete($tarjeta)) {
            $this->TarjetasAcceso->save($card);
            $respuesta['resultado'] = 200;
            $respuesta['msn'] = 'OK';
        }

        die(json_encode($respuesta));
    }

    public function addExtension(){

        $this->loadModel('Extensiones');
        $this->loadModel('ContactosExtensiones');
        
        $extension = $this->ContactosExtensiones->newEntity();

        if ($this->request->is('post')) {
           

           $exists = $this->ContactosExtensiones->exists(['extension_id' => $this->request->data['extension_id']]);
           
           if($exists == false){
                
                $extension['contacto_id'] = $this->request->data['contacto_id'];
                $extension['extension_id'] = $this->request->data['extension_id'];

                if($this->ContactosExtensiones->save($extension)){

                    $extensiones = $this->Extensiones->get($this->request->data['extension_id']);
                    $extensiones->assigned = true;
                    $this->Extensiones->save($extensiones);

                    $respuesta['resultado'] = 200;
                    $respuesta['msn'] = 'OK';
                    $query = $this->ContactosExtensiones->find('all',['fields'=>'id'])->last();
                    $numberExtension = $this->Extensiones->find('all',['fields'=>'extension'])->where(['id' => $this->request->data['extension_id']]);
                    $numExtension = '';
                    foreach ($numberExtension as $value) {
                        $numExtension = $value['extension'];
                    }
                    $extension_id = $query->id;
                    $respuesta['extension_id'] = $extension_id;
                    $respuesta['numExtension'] = $numExtension;
               }
           }else{
                $respuesta['resultado'] = 500;
                $respuesta['msn'] = 'ERROR EXIST DATA';
           }
           
        }

        die(json_encode($respuesta));
    }

    public function removeExtension(){

        $this->loadModel('ContactosExtensiones');
        $this->loadModel('Extensiones');

        $this->request->allowMethod(['post', 'delete']);
        
        $extension = $this->ContactosExtensiones->get($this->request->data['id']);
        $ext = $this->Extensiones->get($extension->extension_id);
        $ext['assigned'] = false;

        if ($this->ContactosExtensiones->delete($extension)) {
            $this->Extensiones->save($ext);
            $respuesta['resultado'] = 200;
            $respuesta['msn'] = 'OK';
        }

        die(json_encode($respuesta));
    }

    public function loadExtensiones(){

        $this->loadModel('ContactosExtensiones');
        $extension = $this->ContactosExtensiones->find('all')->contain('Extensiones')->where(['contacto_id' => $this->request->data['id']])->toArray();
        
        $respuesta = [];

        if (!empty($extension)){
            $respuesta['resultado'] = 200;
            $respuesta['msn'] = 'OK';
            $respuesta['array'] = $extension;
        }else{
            $respuesta['resultado'] = 500;
            $respuesta['msn'] = 'ERROR';
            $respuesta['array'] = $extension;
        }

        die(json_encode($respuesta));
    }


    public function loadTarjetas(){

        $this->loadModel('ContactosTarjetasAcceso');
        $tarjeta = $this->ContactosTarjetasAcceso->find('all')->contain('TarjetasAcceso')->where(['contacto_id' => $this->request->data['id']])->toArray();
        
        $respuesta = [];

        if (!empty($tarjeta)){
            $respuesta['resultado'] = 200;
            $respuesta['msn'] = 'OK';
            $respuesta['array'] = $tarjeta;
        }else{
            $respuesta['resultado'] = 500;
            $respuesta['msn'] = 'ERROR';
            $respuesta['array'] = $tarjeta;
        }

        die(json_encode($respuesta));
    }


    public function loadChips(){

        $this->loadModel('ContactosChips');
        $chips = $this->ContactosChips->find('all')->contain('Chips')->where(['contacto_id' => $this->request->data['id']])->toArray();
        
        $respuesta = [];

        if (!empty($chips)){
            $respuesta['resultado'] = 200;
            $respuesta['msn'] = 'OK';
            $respuesta['array'] = $chips;
        }else{
            $respuesta['resultado'] = 500;
            $respuesta['msn'] = 'ERROR';
            $respuesta['array'] = $chips;
        }

        die(json_encode($respuesta));
    }

    public function loadDID(){

        $this->loadModel('ContactosDids');
        $dids = $this->ContactosDids->find('all')->contain(['Dids','Dids.Ladas','LugaresDid'])->where(['contacto_id' => $this->request->data['id']])->toArray();
        //debug($dids);
        $respuesta = [];

        if (!empty($dids)){
            $respuesta['resultado'] = 200;
            $respuesta['msn'] = 'OK';
            $respuesta['array'] = $dids;
        }else{
            $respuesta['resultado'] = 500;
            $respuesta['msn'] = 'ERROR';
            $respuesta['array'] = $dids;
        }

        die(json_encode($respuesta));
    }

    public function desasociar($id=null){
        if (empty($id)) {
            return true;
        }

        //modelos
        $this->loadModel('Chips');
        $this->loadModel('Extensiones');
        $this->loadModel('TarjetasAcceso');
        $this->loadModel('Dids');

        //submodelos
        $this->loadModel('Contactos');
        $this->loadModel('ContactosChips');
        $this->loadModel('ContactosExtensiones');
        $this->loadModel('ContactosTarjetasAcceso');
        $this->loadModel('ContactosDids');


        //Verificamos que si los contactos de este cliente tengan recursos asignados
        $mycontacts = $this->Contactos->find('all', ['conditions' => ['id' => $id, 'deleted' => false]])->toArray();

        $modelos    = [ 'chip_id'               =>  'Chips',
                        'extension_id'          =>  'Extensiones',
                        'tarjetas_acceso_id'    =>  'TarjetasAcceso',
                        'did_id'                =>  'Dids'];
        
        $submodelos = [ 'chip_id'               => 'ContactosChips',
                        'extension_id'          => 'ContactosExtensiones',
                        'tarjetas_acceso_id'    => 'ContactosTarjetasAcceso',
                        'did_id'                => 'ContactosDids'];
                
        foreach ($mycontacts as $row) {
            foreach ($submodelos as $key => $value) {
                $valor = $this->$value->find('all', ['conditions' => ['contacto_id' => $row['id']]])->toArray();
                foreach ($valor as $x) {
                    $m=$modelos[$key];
                    $i=$x[$key];
                    $model = $this->$m->get($i); 
                    $model->assigned = FALSE;
                    $this->{$modelos[$key]}->save($model);
                }
                $this->$value->deleteAll(['contacto_id' => $row['id']]);
            }   
        } 

        return true;

    }

    function verifica_alarma($contrasenia_alarma=null,$id=null){
        
        $respuesta=array('error'=>0);
        
        $conditions = [];

        if($contrasenia_alarma !=''){

            if ( !empty( $id ) ) {
               $conditions['id !='] = $id;
            }

            $conditions['contrasenia_alarma'] = $contrasenia_alarma;
            $conditions['deleted'] = FALSE;


            $contactos=$this->Contactos->find('all')->where([ $conditions ])->toArray();

            if(sizeof($contactos)>0){
                $respuesta['error']=1;
            }
        }
           die(json_encode($respuesta));

    }


    public function return_errors( $error ){

        if ( isset( $error['rfc']['_isUnique'])  ) {
                        
            $this->Flash->error( $error['rfc']['_isUnique']['message'] );

        }else if( isset( $error['contrasenia_alarma']['_isUnique'] ) ){

            $this->Flash->error( $error['contrasenia_alarma']['_isUnique']['message'] );
        
        }elseif ( isset( $error['nombre']['_empty']) ) {
            
            $this->Flash->error( $error['nombre']['_empty'] );
       
        }elseif ( isset( $error['rfc']['_empty']) ) {
            
            $this->Flash->error( $error['rfc']['_empty'] );
       
        }elseif ( isset( $error['contrasenia_alarma']['_empty']) ) {
            
            $this->Flash->error( $error['contrasenia_alarma']['_empty'] );
        }
    }

}

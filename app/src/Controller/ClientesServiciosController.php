<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;

/**
 * ClientesServicios Controller
 *
 * @property \App\Model\Table\ClientesServiciosTable $ClientesServicios
 */
class ClientesServiciosController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('clientesServicios', $this->ClientesServicios->find('all'));
        $this->set('_serialize', ['clientesServicios']);
    }

    /**
     * View method
     *
     * @param string|null $id Clientes Servicio id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clientesServicio = $this->ClientesServicios->get($id, [
            'contain' => ['Clientes', 'Servicios', 'Facturas', 'Usuarios', 'Tipos', 'Unidades']
        ]);
        $this->set('clientesServicio', $clientesServicio);
        $this->set('_serialize', ['clientesServicio']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */

    public function add($tipo_id, $cliente_id)
    {
        // $start = new \DateTime("2010-01-30", new \DateTimeZone("UTC"));
        // $month_later = clone $start;
        // $month_later->add(new \DateInterval("P1M"));

        // $d = new \DateTime( '2010-01-31' );
        // $d->modify( 'first day of next month' );
        // echo $d->format( 'F' ), "\n";

        // die(debug($month_later));

        $cliente = $this->ClientesServicios->Clientes->get($cliente_id);


        $clientesServicio = $this->ClientesServicios->newEntity();
        if ($this->request->is('post')) {

            $servicio_id = $this->request->data['servicio_id'];
            $clientesServicio = $this->ClientesServicios->patchEntity($clientesServicio, $this->request->data);
            $usuario=$this->UserAuth->getUser(); 

            $clientesServicioTable = TableRegistry::get('ClientesServicios');
            $servicio = $clientesServicioTable->newEntity();

            $servicioObject = $this->ClientesServicios->Servicios->find('all', ['conditions' => "Servicios.id = $servicio_id"]);
            $servicioObject = $servicioObject->toArray()[0];
            $servicio->nombre = $servicioObject['nombre'];
            $servicio->unidad_id = $servicioObject['unidad_id'];
            $servicio->precio_mxn = $servicioObject['precio_mxn'];
            $servicio->precio_usd = $servicioObject['precio_usd'];
            $servicio->descripcion = $servicioObject['descripcion'];
            $servicio->tipo_id = $this->request->data['tipo_id'];
            $servicio->cantidad = $this->request->data['cantidad'];
            $servicio->notificacion = $this->request->data['notificacion'];
            $servicio->servicio_id = $servicio_id;
            $servicio->cliente_id = $cliente_id;
            $servicio->usuario_id = $usuario['User']['id'];
            $today = date("Y-m-d H:i:s"); 
            $servicio->fecha = $today;
            $clientesServicioTable->save($servicio);

            //Tipo renta
            if($tipo_id == 2){
                $recurrente = clone $servicio;
                $rows = array();
                for($i = 1; $i < 12; $i++){
                    //calculating first day of further months
                    $month = date("Y-m-01", time()+2592000*$i);
                    $ultimo=0;
                    if($i == 11){ $ultimo=1; }

                    $servicio_recurrente = $clientesServicioTable->newEntity();
                    $servicio_recurrente->nombre = $servicioObject['nombre'];
                    $servicio_recurrente->unidad_id = $servicioObject['unidad_id'];
                    $servicio_recurrente->precio_mxn = $servicioObject['precio_mxn'];
                    $servicio_recurrente->precio_usd = $servicioObject['precio_usd'];
                    $servicio_recurrente->descripcion = $servicioObject['descripcion'];
                    $servicio_recurrente->tipo_id = $this->request->data['tipo_id'];
                    $servicio_recurrente->cantidad = $this->request->data['cantidad'];
                    $servicio_recurrente->notificacion = $this->request->data['notificacion'];
                    $servicio_recurrente->servicio_id = $servicio_id;
                    $servicio_recurrente->cliente_id = $cliente_id;
                    $servicio_recurrente->usuario_id = $usuario['User']['id'];
                    $servicio_recurrente->fecha = $month;
                    $servicio_recurrente->notificacion = 0;
                    $servicio_recurrente->ultimo = $ultimo;
                    if(!$clientesServicioTable->save($servicio_recurrente)){
                        $this->Flash->error(__('The clientes servicio could not be saved. Please, try again.'));
                        return $this->redirect("/clientes/view/$cliente_id");
                    }
                }

                $this->Flash->success(__('El servicio de renta ha sido agregado'));
            }
            if($tipo_id == 3){
                if($this->request->data['notificacion']){
                    $email = new Email('default');
                    //debug($cliente->primario_correo_electronico);
                    //die;
                    $secundario='noresponder@135-bc.com';
                    if($cliente->secundario_correo_electronico !=''){
                        $secundario=$cliente->secundario_correo_electronico;
                    }


                    $this->loadModel('Usermgmt.UserSettings');
                    $mensaje_config=$this->UserSettings->find('all')->where(['name'=>'MensajeAltaServicio'])->first();
                    $mensaje=$mensaje_config->value.' <br> '. $servicioObject['nombre'];
                    $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                    ->emailFormat('html')
                    ->viewVars(['titulo'=>'Alta de servicio', 'mensaje'=>$mensaje])
                    ->template('notificacion')
                    ->to($cliente->primario_correo_electronico)
                    ->addTo($secundario)
                    ->subject('Alta de servicio')
                    ->send();
                }

                   $this->Flash->success(__('El servicio adicional ha sido agregado'));
            }
            return $this->redirect("/clientes/view/$cliente_id");
        }
        //$tipo = $this->ClientesServicios->Servicios->get($servicio_id);

        //Si es tipo renta se seleccionan sólo los Servicios de renta
        if($tipo_id == 2){
            $tipo_name = 'Renta';
            $servicios = $this->ClientesServicios->Servicios->find('all', ['conditions' => 
               ['Servicios.tipo_id' => '2', 'Servicios.centro_id' => $cliente['centro_id']]]);
            $servicios_json = json_encode($servicios);

        } 
        else{
        //Si es tipo adicional sólo se seleccionan sólo los Servicios Adicionales
            if($tipo_id == 3){
                $tipo_name = 'Servicio Adicional';
                $servicios = $this->ClientesServicios->Servicios->find('all', ['conditions' =>
                    ['Servicios.tipo_id' => '3', 'Servicios.centro_id' => $cliente['centro_id']]]);
                $servicios_json = json_encode($servicios);
            }
        }

        $facturas = $this->ClientesServicios->Facturas->find('list', ['limit' => 200]);
        // $usuarios = $this->ClientesServicios->Usuarios->find('list', ['limit' => 200]);
        $tipos = $this->ClientesServicios->Tipos->find('list', ['limit' => 200]);
        $unidades = $this->ClientesServicios->Unidades->find('list', ['limit' => 200]);
        $this->set(compact('clientesServicio', 'facturas', 'usuarios', 'tipo_name', 'tipo_id', 'unidades', 'servicios', 'servicios_json', 'cliente'));
        $this->set('_serialize', ['clientesServicio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Clientes Servicio id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $clientesServicio = $this->ClientesServicios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clientesServicio = $this->ClientesServicios->patchEntity($clientesServicio, $this->request->data);
            if ($this->ClientesServicios->save($clientesServicio)) {
                $this->Flash->success(__('The clientes servicio has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The clientes servicio could not be saved. Please, try again.'));
            }
        }
        $clientes = $this->ClientesServicios->Clientes->find('list', ['limit' => 200]);
        $servicios = $this->ClientesServicios->Servicios->find('list', ['limit' => 200]);
        $facturas = $this->ClientesServicios->Facturas->find('list', ['limit' => 200]);
        $usuarios = $this->ClientesServicios->Usuarios->find('list', ['limit' => 200]);
        $tipos = $this->ClientesServicios->Tipos->find('list', ['limit' => 200]);
        $unidades = $this->ClientesServicios->Unidades->find('list', ['limit' => 200]);
        $this->set(compact('clientesServicio', 'clientes', 'servicios', 'facturas', 'usuarios', 'tipos', 'unidades'));
        $this->set('_serialize', ['clientesServicio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Clientes Servicio id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clientesServicio = $this->ClientesServicios->get($id);
        if ($this->ClientesServicios->delete($clientesServicio)) {
            $this->Flash->success(__('The clientes servicio has been deleted.'));
        } else {
            $this->Flash->error(__('The clientes servicio could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function add_free($cliente_id)
    {

        $cliente = $this->ClientesServicios->Clientes->get($cliente_id);


        $clientesServicio = $this->ClientesServicios->newEntity();
        if ($this->request->is('post')){

            //die(debug($this->request->data));

            $usuario=$this->UserAuth->getUser(); 

            $clientesServicioTable = TableRegistry::get('ClientesServicios');
            $servicio = $clientesServicioTable->newEntity();

            $servicio->nombre = $this->request->data['nombre'];
            $servicio->unidad_id = $this->request->data['unidad_id'];
            $servicio->precio_mxn = $this->request->data['precio_mxn'];
            $servicio->precio_usd = $this->request->data['precio_usd'];
            $servicio->descripcion = $this->request->data['descripcion'];
            $servicio->tipo_id = 2;
            $servicio->cantidad = $this->request->data['cantidad'];
            $servicio->notificacion = $this->request->data['notificacion'];
            $servicio->servicio_id = 1;
            $servicio->cliente_id = $cliente_id;
            $servicio->usuario_id = $usuario['User']['id'];
            $servicio->fecha = date("Y-m-d H:i:s",strtotime($this->request->data['fecha'])); 
            $clientesServicioTable->save($servicio);

            
                if($this->request->data['notificacion']){
                    $email = new Email('default');
                    //debug($cliente->primario_correo_electronico);
                    //die;

                    $secundario='noresponder@135-bc.com';
                    if($cliente->secundario_correo_electronico !=''){
                        $secundario=$cliente->secundario_correo_electronico;
                    }
                    

                    $this->loadModel('Usermgmt.UserSettings');
                    $mensaje_config=$this->UserSettings->find('all')->where(['name'=>'MensajeAltaServicio'])->first();
                    $mensaje=$mensaje_config->value.' <br> '. $this->request->data['nombre'];

                    $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
                    ->emailFormat('html')
                    ->viewVars(['titulo'=>'Alta de servicio', 'mensaje'=>$mensaje])
                    ->template('notificacion')
                    ->to($cliente->primario_correo_electronico)
                    ->addTo($secundario)
                    ->subject('Alta de servicio')
                    ->send();
                }

            $this->Flash->success(__('El servicio adicional ha sido agregado'));
            

            return $this->redirect("/clientes/view/$cliente_id");
        }
        
        
        $tipo_name = 'Servicio Adicional Libre';
        
        $unidades = $this->ClientesServicios->Unidades->find('list', ['limit' => 200]);
        $this->set(compact('clientesServicio',  'unidades', 'cliente','tipo_name'));
        $this->set('_serialize', ['clientesServicio']);
    }
}

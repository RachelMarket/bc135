<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Folio Controller
 *
 * @property \App\Model\Table\FolioTable $Folio
 */
class FolioController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('folio', $this->paginate($this->Folio));
        $this->set('_serialize', ['folio']);
    }

    /**
     * View method
     *
     * @param string|null $id Folio id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $folio = $this->Folio->get($id, [
            'contain' => []
        ]);
        $this->set('folio', $folio);
        $this->set('_serialize', ['folio']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $folio = $this->Folio->newEntity();
        if ($this->request->is('post')) {
            $folio = $this->Folio->patchEntity($folio, $this->request->data);
            if ($this->Folio->save($folio)) {
                $this->Flash->success(__('The folio has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The folio could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('folio'));
        $this->set('_serialize', ['folio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Folio id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $folio = $this->Folio->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $folio = $this->Folio->patchEntity($folio, $this->request->data);
            if ($this->Folio->save($folio)) {
                $this->Flash->success(__('The folio has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The folio could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('folio'));
        $this->set('_serialize', ['folio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Folio id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $folio = $this->Folio->get($id);
        if ($this->Folio->delete($folio)) {
            $this->Flash->success(__('The folio has been deleted.'));
        } else {
            $this->Flash->error(__('The folio could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

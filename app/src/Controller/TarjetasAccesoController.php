<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\View\CellTrait;
use Cake\Datasource\ConnectionManager;

/**
 * TarjetasAcceso Controller
 *
 * @property \App\Model\Table\TarjetasAccesoTable $TarjetasAcceso
 */
class TarjetasAccesoController extends AppController
{
    use CellTrait;
    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '10'];

    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'TarjetasAcceso'=>[
                'TarjetasAcceso.num_tarjeta'=>[
                    'type'=>'text',
                    'label'=>'Buscar',
                    'condition'=>'multiple',
                    'searchFields'=>['TarjetasAcceso.num_tarjeta'],
                    'inputOptions'=>['style'=>'width:300px; border-radius: 3px;', 'placeholder' => 'Número']
                ],         
            ]
        ]];

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
         $this->paginate = [
         'sortWhitelist' => [
            'TarjetasAcceso.num_tarjeta',
            'Contactos.nombre',
            'TarjetasAcceso.created',
            'TarjetasAcceso.modified'
         ],
         'limit' => 10, 'contain' => ['TarjetasAccesoJoin.Contactos'],
            'order' => [
                'TarjetasAcceso.num_tarjeta' => 'ASC'
            ],
        ];
        //debug($this->paginate($this->TarjetasAcceso)->toArray());die();

        $this->Search->applySearch(['TarjetasAcceso.deleted' => false]);
        $this->set('tarjetasAcceso', $this->paginate($this->TarjetasAcceso)->toArray());
        $this->set('_serialize', ['tarjetasAcceso']);

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_tarjetas_acceso');
        }

    }

    /**
     * View method
     *
     * @param string|null $id Tarjetas Acceso id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tarjetasAcceso = $this->TarjetasAcceso->get($id, [
            'contain' => []
        ]);
        $this->set('tarjetasAcceso', $tarjetasAcceso);
        $this->set('_serialize', ['tarjetasAcceso']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tarjetasAcceso = $this->TarjetasAcceso->newEntity();
        if ($this->request->is('post')) {

            $connection = ConnectionManager::get('default');
            $connection->begin();

            try{

                $tarjetasAcceso = $this->TarjetasAcceso->patchEntity($tarjetasAcceso, $this->request->data);

                $errors = $tarjetasAcceso->errors();

                //debug($this->request->data);die();
                if ($this->TarjetasAcceso->save($tarjetasAcceso)) {
                    $this->Flash->success(__('The access card has been saved.'));
                    $connection->commit();
                    return $this->redirect(['action' => 'index']);
                } else {
                    
                    $isUniqueError = $tarjetasAcceso->errors();
                    
                    if (isset($isUniqueError['num_tarjeta']) && isset($isUniqueError['num_tarjeta']['_isUnique']) ) {

                        $this->Flash->error(__('La tarjeta de acceso no se pudo guardar. El número ya existe.'));

                    }else if( isset($isUniqueError['num_tarjeta']) && isset($isUniqueError['num_tarjeta']['_required']) ){

                        $this->Flash->error(__('Campo requerido, agrega un número de tarjeta.'));

                    }else {
                        throw new \Exception(__('The access card could not be saved. Please, try again.') , 1); 
                    }
                    return $this->redirect(['action' => 'index']);    
                }

            } catch (\Exception $e) {
                $connection->rollback();
                $this->Flash->error($e->getMessage());
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('tarjetasAcceso'));
        $this->set('_serialize', ['tarjetasAcceso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarjetas Acceso id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tarjetasAcceso = $this->TarjetasAcceso->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $tarjetasAcceso = $this->TarjetasAcceso->patchEntity($tarjetasAcceso, $this->request->data);

            $errors = $tarjetasAcceso->errors();

            if ($this->TarjetasAcceso->save($tarjetasAcceso)) {
                $this->Flash->success(__('The access card has been saved.'));
                return $this->redirect(['action' => 'index']);
            }else{

                $isUniqueError = $tarjetasAcceso->errors();

                if ( isset( $isUniqueError['num_tarjeta'] ) && isset( $isUniqueError['num_tarjeta']['_isUnique'] ) ) {

                    $this->Flash->error(__('La tarjeta de acceso no se pudo guardar. El número ya existe. '));

                }else if( isset($isUniqueError['num_tarjeta']) && isset($isUniqueError['num_tarjeta']['_required']) ){
                    
                    $this->Flash->error(__('Campo requerido, agrega un número de tarjeta.'));

                }else{
                    throw new \Exception(__('The access card could not be saved. Please, try again.') , 1); 
                }
                return $this->redirect($this->referer());  
            }
            
        }
        $this->set(compact('tarjetasAcceso'));
        $this->set('_serialize', ['tarjetasAcceso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarjetas Acceso id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tarjetasAcceso = $this->TarjetasAcceso->get($id);
        $tarjetasAcceso->deleted = true;
        if ($this->TarjetasAcceso->save($tarjetasAcceso)) {
            $this->Flash->success(__('The access card has been deleted.'));
        } else {
            $this->Flash->error(__('The access card could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

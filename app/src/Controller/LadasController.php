<?php
namespace App\Controller;

use App\Controller\AppController;

use Usermgmt\Controller\UsermgmtAppController;
use Cake\View\CellTrait; 
use Cake\Mailer\Email;

/**
 * Ladas Controller
 *
 * @property \App\Model\Table\LadasTable $Ladas
 */
class LadasController extends AppController
{
	use CellTrait;

	 /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '100'];


    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Ladas'=>[
                'Ladas'=>[
                    'type'=>'text',
                    'label'=>'Buscar',
                    'tagline'=>'<br>Busca por Lada',
                    'condition'=>'multiple',
                    'searchFields'=>['Ladas.lada'],
                    'inputOptions'=>['style'=>'width:300px;']
                ]
            ]
        ]
    ];


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = ['limit'=>10, 'order'=>['Ladas.id'=>'DESC']];
        $this->Search->applySearch(['Ladas.deleted' => FALSE]);
        $ladas = $this->paginate($this->Ladas)->toArray();
        $this->set(compact('ladas'));

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_ladas');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Lada id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lada = $this->Ladas->get($id, [
            'contain' => []
        ]);
        $this->set('lada', $lada);
        $this->set('_serialize', ['lada']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lada = $this->Ladas->newEntity();
        if ($this->request->is('post')) {
            $lada = $this->Ladas->patchEntity($lada, $this->request->data);

            if ($this->Ladas->save($lada)) {
                $this->Flash->success(__('The lada has been saved.'));
                return $this->redirect(['action' => 'index']);
            }else {

                $errors = $lada->errors();

                if( isset($errors['lada']) && isset($errors['lugar']['_empty']) ){
                    
                    $this->Flash->error(__('Campo requerido, agrega una lugar.'));

                }else if( isset($errors['lada']) && isset($errors['lada']['_isUnique']) ){
                    
                    $this->Flash->error(__('La lada ya existe.'));

                }else{
                    throw new \Exception(__('The lada could not be saved. Please, try again.')); 
                }
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('lada'));
        $this->set('_serialize', ['lada']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lada id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lada = $this->Ladas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lada = $this->Ladas->patchEntity($lada, $this->request->data);
            if ($this->Ladas->save($lada)) {
                $this->Flash->success(__('The lada has been saved.'));
                return $this->redirect(['action' => 'index']);
            }else {
                
                $errors = $lada->errors();

                if( isset($errors['lada']) && isset($errors['lugar']['_empty']) ){
                    
                    $this->Flash->error(__('Campo requerido, agrega una lugar.'));

                }else if( isset($errors['lada']) && isset($errors['lada']['_isUnique']) ){
                    
                    $this->Flash->error(__('La lada ya existe.'));

                }else{
                    throw new \Exception(__('The lada could not be saved. Please, try again.')); 
                }
                return $this->redirect(['action' => 'index']);
                
            }
        }
        $this->set(compact('lada'));
        $this->set('_serialize', ['lada']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lada id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->loadModel('Dids');

        $this->request->allowMethod(['post', 'delete']);
        $lada = $this->Ladas->get($id);
        $lada->deleted = TRUE;

        //Revisamos si tiene dependencias
        try {
            
            $dependencia = $this->Dids->find('all')->where(['Dids.lada_id' => $id])->count();
            
            if($dependencia > 0 ){

                $this->Flash->error(__('La lada no se pudo borrar. Tiene dependencias'));

            }else{

                if ($this->Ladas->save($lada)) {
                    $this->Flash->success(__('The lada has been deleted.'));
                } else {
                    $this->Flash->error(__('The lada could not be deleted. Please, try again.'));
                }
            }

            return $this->redirect(  $_SERVER['HTTP_REFERER']  );

        } catch (\Exception $e) {
            $this->Flash->error($e->getMessage());
            return $this->redirect(  $_SERVER['HTTP_REFERER']  );
        }
    }
}

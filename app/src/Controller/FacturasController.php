<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\View\CellTrait;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;

use Usermgmt\Controller\UsermgmtAppController;
use Cake\Event\Event;
use Cake\Utility\Inflector;

use Cake\Core\Configure;

use Cake\Utility\Xml;
use ZipArchive;

/**
 * Facturas Controller
 *
 * @property \App\Model\Table\FacturasTable $Facturas
 */
class FacturasController extends AppController
{
    use CellTrait;


    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = ['Usermgmt.Tinymce', 'Usermgmt.Ckeditor','Usermgmt.Search'];

    /**
     * Components
     *
     * @var array
     */
    public $components = ['Usermgmt.Search'];

    /**
     * Paginate
     *
     * @var array
     */
    public $paginate = ['limit' => '25'];

    /**
     * This controller uses search filters in following functions for ex index, online function
     *
     * @var array
     */
    public $searchFields = [
        'index'=>[
            'Facturas'=>[
                'Facturas'=>[
                    'position'=>'busqueda',
                    'type'=>'text',
                    'label'=>'Buscar',
                    'tagline'=>'<br>Busca por Folio, total, cantidad en letra, fecha de vencimiento',
                    'condition'=>'multiple',
                    'searchFields'=>['Facturas.fecha','Clientes.nombre','Clientes.rfc'],
                    'inputOptions'=>['style'=>'width:300px;']
                ],

                
                'Facturas.fecha_inicio'=>[
                    'type'=>'text',
                    'label'=>'Desde',
                    'model'=>'Facturas',
                    'inputOptions'=>['id'=>"datepicker1"]
                ],
                'Facturas.fecha_fin'=>[
                    'type'=>'text',
                    'label'=>'Hasta',
                    'model'=>'Facturas',
                    'inputOptions'=>['id'=>"datepicker2"]

                ],
                'Facturas.cliente_id'=>[
                    'type'=>'select',
                    'label'=>'Cliente',
                    'model'=>'Clientes',
                    'selector'=>'GetClientes'
                 
                   
                    
                ],
                'Clientes.tipo_cliente_id'=>[
                    'type'=>'select',
                    'label'=>'Tipo Cliente',
                    'model'=>'TipoClientes',
                    'selector'=>'getTipoClientes'
                    
                ],

                
                              
            ]
        ]
    ];

    public $fecha_inicio=null;
    public $fecha_fin=null;

    public $usuario = 'testing@solucionfactible.com';//'admin@135-bc.com'; //
    public $password = 'a0123456789';//'solrol14'; //

    public function beforeFilter(Event $event){
        if($this->request->params['action']=='index'){

            $sesion=$this->request->session();
            //debug($sesion->read('UserAuth.Search.Facturas.index.Facturas'));


            $this->fecha_inicio='';
            if(isset($this->request->data['Facturas']['fecha_inicio'])){
                //debug($this->request->data['Categorias']['id']);
                $this->fecha_inicio=$this->request->data['Facturas']['fecha_inicio'];
                unset($this->request->data['Facturas']['fecha_inicio']);
            }else{
                
                $fecha_inicio=$sesion->read('UserAuth.Search.Facturas.index.Facturas.fecha_inicio');
                if(!is_null($fecha_inicio)){
                    //debug($categorias['id']);
                    $this->fecha_inicio=$fecha_inicio;
                    
                    $sesion->delete('UserAuth.Search.Facturas.index.Facturas.fecha_inicio');
                }
            }


            $this->fecha_fin='';
            if(isset($this->request->data['Facturas']['fecha_fin'])){
                //debug($this->request->data['Categorias']['id']);
                $this->fecha_fin=$this->request->data['Facturas']['fecha_fin'];
                unset($this->request->data['Facturas']['fecha_fin']);
            }else{
                
                $fecha_fin=$sesion->read('UserAuth.Search.Facturas.index.Facturas.fecha_fin');
                if(!is_null($fecha_fin)){
                    //debug($categorias['id']);
                    $this->fecha_fin=$fecha_fin;
                    
                    $sesion->delete('UserAuth.Search.Facturas.index.Facturas.fecha_fin');
                }
            }



            if(isset($this->request->data['search_clear']) && $this->request->data['search_clear'] ==1){
                $this->fecha_inicio='';
                $this->fecha_fin='';
            }
            //debug($this->categoria_id);
            //$sesion->delete('UserAuth.Search.Productos.masiva');
            //debug($this->request->data);
        }

    }


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->loadModel('Formasdepagos');
        //debug($this->request->data);


        if(isset($this->request->data['start'])){
            $fecha_inicio = date("Y-m-d", strtotime($this->request->data['start']));
        }else{
            $this->request->data['start']='';

        }
        if(isset($this->request->data['end'])){
            $fecha_fin = date("Y-m-d", strtotime($this->request->data['end']));
        }else{
            $this->request->data['end']='';
        }
        if(isset($fecha_inicio)&&isset($fecha_fin)){
            // die(debug($fecha_inicio));
            $this->paginate = [
                'contain' => ['Clientes','Formasdepagos'],
                'conditions' => ['Facturas.created >=' => $fecha_inicio, 'Facturas.created <=' => date('Y-m-d', strtotime($fecha_fin.' + 1 days'))],
                'order' =>['fecha' => 'DESC']
            ];
            $this->Search->applySearch();
            $this->set('facturas', $this->paginate($this->Facturas));
            // $this->set('facturas', $this->Facturas->find('all',  ['contain' => ['Clientes']])
            //                                       ->where(['Facturas.created >=' => $fecha_inicio, 'Facturas.created <=' => $fecha_fin])
            //                                       ->order(['fecha' => 'DESC']));
        }
        else{

             $fecha_inicio="";
             $fecha_fin="";

            $this->paginate = [
                'contain' => ['Clientes', 'Monedas','Formasdepagos'],
                'order' =>['folio' => 'DESC']
            ];
            
            $user = $this->UserAuth->getUser();

            $conditions=[];
            
            if(!is_null($this->fecha_inicio) && $this->fecha_inicio !='' && !is_null($this->fecha_fin) && $this->fecha_fin !=''){
                $conditions=['Facturas.created >=' => $this->fecha_inicio, 'Facturas.created <=' => date('Y-m-d', strtotime($this->fecha_fin.' + 1 days'))];       
                //$this->Search->applySearch($conditions);
            }

            if( $this->UserAuth->getGroupId() != 1 ){
                $conditions = ['Facturas.centro_id' => $user['User']['centro_id']];
            }

            $this->Search->applySearch($conditions);
            $this->request->data['fecha_inicio']='';
            $this->request->data['fecha_fin']='';
            if(!is_null($this->fecha_inicio) && $this->fecha_inicio !='' && !is_null($this->fecha_fin) && $this->fecha_fin !=''){
                $sesion=$this->request->session();
                $sesion->write('UserAuth.Search.Facturas.index.Facturas.fecha_inicio',$this->fecha_inicio);
                $sesion->write('UserAuth.Search.Facturas.index.Facturas.fecha_fin',$this->fecha_fin);
                $this->request->data['fecha_inicio']=$this->fecha_inicio;
                $this->request->data['fecha_fin']=$this->fecha_fin;

            }


            //$this->Search->applySearch();
            $this->set('facturas', $this->paginate($this->Facturas));

            //$this->set('facturas', $this->Facturas->find('all',  ['contain' => ['Clientes']])->order(['Facturas.id' => 'DESC']));    

        }
        
        $this->set('formasdepagos', $this->Formasdepagos->find('list'));
        $this->set('fecha_fin',$this->request->data['fecha_fin']);
        $this->set('fecha_inicio',$this->request->data['fecha_inicio']);

        $this->set('_serialize', ['facturas']);

        if($this->request->is('ajax')) {
            $this->layout = 'ajax';
            $this->render('/Element/all_facturas');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Factura id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $factura = $this->Facturas->get($id, [
            'contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']
        ]);
        $this->set('factura', $factura);
        $this->set('_serialize', ['factura']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($id_cliente = null)
    {


        $factura = $this->Facturas->newEntity();
        if ($this->request->is('post')) {
            $factura = $this->Facturas->patchEntity($factura, $this->request->data);
            if ($this->Facturas->save($factura)) {
                $this->Flash->success(__('The factura has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The factura could not be saved. Please, try again.'));
            }
        }
        //$clientes = $this->Facturas->Clientes->find('list', ['limit' => 200]);
        $clientes = $this->Facturas->Clientes->get($id_cliente, ['contain' => ['Paises', 'Estados', 'Municipios', 'Monedas']]);
        //die(debug($clientes));
        $festados = $this->Facturas->Festados->find('list', ['limit' => 200]);
        $monedas = $this->Facturas->Monedas->find('list', ['limit' => 200]);
        $formasdepagos = $this->Facturas->Formasdepagos->find('list', ['limit' => 200]);
        $this->loadModel('Unidades');
        $unidades = $this->Unidades->find('list', ['limit' => 200]);
        $this->set(compact('factura', 'clientes', 'festados', 'monedas', 'formasdepagos', 'unidades'));
        $this->set('_serialize', ['factura']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Factura id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function pagar($id){

        //die(json_encode($this->request->data));
        $facturasTable = TableRegistry::get('Facturas');
        $factura = $facturasTable->get($id); // article with id 12

        //{"formasdepago":"3","fecha":"07\/16\/2015"}
        //$factura->formasdepago_id = $this->request->data['formasdepago'];
        //die(debug( $this->request->data['fecha']));
        $factura->fecha_pago = date("Y-m-d", strtotime($this->request->data['fecha']));
        $facturasTable->save($factura);

        exit;

    }

    public function edit($id = null)
    {
        $factura = $this->Facturas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $factura = $this->Facturas->patchEntity($factura, $this->request->data);
            if ($this->Facturas->save($factura)) {
                $this->Flash->success(__('The factura has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The factura could not be saved. Please, try again.'));
            }
        }
        $clientes = $this->Facturas->Clientes->find('list', ['limit' => 200]);
        $festados = $this->Facturas->Festados->find('list', ['limit' => 200]);
        $monedas = $this->Facturas->Monedas->find('list', ['limit' => 200]);
        $formasdepagos = $this->Facturas->Formasdepagos->find('list', ['limit' => 200]);
        $this->set(compact('factura', 'clientes', 'festados', 'monedas', 'formasdepagos'));
        $this->set('_serialize', ['factura']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Factura id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        

        //$this->request->allowMethod(['post', 'delete','get']);
        $factura = $this->Facturas->get($id);

        $this->loadModel('ClientesServicios');
        // checo uno de los servicios para determinar la accion
        $servicio = $this->ClientesServicios->find('all',array('conditions'=>array('ClientesServicios.factura_id'=>$id)))->first();

     
        if($servicio->servicio_id == 1){// factura manual

            $this->ClientesServicios->deleteAll(['factura_id' => $id]);
    
        }else{ // factura mensual

            $this->ClientesServicios->updateAll(array('factura_id'=>''), array('factura_id'=>$id));
       
        }
        
        
        if ($this->Facturas->delete($factura)) {
            $this->Flash->success(__('La Prefactura fue eliminada.'));
        } else {
            $this->Flash->error(__('La Prefactura no se pudo eliminar.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    

    public function crearTimbrado($factura_id){
        $this->loadComponent('Facturacion');
        $this->loadModel('ClientesServicios');

        $factura = $this->Facturas->find('all')
                                        ->where(['Facturas.id =' => $factura_id])
                                        ->contain(['Centros','Formasdepagos', 'ClientesServicios' => ['Unidades'], 'Clientes' => ['Municipios', 'Estados', 'Paises']])->first();

        if($factura['festado_id'] != 1){
            $resultado='{"return":{"estatus":202,"mensaje":"El comprobante ya exist\u00eda y no se ha modificado.","resultadosCreacion":{"estatus":202,"mensaje":"El comprobante ya exist\u00eda y no se ha modificado."}}}';

            die( $resultado);
        }

        $data['folio'] = $factura['folio'];
        $data['nombreSerie'] = $factura['serie'];
        $data['etiquetaComprobante'] = "Factura";

        $data['nombreCliente'] = $factura['cliente']['nombre'];
        $data['rfcCliente'] = $factura['cliente']['rfc'];
        $data['emailCliente'] = $factura['cliente']['primario_correo_electronico'];
        $data['calleFiscalCliente'] = $factura['cliente']['calle'];
        $data['numExteriorFiscalCliente'] = $factura['cliente']['numero'];
        $data['numeroInteriorFiscalCliente'] = $factura['cliente']['numero_interior'];
        $data['coloniaFiscalCliente'] = $factura['cliente']['colonia'];
        $data['municipioFiscalCliente'] = $factura['cliente']['municipio']['municipio'];
        $data['estadoFiscalCliente'] = $factura['cliente']['estado']['estado'];
        $data['paisFiscalCliente'] = $factura['cliente']['pais']['nombre'];
        $data['codigoPostalFiscalCliente'] = $factura['cliente']['codigo_postal'];

        $data['fechaPago'] = $factura['fecha'];
        $data['autorizada'] = true;
        $data['cancelada'] = false;
        //default, campo no existe
        $data['formaPago'] = 'Pago en una sola exhibición';
        $data['metodoPago'] = $factura['formasdepago']['clave'];

        
        if($factura['moneda_id'] == 2){
            $data['monedaNombre'] = 'Peso';
            $data['monedaTipoCambio'] = 1; //$factura['tipo_de_cambio'];
        }
        else{
            $data['monedaNombre'] = 'Dólar';
            $data['monedaTipoCambio'] = $factura['tipo_de_cambio'];
        }

        $data['notasFactura']=$factura['observaciones'];
        $data['notasFactura'].="\nTipo de cambio utilizado: ".$factura['tipo_de_cambio'];
        $data['notasFactura'].="\nFecha límite de pago: ".date_format($factura['fecha'],"d")." de ".$this->meses[date_format($factura['fecha'],"n")]." de ".
                                date_format($factura['fecha'],"Y");
        $data['notasFactura'].="\n";


        $data['detalleCFDI'] = [];
        foreach ($factura['clientes_servicios'] as $key => $movimiento) {
            $detalleCFDI = [];            
            $detalleCFDI['concepto'] = $movimiento['nombre'];
            $detalleCFDI['unidad'] = $movimiento['unidad']['nombre'];
            $detalleCFDI['cantidad'] = (int)$movimiento['cantidad'];
            if($factura['moneda_id'] == 2){
                $detalleCFDI['precioUnitario'] = (float)$movimiento['precio_mxn'];    
            }
            else{
                $detalleCFDI['precioUnitario'] = (float)$movimiento['precio_usd'];       
            }
            $detalleCFDI['tasaIva'] = 16;
            $detalleCFDI['comment'] = '';
            $detalleCFDI['importe'] = $detalleCFDI['cantidad'] * $detalleCFDI['precioUnitario'];

            $data['detalleCFDI'][]= $detalleCFDI;
        }

        //$result = $this->Facturacion->crear($this->usuario, $this->password, $data);
        //die(json_encode($data));
        $result = $this->Facturacion->crear($data, $factura['centro']);
        
        if($result->return->estatus == 200){
            $uuid = $result->return->resultadosCreacion->uuid;

            // OBTENER DATOS
            //$obtener_datos = $this->Facturacion->datosFactura($this->usuario, $this->password, $uuid, $factura['folio'], $factura['serie']);
            $obtener_datos = $this->Facturacion->datosFactura($uuid, $factura['folio'], $factura['serie'], $factura['centro']);

            $facturas = TableRegistry::get('Facturas');
            $query = $facturas->query();
            $query->update()
            ->set([
                'festado_id' => 2, 
                'uuid' => $uuid,
                'fecha_timbrado' => $obtener_datos->fechaEmision,
                'ano_y_no_aprobacion' => $obtener_datos->yearAprobacionSerie.$obtener_datos->numeroAprobacionSerie,
                'certificado' => $obtener_datos->numeroSerieCsd,
                'cadena_original' => $obtener_datos->cadenaOriginal,
                'sello_digital' => $obtener_datos->selloDigital,
                ])
            ->where(['id' => $factura_id])
            ->execute();
        }else{
            die(json_encode($result));
        }

        $this->descargarfacturapdf($factura_id); //Guarda PDF y XML

        die(json_encode($result));
        
    }

     public function xml($factura_id){

        $factura = $this->Facturas->get($factura_id);
        $uuid = $factura->uuid;

        $this->loadComponent('Facturacion');

        header('Content-disposition: attachment; filename="facturacion.xml"');
        header("Content-Type: application/xml; charset=utf-8");
        die($this->Facturacion->obtenerDatos($this->usuario, $this->password, $uuid, null, null));

    }

    public function pdf_old($factura_id){

        $factura = $this->Facturas->get($factura_id);
        $uuid = $factura->uuid;

        
        $cliente = $this->Facturas->Clientes->get($factura->cliente_id);
        //debug($cliente);
        $replace=array(' ',',','.');

        $nombre=str_replace($replace,'',$cliente->nombre);
        $nombre=substr($nombre, 0, 10);

        $filename="Factura_".$factura->serie."".$factura->folio."_".$nombre;

        $this->loadComponent('Facturacion');
        $pdf = $this->Facturacion->pdf($this->usuario, $this->password, $uuid, null, null,0);


        //Ahora mando a imprimir, pero con el nombre del cliente.
            header("Content-Description: File Transfer");
            header("Content-Type: application/octet-stream");
            header("Content-Transfer-Encoding: binary");
            header("Content-Disposition: attachment; filename=".$filename.".pdf");
            header('Content-Length: '.  strlen($pdf));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            echo $pdf;
        die;
    }

    public function obtenerDatos(){

        $uuid = 'E58BF47D-FB08-4CE9-BB6F-64AEE571FF5F';

        $this->loadComponent('Facturacion');
        $res = $this->Facturacion->obtenerDatos($this->usuario, $this->password, $uuid, null, null);
    }

    public function cancelar($factura_id){
        $this->loadComponent('Facturacion');

        $factura = $this->Facturas->get($factura_id, ['contain' => ['ClientesServicios', 'Centros']]);

        $uuid = $factura->uuid;

        $solicitudCancelacion = array();
        $solicitudCancelacion['folio'] = null;
        $solicitudCancelacion['serie'] = null;
        $solicitudCancelacion['uuid'] = $uuid;


        $result = $this->Facturacion->cancelar($solicitudCancelacion, $factura['centro']);

        if($result->return->estatus == 200){
            $uuid = $result->return->resultadosCancelacion->uuid;

            $facturas = TableRegistry::get('Facturas');
            $query = $facturas->query();
            $query->update()
            ->set(['festado_id' => 3])
            ->where(['id' => $factura_id])
            ->execute();

            //Tomo los servicios relacionados
            $this->loadModel('ClientesServicios');

            foreach($factura->clientes_servicios as $cs){

                //$servicio = $this->ClientesServicios->find('all',array('conditions'=>array('ClientesServicios.id'=>$cs->id)))->first();     

                $nuevoservicio = $this->ClientesServicios->newEntity();
                $nuevoservicio['cliente_id']=$cs->cliente_id;
                $nuevoservicio['servicio_id']=$cs->servicio_id;
                $nuevoservicio['factura_id']=$cs->factura_id;
                $nuevoservicio['usuario_id']=$cs->usuario_id;
                $nuevoservicio['tipo_id']=$cs->tipo_id;
                $nuevoservicio['fecha']=$cs->fecha;
                $nuevoservicio['nombre']=$cs->nombre;
                $nuevoservicio['unidad_id']=$cs->unidad_id;
                $nuevoservicio['precio_mxn']=$cs->precio_mxn;
                $nuevoservicio['precio_usd']=$cs->precio_usd;
                $nuevoservicio['descripcion']=$cs->descripcion;
                $nuevoservicio['cantidad']=$cs->cantidad;
                $nuevoservicio['notificacion']=$cs->notificacion;


                $this->ClientesServicios->updateAll(array('factura_id'=>''), array('id'=>$cs->id));
                $this->ClientesServicios->save($nuevoservicio);

            }

            $this->descargarfacturacanceladapdf($factura_id); // Genera archvio PDF

        }

        die(json_encode($result));
    }

    public function prefacturas()
    {
        
        $this->loadModel('Usermgmt.UserSettings');
        $comentario=$this->UserSettings->find('all')->where(['name'=>'ComentariosFacturas'])->first();
        //debug($comentario);
        $this->set('comentario',$comentario->value);

    }

    public function listPrefacturaMensual()
    {
        if ($this->request->is('ajax')) { 
            $this->loadModel('Clientes');
            $this->loadModel('ClientesServicios');
            //$this->ClientesServicios->recursive= -1;


            $mes = date('m', strtotime('1-'.$this->request->data['mes']));
            $año = date('Y', strtotime('1-'.$this->request->data['mes']));
            //die(debug($año));
            $ultimo_dia = cal_days_in_month(CAL_GREGORIAN, $mes, $año);

            //$fechaInicial = '01/'.$mes.'/'.$año;
            //$fechaFinal = $ultimo_dia.'/'.$mes.'/'.$año;
            $fechaInicial = $año.'-'.$mes.'-01';
            $fechaFinal = $año.'-'.$mes.'-'.$ultimo_dia.' 23:59:59';

            $usuario = $this->UserAuth->getUser();

            $conditions = [];

            $conditions = ['Clientes.activo' => 1];
            //soy usuario staff
            if ( in_array( $this->UserAuth->getGroupId(), [5,6,7] )) {
                $this->Flash->error(__('No cuenta con permisos para acceder a esta sección'));
                return $this->redirect('/accessDenied');

            }
            if ( in_array( $this->UserAuth->getGroupId(), [4,8,9] )) {
                $conditions[] = ['Clientes.centro_id' =>  $usuario['User']['centro_id'] ];
            }
            
            $clientes = $this->Clientes->find('all',['conditions'=> $conditions ]);

            $listado = array();

            foreach ($clientes as $key => $cliente) {

                $consultaSaldos = $this->ClientesServicios->find('all',array('conditions'=>array('AND' => array('cliente_id'=>$cliente['id'],'factura_id is null',array('fecha >='=>$fechaInicial,'fecha <='=>$fechaFinal))),'fields'=>array('precio_usd', 'precio_mxn', 'cantidad')));

                //$consultaSaldos = $this->ClientesServicios->find('all',array('conditions'=>array('cliente_id'=>$cliente['id']),'fields'=>array('precio_usd', 'precio_mxn')));
                
                $saldo = 0;
                $moneda = '';
                $saldoMXN = 0;
                $saldoUSD = 0;
                
                //$operacion = array('conditions'=>array('cliente_id'=>$cliente['id']), 'fields'=>array('SUM(precio_usd)'));
                foreach ($consultaSaldos as $k => $consultaSaldo) {
                        
                    if($cliente['moneda_id'] == 1) // Es dolar
                    {
                        $saldo += $consultaSaldo['precio_usd'] * $consultaSaldo['cantidad'];
                        $moneda = 'USD';
                        
                    }else{ // 2 es peso
                        $saldo += $consultaSaldo['precio_mxn'] * $consultaSaldo['cantidad'];
                        $moneda = 'MXN';
                    }
                    
                    $saldoMXN += $consultaSaldo['precio_mxn'] * $consultaSaldo['cantidad'];
                    $saldoUSD += $consultaSaldo['precio_usd'] * $consultaSaldo['cantidad'];
                }
                
                if($saldo > 0){
                $listado[$key]['id_cliente'] = $cliente['id'];
                $listado[$key]['nombre'] = $cliente['nombre'];
                $listado[$key]['rfc'] = $cliente['rfc'];
                $listado[$key]['saldo'] = $saldo;
                $listado[$key]['moneda'] = $moneda;
                $listado[$key]['saldoUSD'] = $saldoUSD;
                $listado[$key]['saldoMXN'] = $saldoMXN;
                }
            }
           
            //die(debug()); 
            die(json_encode($listado)); 
        }


    }

    public function genera_facturaManual()
    {
        $this->loadModel('Facturas');
        $this->loadModel('ClientesServicios');
        $this->loadModel('Clientes');
        $this->loadModel('Folio');

        //debug($this->request->data);
        //die;

        $folio = $this->Folio->find('all')->first();
        $folio->numero++;
        $this->Folio->save($folio);

        $datos_factura = $this->request->data['factura'];
        $datos_servicios = $this->request->data['servicios'];
        $cliente=$this->Clientes->get($datos_factura[0]);
       
         
        // guarda facturas
        $factura = $this->Facturas->newEntity();
        $factura->cliente_id = $datos_factura[0]; 
        $factura->folio = $folio->numero; 
        $factura->serie = $folio->serie; 
        //$factura->fecha = date("Y-m-".$cliente->dia_pago." H:i:s");
        $factura->fecha = date('Y-m-d H:i:s',strtotime($datos_factura[7]));
        $factura->subtotal = $datos_factura[1];  
        $factura->iva = $datos_factura[2]; 
        $factura->total = $datos_factura[3]; 
        
        $moneda="Pesos";
        if($datos_factura[4] == 1)
        {
            $moneda="Dólares";
        }
            
        $factura->cantidad_letra = $this->_num2letras($factura->total,false,true,$moneda);

        $factura->festado_id = '1';
        $factura->moneda_id = $datos_factura[4]; 
        $factura->tipo_de_cambio = $datos_factura[5];
        $factura->formasdepago_id = '1';
        $factura->observaciones = $datos_factura[6];
        $factura->centro_id = $cliente->centro_id;
        $factura->created = date('Y-m-d H:i:s');
        //die(debug($factura));
        $registroFactura = $this->Facturas->save($factura);

             
        foreach ($datos_servicios as $datos_servicio)
        {      
            $servicio = $this->ClientesServicios->newEntity();

            $servicio->cliente_id = $registroFactura->cliente_id;
            $servicio->servicio_id = '1'; // Servicio Adicional
            $servicio->factura_id = $registroFactura->id;
            $servicio->usuario_id = '1'; // Administrador PREGUNTAR
            $servicio->tipo_id = '3'; // Adicional
            $servicio->fecha = $registroFactura->fecha;
            $servicio->nombre = 'Servicios Adicionales';
            $servicio->unidad_id = $datos_servicio[0];
            $servicio->precio_mxn = $datos_servicio[2];
            $servicio->precio_usd = $datos_servicio[1];
            $servicio->descripcion = $datos_servicio[3];
            $servicio->cantidad = $datos_servicio[4];
            //die(debug($servicio));    
            $this->ClientesServicios->save($servicio);                       
        }

        $this->descargarprefacturapdf($registroFactura->id); // Genera archvio PDF

        $data = array();
        $data['confirma'] = 100;
        die(json_encode($data)); 
    }


    public function genera_facturaMasiva()
    {
        $this->loadModel('Facturas');
        $this->loadModel('ClientesServicios');
        $this->loadModel('Clientes');
        $this->loadModel('Folio');

        $folio = $this->Folio->find('all')->first();

        $clientes = $this->request->data['clientes_factura'];

        $tipo_cambio = $this->request->data['tipo_cambio']; 
        $observaciones = $this->request->data['comentario']; 

        $filas_clientes = count($clientes);

        $mes = date('m', strtotime('1-'.$this->request->data['mes']));
        $año = date('Y', strtotime('1-'.$this->request->data['mes']));
        $ultimo_dia = cal_days_in_month(CAL_GREGORIAN, $mes, $año);
        $fechaInicial = $año.'-'.$mes.'-01';
        $fechaFinal = $año.'-'.$mes.'-'.$ultimo_dia.' 23:59:59';
        
        // Guarda las Facturas
        for ($i=0; $i < $filas_clientes; $i++) { 

            $cliente = $this->Clientes->get($clientes[$i][0]);

            $factura = $this->Facturas->newEntity();

            $folio->numero++;

            $conditions = array(
                'AND' => array(
                    'cliente_id' => $clientes[$i][0],
                    'factura_id is null',
                    array(
                        'fecha >=' => $fechaInicial,
                        'fecha <='=>$fechaFinal
                    )
                )
            );


            // $clientesServicios = $this->ClientesServicios->find('all', ['conditions' => $conditions]);
            // var_dump($clientesServicios->toArray()); die();

            if ( ! $this->ClientesServicios->exists($conditions) ) {
                continue;
            }

            while($this->Facturas->exists(['folio' => $folio->numero])){
                $folio->numero++;
            }

            $factura->cliente_id = $clientes[$i][0]; 
            $factura->folio = $folio->numero; 
            $factura->serie = $folio->serie;
            $factura->fecha = date("Y-m-".$cliente->dia_pago." H:i:s");
            $factura->subtotal = $clientes[$i][1]; 
            $factura->iva = $clientes[$i][1] * .16;
            $factura->total = $factura->subtotal + $factura->iva;
            $factura->festado_id = '1';
            $factura->centro_id = $cliente->centro_id;

            if($clientes[$i][2] == 'USD')
            {
                $factura->moneda_id = '1';
                $moneda="Dólares";
            }else{
                $factura->moneda_id = '2';  
                $moneda="Pesos";
            }

            $factura->cantidad_letra = $this->_num2letras($factura->total,false,true,$moneda);

            $factura->observaciones = $observaciones;
            $factura->tipo_de_cambio = $tipo_cambio;
            $factura->formasdepago_id = '1';
            $factura->created = date('Y-m-d H:i:s');
            //die(json_encode($factura));
            $registroFactura = $this->Facturas->save($factura);
            //die(debug());
            //Agrega Folio a Clientes_Servicios
            if (!empty($registroFactura)) {
                
                $clientesServicios = $this->ClientesServicios->find('all', ['conditions' => $conditions]);

               
            
                foreach ($clientesServicios as $clientesServicio)
                {
                    //die(debug($registroFactura->id));


                    $partida = $clientesServicio;
                    //$partida = array();
                    $partida->id = $clientesServicio->id;

                    $partida->factura_id = $registroFactura->id;
                    
                    $this->ClientesServicios->save($partida);

                }

                $this->descargarprefacturapdf($registroFactura->id);// Guarda Prefactura
                
            }
        }
        
        $this->Folio->save($folio);

        $data = array();
        $data['confirma'] = 100;
        die(json_encode($data)); 
        
        //$this->redirect('/Facturas/');
        //die(debug($factura));
    }


    public function descargarpdf($id=null) {
        //Tomo la factura.
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');

        $factura = $this->Facturas->get($id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);
         
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();
       
       // die(debug($factura->unidades));
       

        //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        //App::import('Vendor', 'html2pdf', array('file' => 'html2pdf'.DS.'html2pdf.class.php'));
        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        //$body = $this->_pdf_content(compact('factura'), 'Facturas/prefactura');
        $body = $this->cell('Pdf::prefactura', ['factura' => $factura]);
        $html2pdf->WriteHTML($body);

        $html2pdf->Output('factura.pdf');
        exit;
    }

    public function enviarprefacturapdf($factura_id=null){

        set_time_limit(0);

        /*
        $comando="php ".APP."../bin/cake.php EnviaFacturas web_prefactura ".$id;
        //debug($comando);
        $respuesta= shell_exec($comando);
        //debug($respuesta);
        preg_match('/\|(.*?)\|/', $respuesta, $folio);

        if($masivo){
            return true;
            die;
        }
        */

        $factura = $this->Facturas->get($factura_id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);

        $this->loadModel('UserSettings');
        $mensaje_config=$this->UserSettings->find('all')->where(['name'=>'MensajeEnvioFactura'])->first();

        $replace=array(' ',',','.');
        $nombre=str_replace($replace,'',$factura->cliente->nombre);
        $nombre=substr($nombre, 0, 10);
        $nombre="Prefactura_".$factura->serie."".$factura->folio."_".$nombre;

        if(filter_var($factura->cliente->primario_correo_electronico, FILTER_VALIDATE_EMAIL)){

            $email = new Email('default');
            $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
            ->domain('135-bc.com')
            ->emailFormat('html')
            ->viewVars(['factura'=>$factura, 'mensaje' => $mensaje_config->value])
            ->template('enviafactura')
            ->to($factura->cliente->primario_correo_electronico)
            ->subject("Factura 135 Business Center")
            //->attachments(WWW_ROOT.'facturas/factura_'.$factura->id.'.pdf')
            ->attachments([$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/prefacturas/prefactura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf']])
            ->send();
        }

       if(filter_var($factura->cliente->secundario_correo_electronico, FILTER_VALIDATE_EMAIL)){

            $email = new Email('default');
            $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
            ->domain('135-bc.com')
            ->emailFormat('html')
            ->viewVars(['factura'=>$factura, 'mensaje' => $mensaje_config->value])
            ->template('enviafactura')
            ->to($factura->cliente->secundario_correo_electronico)
            ->subject("Factura 135 Business Center")
            //->attachments(WWW_ROOT.'facturas/factura_'.$factura->id.'.pdf')
            ->attachments([$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/prefacturas/prefactura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf']])
            ->send();
        }


        $this->Flash->success(__('La factura <strong>'.$factura->serie.''.$factura->folio.'</strong> ha sido enviada. '));
        return $this->redirect(['action' => 'index']);

    }

    public function enviarfacturapdf($factura_id){

        set_time_limit(0);
        /*
        $factura = $this->Facturas->get($factura_id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);
        // $factura = $this->Facturas->get($factura_id,[
        //     'contain' => ['Clientes','Monedas']
        // ]);
        $uuid = $factura->uuid;

       
        $cliente = $factura->cliente;
        //debug($cliente);
        $replace=array(' ',',','.');

        $nombre=str_replace($replace,'',$cliente->nombre);
        $nombre=substr($nombre, 0, 10);

        $nombre="Factura_".$factura->serie."".$factura->folio."_".$nombre;


//Tomo la factura.
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');
 
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();       

        //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        //App::import('Vendor', 'html2pdf', array('file' => 'html2pdf'.DS.'html2pdf.class.php'));
        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        //$body = $this->_pdf_content(compact('factura'), 'Facturas/prefactura');
        $body = $this->cell('Pdf::factura', ['factura' => $factura]);
        $html2pdf->WriteHTML($body);
        $filename="Factura_".$factura->serie."".$factura->folio."_".$nombre;
        //$pdf=$html2pdf->Output($filename.'.pdf');
        $pdf=$html2pdf->Output('', true);
        // //Ahora mando a imprimir, pero con el nombre del cliente.
        //     header("Content-Description: File Transfer");
        //     header("Content-Type: application/octet-stream");
        //     header("Content-Transfer-Encoding: binary");
        //     header("Content-Disposition: attachment; filename=".$filename.".pdf");
        //     header('Content-Length: '.  strlen($pdf));
        //     header('Expires: 0');
        //     header('Cache-Control: must-revalidate');
        //     header('Pragma: public');
        //     echo $pdf;
        // die;
        //exit;
        

        $this->loadComponent('Facturacion');
        //$pdf = $this->Facturacion->pdf($this->usuario, $this->password, $uuid, null, null,0);
        $xml = $this->Facturacion->obtenerDatos($this->usuario, $this->password, $uuid, null, null);


        //mando el correo
        $this->loadModel('Usermgmt.UserSettings');
        $mensaje_config=$this->UserSettings->find('all')->where(['name'=>'MensajeEnvioFactura'])->first();
        //die(debug($mensaje_config));
        $secundario='noresponder@135-bc.com';
        if($cliente->secundario_correo_electronico !=''){
            $secundario=$cliente->secundario_correo_electronico;
        }

        $email = new Email('default');

        $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
        ->domain('135-bc.com')
        ->emailFormat('html')
        ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
        ->template('enviafactura')
        ->to($cliente->primario_correo_electronico)
        ->addTo($secundario)
        ->subject("Factura 135 Business Center")
        ->attachments([$nombre.'.pdf' => ['data' => $pdf, 'mimetype' => 'application/pdf'],
                        $nombre.'.xml' => ['data' => $xml, 'mimetype' => 'application/xml']])
        ->send();

        if($masivo){
            return true;
            die;
        }
        */
        
        $factura = $this->Facturas->get($factura_id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);

        $this->loadModel('UserSettings');
        $mensaje_config=$this->UserSettings->find('all')->where(['name'=>'MensajeEnvioFactura'])->first();

        $replace=array(' ',',','.');
        $nombre=str_replace($replace,'',$factura->cliente->nombre);
        $nombre=substr($nombre, 0, 10);
        $nombre="Factura_".$factura->serie."".$factura->folio."_".$nombre;


        if(filter_var($factura->cliente->primario_correo_electronico, FILTER_VALIDATE_EMAIL)){

            $email = new Email('default');
            $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
            ->domain('135-bc.com')
            ->emailFormat('html')
            ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
            ->template('enviafactura')
            ->to($factura->cliente->primario_correo_electronico)
            ->subject("Factura 135 Business Center")
            ->attachments([$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/timbradas/factura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf'],
                            $nombre.'.xml' => ['file' => WWW_ROOT.'files/facturas/timbradas/xml_'.$factura_id.'.xml', 'mimetype' => 'application/xml']])
            ->send();
        }

        if(filter_var($factura->cliente->secundario_correo_electronico, FILTER_VALIDATE_EMAIL)){
            $email = new Email('default');
            $email->from(['noresponder@135-bc.com' => 'Notificaciones BCM'])
            ->domain('135-bc.com')
            ->emailFormat('html')
            ->viewVars(['factura'=>$factura,'mensaje'=>$mensaje_config->value])
            ->template('enviafactura')
            ->to($factura->cliente->secundario_correo_electronico)
            ->subject("Factura 135 Business Center")
            ->attachments([$nombre.'.pdf' => ['file' => WWW_ROOT.'files/facturas/timbradas/factura_'.$factura_id.'.pdf', 'mimetype' => 'application/pdf'],
                            $nombre.'.xml' => ['file' => WWW_ROOT.'files/facturas/timbradas/xml_'.$factura_id.'.xml', 'mimetype' => 'application/xml']])
            ->send();

        }


        $this->Flash->success(__('La factura <strong>'.$factura->serie."-".$factura->folio.'</strong> ha sido enviada. '));
        return $this->redirect(['action' => 'index']);
    }

    public function  enviar_seleccion(){     
    set_time_limit(0);  

    $this->loadModel('FacturasMasivas'); 
    $usuario=$this->UserAuth->getUser(); 

    foreach($this->request->data['factura_id'] as $factura_id){

        $factura_masiva = $this->FacturasMasivas->newEntity();
        $factura_masiva['factura_id'] = $factura_id;
        $factura_masiva['usuario_id'] = $usuario['User']['id'];

        $this->FacturasMasivas->save($factura_masiva);

    }
    

        $this->Flash->success(__('La selección de facturas ha sido programada para envío. '));
        return $this->redirect(['action' => 'index']);

    }

    public function  descargar_seleccion(){   
    //Configure::write(['debug'=>true]);  
    set_time_limit(0);  

        $zip = new ZipArchive();
        $filename = WWW_ROOT.'/files/descargar_facturas/facturas.zip';
        if($zip->open($filename, ZipArchive::CREATE)===TRUE) {

            foreach($this->request->data['factura_id'] as $factura_id){
    
                //Tomo la factura, para ver que voy a enviar:
                $factura = $this->Facturas->get($factura_id, ['contain' => ['Clientes']]);

                $replace=array(' ',',','.');
                $nombre=str_replace($replace,'',$factura->cliente->nombre);
                $nombre=substr($nombre, 0, 10);
                $nombre="_".$factura->serie."".$factura->folio."_".$nombre;

                if($factura->festado_id == 1){ // PREFACTURA

                    //$enlace_factura = $this->descargarprefacturapdf($factura_id,1);
                    //$nombre_factura=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura);

                    $enlace_pdf = WWW_ROOT.'/files/facturas/prefacturas/prefactura_'.$factura_id.'.pdf';
                    $zip->addFile($enlace_pdf,'Prefactura'.$nombre.'.pdf');

                }elseif($factura->festado_id == 2){ // TIMBRADA

                    //$enlace_factura = $this->descargarfacturapdf($factura_id,1);
                    //$nombre_factura=array();
                    //$nombre_factura['factura']=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura['factura']);
                    //$nombre_factura['xml']=str_replace(WWW_ROOT.'/files/descargar_facturas/', '', $enlace_factura['xml']);
                    
                    $enlace_pdf = WWW_ROOT.'/files/facturas/timbradas/factura_'.$factura_id.'.pdf';
                    $enlace_xml = WWW_ROOT.'/files/facturas/timbradas/xml_'.$factura_id.'.xml';

                    $zip->addFile($enlace_pdf,'Factura'.$nombre.'.pdf');
                    $zip->addFile($enlace_xml,'Factura'.$nombre.'.xml');
                
                }elseif($factura->festado_id == 3){ // CANCELADO

                    $enlace_pdf = WWW_ROOT.'/files/facturas/canceladas/factura_'.$factura_id.'.pdf';
                    $zip->addFile($enlace_pdf,'Factura_Cancelada'.$nombre.'.pdf');

                }

            }

            $zip->close();  
            header("Content-Type: application/zip");
            header("Content-Length: " . filesize($filename));
            header("Content-Disposition: attachment; filename=\"facturas.zip\"");
            readfile($filename);

            unlink($filename); 
            die();
        }
        
        $this->Flash->success(__('La selección de facturas descargada. '));
        return $this->redirect(['action' => 'index']);
    }

    public function descargarfacturacanceladapdf($factura_id){

        set_time_limit(0);
               
        $factura = $this->Facturas->get($factura_id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);
        
        $uuid = $factura->uuid;

        $cliente = $factura->cliente;
        
        //$replace=array(' ',',','.');

        //$nombre=str_replace($replace,'',$cliente->nombre);
        //$nombre=substr($nombre, 0, 10);

        //$nombre="Prefactura_".$factura->serie."".$factura->folio."_".$nombre;
        $nombre="factura_".$factura_id;

        //Tomo la factura.
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');
 
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();


        //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        $body = $this->cell('Pdf::factura', ['factura' => $factura]);
        $html2pdf->WriteHTML($body);
        
        //$html2pdf->Output(WWW_ROOT.'/files/descargar_facturas/'.$nombre.'.pdf', 'F');    
        $html2pdf->Output(WWW_ROOT.'/files/facturas/canceladas/'.$nombre.'.pdf', 'F');    

        //die;
    }


    public function descargarprefacturapdf($factura_id){

        set_time_limit(0);
               
        $factura = $this->Facturas->get($factura_id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);
        
        $uuid = $factura->uuid;

        $cliente = $factura->cliente;
        
        //$replace=array(' ',',','.');

        //$nombre=str_replace($replace,'',$cliente->nombre);
        //$nombre=substr($nombre, 0, 10);

        //$nombre="Prefactura_".$factura->serie."".$factura->folio."_".$nombre;
        $nombre="prefactura_".$factura_id;

        //Tomo la factura.
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');
 
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();


        //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        $body = $this->cell('Pdf::prefactura', ['factura' => $factura]);
        $html2pdf->WriteHTML($body);
        
        //$html2pdf->Output(WWW_ROOT.'/files/descargar_facturas/'.$nombre.'.pdf', 'F');    
        $html2pdf->Output(WWW_ROOT.'/files/facturas/prefacturas/'.$nombre.'.pdf', 'F');    

        //die;
    }


    public function descargarfacturapdf($factura_id){

        set_time_limit(0);
        $factura = $this->Facturas->get($factura_id, ['contain' => ['Centros','Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);
        
        $uuid = $factura->uuid;

        $cliente = $factura->cliente;
        
        //$replace=array(' ',',','.');

        //$nombre=str_replace($replace,'',$cliente->nombre);
        //$nombre=substr($nombre, 0, 10);

        //$nombre="Factura_".$factura->serie."".$factura->folio."_".$nombre;

        //Tomo la factura.
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');
 
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();       

        //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        //$body = $this->_pdf_content(compact('factura'), 'Facturas/prefactura');
        $body = $this->cell('Pdf::factura', ['factura' => $factura]);
        $html2pdf->WriteHTML($body);
        
        //$pdf=$html2pdf->Output($filename.'.pdf', true);
        //$pdf=$html2pdf->Output('', true);
        $html2pdf->Output(WWW_ROOT.'/files/facturas/timbradas/factura_'.$factura_id.'.pdf', 'F');    // Guarda Factura

        $this->loadComponent('Facturacion');

        $datos_xml = $this->Facturacion->obtenerDatos($uuid, null, null, $factura['centro']);
        $xml = Xml::build($datos_xml);
        $xml->asXML(WWW_ROOT.'/files/facturas/timbradas/xml_'.$factura_id.'.xml');  // Guarda XML

        /*if($masivo){

            $enlace_factura['factura'] = WWW_ROOT.'/files/descargar_facturas/'.$nombre.'.pdf';
            $enlace_factura['xml'] = WWW_ROOT.'/files/descargar_facturas/xml_'.$nombre.'.xml';

            return $enlace_factura;
            die;
        }*/
        
    }

    public function  n2l($total){ 
        $total=100+(rand(1,100)/100);     
        debug($total);
        $cantidad_letra = $this->_num2letras($total,false,true,'MXN');

        debug($cantidad_letra);
        die;
    }

    public function pdf($id=null) {
        //Tomo la factura.
        $this->loadModel('Estados');
        $this->loadModel('Municipios');
        $this->loadModel('Unidades');

        $factura = $this->Facturas->get($id, ['contain' => ['Clientes', 'Festados', 'Monedas', 'Formasdepagos', 'ClientesServicios', 'Partidas']]);
        if($factura->festado_id == 1){
            return $this->redirect(array('action'=>'descargarpdf',$id));
        } 
        $municipio = $this->Municipios->get($factura->cliente->municipio_id);
        $factura->municipio = $municipio->municipio;

        $estado = $this->Estados->get($factura->cliente->estado_id, ['contain' => ['Paises']]);
        $factura->estado = $estado->estado;

        $factura->pais = $estado->pais->nombre;
        
        $unidades = $this->Unidades->find('list');
        $factura->unidades = $unidades->toArray();

        $cliente = $this->Facturas->Clientes->get($factura->cliente_id);
        //debug($cliente);
        $replace=array(' ',',','.');

        $nombre=str_replace($replace,'',$cliente->nombre);
        $nombre=substr($nombre, 0, 10);
       
        //die(debug($factura));
       

        //generamos el pdf
        require_once(APP . 'Vendor' . DS  . 'html2pdf'  . DS . 'html2pdf.class.php');

        //App::import('Vendor', 'html2pdf', array('file' => 'html2pdf'.DS.'html2pdf.class.php'));
        $html2pdf = new \Html2pdf\HTML2PDF('P','LETTER','es');
        
        //$body = $this->_pdf_content(compact('factura'), 'Facturas/prefactura');
        $body = $this->cell('Pdf::factura', ['factura' => $factura]);
        $html2pdf->WriteHTML($body);
        $filename="Factura_".$factura->serie."".$factura->folio."_".$nombre;
        $html2pdf->Output($filename.'.pdf');
        // //Ahora mando a imprimir, pero con el nombre del cliente.
        //     header("Content-Description: File Transfer");
        //     header("Content-Type: application/octet-stream");
        //     header("Content-Transfer-Encoding: binary");
        //     header("Content-Disposition: attachment; filename=".$filename.".pdf");
        //     header('Content-Length: '.  strlen($pdf));
        //     header('Expires: 0');
        //     header('Cache-Control: must-revalidate');
        //     header('Pragma: public');
        //     echo $pdf;
        // die;
        exit;
    }

    public function download($id=null,$xml=0) {
        
        $path=WWW_ROOT.'files/facturas/prefacturas/';
        $sub='prefactura_';
        $ext='pdf';
        $disposition='inline';

        $this->Facturas->recursive=-1;
        
        $factura = $this->Facturas->get($id);
        if($factura->festado_id == 2){
            $path=WWW_ROOT.'/files/facturas/timbradas/';
            $sub='factura_';
        }elseif($factura->festado_id == 3){
            $path=WWW_ROOT.'/files/facturas/canceladas/';
            $sub='factura_';
        }

        if($xml){
            $ext='xml';
            $sub='xml_';
            $disposition='attachment';
        }

        $cliente = $this->Facturas->Clientes->get($factura->cliente_id);
        //debug($cliente);
        $replace=array(' ',',','.');

        $nombre=Inflector::slug($cliente->nombre);
        $nombre=str_replace($replace,'',$nombre);
        $nombre=substr($nombre, 0, 10);

        $filename=$sub.$factura->serie."".$factura->folio."_".$nombre;
        
        $this->response->file($path.$sub.$id.'.'.$ext, ['download'=>false, 'name'=>$filename.'.'.$ext]);
        header("Content-Disposition:$disposition;filename=$filename.$ext");

        return $this->response;
    }


    public function cambiarformapagocuentabanco($factura_id){

        
           

            $factura = $this->Facturas->get($factura_id, [
            'contain' => []
            ]);

            if ($this->request->is(['patch', 'post', 'put'])) {
                    
                $factura->formasdepago_id = $this->request->data['formasdepago_id'];
                $factura->cuentabanco = $this->request->data['cuentabanco'];

                if( $this->Facturas->save($factura) ){
                    $this->Flash->success(__('Forma de pago o cuenta de banco actualizada'));
                }

                
                return $this->redirect( ['action' => 'index'] );

            }

            $this->loadModel('Formasdepagos');

            $formasdepagos = $this->Formasdepagos->find('list');

            $this->set( compact( 'formasdepagos','factura' ) );

    }   


}

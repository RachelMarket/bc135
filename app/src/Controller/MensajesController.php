<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Mensajes Controller
 *
 * @property \App\Model\Table\MensajesTable $Mensajes
 */
class MensajesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    // Mensajes
    public function listMensajes()
    {
      //if ($this->request->is('ajax')) { 
        //$usuario = $this->request->data['usuario'];

        $usuario = $this->UserAuth->getUser(); 

        $this->loadModel('Mensajes');

        $mensajes_usuario = $this->Mensajes->find('all', ['limit' => 200, 'conditions'=> ["Mensajes.usuario_id =".$usuario['User']['id'], "Mensajes.visto = 0"], 'contain' => ['Usuarios'], 'order' => ['Mensajes.id DESC']]);

        //$datos_mensajes = array();
        //$datos_mensajes['cantidad'] = count($mensajes_usuario->toArray());
        //$datos_mensajes['mensajes'] = $mensajes_usuario;

        //$datos_mensajes['mensajes'][0]['mensaje']
        die(json_encode($mensajes_usuario)); 
       
      //}
    }

    public function index()
    {
        //$this->paginate = [
        //    'contain' => ['Usuarios']
        //];
        //$this->set('mensajes', $this->paginate($this->Mensajes));
        //$this->set('_serialize', ['mensajes']);

        $usuario = $this->UserAuth->getUser();
        $mensajes_usuario = $this->Mensajes->find('all', ['limit' => 200, 'conditions'=> ["Mensajes.usuario_id =".$usuario['User']['id'], "Mensajes.eliminado = 0"], 'contain' => ['Usuarios'], 'order' => ['Mensajes.id DESC']]);

        $mensajes_enviados = $this->Mensajes->find('all', ['limit' => 200, 'conditions'=> ["Mensajes.envia =".$usuario['User']['id']], 'contain' => ['Usuarios'], 'order' => ['Mensajes.id DESC']]);

        $mensajes_eliminados = $this->Mensajes->find('all', ['limit' => 200, 'conditions'=> ["Mensajes.usuario_id =".$usuario['User']['id'], "Mensajes.eliminado = 1"], 'contain' => ['Usuarios'], 'order' => ['Mensajes.id DESC']]);
        //die(debug($mensajes_usuario->toArray()));

        $mensaje = $this->Mensajes->newEntity();
        if ($this->request->is('post')) {
            foreach($this->request->data['usuario_id'] as $usuario_id){
                $mensaje = $this->Mensajes->newEntity();
                $mensaje = $this->Mensajes->patchEntity($mensaje, array('usuario_id' =>$usuario_id, 'mensaje' => $this->request->data['mensaje']));
                $mensaje->fecha = date("Y-d-m H:i:s");
                $mensaje->envia = $usuario['User']['id'];

                //die(debug($mensaje));

                $this->Mensajes->save($mensaje);
                $this->Flash->success(__('El mensaje fue enviado.'));
                
            }           
        }

        $this->loadModel('Usuarios');
        $usuarios = $this->Usuarios->find('list', ['limit' => 200])->where(['OR'=>[['user_group_id'=>1],['user_group_id'=>4]]]);
        $usuarios = $usuarios->toArray();

        //die(debug($usuarios->toArray()));
        $this->set(compact('mensaje', 'usuarios', 'mensajes_usuario', 'mensajes_enviados', 'mensajes_eliminados'));
    }

    /**
     * View method
     *
     * @param string|null $id Mensaje id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mensaje = $this->Mensajes->get($id, [
            'contain' => ['Usuarios']
        ]);
        $this->set('mensaje', $mensaje);
        $this->set('_serialize', ['mensaje']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mensaje = $this->Mensajes->newEntity();
        if ($this->request->is('post')) {
            $mensaje = $this->Mensajes->patchEntity($mensaje, $this->request->data);
            if ($this->Mensajes->save($mensaje)) {
                $this->Flash->success(__('The mensaje has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The mensaje could not be saved. Please, try again.'));
            }
        }
        $usuarios = $this->Mensajes->Usuarios->find('list', ['limit' => 200]);
        $this->set(compact('mensaje', 'usuarios'));
        $this->set('_serialize', ['mensaje']);
    }

    public function eliminarMensaje($id = null)
    {

        $mensaje = $this->Mensajes->get($id, ['contain' => []]);

        $mensaje->eliminado = 1;
        $mensaje->visto = 1;

            if ($this->Mensajes->save($mensaje)) {
                $this->Flash->success(__('El mensaje se elimino.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('El mensaje no se pudo eliminar.'));
            }
    }

    public function vistoMensaje($id = null)
    {

        $mensaje = $this->Mensajes->get($id, ['contain' => []]);

        $mensaje->visto = 1;

            if ($this->Mensajes->save($mensaje)) {
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('El mensaje no se pudo marcar como visto.'));
            }
    }
    /**
     * Edit method
     *
     * @param string|null $id Mensaje id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mensaje = $this->Mensajes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mensaje = $this->Mensajes->patchEntity($mensaje, $this->request->data);
            if ($this->Mensajes->save($mensaje)) {
                $this->Flash->success(__('The mensaje has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The mensaje could not be saved. Please, try again.'));
            }
        }
        $usuarios = $this->Mensajes->Usuarios->find('list', ['limit' => 200]);
        $this->set(compact('mensaje', 'usuarios'));
        $this->set('_serialize', ['mensaje']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mensaje id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mensaje = $this->Mensajes->get($id);
        if ($this->Mensajes->delete($mensaje)) {
            $this->Flash->success(__('El mensaje se elimino.'));
        } else {
            $this->Flash->error(__('El mensaje no se pudo eliminar.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

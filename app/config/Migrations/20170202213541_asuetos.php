<?php

use Phinx\Migration\AbstractMigration;

class Asuetos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function up()
    {


        $table = $this->table('asuetos');
        

        $table->addColumn('dia', 'date' );

        $table ->addColumn('created', 'datetime');

        $table->addColumn('user_id', 'integer' ,[
            'limit' => 11,
            'null' => false]);

        $table->create();

        $table->addIndex(array('dia'));
        



    }

    public function down(){

         $this->execute('SET foreign_key_checks = 0;');


        $table = $this->table('asuetos')->drop();
       
        

        $this->execute('SET foreign_key_checks = 1;');

    }
}

<?php

use Phinx\Migration\AbstractMigration;

class UpdateTypeDataOfTarjetasAcceso extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('tarjetas_acceso');
        $table->changeColumn('num_tarjeta', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ]);
        $table->update();
    }
}

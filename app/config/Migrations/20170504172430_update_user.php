<?php

use Phinx\Migration\AbstractMigration;

class UpdateUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('centro_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true])
        ->update();
    }
}

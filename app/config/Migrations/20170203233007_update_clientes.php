<?php

use Phinx\Migration\AbstractMigration;

class UpdateClientes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('clientes');
        $table->addColumn('saldo', 'decimal', [
            'default' => null,
            'precision' => 10,
            'scale' => 2,
            'null' => true])
        ->update();
    }
}

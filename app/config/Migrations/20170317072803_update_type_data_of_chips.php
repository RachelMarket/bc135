<?php

use Phinx\Migration\AbstractMigration;

class UpdateTypeDataOfChips extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('chips');
        $table->changeColumn('numero', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ]);
        $table->update();
    }
}

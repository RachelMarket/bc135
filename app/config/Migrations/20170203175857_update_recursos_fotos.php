<?php

use Phinx\Migration\AbstractMigration;

class UpdateRecursosFotos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('recursos_fotos');
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true]);
        $table->addColumn('name_real', 'string', [
            'default' => null,
            'limit' => 250,
            'null' => true]);
        $table->addColumn('size', 'string', [
            'default' => null,
            'limit' => 250,
            'null' => true])
        ->update();

    }
}

<?php

use Phinx\Migration\AbstractMigration;

class UpdateRecursos13Mar2017 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $table = $this->table('recursos');
        $table->changeColumn('hora_inicio', 'datetime');
        $table->changeColumn('hora_fin', 'datetime');
        $table->update();
    }
}

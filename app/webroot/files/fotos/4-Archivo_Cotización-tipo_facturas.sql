/*
 Navicat Premium Data Transfer

 Source Server         : andamios
 Source Server Type    : MySQL
 Source Server Version : 50505
 Source Host           : 172.99.97.15
 Source Database       : 587580_andamiosdev

 Target Server Type    : MySQL
 Target Server Version : 50505
 File Encoding         : utf-8

 Date: 01/27/2017 17:33:54 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `tipo_facturas`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_facturas`;
CREATE TABLE `tipo_facturas` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(90) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Records of `tipo_facturas`
-- ----------------------------
BEGIN;
INSERT INTO `tipo_facturas` VALUES ('1', 'Inicio de Contrato', 'Inicio de Contrato', 'Renta'), ('2', 'Quincenal', 'Quincenal', 'Renta'), ('3', 'Prorroga', 'Prorroga', 'Renta'), ('4', 'Extension', 'Extension', 'Renta'), ('5', 'Venta', 'Venta', 'Venta'), ('6', 'Reposicion', 'Reposicion', 'Venta'), ('7', 'Maniobras', 'Maniobras', 'Venta'), ('8', 'Otro', 'Otro', 'Venta'), ('9', 'Garantía', 'Garantía', 'Venta');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

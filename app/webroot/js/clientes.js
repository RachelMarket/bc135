 window.count = 0;

 $('#add-client').on('click', function(){

    var obj = {
                "#estado-id"    :  $('#estado-id option:selected'),
                "#municipio-id" :  $('#municipio-id option:selected')
              };

    $.each(obj, function( key, value){
        
        if( value.val() != ''){ window.count++; } else {
          $(key).parent().find('label').css('color','#ed5565');

          if( $(key).parent().find('p').length == 0 ){
            if( $(key).parent().find('div').length == 0 ){
              $(key).parent().append('<div style="color:#a94442" class"message-error">This field cannot be left empty</div>');
            }
          }
        }
    });

    if( window.count > 1  && $('#estado-id option:selected').val() != '' && $('#municipio-id option:selected').val() != '' ){ 
      $('#FormAddClient').submit(); 
    } 

});


 $('.input-select').on('change', function(){

      if( $(this).val() != '' ){
          var parent = $(this).parent();
          parent.find('label').css('color','#b3b3b3');
          parent.find('div').remove();
      }
 }); 
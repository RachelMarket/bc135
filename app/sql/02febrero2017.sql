/*
 Navicat Premium Data Transfer

 Source Server         : BC135
 Source Server Type    : MySQL
 Source Server Version : 50505
 Source Host           : 172.99.97.155
 Source Database       : 587580_bc135

 Target Server Type    : MySQL
 Target Server Version : 50505
 File Encoding         : utf-8

 Date: 02/02/2017 15:39:03 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `recursos`
-- ----------------------------
DROP TABLE IF EXISTS `recursos`;
CREATE TABLE `recursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `descripcion` text,
  `centro_id` int(11) NOT NULL,
  `capacidad` int(11) DEFAULT NULL,
  `tiempo_minimo` int(11) DEFAULT NULL,
  `tipo_id` int(11) NOT NULL,
  `hora_inicio` date NOT NULL,
  `hora_fin` date NOT NULL,
  `minutos_espera` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;



/*
 Navicat Premium Data Transfer

 Source Server         : BC135
 Source Server Type    : MySQL
 Source Server Version : 50505
 Source Host           : 172.99.97.155
 Source Database       : 587580_bc135

 Target Server Type    : MySQL
 Target Server Version : 50505
 File Encoding         : utf-8

 Date: 02/02/2017 15:39:12 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `recursos_fotos`
-- ----------------------------
DROP TABLE IF EXISTS `recursos_fotos`;
CREATE TABLE `recursos_fotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recurso_id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;

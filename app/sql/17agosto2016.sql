ALTER TABLE `formasdepagos` ADD COLUMN `clave` varchar(4) NOT NULL AFTER `nombre`;

ALTER TABLE `formasdepagos` CHANGE COLUMN `clave` `clave` varchar(4) NOT NULL DEFAULT '99';


ALTER TABLE `clientes` ADD COLUMN `formasdepago` int(11) NOT NULL DEFAULT '10' AFTER `centro_id`, ADD COLUMN `cuentabanco` varchar(20) NOT NULL AFTER `formasdepago`;

ALTER TABLE `clientes` CHANGE COLUMN `formasdepago` `formasdepago_id` int(11) NOT NULL DEFAULT '10', ADD INDEX `id` (id);

ALTER TABLE `clientes` ADD INDEX `formaspago_id` (formasdepago_id);

ALTER TABLE `clientes` ADD CONSTRAINT `fk_formaspago` FOREIGN KEY (`formasdepago_id`) REFERENCES `formasdepagos` (`id`);

ALTER TABLE `facturas` ADD COLUMN `cuentabanco` varchar(20) AFTER `sello_digital`;

ALTER TABLE `facturas` ADD INDEX `formaspago_id` (formasdepago_id);

ALTER TABLE `facturas` ADD CONSTRAINT `fk_formaspago_id` FOREIGN KEY (`formasdepago_id`) REFERENCES `formasdepagos` (`id`);


DROP TABLE IF EXISTS `formasdepagos`;
CREATE TABLE `formasdepagos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `clave` varchar(4) NOT NULL DEFAULT '99',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `formasdepagos`
-- ----------------------------
BEGIN;
INSERT INTO `formasdepagos` VALUES ('1', 'Otros', '99'), ('2', 'Efectivo', '01'), ('3', 'Transferencia', '03'), ('4', 'Cheque', '02'), ('5', 'Tarjeta de Crédito', '04'), ('6', 'Monederos electrónicos', '05'), ('7', 'Dinero electrónico', '06'), ('8', 'Vales de despensa', '08'), ('9', 'Tarjeta de Débito', '28'), ('10', 'Tarjeta de Servicio', '29');
COMMIT;
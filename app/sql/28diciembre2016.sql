/*
 Navicat Premium Data Transfer

 Source Server         : BC135
 Source Server Type    : MySQL
 Source Server Version : 50505
 Source Host           : 172.99.97.155
 Source Database       : 587580_bc135

 Target Server Type    : MySQL
 Target Server Version : 50505
 File Encoding         : utf-8

 Date: 12/28/2016 12:08:28 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `lugares_did`
-- ----------------------------
DROP TABLE IF EXISTS `lugares_did`;
CREATE TABLE `lugares_did` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lugar` varchar(250) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `assigned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `lugares_did`
-- ----------------------------
BEGIN;
INSERT INTO `lugares_did` VALUES ('1', 'Lugar 3', '2016-12-28 11:38:50', '2016-12-28 12:05:20', '1', '0'), ('3', 'Lugar 2', '2016-12-28 12:04:41', '2016-12-28 12:04:41', '0', '0'), ('4', 'Lugar 4', '2016-12-28 12:05:01', '2016-12-28 12:05:01', '0', '0'), ('5', 'luga', '2016-12-28 12:05:08', '2016-12-28 12:05:34', '1', '0');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

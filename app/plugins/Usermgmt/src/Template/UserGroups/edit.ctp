<?php
/* Cakephp 3.x User Management Premium Version (a product of Ektanjali Softwares Pvt Ltd)
Website- http://ektanjali.com
Plugin Demo- http://cakephp3-user-management.ektanjali.com/
Author- Chetan Varshney (The Director of Ektanjali Softwares Pvt Ltd)
Plugin Copyright No- 11498/2012-CO/L

UMPremium is a copyrighted work of authorship. Chetan Varshney retains ownership of the product and any copies of it, regardless of the form in which the copies may exist. This license is not a sale of the original product or any copies.

By installing and using UMPremium on your server, you agree to the following terms and conditions. Such agreement is either on your own behalf or on behalf of any corporate entity which employs you or which you represent ('Corporate Licensee'). In this Agreement, 'you' includes both the reader and any Corporate Licensee and Chetan Varshney.

The Product is licensed only to you. You may not rent, lease, sublicense, sell, assign, pledge, transfer or otherwise dispose of the Product in any form, on a temporary or permanent basis, without the prior written consent of Chetan Varshney.

The Product source code may be altered (at your risk)

All Product copyright notices within the scripts must remain unchanged (and visible).

If any of the terms of this Agreement are violated, Chetan Varshney reserves the right to action against you.

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Product.

THE PRODUCT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE PRODUCT OR THE USE OR OTHER DEALINGS IN THE PRODUCT. */
?>

<div class="um-panel">
	<div class="ibox-title">
    <h2><?php echo __('Edit Group') ?></h2><?php $page = (isset($this->request->query['page'])) ? $this->request->query['page'] : 1; ?>
			<?php echo $this->Html->link(__('Back', true), ['action'=>'index', 'page'=>$page], ['class'=>'btn btn-cancel btn-back']);?>
  </div>

  <div class="ibox-content">
  	<?php echo $this->element('Usermgmt.ajax_validation', ['formId'=>'editUserGroupForm', 'submitButtonId'=>'editUserGroupSubmitBtn']);?>
  	<div class="row ca-forms mtp-40">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
          <div class="col-md-12">
          	<?php echo $this->Form->create($userGroupEntity, ['id'=>'editUserGroupForm', 'class'=>'form-horizontal', 'novalidate'=>true]);?>
          </div>
          <div class="col-md-6">
          	<div class="form-group">
							<label class="required"><?php echo __('Group Name');?></label>
							<span class='tagline'><?php echo __('for ex. Business User');?>!</span>
								<?php echo $this->Form->input('UserGroups.name', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']);?>
						</div>
          </div>
          <div class="col-md-6">
          	<label class=""><?php echo __('Parent Group');?></label>
          	<?php echo $this->Form->input('UserGroups.parent_id', ['type'=>'select', 'options'=>$parentGroups, 'label'=>false, 'div'=>false, 'class'=>'form-control']);?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          	<label class=""><?php echo __('Description');?></label>
						<?php echo $this->Form->input('UserGroups.description', ['type'=>'textarea', 'label'=>false, 'div'=>false, 'class'=>'form-control']);?>
          </div>
          <div class="col-md-12">
          	<label class=""><?php echo __('Allow Registration');?></label>
          	<?php echo $this->Form->input('UserGroups.registration_allowed', ['type'=>'checkbox', 'label'=>false, 'div'=>false, 'autocomplete'=>'off', 'style'=>'margin-left:0px;']);?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          	<div class="last-btns">
            	<?php echo $this->Html->link(__d('usermgmt', 'Back'), ['action'=>'index'], ['div'=>false, 'class'=>'btn btn-cancel']); ?>
            	<?php echo $this->Form->Submit(__('Save'), ['class'=>'btn btn-save', 'id'=>'editUserGroupSubmitBtn']);?>
            </div>
          </div>
        </div>
        <?php echo $this->Form->end();?>
			</div>
		</div>
	</div>
</div>
<?php
  /* Cakephp 3.x User Management Premium Version (a product of Ektanjali Softwares Pvt Ltd)
  Website- http://ektanjali.com
  Plugin Demo- http://cakephp3-user-management.ektanjali.com/
  Author- Chetan Varshney (The Director of Ektanjali Softwares Pvt Ltd)
  Plugin Copyright No- 11498/2012-CO/L
  
  UMPremium is a copyrighted work of authorship. Chetan Varshney retains ownership of the product and any copies of it, regardless of the form in which the copies may exist. This license is not a sale of the original product or any copies.
  
  By installing and using UMPremium on your server, you agree to the following terms and conditions. Such agreement is either on your own behalf or on behalf of any corporate entity which employs you or which you represent ('Corporate Licensee'). In this Agreement, 'you' includes both the reader and any Corporate Licensee and Chetan Varshney.
  
  The Product is licensed only to you. You may not rent, lease, sublicense, sell, assign, pledge, transfer or otherwise dispose of the Product in any form, on a temporary or permanent basis, without the prior written consent of Chetan Varshney.
  
  The Product source code may be altered (at your risk)
  
  All Product copyright notices within the scripts must remain unchanged (and visible).
  
  If any of the terms of this Agreement are violated, Chetan Varshney reserves the right to action against you.
  
  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Product.
  
  THE PRODUCT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE PRODUCT OR THE USE OR OTHER DEALINGS IN THE PRODUCT. */
  ?>

<div id="updateUsersIndex">
  <?php 
    $buttons = [];
        
    echo $this->Search->searchForm('Users', ['legend'=>false, 'updateDivId'=>'updateUsersIndex', 'custom'=>true, 'buttons'=>$buttons]); 
  ?>

  <?php echo $this->element('Usermgmt.paginator', ['updateDivId'=>'updateUsersIndex']); ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins no-mar">
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table ca-table">
              <thead>
                <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
                  <th>
                    <h5><?php echo __d('usermgmt', '#'); ?></h5>
                  </th>
                  <th>
                    <h5><?php echo  __d('usermgmt', 'Id'); ?></h5>
                  </th>
                  <th>
                    <h5><?php echo __d('usermgmt', 'Name'); ?></h5>
                  </th>
                  <!--<th><h5><?php echo __d('usermgmt', 'Username'); ?></h5></th>-->
                  <th>
                    <h5><?php echo __d('usermgmt', 'Email'); ?></h5>
                  </th>
                  <th>
                    <h5><?php echo __d('usermgmt', 'Groups(s)'); ?></h5>
                  </th>
                  <!--<th><h5><?php echo __d('usermgmt', 'Email Verified'); ?></h5></th>-->
                  <th>
                    <h5><?php echo __d('usermgmt', 'Status'); ?></h5>
                  </th>
                  <!--<th><h5><?php echo __d('usermgmt', 'Created'); ?></h5></th>-->
                  <th>
                    <h5><?php echo __d('usermgmt', 'Action'); ?></h5>
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php	if(!empty($users)) {
                  $page = $this->request->params['paging']['Users']['page'];
                  $limit = $this->request->params['paging']['Users']['perPage'];
                  $i = ($page-1) * $limit;
                  foreach($users as $row) {
                  	$i++;
                  	echo "<tr>";
                  		echo "<td><p>".$i."</p></td>";
                  		echo "<td><p>".$row['id']."</p></td>";
                  		echo "<td><p>".h($row['first_name']).' '.h($row['last_name'])."</p></td>";
                  		/*echo "<td><p>".h($row['username'])."</p></td>";*/
                  		echo "<td><p>".h($row['email'])."</p></td>";
                  		echo "<td><p><span class='label'>".$row['user_group_name']."</span></p></td>";
                  		/*echo "<td><p>";
                  			if($row['email_verified']) {
                  				echo "<span class='label label-success'>".__d('usermgmt', 'Yes')."</span>";
                  			} else {
                  				echo "<span class='label label-danger'>".__d('usermgmt', 'No')."</span>";
                  			}
                  		echo"</p></td>";*/
                  		echo "<td><p>";
                  			if($row['active']) {
                  				echo "<span class='label label-success'>".__d('usermgmt', 'Active')."</span>";
                  			} else {
                  				echo "<span class='label label-danger'>".__d('usermgmt', 'Inactive')."</span>";
                  			}
                  		echo"</p></td>";
                  		/*echo "<td><p>".$row['created']."</p></td>";*/
                  		echo "<td><p>";
                  			echo "<div class='btn-group'>";
                  				echo "<button class='btn btn-actions dropdown-toggle' data-toggle='dropdown'>".__d('usermgmt', 'Action')."</button>";
                  				echo "<ul class='dropdown-menu'>";
                  					echo "<li>".$this->Html->link(__d('usermgmt', 'View User'), ['controller'=>'Users', 'action'=>'viewUser', $row['id'], 'page'=>$page], ['escape'=>false])."</li>";
                  					echo "<li>".$this->Html->link(__d('usermgmt', 'Edit User'), ['controller'=>'Users', 'action'=>'editUser', $row['id'], 'page'=>$page], ['escape'=>false])."</li>";
                  					echo "<li>".$this->Html->link(__d('usermgmt', 'Change Password'), ['controller'=>'Users', 'action'=>'changeUserPassword', $row['id'], 'page'=>$page], ['escape'=>false])."</li>";
                  					if($row['id'] != 1 && strtolower($row['username']) != 'admin') {
                  						if($row['active']) {
                  							echo "<li>".$this->Form->postLink(__d('usermgmt', 'Inactivate'), ['controller'=>'Users', 'action'=>'setInactive', $row['id'], 'page'=>$page], ['escape'=>false, 'confirm'=>__d('usermgmt', 'Are you sure you want to inactivate this user?')])."</li>";
                  						} else {
                  							echo "<li>".$this->Form->postLink(__d('usermgmt', 'Activate'), ['controller'=>'Users', 'action'=>'setActive', $row['id'], 'page'=>$page], ['escape'=>false, 'confirm'=>__d('usermgmt', 'Are you sure you want to activate this user?')])."</li>";
                  						}
                  						if(!$row['email_verified']) {
                  							echo "<li>".$this->Form->postLink(__d('usermgmt', 'Verify Email'), ['action'=>'verifyEmail', $row['id'], 'page'=>$page], ['escape'=>false, 'confirm'=>__d('usermgmt', 'Are you sure you want to verify email of this user?')])."</li>";
                  						}
                  						echo "<li>".$this->Form->postLink(__d('usermgmt', 'Delete User'), ['controller'=>'Users', 'action'=>'deleteUser', $row['id'], 'page'=>$page], ['escape'=>false, 'confirm'=>__d('usermgmt', 'Are you sure you want to delete this user?')])."</li>";
                  					}
                  					echo "<li>".$this->Html->link(__d('usermgmt', 'View Permissions'), ['controller'=>'Users', 'action'=>'viewUserPermissions', $row['id'], 'page'=>$page], ['escape'=>false])."</li>";
                  					echo "<li>".$this->Html->link(__d('usermgmt', 'Send Mail'), ['controller'=>'UserEmails', 'action'=>'sendToUser', $row['id'], 'page'=>$page], ['escape'=>false])."</li>";
                  				echo "</ul>";
                  			echo "</div>";
                  		echo "</p></td>";
                  	echo "</tr>";
                  }
                  } else {
                  echo "<tr><td colspan=10><br/><br/>".__d('usermgmt', 'No Records Available')."</td></tr>";
                  } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if(!empty($users)) {
    echo $this->element('Usermgmt.pagination', ['paginationText'=>__d('usermgmt', 'Number of Users')]);
    }?>
</div>
<script type="text/javascript">
  $(document).ready(function() {
      $('.dataTables-example').dataTable({
          responsive: true,
          "paging":   false,
          "info":     false,
          "dom": 'T<"clear">lfrtip',
          "lengthChange": false,
           "footerCallback": function( tfoot, data, start, end, display ) {
              $(tfoot).html( "" );
          }
      });
  });
  
</script>
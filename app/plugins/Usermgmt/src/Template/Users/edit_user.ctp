<?php
/* Cakephp 3.x User Management Premium Version (a product of Ektanjali Softwares Pvt Ltd)
Website- http://ektanjali.com
Plugin Demo- http://cakephp3-user-management.ektanjali.com/
Author- Chetan Varshney (The Director of Ektanjali Softwares Pvt Ltd)
Plugin Copyright No- 11498/2012-CO/L

UMPremium is a copyrighted work of authorship. Chetan Varshney retains ownership of the product and any copies of it, regardless of the form in which the copies may exist. This license is not a sale of the original product or any copies.

By installing and using UMPremium on your server, you agree to the following terms and conditions. Such agreement is either on your own behalf or on behalf of any corporate entity which employs you or which you represent ('Corporate Licensee'). In this Agreement, 'you' includes both the reader and any Corporate Licensee and Chetan Varshney.

The Product is licensed only to you. You may not rent, lease, sublicense, sell, assign, pledge, transfer or otherwise dispose of the Product in any form, on a temporary or permanent basis, without the prior written consent of Chetan Varshney.

The Product source code may be altered (at your risk)

All Product copyright notices within the scripts must remain unchanged (and visible).

If any of the terms of this Agreement are violated, Chetan Varshney reserves the right to action against you.

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Product.

THE PRODUCT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE PRODUCT OR THE USE OR OTHER DEALINGS IN THE PRODUCT. */
?>
<script type="text/javascript">
$(document).ready(function(e) {
	if($.fn.chosen) {
		$("#users-user-group-id").chosen();
	}
});
</script>

<div class="um-panel">
	<div class="ibox-title">
    <h2><?php echo __d('usermgmt', 'Edit User'); ?></h2>
    <?php echo $this->Html->link(__d('usermgmt', 'Back'), ['action'=>'index'], ['class'=>'btn btn-cancel btn-back']); ?>
  </div>
  
  <div class="ibox-content">
    <?php echo $this->element('Usermgmt.ajax_validation', ['formId'=>'editUserForm', 'submitButtonId'=>'editUserSubmitBtn']); ?>
    <div class="row ca-forms mtp-40">
      <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
          <div class="col-md-6">
	          <?php echo $this->Form->create($userEntity, ['id'=>'editUserForm', 'class'=>'form-horizontal', 'type'=>'file']);?>
	          <div class="sp-dp">
	            <label class="col-sm-2 control-label required"><?php echo __('Group');?></label>
	            <div>
	              <?php echo $this->Form->input('Users.user_group_id', ['type'=>'select', 'label'=>false, 'div'=>false, 'multiple'=>true, 'class'=>'form-control', 'data-placeholder'=>'Select Group(s)']);?>
	            </div>
	          </div>
	        </div>
	        <div class="col-md-6">
            <div class="form-group">
              <label class="required"><?php echo __d('usermgmt', 'Username'); ?></label>
              <div>
                <?php echo $this->Form->input('Users.username', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?php 
							$encontrado = false;
							foreach ($userEntity['user_group_id'] as $key => $groupId) : ?>
								<?php if( in_array( $groupId , [5,6]  )){
									$encontrado  = true;
									} ?>
							<?php endforeach; ?>
								
							<?php if( $encontrado ): ?>
							<div class="form-group">
								<label class="required"><?php echo __('Cliente');?></label>
								<div>
									<?php echo $this->Form->input('Users.cliente_id', ['options' => $clientes ,  'label'=>false, 'div'=>false, 'class'=>'form-control']);?>
								</div>
							</div>

							<script type="text/javascript">
							$(document).ready(function(e) {
								if($.fn.chosen) {
									$("#users-cliente-id").chosen();
								}
							});
							</script>
						<?php endif; ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="required"><?php echo __d('usermgmt', 'First Name'); ?></label>
              <div>
                <?php echo $this->Form->input('Users.first_name', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']); ?>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="required"><?php echo __d('usermgmt', 'Last Name'); ?></label>
              <div>
                <?php echo $this->Form->input('Users.last_name', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']); ?>
              </div>
            </div>
          </div>
        </div>

				<div class="row">
					<div class="col-md-6">
            <div class="form-group">
              <label class="required"><?php echo __d('usermgmt', 'Email'); ?></label>
              <div>
                <?php echo $this->Form->input('Users.email', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']); ?>
              </div>
            </div>
          </div>
				  <div class="col-md-6">
				    <div class="form-group slipt-radio">
				      <label class="required"><?php echo __('Gender');?></label>
				      <div>
				        <?php echo $this->Form->input('Users.gender', ['type'=>'radio', 'label'=>'false', 'div'=>'false', 'options'=>$genders, 'class'=>'form-control']);?>
				      </div>
				    </div>
				  </div>
				</div>

				<div class="row">
					<div class="col-md-6">
				    <div class="form-group">
				      <label class="required"><?php echo __('Birthday');?></label>
				      <div>
				        <?php echo $this->Form->input('Users.bday', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control datepicker']);?>
				      </div>
				    </div>
				  </div>
				  <div class="col-md-6">
				    <div class="form-group">
				      <label class="required"><?php echo __('Photo');?></label>
				      <div>
				        <?php echo $this->Form->input('Users.photo_file', ['type'=>'file', 'label'=>false, 'div'=>false]);?>
				      </div>
				    </div>
				  </div>
				</div>

				<div class="row">
					<div class="col-md-6">
				    <div class="form-group">
				      <label class="required"><?php echo __('Location');?></label>
				      <div>
				        <?php echo $this->Form->input('Users.user_detail.location', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']);?>
				      </div>
				    </div>
				  </div>
				  <div class="col-md-6">
				    <div class="form-group">
				      <label class="required"><?php echo __('Cellphone');?></label>
				      <div>
				        <?php echo $this->Form->input('Users.user_detail.cellphone', ['type'=>'text', 'label'=>false, 'div'=>false, 'class'=>'form-control']);?>
				      </div>
				    </div>
				  </div>
				</div>
				<div class="row">
				  <div class="col-md-6">
				    <div class="form-group">
				      <label class="required"><?php echo __d('usermgmt', 'Centro'); ?></label>
				      <div>
				        <?php echo $this->Form->input('Users.centro_id', ['label'=>false, 'div'=>false, 'class'=>'form-control', 'options' => $centros]); ?>
				      </div>
				    </div>
				  </div>
				  <div class="col-md-12">
            <div class="last-btns">
            	<?php echo $this->Html->link(__d('usermgmt', 'Back'), ['action'=>'index'], ['div'=>false, 'class'=>'btn btn-cancel']); ?>
            	<?php echo $this->Form->Submit(__d('usermgmt', 'Save'), ['div'=>false, 'class'=>'btn btn-save', 'id'=>'editUserSubmitBtn']); ?>
            </div>
          </div>
				</div>
      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>
</div>
<?php
/* Cakephp 3.x User Management Premium Version (a product of Ektanjali Softwares Pvt Ltd)
Website- http://ektanjali.com
Plugin Demo- http://cakephp3-user-management.ektanjali.com/
Author- Chetan Varshney (The Director of Ektanjali Softwares Pvt Ltd)
Plugin Copyright No- 11498/2012-CO/L

UMPremium is a copyrighted work of authorship. Chetan Varshney retains ownership of the product and any copies of it, regardless of the form in which the copies may exist. This license is not a sale of the original product or any copies.

By installing and using UMPremium on your server, you agree to the following terms and conditions. Such agreement is either on your own behalf or on behalf of any corporate entity which employs you or which you represent ('Corporate Licensee'). In this Agreement, 'you' includes both the reader and any Corporate Licensee and Chetan Varshney.

The Product is licensed only to you. You may not rent, lease, sublicense, sell, assign, pledge, transfer or otherwise dispose of the Product in any form, on a temporary or permanent basis, without the prior written consent of Chetan Varshney.

The Product source code may be altered (at your risk)

All Product copyright notices within the scripts must remain unchanged (and visible).

If any of the terms of this Agreement are violated, Chetan Varshney reserves the right to action against you.

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Product.

THE PRODUCT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE PRODUCT OR THE USE OR OTHER DEALINGS IN THE PRODUCT. */
?>
<div class="ibox float-e-margins">
	<div class="ibox-title">
    <h2><?php echo __('Site Permissions for').' '.$name;?></h2>
    <?php $page = (isset($this->request->query['page'])) ? $this->request->query['page'] : 1; ?>
			<?php echo $this->Html->link(__('Back', true), ['action'=>'index', 'page'=>$page], ['class'=>'btn btn-cancel btn-back']); ?>
  </div>
	<div class="ibox-content">
		<div class="row">
		  <div class="col-lg-12">
		    <div class="ibox float-e-margins no-mar">
		      <div class="ibox-content">
		      	<?php echo __('Please make sure permissions are based on groups.');?><br>
		        <div class="table-responsive">
		          <table class="table ca-table">
		            <thead>
		              <tr style="background-color:#f2f4f8; border-bottom:1px solid #ceac71;">
		                <th><h5><?php echo __('#'); ?></h5></th>
							      <th><h5><?php echo __('Plugin'); ?></h5></th>
										<th><h5><?php echo __('Controller'); ?></h5></th>
										<th><h5><?php echo __('Action'); ?></h5></th>
										<th><h5><?php echo __('Group(s)'); ?></h5></th>
		              </tr>
		            </thead>
		            <tbody>
		            	<?php	if(!empty($permissions)) {
										$i = 0;
										foreach($permissions as $row) {
											$i++;
											echo "<tr>";
												echo "<td><p>".$i."</p></td>";
												echo "<td><p>".$row['plugin']."</p></td>";
												echo "<td><p>".$row['controller']."</p></td>";
												echo "<td><p>".$row['action']."</p></td>";
												echo "<td><p>".$row['group']."</p></td>";
											echo "</tr>";
										}
									} else {
										echo "<tr><td colspan=5><br/><br/>".__('No Records Available')."</td></tr>";
									} ?>
		            </tbody>
		          </table>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>

<%

use Cake\Utility\Inflector;

$associations += ['BelongsTo' => [], 'HasOne' => [], 'HasMany' => [], 'BelongsToMany' => []];
$immediateAssociations = $associations['BelongsTo'] + $associations['HasOne'];
$associationFields = collection($fields)
        ->map(function($field) use ($immediateAssociations) {
            foreach ($immediateAssociations as $alias => $details) {
                if ($field === $details['foreignKey']) {
                    return [$field => $details];
                }
            }
        })
        ->filter()
        ->reduce(function($fields, $value) {
    return $fields + $value;
}, []);

$groupedFields = collection($fields)
        ->filter(function($field) use ($schema) {
            return $schema->columnType($field) !== 'binary';
        })
        ->groupBy(function($field) use ($schema, $associationFields) {
            $type = $schema->columnType($field);
            if (isset($associationFields[$field])) {
                return 'string';
            }
            if (in_array($type, ['integer', 'float', 'decimal', 'biginteger'])) {
                return 'number';
            }
            if (in_array($type, ['date', 'time', 'datetime', 'timestamp'])) {
                return 'date';
            }
            return in_array($type, ['text', 'boolean']) ? $type : 'string';
        })
        ->toArray();

$groupedFields += ['number' => [], 'string' => [], 'boolean' => [], 'date' => [], 'text' => []];
$pk = "\$$singularVar->{$primaryKey[0]}";
%>

<div class="ibox">
    <div class="ibox-title">

        <h5> <?php echo __('<%= $singularVar  %> Details'); ?> </h5>

        <span class="ibox-tools">
            

            <?= $this->Html->link(__('PDF'), ['action' => 'view', '_ext'=>'pdf' , <%= $pk %>] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

            <?= $this->Html->link(__('Edit <%= $singularHumanName %>'), ['action' => 'edit', <%= $pk %>] , ['class'=>'btn btn-primary btn-xs pull-right']) ?>

        </span>

    </div>

    <div class="ibox-content">
        
  

<dl class="dl-horizontal">

    <% if ($groupedFields['string']) : %>
    
            <% foreach ($groupedFields['string'] as $field) : %>
           
                <%
                if (isset($associationFields[$field])) :
                    $details = $associationFields[$field];
                    %>
        <dt><?= __('<%= Inflector::humanize($details['property']) %>') ?>:</dt>
                    <dd><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></dd>
                <% else : %>
        <dt><?= __('<%= Inflector::humanize($field) %>') ?>:</dt> 
                    <dd><?= h($<%= $singularVar %>-><%= $field %>) ?></dd>
                <% endif; %>

          
            <% endforeach; %>
    
    <% endif; %>



    <% if ($groupedFields['number']) : %>
    
            <% foreach ($groupedFields['number'] as $field) : %>
           
        <dt><?= __('<%= Inflector::humanize($field) %>') ?>:</dt> 
            <dd><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></dd>
         
            <% endforeach; %>
   
    <% endif; %>
    <% if ($groupedFields['date']) : %>
    
            <% foreach ($groupedFields['date'] as $field) : %>
           
        <dt><%= "<%= __('" . Inflector::humanize($field) . "') %>" %>:</dt> 

                <dd><?= h($<%= $singularVar %>-><%= $field %>) ?></dd>
          
            <% endforeach; %>

    <% endif; %>
    <% if ($groupedFields['boolean']) : %>
    
            <% foreach ($groupedFields['boolean'] as $field) : %>
      
        <dt><?= __('<%= Inflector::humanize($field) %>') ?>:</dt>
                <dd><?= $<%= $singularVar %>-><%= $field %> ? __('Yes') : __('No'); ?></dd>
             
            <% endforeach; %>
   
    <% endif; %>

<% if ($groupedFields['text']) : %>
    <% foreach ($groupedFields['text'] as $field) : %>

                <dt><?= __('<%= Inflector::humanize($field) %>') ?>:</dt> 
                <dd><?= $this->Text->autoParagraph(h($<%= $singularVar %>-><%= $field %>)); ?></dd>
         
    <% endforeach; %>
<% endif; %>


</dl>

<ul id="myTab" class="nav nav-tabs" role="tablist">
<%
//Vamos a usar tabs, asi que recorro los asociados para ponerlos.
$relations = $associations['HasMany'] + $associations['BelongsToMany'];
$active="active";
foreach ($relations as $alias => $details):
    $otherSingularVar = Inflector::variable($alias);
    $otherPluralHumanName = Inflector::humanize($details['controller']);
    %>
    <li role="presentation" class="<%= $active; %>">
        <a href="#<%= $alias; %>" id="<%= $alias; %>-tab" role="tab" data-toggle="tab" aria-controls="<%= $alias; %>" aria-expanded="true"><%= $otherPluralHumanName %></a>
      </li>
      <% $active=""; %>
<% endforeach; %>   
</ul>

<div id="myTabContent" class="tab-content">
<%
$active="active";
foreach ($relations as $alias => $details):
    $otherSingularVar = Inflector::variable($alias);
    $otherPluralHumanName = Inflector::humanize($details['controller']);
    %>
<div role="tabpanel" class="tab-pane fade in <%= $active; %>" id="<%= $alias; %>" aria-labelledBy="<%= $alias; %>-tab">
    <div class="related row">
        <div class = "col-lg-12"><br>            
            <?php if (!empty($<%= $singularVar %>-><%= $details['property'] %>)): ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <% foreach ($details['fields'] as $field): %>
                    <th><?= __('<%= Inflector::humanize($field) %>') ?></th>
                        <% endforeach; %>
                    <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($<%= $singularVar %>-><%= $details['property'] %> as $<%= $otherSingularVar %>): ?>
                    <tr>
                        <% foreach ($details['fields'] as $field): %>
                    <td><?= h($<%= $otherSingularVar %>-><%= $field %>) ?></td>
                        <% endforeach; %>
                        <% $otherPk = "\${$otherSingularVar}->{$details['primaryKey'][0]}"; %>
                    <td class="actions">
                            <?= $this->Html->link('', ['controller' => '<%= $details['controller'] %>', 'action' => 'view', <%= $otherPk %>],['title' => __('View'), 'class' => 'btn btn-default fa fa-eye']) ?>
                            <?= $this->Html->link('', ['controller' => '<%= $details['controller'] %>', 'action' => 'edit', <%= $otherPk %>], ['title' => __('Edit'), 'class' => 'btn btn-default fa fa-pencil']) ?>
                            <?= $this->Form->postLink('', ['controller' => '<%= $details['controller'] %>', 'action' => 'delete', <%= $otherPk %>], ['confirm' => __('Are you sure you want to delete # {0}?', <%= $otherPk %>), 'title' => __('Delete'), 'class' => 'btn btn-default fa fa-trash']) ?>                            
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
         <?php else: ?>
            <h4><?= __('No existen <%= $otherPluralHumanName %> asociados') ?></h4>
        <?php endif; ?>
        </div>
    </div>
</div>
<% $active=""; %>
<% endforeach; %>
</div>
</div>
</div>


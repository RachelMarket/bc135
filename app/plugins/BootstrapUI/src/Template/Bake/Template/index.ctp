<%
use Cake\Utility\Inflector;
%>

<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="panel-title">
            <%= $pluralHumanName %>
        </span>
        <span class="panel-title-right">
            <?= $this->Html->link(__('Agregar <%= $singularHumanName %>'), ['action' => 'add']); ?> 

            <?= $this->Html->link('<i class="fa fa-download"></i> Exportar Excel', '/<%= Inflector::underscore($pluralHumanName) %>.xlsx', ['class'=>'btn-success', 'escape'=>false]); ?> 

        </span>
    </div>
    <div class="panel-body">
        <?php echo $this->element('all_<%= Inflector::underscore($pluralHumanName) %>');?>
    </div>
</div>
<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CotizacionesOficinasFixture
 *
 */
class CotizacionesOficinasFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'cotizaciones_oficinas';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'cotizacion_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'oficina_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_cotizacion_id' => ['type' => 'index', 'columns' => ['cotizacion_id'], 'length' => []],
            'fk_oficina_id' => ['type' => 'index', 'columns' => ['oficina_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_cotizacion_id' => ['type' => 'foreign', 'columns' => ['cotizacion_id'], 'references' => ['cotizacion', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_oficina_id' => ['type' => 'foreign', 'columns' => ['oficina_id'], 'references' => ['oficina', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'cotizacion_id' => 1,
            'oficina_id' => 1
        ],
    ];
}

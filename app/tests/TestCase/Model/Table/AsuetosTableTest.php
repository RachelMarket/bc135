<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AsuetosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AsuetosTable Test Case
 */
class AsuetosTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.asuetos',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Asuetos') ? [] : ['className' => 'App\Model\Table\AsuetosTable'];
        $this->Asuetos = TableRegistry::get('Asuetos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Asuetos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
       

        $rows = $this->Asuetos->find('all');

        $total =  $rows->count();

        $this->assertTrue( ($total == 0)  ) ;

    }

   

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testInsertOK()
    {
        $asueto = $this->Asuetos->newEntity();

        $data['user_id'] = 1;
        $data['dia'] = '2016-10-12';

        $asueto = $this->Asuetos->patchEntity($asueto, $data );

        $this->Asuetos->save($asueto) ;

        $rows = $this->Asuetos->find('all');

        $total =  $rows->count();

        $this->assertTrue( ($total == 1)  ) ;

        
    }

     public function testBuildRules()
    {
        $asueto = $this->Asuetos->newEntity();

        $data['user_id'] = 2;
        $data['dia'] = '2016-10-12';

        $asueto = $this->Asuetos->patchEntity($asueto, $data );

        $t = $this->Asuetos->save($asueto) ;
       

        $rows = $this->Asuetos->find('all');

        $total =  $rows->count();

        $this->assertTrue( ($total == 0)  ) ;



        $asueto = $this->Asuetos->newEntity();

        
        $data['dia'] = '2016-10-12';

        $asueto = $this->Asuetos->patchEntity($asueto, $data );

        $t = $this->Asuetos->save($asueto) ;
       

        $rows = $this->Asuetos->find('all');

        $total =  $rows->count();

        $this->assertTrue( ($total == 0)  ) ;




        $asueto = $this->Asuetos->newEntity();

        $data['user_id'] = 1;
        $data['dia'] = '2016-10-12';

        $asueto = $this->Asuetos->patchEntity($asueto, $data );

        $t = $this->Asuetos->save($asueto) ;
       

        $rows = $this->Asuetos->find('all');

        $total =  $rows->count();

        $this->assertTrue( ($total == 1)  ) ;



         $asueto = $this->Asuetos->newEntity();

        $data['user_id'] = 1;
        $data['dia'] = '2016-10-12';

        $asueto = $this->Asuetos->patchEntity($asueto, $data );

        $t = $this->Asuetos->save($asueto) ;

        $rows = $this->Asuetos->find('all');

        $total =  $rows->count();

        $this->assertTrue( ($total == 1)  ) ;
        
    }

}

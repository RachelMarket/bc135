<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CotizacionesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CotizacionesTable Test Case
 */
class CotizacionesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cotizaciones',
        'app.oficinas',
        'app.centros',
        'app.clientes',
        'app.paises',
        'app.estados',
        'app.municipios',
        'app.monedas',
        'app.facturas',
        'app.festados',
        'app.formasdepagos',
        'app.clientes_servicios',
        'app.servicios',
        'app.tipos',
        'app.unidades',
        'app.partidas',
        'app.usuarios',
        'app.mensajes',
        'app.tipo_clientes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
<<<<<<< HEAD
        $config = TableRegistry::exists('Cotizaciones') ? [] : ['className' => CotizacionesTable::class];
=======
        $config = TableRegistry::exists('Cotizaciones') ? [] : ['className' => 'App\Model\Table\CotizacionesTable'];
>>>>>>> 93f56c454d438e8c46d35a86d214bcde53365128
        $this->Cotizaciones = TableRegistry::get('Cotizaciones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cotizaciones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

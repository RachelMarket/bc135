<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OficinasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OficinasTable Test Case
 */
class OficinasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OficinasTable
     */
    public $Oficinas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.oficinas',
        'app.centros',
        'app.clientes',
        'app.paises',
        'app.estados',
        'app.municipios',
        'app.monedas',
        'app.facturas',
        'app.festados',
        'app.formasdepagos',
        'app.clientes_servicios',
        'app.servicios',
        'app.tipos',
        'app.unidades',
        'app.partidas',
        'app.usuarios',
        'app.mensajes',
        'app.tipo_clientes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Oficinas') ? [] : ['className' => OficinasTable::class];
        $this->Oficinas = TableRegistry::get('Oficinas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Oficinas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

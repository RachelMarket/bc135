<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CotizacionesOficinasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CotizacionesOficinasTable Test Case
 */
class CotizacionesOficinasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CotizacionesOficinasTable
     */
    public $CotizacionesOficinas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cotizaciones_oficinas',
        'app.cotizacion',
        'app.oficina'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CotizacionesOficinas') ? [] : ['className' => CotizacionesOficinasTable::class];
        $this->CotizacionesOficinas = TableRegistry::get('CotizacionesOficinas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CotizacionesOficinas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

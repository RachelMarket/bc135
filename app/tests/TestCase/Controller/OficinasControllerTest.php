<?php
namespace App\Test\TestCase\Controller;

use App\Controller\OficinasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\OficinasController Test Case
 */
class OficinasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.oficinas',
        'app.centros',
        'app.clientes',
        'app.paises',
        'app.estados',
        'app.municipios',
        'app.monedas',
        'app.facturas',
        'app.festados',
        'app.formasdepagos',
        'app.clientes_servicios',
        'app.servicios',
        'app.tipos',
        'app.unidades',
        'app.partidas',
        'app.usuarios',
        'app.mensajes',
        'app.tipo_clientes'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
